﻿
namespace MalterFormApplication
{
    static class FProgram
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            MalterLib.MainMalter.MalterClient.ClientGUIMain();
        }
    }
}
