﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Linq;


namespace MalterLib
{
    using MalterObjects;
    namespace MalterData
    {
        using MainMalter;
        using MalterDBDetails;
        using Microsoft.Win32;
        using System.IO;

        namespace MalterDBDetails
        {
            public static class DBFiles
            {
                                
                internal const string BINDIR = @"C:\";
                internal const string BINDIR2 = @"C:\Program Files (x86)\Malter";
                public const string BINDIR3 = @"C:\Malter\";

                internal const string _CLIENT_SERVICE_LOG = "clog.txt";
                internal const string _CLIENT_DB = "ClientDB.db";
                internal const string _MALTER_DATA_FILE = @"MalterData.json";
                internal const string _PRODUCT_FILE = @"Product.txt";
                internal const string _INJECTION_LIB = "MalterLib.dll";
                internal const string _ICON = "icon.ico";

                internal const string _SERVER_LOG = "slog.txt";
                internal const string _SERVER_DB = "ServerDB.db";
                internal const string _SERVER_MALTER_DATA = "ServerMalterData.json";
                internal const string _MALTER_SECURITY_DATA = @"MalterSecurityData.txt";
                
                /// <summary>
                /// Gets the client's database file.
                /// </summary>
                public static string CLIENT_DB
                {
                    get
                    {
                        return GetClientBinDirPath(_CLIENT_DB);
                    }
                }

                /// <summary>
                /// Gets the client's icon file.
                /// </summary>
                public static string ICON
                {
                    get
                    {
                        return GetClientBinDirPath(_ICON);
                    }
                }

                /// <summary>
                /// Gets the client service's log file.
                /// </summary>
                public static string CLIENT_SERVICE_LOG
                {
                    get
                    {
                        return GetClientBinDirPath(_CLIENT_SERVICE_LOG);
                    }
                }

                /// <summary>
                /// Gets the client's Product file.
                /// </summary>
                public static string PRODUCT_FILE
                {
                    get
                    {
                        return GetClientBinDirPath(_PRODUCT_FILE);
                    }
                }

                /// <summary>
                /// Gets the client's injection library.
                /// </summary>
                public static string INJECTION_LIB
                {
                    get
                    {
                        return Path.GetDirectoryName(WindowsModule.WindowsInterface.GetMyPath()) + "\\" + _INJECTION_LIB;
                    }
                }

                /// <summary>
                /// Gets the client's MalterData file.
                /// </summary>
                public static string MALTER_DATA_FILE
                {
                    get
                    {
                        return GetClientBinDirPath(_MALTER_DATA_FILE);
                    }
                }

                /// <summary>
                /// Gets the server's log file.
                /// </summary>
                public static string SERVER_LOG
                {
                    get
                    {
                        return GetServerDirPath(_SERVER_LOG);
                    }
                }

                /// <summary>
                /// Gets the server's database file.
                /// </summary>
                public static string SERVER_DB
                {
                    get
                    {
                        return GetServerDirPath(_SERVER_DB);
                    }
                }

                /// <summary>
                /// Gets the server's MalterData file.
                /// </summary>
                public static string SERVER_MALTER_DATA
                {
                    get
                    {
                        return GetServerDirPath(_SERVER_MALTER_DATA);                        
                    }
                }                

                /// <summary>
                /// Gets the server's security information file.
                /// </summary>
                public static string MALTER_SECURITY_DATA
                {
                    get
                    {
                        return  GetServerDirPath(_MALTER_SECURITY_DATA);
                    }
                }

                public static string GetClientBinDirPath(string file)
                {
                    return BINDIR3 + "\\" + file;
                }

                public static string GetServerDirPath(string fileName)
                {
                    return Path.GetDirectoryName(WindowsModule.WindowsInterface.GetMyPath()) + "\\sfiles\\"  + fileName;
                }

            }

            public static class JsonDBKeys
            {
                public const string DATAIDS = "DataIds";
                public const string DEVICEDATA = "DeviceData";
                internal const string CURRENTDEVICEID = "CurrentDeviceId";
                internal const string CURRENTUSERID = "CurrentUserId";
                internal static string PRODUCT_ID = "ProductId";
                internal static string DBPassword = "DBPassword";
                internal static string ServerDetails = "ServerDetails";
            }
                       
            static class UserTable
            {
                internal const string TableName = "User";
                internal const string Id = "Id";
                internal const string PrivateName = "PrivateName";
                internal const string Username = "Username";
                internal const string Email = "Email";
                internal const string Password = "Password";
            }

            static class DeviceTable
            {
                internal const string TableName = "Device";
                internal const string Id = "Id";
                internal const string OwnerId = "OwnerId";
                internal const string DeviceName = "DeviceName";
                internal const string Info = "Info";
                internal const string Processes = "Processes";
                internal const string UpdateEvents = "UpdateEvents";
                internal const string Keys = "Keys";
            }

            static class ProcessTable
            {
                internal const string Mid = "Mid";
                internal const string TableName = "Process";
                internal const string Name = "Name";
                internal const string Path = "Path";
                internal const string Pid = "Pid";
                internal const string Hash = "Hash";
                internal const string Profile = "Profile";
                internal const string CheckCounter = "CheckCounter";
                internal const string DidSurvey = "DidSurvey";
            }

            static class APICallTable
            {
                internal const string TableName = "APICall";                
                internal const string Mid = "Mid";
                internal const string Pid = "Pid";
                internal const string Id = "Id";
                internal const string Name = "Name";
                internal const string Time = "Time";
                internal const string SCall = "SCall";
            }

            static class SystemEventTable
            {
                internal const string TableName = "SystemEvent";
                internal const string TableNameForNotification = "Notification";
                internal const string TableNameForUpdate = "UpdateEvent";
                internal const string Id = "Id";
                internal const string Type = "Type";
                internal const string Time = "Time";
                internal const string SEvent = "SEvent";
            }

            static class MonitorCallTable
            {
                internal const string TableName = "MonitorCall";
                internal const string Id = "Id";
                internal const string CallDllFunction = "CallDllFunction";
                internal const string BlockFiltersIds = "BlockFiltersIds";
                internal const string StoreFiltersIds = "StoreFiltersIds";
                
            }

            static class ProfileTable
            {
                internal const string TableName = "Profile";
                internal const string Id = "Id";
                internal const string MonitorsIds = "MonitorsIds";
                internal const string TestsIds = "TestsIds";
                internal const string TestIdForProfile = "TestIdForProfile";
                internal const string OptionalProfilesIds = "OptionalProfilesIds";
                internal const string InheritedProfiles = "InheritedProfiles";
            }

            static class TestsCountersTable
            {
                internal const string TableName = "TestsCounters";
                internal const string Mid = "Mid";
                internal const int START_COUNTER = 0;
                public static string GetTestColumnName(int testId)
                {
                    return string.Format("Test{0}", testId);
                }
                public static int GetTestId(string testColumnName)
                {
                    int i;                  
                    for (i= testColumnName.Length-1; i!=0; i--)
                    {
                        if (!Char.IsDigit(testColumnName[i]))
                            break;
                    }
                    i++;
                    return int.Parse(testColumnName.Substring(i, testColumnName.Length - i));
                }
            }
            static class MiscTable
            {
                internal const string TableName = "Misc";
                internal const string Type = "Type";
                internal const string Data = "Data";
            }

            static class ProductTable
            {
                internal const string TableName = "Product";
                internal const string Id = "Id";
                internal const string Keys = "Keys";

                /// <summary>
                /// Returns the product's file name by id.
                /// </summary>
                /// <param name="Id"></param>
                /// <returns></returns>
                public static string GetProductFileName(int Id)
                {
                    return DBFiles.GetServerDirPath(string.Format("Product.txt", Id));
                }
            }
        }
                
        public struct IDataDetails
        {
            public string MalterDataFile;
            public SQLDBDetails DBDetails;
            public string LogFile;

            public static IDataDetails DefaultGUIClientDB = new IDataDetails()      // DB for GUI app
            {
                DBDetails = new SQLDBDetails
                {
                    DataSource = DBFiles.CLIENT_DB,
                    Version = "3",
                    Password = null,
                },
                MalterDataFile = null,
                LogFile = null
            };

            public static IDataDetails DefaultServiceClientDB = new IDataDetails() // DB for service app
            {
                DBDetails = new SQLDBDetails
                {
                    DataSource = DBFiles.CLIENT_DB,
                    Version = "3",
                    Password = null,
                },
                MalterDataFile = null,
                LogFile = DBFiles.CLIENT_SERVICE_LOG
            };
            public static IDataDetails DefaultServerDB = new IDataDetails() // DB for server app
            {
                DBDetails = new SQLDBDetails
                {
                    DataSource = DBFiles.SERVER_DB,
                    Version = "3",
                    Password = null,
                },
                MalterDataFile = DBFiles.SERVER_MALTER_DATA,
                LogFile = DBFiles.SERVER_LOG
            };            
        }

        /// <summary>
        /// Class for holding database details.
        /// </summary>
        public struct SQLDBDetails
        {
            public string DataSource;
            public string Version;
            public string Password;
        }
        /// <summary>
        /// Returns a new password.
        /// </summary>
        /// <returns></returns>
        public delegate string PasswordGenerator();

        /// <summary>
        /// Class for providing database access.
        /// </summary>
        public class DataInterface
        {
            private SQLiteDB Sdb;
            private IDataDetails DataDetails;
            private PasswordGenerator PGenerator;
            private static Dictionary<string, SQLiteDB.GetRowData> rdict = new Dictionary<string, SQLiteDB.GetRowData>()
            {
                { UserTable.TableName, UserData.GetRowData },
                { DeviceTable.TableName, DeviceData.GetRowData },
                { APICallTable.TableName, APICallData.GetRowData },
                { ProcessTable.TableName, ProcessData.GetRowData },
                { SystemEventTable.TableName, SystemEventData.GetRowData},
                { SystemEventTable.TableNameForNotification, SystemEventData.GetRowData},
                { SystemEventTable.TableNameForUpdate, SystemEventData.GetRowData},
                { MonitorCallTable.TableName, MonitorCallData.GetRowData },
                { ProfileTable.TableName, ProfileData.GetRowData},
                { TestsCountersTable.TableName, ProcessData.GetTestsCounterRowData },
                { MiscTable.TableName, MiscData.GetRowData },
                { ProductTable.TableName, ProductData.GetRowData }
            };

            #region GENERAL 
            public DataInterface(IDataDetails dataDetails, PasswordGenerator pGenerator)
            {                
                this.DataDetails = dataDetails;
                this.PGenerator = pGenerator;
            }

            public DataInterface(IDataDetails dataDetails)
            {
                this.DataDetails = dataDetails;                
            }

            public DataInterface()
            {
                this.Sdb = new SQLiteDB(DataInterface.rdict, IDataDetails.DefaultServiceClientDB.DBDetails);
            }

            /// <summary>
            /// Connects to the Database.
            /// </summary>
            public void Connect()
            {
                if (AppTypeClass.IsClient)
                {
                    // Get the password for the database
                    this.DataDetails.DBDetails.Password = GetDBPassword(); 
                }

                // Connect to the database
                this.Sdb = new SQLiteDB(DataInterface.rdict, this.DataDetails.DBDetails);
                
                if (AppTypeClass.IsClient)
                {
                    if (this.DataDetails.DBDetails.Password == "")
                    {
                        SetDBPassword(PGenerator());
                        this.DataDetails.DBDetails.Password = GetDBPassword();
                        this.Sdb = new SQLiteDB(DataInterface.rdict, this.DataDetails.DBDetails);
                    }
                }
            }            

            /// <summary>
            /// Returns the malter data of the client.
            /// </summary>
            /// <returns></returns>
            public string GetMalterData()
            {
                if (AppTypeClass.IsClient)
                    return MalterRegistry.GetMalterData();
                return System.IO.File.ReadAllText(this.DataDetails.MalterDataFile);
            }

            /// <summary>
            /// Sets the malter data for the client.
            /// </summary>
            /// <param name="data">New malter data</param>
            public void SetMalterData(string data)
            {
                if (AppTypeClass.IsClient)
                {
                    MalterRegistry.SetMalterData(data);
                    return;
                }                    
                System.IO.File.WriteAllText(this.DataDetails.MalterDataFile, data);
            }            

            /// <summary>
            /// Gets a json value in the client's malter data.
            /// </summary>
            /// <param name="key"></param>
            /// <returns>The json value for the malter Data key </returns>
            public string GetMalterJsonData(string key)
            {
                string data = GetMalterData();
                JToken t = JObject.Parse(data).Property(key).Value;
                return t.ToString();
            }

            /// <summary>
            /// Sets a malter data json value.
            /// </summary>
            /// <param name="key">The key to be set to.</param>
            /// <param name="newJsonData">The new data</param>
            public void SetMalterJsonData(string key, JToken newJsonData)
            {
                string data = this.GetMalterData();
                JObject jdata = JObject.Parse(data);
                jdata.Property(key).Value = newJsonData; // JObject.Parse(newJsonData);
                this.SetMalterData(jdata.ToString());                
            }

            /// <summary>
            /// Sets the client's product id.
            /// </summary>
            /// <param name="productId">The product Id to be set to.</param>
            public void SetProductId(int productId)
            {
                SetMalterJsonData(JsonDBKeys.PRODUCT_ID, JToken.FromObject(productId));
            }            

            /// <summary>
            /// Get the client's product id.
            /// </summary>
            /// <returns>The product's client id</returns>
            public int GetThisProductId()
            {
                return int.Parse(GetMalterJsonData(JsonDBKeys.PRODUCT_ID));
            }

            /// <summary>
            /// Get the server details for the client.
            /// </summary>
            /// <returns>Server details for the client.</returns>
            public string GetThisServerDetails()
            {
                return GetMalterJsonData(JsonDBKeys.ServerDetails);
            }

            /// <summary>
            /// Get a new id for a table.
            /// </summary>
            /// <param name="table">The table's name</param>
            /// <returns>The new id</returns>
            internal int GetNewId(string table)
            {
                JObject jsonIds = JObject.Parse(this.GetMalterJsonData(JsonDBKeys.DATAIDS));
                int id = (int)jsonIds.Property(table).Value;
                jsonIds.Property(table).Value = id + 1;
                this.SetMalterJsonData(JsonDBKeys.DATAIDS, jsonIds);
                return id;
            }

            /// <summary>
            /// Delete all the data the client gathered.
            /// </summary>
            public void DeleteAllClientGatheredData()
            {
                this.Sdb.SQLDelete(ProcessTable.TableName);
                this.Sdb.SQLDelete(SystemEventTable.TableName);
                this.Sdb.SQLDelete(SystemEventTable.TableNameForNotification);
                this.Sdb.SQLDelete(SystemEventTable.TableNameForUpdate);
                this.Sdb.SQLDelete(APICallTable.TableName);
                this.Sdb.SQLDelete(DeviceTable.TableName);
                this.Sdb.SQLDelete(UserTable.TableName);
            }

            /// <summary>
            /// Delete all the data the server gathered.
            /// </summary>
            public void DeleteAllServerGatheredData()
            {
                this.Sdb.SQLDelete(ProcessTable.TableName);
                this.Sdb.SQLDelete(TestsCountersTable.TableName);
                this.Sdb.SQLDelete(SystemEventTable.TableNameForUpdate);
                this.Sdb.SQLDelete(DeviceTable.TableName);
                this.Sdb.SQLDelete(UserTable.TableName);                

            }

            /// <summary>
            /// Delete all the data that was configured for the client.
            /// </summary>
            public void DeleteAllConfiguredData()
            {
                this.Sdb.SQLDelete(MonitorCallTable.TableName);
                this.Sdb.SQLDelete(ProfileTable.TableName);
                this.Sdb.SQLDelete(MiscTable.TableName);
            }
            
            /// <summary>
            /// Delete all API calls for client.
            /// </summary>
            public void DeleteAPICalls()
            {
                this.Sdb.SQLDelete(APICallTable.TableName);
            }

            /// <summary>
            /// Log a message in the client's log file.
            /// </summary>                                                       
            public void LogMsg(string msg)
            {
                int NumberOfRetries = 25;
                for (int i = 0; i <= NumberOfRetries; i++)
                {
                    try
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(this.DataDetails.LogFile, true))
                        {
                            //string h = System.Diagnostics.Process.GetCurrentProcess().Id.ToString() + ":";
                            file.WriteLine(msg);
                        }
                        break; 
                    }
                    catch (IOException e)
                    {
                        e.ToString(); // meant drop visual studio warning    
                        if (i == NumberOfRetries) 
                            throw;

                        System.Threading.Thread.Sleep(300);
                    }
                }
            }

            /// <summary>
            /// Returns the server's security information.
            /// </summary>
            /// <returns>Security information</returns>
            public string GetMalterSecurityData()
            {
                try
                {
                    return System.IO.File.ReadAllText(DBFiles.MALTER_SECURITY_DATA);
                }
                catch
                {
                    return null;
                }
                
            }            

            /// <summary>
            /// Clears the database's password.           
            /// </summary>
            public void ClearDBPassword()
            {
                this.Sdb.ClearPassword();
            }

            /// <summary>
            /// Sets the database's password.
            /// </summary>
            /// <param name="newPassword">New password.</param>
            public void SetDBPassword(string newPassword)
            {
                // Set password for db
                this.Sdb.SetPassword(newPassword);

                // Save Password 
                JObject data = JObject.Parse(this.GetMalterData());
                data.Property(JsonDBKeys.DBPassword).Value = newPassword;
                this.SetMalterData(data.ToString());
            }

            /// <summary>
            /// Gets the database's password.
            /// </summary>
            /// <returns>The database's password.</returns>
            public string GetDBPassword()
            {
                JProperty passwordProperty = JObject.Parse(this.GetMalterData())
                    .Property(JsonDBKeys.DBPassword);
                if (passwordProperty == null)
                    return null;
                return (string)passwordProperty.Value;             
            }
            #endregion GENERAL 

            #region USER 
            /// <summary>
            /// Adds a new user to the database. 
            /// </summary>
            /// <param name="data">Details for new user.</param>
            public void NewUser(UserData data)
            {
                Dictionary<string, string> detailsDict = data.GetDict();
                this.Sdb.SQLInsert(UserTable.TableName, detailsDict);
            }

            /// <summary>
            /// Updates a user's information in the database.
            /// </summary>
            /// <param name="data">User's new information.</param>
            public void UpdateUserData(UserData data)
            {
                this.Sdb.SQLUpdate(UserTable.TableName,
                    data.GetDict(),
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>
                    { { UserTable.Id, data.Id.ToSQLParm() } }));
            }

            /// <summary>
            /// Inserts a user's data whether new or not.
            /// </summary>
            /// <param name="data"></param>
            public void PutUserData(UserData data)  
            {
                try
                {
                    this.NewUser(data);
                }
                catch (SQLiteException e)
                {
                    this.UpdateUserData(data);
                    MDebug.WriteLine(e.ToString());
                }
            }

            /// <summary>
            /// Returns the current user.
            /// </summary>
            /// <returns>Current user.</returns>
            public UserData GetCurrentUser()
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { UserTable.Id, this.GetCurrentUserId().ToSQLParm() } });
                UserData[] arr = this.Sdb.SQLSelect<UserData>(UserTable.TableName, whereClause: where);

                if (arr.Length == 1) return arr[0];
                return null;
            }

            /// <summary>
            /// Returns the current user's id.
            /// </summary>
            /// <returns>Current user id.</returns>
            public int GetCurrentUserId()
            {
                return int.Parse(this.GetDeviceDataProperty(JsonDBKeys.CURRENTUSERID));
            }

            /// <summary>
            /// Sets the current user's id.
            /// </summary>
            /// <param name="id">New id.</param>
            public void SetCurrentUserId(int id)
            {
                this.SetDeviceDataProperty(JsonDBKeys.CURRENTUSERID, id);
            }

            /// <summary>
            /// Returns an Id for a new user.
            /// </summary>
            /// <returns>New id.</returns>
            public int GetNewUserId()
            {
                return this.GetNewId(UserTable.TableName);
            }

            /// <summary>
            /// Gets a user's full information by unique details.
            /// </summary>
            /// <param name="data">The user's data</param>
            /// <returns>User's full information or null if there is no such user.</returns>
            public UserData GetUniqueUser(UserData data)
            {
                UserData[] results = this.Sdb.SQLSelect<UserData>(UserTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { UserTable.Username, data.UserName.ToSQLParm() } }));
                if (results.Length > 0)
                    return results[0];
                else
                    return null;
            }

            /// <summary>
            /// Gets a user's information by his id.
            /// </summary>
            /// <param name="id">The user's id.</param>
            /// <returns>User's information</returns>
            public UserData GetUserById(int id)
            {
                UserData[] data = this.Sdb.SQLSelect<UserData>(UserTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { UserTable.Id, id.ToSQLParm() } }));
                if (data.Length > 0)
                    return data[0];
                return null;
            }
            #endregion USER 

            #region Device
            /// <summary>
            /// Adds a new device to the database.
            /// </summary>
            /// <param name="data">The new device</param>
            public void NewDevice(DeviceData data)
            {
                this.Sdb.SQLInsert(DeviceTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Gets a device's UpdateEvents list.
            /// </summary>
            /// <param name="deviceId">Device's id </param>
            /// <returns></returns>
            public SystemEventData[] GetDeviceUpdateEvents(int deviceId)
            {
                int[] eventIds = JsonConvert.DeserializeObject<int[]>(GetDeviceUpdateEventIds(deviceId));
                return (from eventId in eventIds select GetUpdateEvent(eventId)).ToArray();
            }

            /// <summary>
            /// Gets the device that is currently logged in.
            /// </summary>
            /// <returns></returns>
            public DeviceData GetCurrentDevice()
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { DeviceTable.Id, this.GetCurrentDeviceId().ToSQLParm() } });
                DeviceData[] arr = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName, whereClause: where);

                if (arr.Length == 1) return arr[0];
                return null;
            }

            /// <summary>
            /// Gets the malter data device property.
            /// </summary>
            /// <param name="property"></param>
            /// <returns></returns>
            public string GetDeviceDataProperty(string property)
            {
                JObject jsonData = JObject.Parse(this.GetMalterJsonData(JsonDBKeys.DEVICEDATA));
                return (string)jsonData.Property(property).Value;
            }

            /// <summary>
            /// Sets the malter data device property.
            /// </summary>
            /// <param name="property"></param>
            /// <param name="value"></param>
            public void SetDeviceDataProperty(string property, int value)
            {
                JObject jsonData = JObject.Parse(this.GetMalterJsonData(JsonDBKeys.DEVICEDATA));
                jsonData.Property(property).Value = value;
                this.SetMalterJsonData(JsonDBKeys.DEVICEDATA, jsonData);
            }

            /// <summary>
            /// Gets current logged in device id.
            /// </summary>
            /// <returns></returns>
            public int GetCurrentDeviceId()
            {
                return int.Parse(this.GetDeviceDataProperty(JsonDBKeys.CURRENTDEVICEID));
            }

            /// <summary>
            /// Sets current logged in device id.
            /// </summary>
            /// <param name="id"></param>
            public void SetCurrentDeviceId(int id)
            {
                this.SetDeviceDataProperty(JsonDBKeys.CURRENTDEVICEID, id);
            }

            /// <summary>
            /// Gets a new device's id.
            /// </summary>
            /// <returns></returns>
            public int GetNewDeviceId()
            {
                return this.GetNewId(DeviceTable.TableName);
            }

            /// <summary>
            /// Gets full device information by device's unique details.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public DeviceData GetUniqueDevice(DeviceData data)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>
                { { DeviceTable.DeviceName, data.DeviceName.ToSQLParm()},
                    { DeviceTable.OwnerId, data.OwnerId.ToSQLParm()} });
                DeviceData[] results = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName, whereClause: where);
                if (results.Length > 0)
                    return results[0];
                else
                    return null;
            }

            /// <summary>
            /// Gets the device's info cookie by its id.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public string GetDeviceInfo(int id)
            {
                DeviceData[] data = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    new string[] { DeviceTable.Info },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { DeviceTable.Id, id.ToSQLParm() } }));
                if (data.Length > 0)
                    return data[0].Info;
                else
                    return null;
            }

            /// <summary>
            /// Gets a device by its id.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public DeviceData GetDeviceById(int id)
            {
                DeviceData[] data = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { DeviceTable.Id, id.ToSQLParm() } }));
                if (data.Length > 0)
                    return data[0];
                return null;

            }

            /// <summary>
            /// Gets the device's processes list.
            /// </summary>
            /// <param name="deviceId"></param>
            /// <returns></returns>
            public string GetDeviceProcesses(int deviceId)
            {
                DeviceData[] data = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    new string[] { DeviceTable.Processes },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
                return data.Length > 0 ? data[0].Processes : null;
            }

            /// <summary>
            /// Sets the device's processes list.
            /// </summary>
            /// <param name="deviceId"></param>
            /// <param name="processesString"></param>
            public void SetDeviceProcesses(int deviceId, string processesString)
            {
                this.Sdb.SQLUpdate(DeviceTable.TableName,
                    updateDetailsDict: new Dictionary<string, string> { { DeviceTable.Processes, processesString.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
            }

            /// <summary>
            /// Adds a process id to the device's processes list.
            /// </summary>
            /// <param name="deviceId"></param>
            /// <param name="mid"></param>
            public void AddProcessToDevice(int deviceId, int mid)
            {
                string processesString = this.GetDeviceProcesses(deviceId);
                JArray processesJArray = JArray.Parse(processesString);
                processesJArray.Add(mid);
                string newProcessesString = processesJArray.ToString();
                this.SetDeviceProcesses(deviceId, newProcessesString.RemoveBR());
            }

            /// <summary>
            /// Returns whether the device has the process.
            /// </summary>
            /// <param name="deviceId"></param>
            /// <param name="mid"></param>
            /// <returns></returns>
            public bool DoesDeviceHasProcess(int deviceId, int mid)
            {
                string processesString = this.GetDeviceProcesses(deviceId);
                //MDebug.WriteLine(processesString);

                if (processesString == null)
                    return false;
                return JArray.Parse(processesString).ToArray().Contains(mid);
            }

            /// <summary>
            /// Gets all devices that have a process.
            /// </summary>
            /// <param name="mid">Process' MID.</param>
            /// <returns></returns>
            public int[] GetAllDevicesThatHaveProcess(int mid)
            {
                int[] devicesIds = GetAllDevicesIds();
                int[] ownerDevices = (from deviceId in devicesIds
                                      where DoesDeviceHasProcess(deviceId, mid)
                                      select deviceId).ToArray();
                return ownerDevices;
            }

            /// <summary>
            /// Gets a device's update events.
            /// </summary>
            /// <param name="deviceId">Device's ID.</param>
            /// <returns></returns>
            public string GetDeviceUpdateEventIds(int deviceId)
            {
                DeviceData[] data = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    new string[] { DeviceTable.UpdateEvents },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
                return data.Length > 0 ? data[0].UpdateEvents : null;
            }

            /// <summary>
            /// Sets a device's update events.
            /// </summary>
            /// <param name="deviceId">Device's ID</param>
            /// <param name="eventsString">New events string.</param>
            public void SetDeviceUpdateEventIds(int deviceId, string eventsString)
            {
                this.Sdb.SQLUpdate(DeviceTable.TableName,
                    updateDetailsDict: new Dictionary<string, string> { { DeviceTable.UpdateEvents, eventsString.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
            }

            /// <summary>
            /// Clears device update events.
            /// </summary>
            /// <param name="deviceId"></param>
            public void ClearDeviceUpdateEventsIds(int deviceId)
            {
                SetDeviceUpdateEventIds(deviceId, GeneralData.JArrayGen);
            }

            /// <summary>
            /// Adds Update event to device.
            /// </summary>
            /// <param name="deviceId">Device ID</param>
            /// <param name="eventId">Event ID</param>
            public void AddUpdateEventToDevice(int deviceId, int eventId)
            {
                string eventsString = this.GetDeviceUpdateEventIds(deviceId);
                JArray eventsJArray = JArray.Parse(eventsString);
                eventsJArray.Add(eventId);
                string newEventsString = eventsJArray.ToString();
                this.SetDeviceUpdateEventIds(deviceId, newEventsString.RemoveBR());
            }

            public int[] GetAllDevicesIds()
            {
                DeviceData[] datas = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    selectionsArray: new string[] { DeviceTable.Id });
                int[] ids = (from data in datas select data.Id).ToArray();
                return ids;
            }

            /*
            public void SetDeviceKeys(int deviceId, string keys)
            {
                this.Sdb.SQLUpdate(DeviceTable.TableName,
                    new Dictionary<string, string> { { DeviceTable.DeviceName, keys.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(
                        new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
            }

            public string GetDeviceKeys(int deviceId)
            {
                DeviceData[] data = this.Sdb.SQLSelect<DeviceData>(DeviceTable.TableName,
                    selectionsArray: new string[] { DeviceTable.Keys},
                    whereClause: SQLiteDB.GetWhereClause(
                        new Dictionary<string, string> { { DeviceTable.Id, deviceId.ToSQLParm() } }));
                return data.Length > 0 ? data[0].Keys : null;
            }
            */
            #endregion Device

            #region CALL 

            /// <summary>
            /// Adds a call to the database.
            /// </summary>
            /// <param name="data"></param>
            public void NewCall(APICallData data)
            {
                data.Id = GetNewId(APICallTable.TableName);
                this.Sdb.SQLInsert(APICallTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Adds a MonitorCall to the database.
            /// </summary>
            /// <param name="data"></param>
            public void NewMonitorCall(MonitorCallData data)
            {
                this.Sdb.SQLInsert(MonitorCallTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Gets a MonitorCall by id.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public MonitorCallData GetMonitorCallById(int id)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { MonitorCallTable.Id, id.ToSQLParm() } });
                MonitorCallData[] data = this.Sdb.SQLSelect<MonitorCallData>(MonitorCallTable.TableName, whereClause: where);
                if (data.Length > 0)
                    return data[0];
                else
                    return null;
            }

            /// <summary>
            /// Get all calls in database.
            /// </summary>
            /// <returns></returns>
            public APICallData[] GetAllCalls()
            {
                return this.Sdb.SQLSelect<APICallData>(APICallTable.TableName);
            }
           
            /// <summary>
            /// Deletes all calls for a process.
            /// </summary>
            /// <param name="pid"></param>
            public void DeleteCalls(int pid)
            {
                this.Sdb.SQLDelete(APICallTable.TableName,
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { APICallTable.Pid, pid.ToSQLParm() } }));
            }
            #endregion CALL             

            #region Event
            /// <summary>
            /// Adds a new event to the database.
            /// </summary>
            /// <param name="data"></param>
            public void NewEvent(SystemEventData data)
            {
                data.Id = GetNewEventId();
                this.Sdb.SQLInsert(SystemEventTable.TableName, data.GetDict());
                if (AppTypeClass.IsClient && SystemEventData.IsUpdateEvent(data.Type))
                    StoreUpdateEvent(data);
                if (AppTypeClass.IsClient && SystemEventData.IsNotification(data.Type))
                        this.Sdb.SQLInsert(SystemEventTable.TableNameForNotification, data.GetDict());                
            }

            /// <summary>
            /// Adds a new update event to the server's database.
            /// </summary>
            /// <param name="data"></param>
            /// <param name="deviceIds"></param>
            public void NewServerUpdateEvent(SystemEventData data, int[] deviceIds = null)
            {
                data.Id = GetNewEventId();
                MDebug.WriteLine(data.GetDict());
                this.Sdb.SQLInsert(SystemEventTable.TableNameForUpdate, data.GetDict());
                if (deviceIds == null)
                    deviceIds = GetAllDevicesIds();
                foreach (int deviceId in deviceIds)
                    AddUpdateEventToDevice(deviceId, data.Id);

            }

            /// <summary>
            /// Gets an update event by its ID.
            /// </summary>
            /// <param name="eventId"></param>
            /// <returns></returns>
            public SystemEventData GetUpdateEvent(int eventId)
            {
                SystemEventData[] data = this.Sdb.SQLSelect<SystemEventData>(SystemEventTable.TableNameForUpdate,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { SystemEventTable.Id, eventId.ToSQLParm() } }));
                return data.Length > 0 ? data[0] : null;
            }

            /// <summary>
            /// Gets a new event's ID.
            /// </summary>
            /// <returns></returns>
            public int GetNewEventId()
            {
                return GetNewId(SystemEventTable.TableName);
            }

            /// <summary>
            /// Stores an update event.
            /// </summary>
            /// <param name="data"></param>
            public void StoreUpdateEvent(SystemEventData data)
            {
                MDebug.WriteLine("Update event stored " + data.ToJson());
                this.Sdb.SQLInsert(SystemEventTable.TableNameForUpdate, data.GetDict());                
            }

            /// <summary>
            /// Gets all update event that are of PassedTest type.
            /// </summary>
            /// <returns></returns>
            public SystemEventData[] GetUpdateEvents()
            {
                return this.Sdb.SQLSelect<SystemEventData>(SystemEventTable.TableNameForUpdate);
            }

            /// <summary>
            /// Inserts update events of PassedTest type.
            /// </summary>
            /// <param name="tests"></param>
            public void InsertUpdateTests(SystemEventData[] tests)
            {
                foreach (SystemEventData test in tests)
                    this.Sdb.SQLInsert(SystemEventTable.TableNameForUpdate, test.GetDict());
            }

            public void ClearUpdateEvents()
            {
                this.Sdb.SQLDelete(SystemEventTable.TableNameForUpdate);
            }
            #endregion Event

            #region Process
            /// <summary>
            /// Adds a process to the database.
            /// </summary>
            /// <param name="data"></param>
            public void NewProcess(ProcessData data)
            {
                // Get new mid 
                data.Mid = this.GetNewProcessId();
                // Insert to process table 
                this.Sdb.SQLInsert(ProcessTable.TableName, data.GetDict());

                // Update TestsCounters table, to add counters for this process
                this.Sdb.SQLInsert(TestsCountersTable.TableName,
                    new Dictionary<string, string> {
                        { TestsCountersTable.Mid, data.Mid.ToSQLParm() } });
            }

            public void StoreProcess(ProcessData data)
            {
                this.Sdb.SQLInsert(ProcessTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Changes a process' PID
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="pid"></param>
            private void ChangeProcessPid(int mid, int pid)
            {
                this.Sdb.SQLUpdate(ProcessTable.TableName, new Dictionary<string, string>() { { ProcessTable.Pid, pid.ToSQLParm() } },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } }));
            }

            /// <summary>
            /// Signals a process has started in the database.
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="pid"></param>
            public void ProcessStarted(int mid, int pid)
            {
                this.ChangeProcessPid(mid, pid);
            }

            /// <summary>
            /// Signals a process has terminated in the database.
            /// </summary>
            /// <param name="mid"></param>
            public void ProcessTerminated(int mid)
            {
                this.ChangeProcessPid(mid, ProcessData.NOPID);
            }

            /// <summary>
            /// Gets all processes from database.
            /// </summary>
            /// <returns></returns>
            public ProcessData[] GetAllProcesses()
            {
                return this.Sdb.SQLSelect<ProcessData>(ProcessTable.TableName);
            }

            /// <summary>
            /// Gets a new process' PID.
            /// </summary>
            /// <returns></returns>
            public int GetNewProcessId()
            {
                return this.GetNewId(ProcessTable.TableName);
            }

            /// <summary>
            /// Gets a process' MID by its data.
            /// </summary>
            /// <param name="processData"></param>
            /// <returns></returns>
            public int GetProcessMid(ProcessData processData)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Hash, processData.Hash.ToSQLParm() } });
                ProcessData[] processes = Sdb.SQLSelect<ProcessData>(ProcessTable.TableName, whereClause: where);
                if (processes.Length > 0)
                    return processes[0].Mid;
                else
                    return -1;
            }

            /// <summary>
            /// Gets a process MID by its PID.
            /// </summary>
            /// <param name="pid"></param>
            /// <returns></returns>
            public int GetProcessMid(int pid)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Pid, pid.ToSQLParm() } });
                ProcessData[] processes = Sdb.SQLSelect<ProcessData>(ProcessTable.TableName, whereClause: where);
                if (processes.Length > 0)
                    return processes[0].Mid;
                else
                    return -1;
            }

            /// <summary>
            /// Gets a process' profile by its PID
            /// </summary>
            /// <param name="Pid"></param>
            /// <returns></returns>
            public int GetProcessProfileByPid(int Pid)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Pid, Pid.ToSQLParm() } });
                return this.Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    selectionsArray: new string[] { ProcessTable.Profile },
                    whereClause: where)[0].Profile;
            }

            /// <summary>
            /// Gets a process' profile by its MID.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public int GetProcessProfileByMid(int mid)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } });
                return this.Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    selectionsArray: new string[] { ProcessTable.Profile },
                    whereClause: where)[0].Profile;
            }

            /// <summary>
            /// Gets a process' name by its MID.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public string GetProcessName(int mid)
            {
                ProcessData[] data = this.Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    new string[] { ProcessTable.Name },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProcessTable.Mid, mid.ToSQLParm() } }));

                return data.Length > 0 ? data[0].Name : "N/A";
            }

            /// <summary>
            /// Gets a process' name by its PID
            /// </summary>
            /// <param name="pid"></param>
            /// <returns></returns>
            public string GetProcessNameByPid(int pid)
            {
                ProcessData[] data = this.Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    new string[] { ProcessTable.Name },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProcessTable.Pid, pid.ToSQLParm() } }));

                return data.Length > 0 ? data[0].Name : "N/A";
            }

            public void DeleteProcessTable()
            {
                this.Sdb.SQLDelete(ProcessTable.TableName);
            }

            /// <summary>
            /// Gets recent calls for a process.
            /// </summary>
            /// <param name="pid"></param>
            /// <param name="limit"></param>
            /// <returns></returns>
            public APICallData[] GetRecentProcessAPICalls(int pid, int limit)
            {
                string select = SQLiteDB.GetSelectStatement(APICallTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { APICallTable.Pid, pid.ToSQLParm() } }));
                string command = string.Format(select + " ORDER BY {0} DESC Limit {1}", APICallTable.Id, limit);

                APICallData[] calls = this.Sdb.GetTableData<APICallData>(this.Sdb.ExecuteSQLCommandWithData(command),
                    APICallTable.TableName, selectionsArray: null);

                Array.Reverse(calls);
                return calls;
            }

            /// <summary>
            /// Gets process data by MID.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public ProcessData GetProcessByMid(int mid)
            {
                ProcessData[] processes = Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } }));
                if (processes.Length > 0)
                    return processes[0];
                else
                    return null;
            }

            /// <summary>
            /// Increases check counter for process.
            /// </summary>
            /// <param name="mid"></param>
            public void IncreaseProcessCounter(int mid)
            {
                this.Sdb.AddNumToColumn(ProcessTable.TableName, ProcessTable.CheckCounter, num: -1,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } }));
            }

            /// <summary>
            /// Gets check counter for process.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public int GetProcessCheckCounter(int mid)
            {
                ProcessData[] processes = Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    new string[] { ProcessTable.CheckCounter },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } }));
                if (processes.Length > 0)
                    return processes[0].CheckCounter;
                else
                    return -1;
            }

            /// <summary>
            /// Sets check counter for process.
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="counter"></param>
            public void SetProcessCheckCounter(int mid, int counter)
            {
                this.Sdb.SQLUpdate(ProcessTable.TableName,
                    updateDetailsDict: new Dictionary<string, string> { { ProcessTable.CheckCounter, counter.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProcessTable.Mid, mid.ToSQLParm() } }));
            }

            /// <summary>
            /// Get TestCounter of a process.
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="testId"></param>
            /// <returns></returns>
            public int GetProcessTestCounter(int mid, int testId)
            {
                int[][] result = this.Sdb.SQLSelect<int[]>(TestsCountersTable.TableName,
                    selectionsArray: new string[] { TestsCountersTable.GetTestColumnName(testId) },
                    whereClause: SQLiteDB.GetWhereClause(
                        new Dictionary<string, string> { { TestsCountersTable.Mid, mid.ToSQLParm() } }));

                return result.Length > 0 ? result[0][0] : -1;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="testId"></param>
            public void AddTestToCounters(int testId)
            {
                this.Sdb.SQLAddColumn(TestsCountersTable.TableName,
                    TestsCountersTable.GetTestColumnName(testId),
                    SQLiteDB.INT_TYPE,
                    TestsCountersTable.START_COUNTER.ToSQLParm());
            }

            /// <summary>
            /// Increase test counter for a process.
            /// </summary>
            /// <param name="mid">Process' MID.</param>
            /// <param name="testId">Test's ID.</param>
            public void IncreaseTestCounterForProcess(int mid, int testId)
            {
                this.Sdb.AddNumToColumn(TestsCountersTable.TableName,
                    TestsCountersTable.GetTestColumnName(testId),
                    num: 1,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>()
                        { { TestsCountersTable.Mid, mid.ToSQLParm() } }));
            }

            /// <summary>
            /// Reset all Process' PIDs.
            /// </summary>
            public void ResetAllProcessesPids()
            {
                this.Sdb.SQLUpdate(ProcessTable.TableName,
                    new Dictionary<string, string> { { ProcessTable.Pid, ProcessData.NOPID.ToSQLParm() } });
            }

            #endregion Process                                                                             

            #region Profile
            /// <summary>
            /// Adds a new profile in the database.
            /// </summary>
            /// <param name="data"></param>
            public void NewProfile(ProfileData data)
            {
                this.Sdb.SQLInsert(ProfileTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Gets all events.
            /// </summary>
            /// <returns></returns>
            public SystemEventData[] GetAllEvents()
            {
                return this.Sdb.SQLSelect<SystemEventData>(SystemEventTable.TableName);
            }

            /// <summary>
            /// Gets recent notifications for device by limit.
            /// </summary>
            /// <param name="limit"></param>
            /// <returns></returns>
            public SystemEventData[] GetRecentNotifications(int limit=20)
            {
                string select = SQLiteDB.GetSelectStatement(SystemEventTable.TableNameForNotification);              
                string command = string.Format(select + " ORDER BY {0} DESC Limit {1}", APICallTable.Id, limit);

                SystemEventData[] events = this.Sdb.GetTableData<SystemEventData>(this.Sdb.ExecuteSQLCommandWithData(command),
                    SystemEventTable.TableName, selectionsArray: null);
                return events;
            }

            /// <summary>
            /// Gets a profile by its ID.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public ProfileData GetProfileById(int id)
            {
                return this.GetProfileDataByIdWithSelect(id);
            }

            /// <summary>
            /// Gets a profile's tests by its ID.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public ProfileData GetProfileTestsByProfileId(int id)
            {
                return this.GetProfileDataByIdWithSelect(id, new string[] { ProfileTable.TestsIds });
            }

            /// <summary>
            /// Gets a profile's monitor calls by its ID.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public ProfileData GetProfileMonitorsByProfileId(int id)
            {
                return this.GetProfileDataByIdWithSelect(id, new string[] { ProfileTable.MonitorsIds });
            }

            /// <summary>
            /// Gets profile data by selections.
            /// </summary>
            /// <param name="id"></param>
            /// <param name="selections"></param>
            /// <returns></returns>
            public ProfileData GetProfileDataByIdWithSelect(int id, string[] selections = null)
            {
                string where = SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProfileTable.Id, id.ToSQLParm() } });
                ProfileData[] data = this.Sdb.SQLSelect<ProfileData>(ProfileTable.TableName,
                    whereClause: where, selectionsArray: selections);
                if (data.Length > 0)
                    return data[0];
                else
                    return null;
            }

            /// <summary>
            /// Sets a process' profile.
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="profile"></param>
            public void SetProcessProfile(int mid, int profile)
            {
                this.Sdb.SQLUpdate(ProcessTable.TableName,
                    new Dictionary<string, string> { { ProcessTable.Profile, profile.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProcessTable.Mid, mid.ToSQLParm() } }));
            }

            /// <summary>
            /// Gets profiles the process can turn into.
            /// </summary>
            /// <param name="profileId"></param>
            /// <returns></returns>
            public ProfileData GetOptionalProfiles(int profileId)
            {
                ProfileData[] data = this.Sdb.SQLSelect<ProfileData>(
                    ProfileTable.TableName,
                    selectionsArray: new string[] { ProfileTable.OptionalProfilesIds },
                    whereClause: SQLiteDB.GetWhereClause(
                        new Dictionary<string, string> { { ProfileTable.Id, profileId.ToSQLParm() } }));
                return data.Length > 0 ? data[0] : null;
            }

            /// <summary>
            /// Get profiles the profile inherits from.
            /// </summary>
            /// <param name="profileId"></param>
            /// <returns></returns>
            public ProfileData GetInheritedProfiles(int profileId)
            {
                ProfileData[] data = this.Sdb.SQLSelect<ProfileData>(
                    ProfileTable.TableName,
                    selectionsArray: new string[] { ProfileTable.InheritedProfiles },
                    whereClause: SQLiteDB.GetWhereClause(
                        new Dictionary<string, string> { { ProfileTable.Id, profileId.ToSQLParm() } }));
                return data.Length > 0 ? data[0] : null;
            }

            /// <summary>
            /// Gets the all of the tests ids.
            /// </summary>
            /// <returns></returns>
            public int[] GetTestsIds()
            {
                string[] names = this.Sdb.GetTableColumns(TestsCountersTable.TableName).Skip(1).ToArray();
                int[] testsIds = (from name in names select TestsCountersTable.GetTestId(name)).ToArray();
                return testsIds;
            }

            /// <summary>
            /// Gets whether the user filled the survey for the process.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public int GetProcessDidSurvey(int mid)
            {
                ProcessData[] processes = Sdb.SQLSelect<ProcessData>(ProcessTable.TableName,
                    new string[] { ProcessTable.DidSurvey },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>() { { ProcessTable.Mid, mid.ToSQLParm() } }));
                return processes.Length > 0 ? processes[0].DidSurvey : -1;
            }

            /// <summary>
            /// Sets a user had done a survey on a.
            /// </summary>
            /// <param name="mid">The subject process.</param>
            public void SetProcessDidSurvey(int mid)
            {
                this.Sdb.SQLUpdate(ProcessTable.TableName,
                    updateDetailsDict: new Dictionary<string, string> { { ProcessTable.DidSurvey, 1.ToSQLParm() } },
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProcessTable.Mid, mid.ToSQLParm() } }));
            }

            #endregion Profile

            #region MiscTable
            /// <summary>
            /// Adds a new misc to the database.
            /// </summary>
            /// <param name="data"></param>
            public void AddMisc(MiscData data)
            {
                this.Sdb.SQLInsert(MiscTable.TableName, data.GetDict());
            }

            /// <summary>
            /// Removes a misc from the database.
            /// </summary>
            /// <param name="data"></param>
            public void RemoveMisc(MiscData data)
            {
                this.Sdb.SQLDelete(MiscTable.TableName,
                    SQLiteDB.GetWhereClause(new Dictionary<string, string>
                    { { MiscTable.Type, data.Type.ToSQLParm()}, { MiscTable.Data, data.Data.ToSQLParm()} }));
            }

            /// <summary>
            /// Gets all misc of a certain type.
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public string[] GetAllMiscType(int type)
            {
                MiscData[] data = this.Sdb.SQLSelect<MiscData>(MiscTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string> { { MiscTable.Type, type.ToSQLParm() } }));
                string[] detailsArray = (from sdata in data select (sdata.Data)).ToArray();
                return detailsArray;
            }

            /// <summary>
            /// Gets all misc from the database.
            /// </summary>
            /// <returns></returns>
            public MiscData[] GetAllMiscs()
            {
                return this.Sdb.SQLSelect<MiscData>(MiscTable.TableName);
            }

            /// <summary>
            /// Gets miscs of a certain type from the database.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public MiscData[] GetMisc(string data)
            {
                return this.Sdb.SQLSelect<MiscData>(MiscTable.TableName,
                    whereClause: SQLiteDB.GetWhereClause(new Dictionary<string, string>
                    { { MiscTable.Data, data.ToSQLParm() } }));
            }
            #endregion MiscTable            

            #region Product
            /// <summary>
            /// Adds a new Product to the database.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public string NewProduct(ProductData data)
            {
                data.Id = GetNewId(ProductTable.TableName);
                this.Sdb.SQLInsert(ProductTable.TableName, data.GetDict());
                string productFileName = this.GenerateNewProductTextFile(data);
                return productFileName;
            }

            /// <summary>
            /// Gets the product's encryption keys in string format.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public string GetProductStringKeys(int id)
            {
                ProductData[] data = this.Sdb.SQLSelect<ProductData>(ProductTable.TableName,
                    new string[] { ProductTable.Keys },
                    SQLiteDB.GetWhereClause(new Dictionary<string, string> { { ProductTable.Id, id.ToSQLParm() } }));
                return data.Length > 0 ? data[0].Keys : null;
            }

            /// <summary>
            /// Generates a new Product file.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public string GenerateNewProductTextFile(ProductData data)
            {
                string productFileName = ProductTable.GetProductFileName(data.Id);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(productFileName, true))
                {
                    file.WriteLine(data.ToJson());
                }
                return productFileName;
            }

            /// <summary>
            /// Gets a product's keys as bytes.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public byte[][] GetProductByteKeys(int id)
            {
                string stringKeys = GetProductStringKeys(id);
                return stringKeys != null ? ProductData.MalterKeysFromJson(stringKeys) : null;
            }

            /// <summary>
            /// Sets a product's keys as bytes.
            /// </summary>
            /// <param name="keys"></param>
            public void SetThisProductByteKeys(byte[][] keys)
            {
                MalterRegistry.SetThisProductByteKeys(keys[0], keys[1]);
            }

            /// <summary>
            /// Gets the client's product keys as bytes.
            /// </summary>
            /// <returns></returns>
            public byte[][] GetThisProductByteKeys()
            {
                return MalterRegistry.GetThisProductByteKeys();
            }

            /// <summary>
            /// Gets product data from a product data file.
            /// </summary>
            /// <param name="filePath"></param>
            /// <returns></returns>
            public ProductData GetProductDataFromFile(string filePath)
            {
                string productJson = System.IO.File.ReadAllText(filePath);
                return productJson.FromJson<ProductData>();
            }            

            #endregion Product
        }

        public class SQLiteDB
        {
            private string connectionDetails;
            private SQLDBDetails DBDetails;
            internal delegate object GetRowData(DataRow row, string[] selectionsArray);
            private Dictionary<string, SQLiteDB.GetRowData> rdict;
            public static bool Debug = false   ;
            internal const string INSERT = "INSERT INTO {0} ({1}) VALUES ({2})";    // 0=table, 1=colummns, 2=values
            internal const string SELECT = "SELECT {1} FROM {0} {2}";                   // 0=table, 1=selections, 2=where
            internal const string UPDATE = "UPDATE {0} SET {1} {2}";                        // 0=table, 1=updates, 2=where
            internal const string DELETE = "DELETE FROM {0} {1}";                       // 0=table 1=where
            internal const string ADD_COLUMN = "ALTER TABLE {0} ADD {1} {2} DEFAULT {3}"; // 0=table, 1=column name, 2= column type, 3=default value
            internal const string AND = " AND ";
            internal const string OR = " OR ";
            internal const string COMMA = ",";
            internal const string INT_TYPE = "INT";

            /// <summary>
            /// Constructor of SQLiteDB.
            /// </summary>
            /// <param name="rdict">GetRowData dictionary, where the keys are the tables in the database.</param>
            /// <param name="dbDetails">The database's details.</param>
            internal SQLiteDB(Dictionary<string, SQLiteDB.GetRowData> rdict, SQLDBDetails dbDetails)
            {
                this.DBDetails = dbDetails;
                this.connectionDetails = SQLiteDB.CalculateConnectionDetaiils(dbDetails.DataSource, DBDetails.Version, DBDetails.Password);
                this.rdict = rdict;

            }
            /// <summary>
            /// Returns whether the database is protected.
            /// </summary>
            /// <returns></returns>
            internal bool IsProtected()
            {
                try
                {
                    using (SQLiteConnection conn = new SQLiteConnection(
                        CalculateConnectionDetaiils(this.DBDetails.DataSource)))
                    {
                        conn.Open();                        
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// Creates a new database by path.
            /// </summary>
            /// <param name="fileFullPath"></param>
            /// <param name="version"></param>
            public static void CreateDB(string fileFullPath, int version=3)
            {
                SQLiteConnection.CreateFile(fileFullPath);
            }

            /// <summary>
            /// Calculates the connection's details string, used when connecting to the database.
            /// </summary>
            /// <param name="dataSource"></param>
            /// <param name="version"></param>
            /// <param name="password"></param>
            /// <returns></returns>
            internal static string CalculateConnectionDetaiils(string dataSource, string version = "3", string password = null)
            {
                if (password == null)
                {
                    return string.Format("Data Source={0};Version={1};", dataSource, version);

                }
                return string.Format("Data Source={0};Version={1};Password={2}", dataSource, version, password);
            }

            /// <summary>
            /// Executes an SQL command, and no data is queried.
            /// </summary>
            /// <param name="RawSQLCommand"></param>
            internal void ExecuteSQLCommandNoData(string RawSQLCommand)
            {
                if (Debug)
                    MainMalter.MDebug.WriteLine(RawSQLCommand);
                using (SQLiteConnection conn = new SQLiteConnection(this.connectionDetails))
                {
                    conn.Open();
                    SQLiteCommand command = new SQLiteCommand(RawSQLCommand, conn);
                    command.ExecuteNonQuery();

                }
            }
            /// <summary>
            /// Sets the database's password.
            /// </summary>
            /// <param name="password"></param>
            internal void SetPassword(string password)
            {
                using (SQLiteConnection conn = new SQLiteConnection(this.connectionDetails))
                {
                    conn.Open();
                    conn.ChangePassword(password);
                    
                }
            }

            internal void ClearPassword()
            {
                SetPassword(null);
            }

            /// <summary>
            /// Queries data from the database by SQL command.
            /// </summary>
            /// <param name="RawSQLCommand"></param>
            /// <returns></returns>
            internal DataTable ExecuteSQLCommandWithData(string RawSQLCommand)
            {
                if (Debug)  
                    MainMalter.MDebug.WriteLine(RawSQLCommand);
                DataTable dt = new DataTable();
                using (SQLiteConnection conn = new SQLiteConnection(this.connectionDetails))
                {
                    conn.Open();
                    SQLiteCommand command = new SQLiteCommand(RawSQLCommand, conn);
                    SQLiteDataReader reader = command.ExecuteReader();
                    dt.Load(reader);
                }
                return dt;
            }

            /// <summary>
            /// Returns the table data by a DataTable object.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="dt"></param>
            /// <param name="table"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            internal T[] GetTableData<T>(DataTable dt, string table, string[] selectionsArray)
            {
                T[] darr = new T[dt.Rows.Count];
                for (int i = 0; i < darr.Length; i++)
                    darr[i] = (T) rdict[table](dt.Rows[i], selectionsArray);
                return darr;
            }

            /// <summary>
            /// Performs an SQL Select command.
            /// </summary>
            /// <typeparam name="T">Return type</typeparam>
            /// <param name="table">Table</param>
            /// <param name="selectionsArray">Columns to select.</param>
            /// <param name="whereClause">Where clause for comand.</param>
            /// <returns></returns>
            internal T[] SQLSelect<T>(string table, string[] selectionsArray = null, string whereClause = "")
            {
                string command = SQLiteDB.GetSelectStatement(table, selectionsArray, whereClause);
                return this.GetTableData<T>(this.ExecuteSQLCommandWithData(command), table, selectionsArray);
            }

            /// <summary>
            /// Performs an SQL Insert command.
            /// </summary>
            /// <param name="table">Table</param>
            /// <param name="detailsDict">The insert details</param>
            internal void SQLInsert(string table, Dictionary<string, string> detailsDict)
            {
                string command = SQLiteDB.GetInsertStatement(table, detailsDict);
                this.ExecuteSQLCommandNoData(command);
            }

            /// <summary>
            /// Performs an SQL Update command.
            /// </summary>
            /// <param name="table">Table</param>
            /// <param name="updateDetailsDict">Update details.</param>
            /// <param name="whereClause">Where clause for command.</param>
            internal void SQLUpdate(string table, Dictionary<string, string> updateDetailsDict, string whereClause="")
            {
                string command = SQLiteDB.GetUpdateStatement(table, updateDetailsDict, whereClause);
                this.ExecuteSQLCommandNoData(command);
            }

            /// <summary>
            /// Performs an SQL Delete command
            /// </summary>
            /// <param name="table"></param>
            /// <param name="whereClause"></param>
            internal void SQLDelete(string table, string whereClause="")
            {
                this.ExecuteSQLCommandNoData(GetDeleteStatement(table, whereClause));
            }

            /// <summary>
            /// Adds a column to a table.
            /// </summary>
            /// <param name="table">Table</param>
            /// <param name="columnName">Column name</param>
            /// <param name="columnType">Column type</param>
            /// <param name="defaultValue">Column default value.</param>
            internal void SQLAddColumn(string table, string columnName, string columnType, string defaultValue)
            {
                string command = GetAddColumnStatement(table, columnName, columnType, defaultValue);
                this.ExecuteSQLCommandNoData(command);
            }
            
            /// <summary>
            /// Converts the parmeter to a SQL string parameter.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string StringParm(string st)
            {
                st = st.Replace("'", "''");
                return "'" + st + "'";
            }

            /// <summary>
            /// Gets a SQL where clause
            /// </summary>
            /// <param name="detailsDict">Details</param>
            /// <param name="format">Clause format</param>
            /// <param name="sep">Seperator for terms</param>
            /// <returns></returns>
            public static string GetWhereClause(Dictionary<string, string> detailsDict, string format="", string sep= SQLiteDB.AND)
            {
                string clause = "WHERE " + SQLiteDB.GetDetailsString(detailsDict, format, sep);
                return clause;
            }
        
            internal static string GetDetailsString(Dictionary<string, string> details, string format="", string sep=",")
            {
                string[] detailsArray = (from item in details select string.Format("{0}={1}", item.Key, item.Value)).ToArray();
                string detailsString;
                if (format == "")
                    detailsString = string.Join(sep, detailsArray);
                else
                    detailsString = string.Format(format, detailsArray);

                return detailsString;

            }

            /// <summary>
            /// Gets an SQL Delete statement.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="whereClause"></param>
            /// <returns></returns>
            public static string GetDeleteStatement(string table, string whereClause="")
            {
                return string.Format(DELETE, table, whereClause);
            }

            /// <summary>
            /// Gets an SQL Select Statement.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="selectionsArray"></param>
            /// <param name="whereClause"></param>
            /// <returns></returns>
            public static string GetSelectStatement(string table, string[] selectionsArray=null, string whereClause="")
            {
                // Calculating the selections
                string selections;
                if (selectionsArray == null)
                    selections = "*";
                else
                    selections = string.Join(",", selectionsArray);

                string command = string.Format(SQLiteDB.SELECT, table, selections, whereClause);
                return command;
            }

            /// <summary>
            /// Gets an SQL Insert statement.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="detailsDict"></param>
            /// <returns></returns>
            public static string GetInsertStatement(string table, Dictionary<string, string> detailsDict)
            {
                string columns = string.Join(",", detailsDict.Keys);
                string values = string.Join(",", detailsDict.Values);
                string command = string.Format(SQLiteDB.INSERT, table, columns, values);
                return command;

            }

            /// <summary>
            /// Gets an SQL Update statement.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="updateDetailsDict"></param>
            /// <param name="whereClause"></param>
            /// <returns></returns>
            public static string GetUpdateStatement(string table, Dictionary<string, string> updateDetailsDict, string whereClause)
            {
                string updateDetailsString = SQLiteDB.GetDetailsString(updateDetailsDict);
                string command = string.Format(SQLiteDB.UPDATE, table, updateDetailsString, whereClause);
                return command;
            }

            /// <summary>
            /// Gets a statement that adds a column to a table.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="columnName"></param>
            /// <param name="columnType"></param>
            /// <param name="defaultValue"></param>
            /// <returns></returns>
            public static string GetAddColumnStatement(string table, string columnName, string columnType, string defaultValue)
            {
                return string.Format(ADD_COLUMN,table, columnName, columnType, defaultValue);
            }

            public void AddNumToColumn(string tableName, string column, int num=1, string whereClause="")
            {
                string command = string.Format("UPDATE {0} SET {1}={1}{4}{2} {3}", tableName, column, Math.Abs(num), whereClause, num>0?"+":"-");
                this.ExecuteSQLCommandNoData(command);
            }

            /// <summary>
            /// Gets a table's columns.
            /// </summary>
            /// <param name="tableName"></param>
            /// <returns></returns>
            public string[] GetTableColumns(string tableName)
            {
                DataTable columnsData = this.ExecuteSQLCommandWithData(string.Format("PRAGMA table_info({0});", tableName));
                string[] names = new string[columnsData.Rows.Count];
                for (int i=0; i< names.Length; i++)
                {
                    names[i] = (string) columnsData.Rows[i]["name"];
                }
                return names;
            }
        }

        public static class MalterRegistry
        {
            private static string MalterKeyPath = "SOFTWARE";
            private static string MalterKeyName = "Malter";
            private static string MalterKeyFullPath
            {
                get
                {
                    return string.Format(@"{0}\{1}", MalterKeyPath, MalterKeyName);
                }
            }
            private static string Keys = "Keys";
            private static string MalterData = "MalterData";

            /// <summary>
            ///  Creates the malter data subkey directory.
            /// </summary>
            public static void CreateMalterRegKeys()
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(MalterKeyFullPath, true);
                if (key != null)
                    return; 

                key = Registry.LocalMachine.OpenSubKey(MalterKeyPath, true);
                key.CreateSubKey(MalterKeyName);
                key.Close();
            }

            /// <summary>
            /// Deletes the malter data subkey directory.
            /// </summary>
            public static void DeleteMalterRegKeys()
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(MalterKeyPath, true);
                if (key != null)
                    key.DeleteSubKeyTree(MalterKeyName);
                key.Close();
            }

            /// <summary>
            /// Sets a malter data directory value by its name and new value.
            /// </summary>
            /// <param name="subkey"></param>
            /// <param name="value"></param>
            public static void SetMalterSubKey(string subkey, object value)
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(MalterKeyFullPath, true);
                if (key == null)
                {
                    CreateMalterRegKeys();
                    key = Registry.LocalMachine.OpenSubKey(MalterKeyFullPath, true);
                }
                
                key.SetValue(subkey, value);
                key.Close();
            }

            /// <summary>
            /// Gets a malter data directory value by its name.
            /// </summary>
            /// <param name="subkey"></param>
            /// <returns></returns>
            public static object GetMalterSubKey(string subkey)
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(MalterKeyFullPath, true);
                if (key == null)
                {
                    CreateMalterRegKeys();
                    key = Registry.LocalMachine.OpenSubKey(MalterKeyFullPath, true);
                }
                object value =  key.GetValue(subkey);
                key.Close();
                return value;
            }            

            /// <summary>
            /// Sets this product's encryption keys.
            /// </summary>
            /// <param name="vector"></param>
            /// <param name="key"></param>
            public static void SetThisProductByteKeys(byte[] vector, byte[] key)
            {
                SetThisProductStringKeys(ProductData.MalterKeysToJson(vector, key));
            }
            
            /// <summary>
            /// Gets this product's encryption keys.
            /// </summary>
            /// <returns></returns>
            public static byte[][] GetThisProductByteKeys()
            {
                return ProductData.MalterKeysFromJson(GetThisProductStringKeys());
            }

            /// <summary>
            /// Sets this product's encryption keys in byte format.
            /// </summary>
            /// <param name="vector"></param>
            /// <param name="key"></param>
            public static void SetThisProductStringKeys(string keys)
            {
                SetMalterSubKey(Keys, keys);
            }

            /// <summary>
            /// Gets this product's encryption keys in string format.
            /// </summary>
            /// <returns></returns>
            public static string GetThisProductStringKeys()
            {
                return (string)GetMalterSubKey(Keys);
            }

            /// <summary>
            /// Sets malter data of client.
            /// </summary>
            /// <param name="data">New data</param>
            public static void SetMalterData(string data)
            {
                SetMalterSubKey(MalterData, data);
            }

            /// <summary>
            /// Gets malter data of client
            /// </summary>
            /// <returns>Malter data</returns>
            public static string GetMalterData()
            {
                return (string)GetMalterSubKey(MalterData);
            }            
        }
    }
}
