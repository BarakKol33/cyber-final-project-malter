﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using Newtonsoft.Json;

namespace MalterLib
{
    using MalterData;
    using MainMalter;
    namespace GeneralOS
    {
        using OSAPI;

        namespace APIHooking
        {
            using MalterObjects;
            using MalterEngine;
            using System.Linq;
            
            [Serializable]
            public class MonitorCall
            {
                public int Id;
                /// <summary>
                /// Filters IDs for blockig calls.
                /// </summary>
                public int[] BlockFiltersIds;
                /// <summary>
                /// Filters IDs for storing calls.
                /// </summary>
                public int[] StoreFiltersIds;
                public DllFunction CallDllFunction { get; set; }
                
                public MonitorCall(int id, DllFunction callDllFunction, int[] blockids = null, int[] monitorids=null)
                {
                    this.Id = id;
                    this.StoreFiltersIds = monitorids;
                    this.BlockFiltersIds = blockids;
                    this.CallDllFunction = callDllFunction ;
                }
                /// <summary>
                /// Gets a data object for instance.
                /// </summary>
                /// <returns></returns>
                public MonitorCallData GetDataStruct()
                {
                    return new MonitorCallData { Id = this.Id, CallDllFunction = this.CallDllFunction.GetJson(), 
                    StoreFiltersIds = JsonConvert.SerializeObject(this.StoreFiltersIds),
                    BlockFiltersIds = JsonConvert.SerializeObject(this.BlockFiltersIds)};
                }

                public static MonitorCall GetObjectFromData(MonitorCallData data)
                {
                    return new MonitorCall(id: data.Id,
                        blockids: JsonConvert.DeserializeObject<int[]>(data.BlockFiltersIds),
                        monitorids: JsonConvert.DeserializeObject<int[]>(data.StoreFiltersIds),
                        callDllFunction: DllFunction.GetDataFromJson(data.CallDllFunction));
                }

                /// <summary>
                /// Gets a list of calls to be monitored by their ids.
                /// </summary>
                /// <param name="db"></param>
                /// <param name="monitorsIds"></param>
                /// <returns></returns>
                public static MonitorCall[] GetMonitorCallsById(DataInterface db, int[] monitorsIds)
                {
                    return (from id in monitorsIds select MonitorCall.GetObjectFromData(db.GetMonitorCallById(id))).ToArray();
                }

                public override string ToString()
                {
                    return string.Format("MonitorCall Id:{0} CallDllFunction:{1} MonitorFilters:{2} BlockFilters:{3} ",
                        this.Id, this.CallDllFunction, this.StoreFiltersIds, this.BlockFiltersIds);
                }
            }

            public class MonitorCallBuilt
            {
                public int Id;
                /// <summary>
                /// Filters 
                /// </summary>
                public IFilter BlockFilters { get; set; }
                public IFilter MonitorFilters { get; set; }
                public DllFunction CallDllFunction { get; set; }
                public EHHook CallHook;                

                public MonitorCallBuilt(MonitorCall monitor, HookingClient hclient)
                {
                    this.Id = monitor.Id;
                    this.CallDllFunction = monitor.CallDllFunction;
                    this.MonitorFilters = CallsCollectionFilter.GetFilter(monitor.StoreFiltersIds);
                    this.BlockFilters = CallsCollectionFilter.GetFilter(monitor.BlockFiltersIds);
                    this.CallHook = new EHHook(monitor.CallDllFunction);
                    this.CallHook.CreateHook(hclient);
                }                
            }

            public class EHHook 
            {
                private EasyHook.LocalHook hookObj { get; set; }
                public DllFunction Hooked;
                private bool Succeeded;

                public EHHook(DllFunction hooked)
                {
                    this.Hooked = hooked;
                }

                /// <summary>
                /// Creates the hook object.
                /// </summary>
                /// <param name="iep"></param>
                public void CreateHook(HookingClient iep)
                {
                    try
                    {
                        //this.ReportMsg(string.Format("Hook: {0}, {1}", this.Hooked.ToString(), iep.GetHookFunction(this.Hooked)));
                        this.hookObj = EasyHook.LocalHook.Create(
                            EasyHook.LocalHook.GetProcAddress(this.Hooked.DllName, this.Hooked.FunctionName),
                            iep.GetHookFunction(this.Hooked),
                            iep);
                        this.Succeeded = true;
                    }
                    catch (System.DllNotFoundException e)
                    {
                        this.Succeeded = false;
                        // If the dll is not present, there is no point in hooking
                    }
                }

                /// <summary>
                /// Activates the hook on the process.
                /// </summary>
                public void ActivateHook()
                {
                    if (this.Succeeded)
                        this.hookObj.ThreadACL.SetExclusiveACL(new Int32[] { 0 });
                }

                /// <summary>
                /// Deactivates the hook on the process.
                /// </summary>
                public void DeactivateHook()
                {
                    if (this.Succeeded)
                        this.hookObj.Dispose();
                }
            }

            public class ProcessHookingManager
            {
                public string channelName { get; set; }                
                public int HostProcessId;

                public ProcessHookingManager()
                {
                    string channelName = null;
                    EasyHook.RemoteHooking.IpcCreateServer<HookingServer>(ref channelName,
                                                                System.Runtime.Remoting.WellKnownObjectMode.Singleton);
                    this.channelName = channelName;
                    this.HostProcessId = System.Diagnostics.Process.GetCurrentProcess().Id;
                }

                /// <summary>
                /// Injects a process by its PID.
                /// </summary>
                /// <param name="targetPID">Process's PID</param>
                public void Inject(Int32 targetPID)
                {
                    if (targetPID == this.HostProcessId)
                    {
                        return;
                    }
                    MDebug.WriteLine(string.Format("Attempting to inject into process {0}", targetPID));
                    // inject into existing process
                    try
                    {
                        EasyHook.RemoteHooking.Inject(
                        targetPID,          // ID of process to inject into
                        MalterData.MalterDBDetails.DBFiles.INJECTION_LIB,   // 32-bit library to inject (if target is 32-bit)
                        MalterData.MalterDBDetails.DBFiles.INJECTION_LIB,   // 64-bit library to inject (if target is 64-bit)
                        channelName         // the parameters to pass into injected library
                                            // ...
                        );
                    }
                    catch (Exception e)
                    {
                        MDebug.WriteLine("There was an error while injecting into target.");
                        MDebug.WriteLine(e.ToString());
                    }
                }               
            }

            public class HookingServer : MarshalByRefObject
            {
                private DataInterface db { get; set; }

                public HookingServer() : base()
                {
                    this.db = AppResources.CurrentResources.db;
                }
                
                public void IsInstalled(int clientPID) {}

                /// <summary>
                /// Check whether the server is still alive.
                /// </summary>
                public void Ping() { }

                public MonitorCall[] GetMonitorCalls(int pid)
                {
                    int profileId = this.db.GetProcessProfileByPid(pid);
                    int[] monitorIds = Profile.GetMonitorsForProfile(this.db, (Profiles)profileId);
                    MonitorCall[] monitors = MonitorCall.GetMonitorCallsById(this.db, monitorIds);
                    return monitors;
                }

                /// <summary>
                /// Report a list of calls that were captured by the client to the server.
                /// </summary>
                /// <param name="calls"></param>
                public void ReportCalls(APICall[] calls)
                {
                    //this.ReportMsg("Report Calls pid: " + System.Diagnostics.Process.GetCurrentProcess().Id.ToString());
                    foreach (APICall call in calls)
                    {
                        Process.NewCall(call.GetDataStruct(), AppResources.CurrentResources.db);
                    }
                }

                /// <summary>
                /// Report a message to the server.
                /// </summary>
                /// <param name="msg"></param>
                public void ReportMsg(object msg = null)
                {
                    if (msg == null)
                    {
                        MainMalter.MDebug.WriteLine("hello");
                        return;
                    }
                    MainMalter.MDebug.WriteLine(msg.ToString());
                }

                /// <summary>
                /// Gets a process's MID by its PID.
                /// </summary>
                /// <param name="pid"></param>
                /// <returns></returns>
                public int GetProcessMid(int pid)
                {
                    return db.GetProcessMid(pid);
                }
            }

            public partial class HookingClient : EasyHook.IEntryPoint
            {
                private EasyHook.RemoteHooking.IContext Context;
                private HookingServer HServer = null;
                private Queue<APICall> _APIQueue = new Queue<APICall>();
                /// <summary>
                /// Dictionary for saving all hooked functions
                /// </summary>
                private Dictionary<string, Delegate> HookFunc;
                /// <summary>
                /// Dicitionary for saving all monitored functions.
                /// </summary>
                private Dictionary<string, MonitorCallBuilt> Monitored;
                /// <summary>
                /// The process' Pid.
                /// </summary>
                private int InjectedPid;
                /// <summary>
                /// The process' Mid.
                /// </summary>
                private int InjectedMid;
                /// <summary>
                /// Should the WriteFile function needs to be ignored 
                /// (because it was intiated by the injection library.)
                /// </summary>
                private bool IgnoreWriteFile = false;

                public HookingClient(EasyHook.RemoteHooking.IContext context, string channelName)
                {
                    // Connect to server object using provided channel name
                    HServer = EasyHook.RemoteHooking.IpcConnectClient<HookingServer>(channelName);

                    // If Ping fails then the Run method will be not be called
                    this.HServer.Ping();
                    this.Context = context;
                    this.InjectedPid = System.Diagnostics.Process.GetCurrentProcess().Id;
                    this.InjectedMid = this.HServer.GetProcessMid(this.InjectedPid);
                    this.CreateHooksDictionary();

                    // Initialize the dictionary for the MonitorCall objects, this changes as new calls are monitored for the processs.
                    this.Monitored = new Dictionary<string, MonitorCallBuilt>();
                    //this.HServer.Debug("started");

                }

                ///<summary> Receives an API function struct and returns its hook delegate. </summary>
                public Delegate GetHookFunction(DllFunction APIFunction)
                {
                    Delegate func;
                    if (this.HookFunc.TryGetValue(APIFunction.FunctionName, out func))
                        return func;
                    return null; 

                }
                                
                public void CreateMonitors()
                {
                    MonitorCall[] monitors = HServer.GetMonitorCalls(this.InjectedPid);
                    this.ReportMsg("CallMonitors: " + MDebug.EnumrableToString(monitors));
                    foreach (MonitorCall monitor in monitors)
                    {
                        // Append to client dictionary.
                        this.Monitored[monitor.CallDllFunction.FunctionName] = new MonitorCallBuilt(monitor, this);
                    }
                }

                public void ActivateMonitoring()
                {
                    this.ReportMsg("Activating monitors");
                    foreach (MonitorCallBuilt monitorBuilt in this.Monitored.Values)
                    {
                        this.ReportMsg(string.Format("Activated {0} on {1}", monitorBuilt.CallHook.Hooked.FunctionName, this.InjectedPid));
                        // Activate hook.
                        monitorBuilt.CallHook.ActivateHook();
                    }
                }

                public void DeactivateMonitoring()
                {
                    this.ReportMsg("Deactivating monitors");
                    foreach (MonitorCallBuilt monitorBuilt in this.Monitored.Values)
                    {
                        monitorBuilt.CallHook.DeactivateHook();
                    }
                }

                public void CleanMonitoring()
                {
                    this.ReportMsg("Cleaning monitors");
                    EasyHook.LocalHook.Release();
                }

                public void ReportMsg(string msg)
                {
                    this.IgnoreWriteFile = true;                 
                    try
                    {                        
                        msg = string.Format("{0}:{1}", this.InjectedPid, msg);
                        try
                        {
                            HServer.ReportMsg(msg);
                        }
                        catch
                        {
                            //MDebug.LogMsg(msg);
                        }
                    }
                    catch { }
                    this.IgnoreWriteFile = false;
                    
                }

                /// <summary>
                /// Report a call that was just captured.
                /// </summary>
                /// <param name="call"></param>
                public void CallFound(APICall call)
                {
                    lock (this._APIQueue)
                    {
                        if (this._APIQueue.Count < 1000)
                            this._APIQueue.Enqueue(call);
                    }
                }

                /// <summary>
                /// Main Function for HookingClient.
                /// </summary>
                /// <param name="context"></param>
                /// <param name="channelName"></param>
                public void Run(EasyHook.RemoteHooking.IContext context, string channelName)
                {
                    try
                    {                      
                        this.ReportMsg("Running");
                        // Injection is now complete and the server interface is connected
                        HServer.IsInstalled(EasyHook.RemoteHooking.GetCurrentProcessId());
                        this.CreateMonitors();
                        this.ActivateMonitoring();
                        this.MainAction();
                        this.DeactivateMonitoring();

                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                    }
                    finally
                    {
                        this.CleanMonitoring();
                    }
                }

                /// <summary>
                /// Main Logic for HookingClient.
                /// </summary>
                public void MainAction()
                {
                    try
                    {
                        while (true)
                        {
                            APICall[] queued = null;
                            //this.HServer.Debug("running");
                            lock (_APIQueue)
                            {
                                queued = _APIQueue.ToArray();
                                _APIQueue.Clear();
                            }

                            // Send newly monitored file accesses to FileMonitor
                            if (queued != null && queued.Length > 0)
                            {
                                HServer.ReportCalls(queued);
                            }
                            else
                            {
                                // If no calls need to be send, still check if server is alive.
                                HServer.Ping();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                    }
                }
            }

        }

    }
}

