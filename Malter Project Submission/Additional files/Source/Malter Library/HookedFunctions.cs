﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MalterLib
{
    namespace GeneralOS
    {
        namespace APIHooking
        {
            using GeneralOS;
            using GeneralOS.OSAPI;
            using System.ComponentModel;
            using System.Runtime.Versioning;
            using MainMalter;

            public partial class HookingClient : EasyHook.IEntryPoint
            {
                private const CharSet dcharset = CharSet.Unicode;
                private const CallingConvention dconvention = CallingConvention.StdCall;
                private const bool dsetLastError = true;

                /// <summary>
                /// Create Dictionary for all hooked calls.
                /// </summary>
                private void CreateHooksDictionary()
                {
                    // Initialize hooks dictionary, used for getting the function used for hooking.
                    // This dictionary stays constant throught the client's life, 
                    // but can't be static because the method is an object member.
                    this.HookFunc = new Dictionary<string, Delegate>()
                    {
                        {OSAPI.CreateFileW.CallDllFunction.FunctionName , new CreateFile_Delegate(this.CreateFileW_Hook) },
                        {OSAPI.RegSetValueEx.CallDllFunction.FunctionName,  new RegSetValueEx_Delegate(this.RegSetValueEx_Hook)},
                        {OSAPI.OpenFile.CallDllFunction.FunctionName, new OpenFile_Delegate(this.OpenFile_Hook) },
                        {OSAPI.WriteFile.CallDllFunction.FunctionName, new WriteFile_Delegate(this.WriteFile_Hook) },
                        {OSAPI.DNSQuery_W.CallDllFunction.FunctionName, new DNSQuery_W_Delegate(this.DNSQuery_W_Hook) },
                        {OSAPI.RegOpenKeyW.CallDllFunction.FunctionName, new RegOpenKey_Delegate(this.RegOpenKey_Hook) },
                        {OSAPI.DeleteFileA.CallDllFunction.FunctionName, new DeleteFileA_Delegate(this.DeleteFileA_Hook) },
                        {OSAPI.ShellExecuteA.CallDllFunction.FunctionName, new ShellExecuteA_Delegate(this.ShellExecuteA_Hook) }
                };
                }

                /* Some Signatures are based on examples in : http://www.pinvoke.net/index.aspx
                 */

                #region CreateFileW Hook

                [UnmanagedFunctionPointer(dconvention, CharSet = dcharset,SetLastError = dsetLastError)]

                private delegate IntPtr CreateFile_Delegate(
                            String filename,
                            UInt32 desiredAccess,
                            UInt32 shareMode,
                            IntPtr securityAttributes,
                            UInt32 creationDisposition,
                            UInt32 flagsAndAttributes,
                            IntPtr templateFile);

                [DllImport("kernel32.dll", CharSet = dcharset, SetLastError = dsetLastError, CallingConvention = dconvention)]
                private static extern IntPtr CreateFileW(
                    String filename,
                    UInt32 desiredAccess,
                    UInt32 shareMode,
                    IntPtr securityAttributes,
                    UInt32 creationDisposition,
                    UInt32 flagsAndAttributes,
                    IntPtr templateFile);

                /// <summary>
                /// Hook for CreateFileW
                /// </summary>
                /// <param name="filename"></param>
                /// <param name="desiredAccess"></param>
                /// <param name="shareMode"></param>
                /// <param name="securityAttributes"></param>
                /// <param name="creationDisposition"></param>
                /// <param name="flagsAndAttributes"></param>
                /// <param name="templateFile"></param>
                /// <returns></returns>
                private IntPtr CreateFileW_Hook(
                    String filename,
                    UInt32 desiredAccess,
                    UInt32 shareMode,
                    IntPtr securityAttributes,
                    UInt32 creationDisposition,
                    UInt32 flagsAndAttributes,
                    IntPtr templateFile)
                {
                    try
                    {
                        // Log that call was captured
                        // Report call
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        this.ReportMsg("CreateFileW call! " + filename);
                        //this.ReportMsg("CreateFileW pid: " + System.Diagnostics.Process.GetCurrentProcess().Id.ToString());
                        OSAPI.CreateFileW call = new OSAPI.CreateFileW(fileName: filename, mid: this.InjectedMid, pid: this.InjectedPid);
                        this.CallFound(call);
                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                        // swallow exceptions so that any issues caused by this code do not crash target process
                    }

                    return CreateFileW(
                            filename,
                            desiredAccess,
                            shareMode,
                            securityAttributes,
                            creationDisposition,
                            flagsAndAttributes,
                            templateFile);                    
                }

                #endregion

                #region OpenFile Hook 
                [UnmanagedFunctionPointer(dconvention, CharSet = dcharset, SetLastError = dsetLastError)]
                private delegate IntPtr OpenFile_Delegate(string filename, IntPtr reopen, uint style);

                [DllImport("kernel32.dll", CharSet = dcharset, SetLastError = dsetLastError, CallingConvention = dconvention)]
                public static extern IntPtr OpenFile(string filename, IntPtr reopen, uint style);

                /// <summary>
                /// Hook for OpenFile
                /// </summary>
                /// <param name="filename"></param>
                /// <param name="reopen"></param>
                /// <param name="style"></param>
                /// <returns></returns>
                private IntPtr OpenFile_Hook(string filename, IntPtr reopen, uint style)
                {
                    try
                    {
                        // Log that call was captured
                        // Report call
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        string filenameAscii = Encoding.ASCII.GetString(Encoding.Unicode.GetBytes(filename));
                        int nullIndex = filenameAscii.IndexOf('\0');
                        if (nullIndex != -1)
                            filenameAscii = filenameAscii.Substring(0, nullIndex);
                        this.ReportMsg("OpenFile call! " + filenameAscii);
                        this.CallFound(new OSAPI.OpenFile(filenameAscii, mid: this.InjectedMid, pid: this.InjectedPid));
                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                    }
                    
                    return OpenFile(filename, reopen, style);
                }
                #endregion OpenFile Hook

                #region WriteFileW Hook
                [UnmanagedFunctionPointer(dconvention, CharSet = dcharset, SetLastError = dsetLastError)]
                private delegate bool WriteFile_Delegate(IntPtr hfile, IntPtr buffer, int bytesToWrite, out IntPtr bytesWritten, IntPtr overLapped);

                [DllImport("kernel32.dll", CharSet = dcharset, SetLastError = dsetLastError, CallingConvention = dconvention)]
                public static extern bool WriteFile(IntPtr hfile, IntPtr buffer, int bytesToWrite, out IntPtr bytesWritten, IntPtr overLapped);

                /// <summary>
                /// Hook for WriteFile
                /// </summary>
                /// <param name="hfile"></param>
                /// <param name="buffer"></param>
                /// <param name="bytesToWrite"></param>
                /// <param name="bytesWritten"></param>
                /// <param name="overLapped"></param>
                /// <returns></returns>
                private bool WriteFile_Hook(IntPtr hfile, IntPtr buffer, int bytesToWrite, out IntPtr bytesWritten, IntPtr overLapped)
                {                    
                    try
                    {                                                
                        if (!IgnoreWriteFile)
                        {                              
                            string written = Encoding.ASCII.GetString(Encoding.Unicode.GetBytes(Marshal.PtrToStringAuto(buffer)));
                            if (bytesToWrite < written.Length && bytesToWrite > 0)
                                written = written.Substring(0, bytesToWrite);
                            else
                                written = "";
                            string fileName = GetNameByHandle(hfile);

                            
                            // Log that call was captured
                            this.ReportMsg("WriteFile call! " + fileName +  " " + written );
                            //this.ReportMsg("WriteFile pid: " + System.Diagnostics.Process.GetCurrentProcess().Id.ToString());
                            // Report call
                            this.CallFound(new OSAPI.WriteFile(fileName, mid: this.InjectedMid, pid: this.InjectedPid));
                        }
                    }
                    catch (Exception e)
                    {
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        this.ReportMsg(e.ToString());
                    }
                    return WriteFile(hfile, buffer, bytesToWrite, out bytesWritten, overLapped);
                }

                [DllImport("kernel32.dll", SetLastError = true)]
                private static extern bool GetFileInformationByHandleEx(IntPtr hFile, FILE_INFO_BY_HANDLE_CLASS infoClass,
                    out FILE_ID_BOTH_DIR_INFO dirInfo, uint dwBufferSize);
                
                /// <summary>
                /// Gets a file name by its handle.
                /// </summary>
                /// <param name="hnd"></param>
                /// <returns></returns>
                internal string GetNameByHandle(IntPtr hnd)
                {
                    var fileStruct = new FILE_ID_BOTH_DIR_INFO();
                    GetFileInformationByHandleEx(hnd, FILE_INFO_BY_HANDLE_CLASS.FileNameInfo, out fileStruct, (uint)Marshal.SizeOf(fileStruct));
                    var win32Error = Marshal.GetLastWin32Error();
                    if (win32Error != 0)
                        this.ReportMsg("GetNameByHandle error " + win32Error.ToString());
                    string fileName = fileStruct.ShortName;
                    string[] nameParts = fileName.Split('\\');
                    fileName = nameParts.Length > 0 ? nameParts[nameParts.Length - 1] : fileName;
                    return fileName;
                }
                #endregion WriteFileW Hook

                #region DeleteFileA Hook

                private delegate bool DeleteFileA_Delegate(string fileName);

                [DllImport("kernel32.dll", CharSet = dcharset, SetLastError = dsetLastError, CallingConvention = dconvention)]
                public static extern bool DeleteFileA(string fileName);

                private bool DeleteFileA_Hook(string fileName)
                {
                    try
                    {
                        // Log that call was captured                                                
                        this.ReportMsg("DeleteFile call! " + fileName);
                        // Report call
                        DeleteFileA call = new DeleteFileA(fileName, mid: this.InjectedMid, pid: this.InjectedPid);
                        this.CallFound(call);
                    }
                    catch (Exception e)
                    {
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        this.ReportMsg(e.ToString());
                    }
                    
                    return DeleteFileA(fileName);
                }
                #endregion DeleteFileA Hook                

                #region RegOpenKey Hook
                // RegOpenKey: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724897(v=vs.85).aspx
                [UnmanagedFunctionPointer(dconvention, CharSet = dcharset, SetLastError = dsetLastError)]
                private delegate int RegOpenKey_Delegate(IntPtr hkey, string subkey, IntPtr result);

                [DllImport("Advapi32.dll", CharSet = dcharset, SetLastError = dsetLastError, CallingConvention = dconvention)]
                public static extern int RegOpenKey(IntPtr hkey, string subkey, IntPtr result);

                private int RegOpenKey_Hook(IntPtr hkey, string subkey, IntPtr result)
                {
                    try
                    {
                        // Log that call was captured                                                
                        this.ReportMsg("RegOpenKeyExW call! " + RegOpenKeyW.GetFullKey((uint)hkey, subkey));
                        // Report call
                        RegOpenKeyW call = new RegOpenKeyW((uint)hkey, subkey, mid: this.InjectedMid, pid: this.InjectedPid);
                        this.CallFound(call);
                    }
                    catch (Exception e)
                    {
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        this.ReportMsg(e.ToString());
                    }
                    return RegOpenKey(hkey, subkey, result);
                }
                #endregion RegOpenKey Hook

                #region RegSetValue Hook
                // RegSetValueEx: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724923(v=vs.85).aspx
                //[UnmanagedFunctionPointer(dconvention, CharSet = dcharset, SetLastError = dsetLastError)];
                [UnmanagedFunctionPointer(dconvention, CharSet = dcharset, SetLastError = dsetLastError)]
                private delegate int RegSetValueEx_Delegate(IntPtr hKey, String lpValueName, int Reserved, int dwType, String val, int cbData);

                [DllImport("advapi32.dll", CharSet = CharSet.Unicode, BestFitMapping = false, EntryPoint ="RegSetValueExA")]
                [ResourceExposure(ResourceScope.None)]
                internal static extern int RegSetValueEx(IntPtr hKey, String lpValueName, int Reserved, int dwType, String val, int cbData);
                
                private int RegSetValueEx_Hook(IntPtr hKey, String lpValueName, int Reserved, int dwType, String val, int cbData)
                {
                    // Log that call was captured.
                    this.ReportMsg("RegSetValueEx call! " + lpValueName);
                    try
                    {
                        // Report call
                        this.CallFound(new OSAPI.RegSetValueEx(hKey.ToString(), lpValueName, val, cbData.ToString(),
                            mid:this.InjectedMid, pid:this.InjectedPid));
                    }
                    catch (Exception e)
                    {
                        // swallow exceptions so that any issues caused by this code do not crash target process
                        this.ReportMsg(e.ToString());
                    }
                    return RegSetValueEx(hKey, lpValueName, Reserved, dwType, val, cbData);
                }
                
                #endregion RegSetValue Hook                

                #region DNSQuery_W
                [DllImport("dnsapi", EntryPoint = "DnsQuery_W", CharSet = dcharset, CallingConvention = dconvention, SetLastError = dsetLastError)]
                public static extern int DNSQuery_W(IntPtr lpstrName,
            DNSRecordTypes wType, DNSQueryOptions Options, IntPtr pExtra, out IntPtr ppQueryResultsSet, IntPtr pReserved);
                    
                public delegate int DNSQuery_W_Delegate(IntPtr lpstrName,
            DNSRecordTypes wType, DNSQueryOptions Options, IntPtr pExtra, out IntPtr ppQueryResultsSet, IntPtr pReserved);
                
                /// <summary>
                /// Hook for DNSQuery_W
                /// </summary>
                /// <param name="lpstrName"></param>
                /// <param name="wType"></param>
                /// <param name="Options"></param>
                /// <param name="pExtra"></param>
                /// <param name="ppQueryResultsSet"></param>
                /// <param name="pReserved"></param>
                /// <returns></returns>
                public int DNSQuery_W_Hook(IntPtr lpstrName,DNSRecordTypes wType, DNSQueryOptions Options,
                    IntPtr pExtra, out IntPtr ppQueryResultsSet, IntPtr pReserved)
                {
                    try
                    {                        
                        string hostname = Marshal.PtrToStringAuto(lpstrName); // Get hostname
                        this.ReportMsg("DNS call! " + hostname);
                        // Create call object 
                        OSAPI.DNSQuery_W call = new OSAPI.DNSQuery_W(domainName: hostname,
                            queryOption: Options,
                            recordType: wType,
                            mid: this.InjectedMid,
                            pid: this.InjectedPid);
                        // Report call 
                        this.CallFound(call);
                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                        // swallow exceptions so that any issues caused by this code do not crash target process
                    }
                    // Continue the call                     
                    return DNSQuery_W(lpstrName, wType, Options, pExtra, out ppQueryResultsSet, pReserved);
                }



                #endregion DNSQuery_W  

                #region ShellExecuteA
                [DllImport("shell32.dll", CharSet = CharSet.Ansi, CallingConvention = dconvention, SetLastError = dsetLastError)]
                private extern static IntPtr ShellExecuteA(IntPtr hWnd, string operation, string file, string parameters, string directory, int nShowCmd);

                private delegate IntPtr ShellExecuteA_Delegate(IntPtr hWnd, string operation, string file, string parameters, string directory, int nShowCmd);

                /// <summary>
                /// Hook for ShellExecuteA
                /// </summary>
                /// <param name="hWnd"></param>
                /// <param name="operation"></param>
                /// <param name="file"></param>
                /// <param name="parameters"></param>
                /// <param name="directory"></param>
                /// <param name="nShowCmd"></param>
                /// <returns></returns>
                private IntPtr ShellExecuteA_Hook(IntPtr hWnd, string operation, string file, string parameters, string directory, int nShowCmd)
                {                    
                    try
                    {                                                                       
                        if (operation == "open")
                        {                            
                            // Log that call was captured
                            this.ReportMsg("ShellExecuteA call! " + file);
                            // Report call
                            this.CallFound(new OSAPI.ShellExecuteA(file, mid: this.InjectedMid, pid: this.InjectedPid));
                        }
                    }
                    catch (Exception e)
                    {
                        this.ReportMsg(e.ToString());
                        // swallow exceptions so that any issues caused by this code do not crash target process
                    }
                    return ShellExecuteA(hWnd, operation, file, parameters, directory, nShowCmd);

                }            
                #endregion ShellExecuteA
            }
        }

    }
}
