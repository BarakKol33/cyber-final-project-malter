using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace MalterLib
{
    namespace MainMalter
    {
        using MalterData;
        using MalterObjects;
        using MalterEngine;
        using MalterNetwork;

        /// <summary>
        /// Malter main server interface.
        /// </summary>
        public interface IMalterServer
        {
            /// <summary>
            /// Handles a request and returns its response.
            /// </summary>
            /// <param name="request"></param>
            /// <returns></returns>
            Response GetResponse(Request request);
        }

        public class MalterClient
        {
            /// <summary>
            /// Creates a new MalterClient instance.
            /// </summary>
            public MalterClient()
            {                
            }

            /// <summary>
            /// Deletes all the data the client has gathered.
            /// </summary>
            public void DeleteAllData()
            {
                AppResources.CurrentResources.db.DeleteAllClientGatheredData();
            }

            /// <summary>
            /// Updates all the service's elements (mainly ProcessMonitor)
            /// </summary>
            public static void UpdateMonitor()
            {
                try
                {
                    if (AppResources.CurrentResources.AppDevice == null)
                        return;
                    AppResources.CurrentResources.pm.Monitor();
                    UpdateServer();
                    UpdateLocalDB();
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
                
            }

            /// <summary>
            /// Sends updates to the server.
            /// </summary>
            public static void UpdateServer()
            {
                MDebug.WriteLine("Updating server");
                SystemEventData[] updates = AppResources.CurrentResources.db.GetUpdateEvents();
                AppResources.CurrentResources.db.ClearUpdateEvents();
                List<SystemEventData> notSent = new List<SystemEventData>(); // List for updates that werent sent.
                foreach (SystemEventData update in updates)
                {
                    Response response = AppResources.CurrentResources.PClient.GetResponse(new UpdatePassedTest() { Data = update });
                    if (response.Code != MalterExitCodes.Success)
                        notSent.Add(update);
                }
                foreach (SystemEventData test in notSent)
                    AppResources.CurrentResources.db.StoreUpdateEvent(test);
            }

            /// <summary>
            /// Updates the local database with the server.
            /// </summary>
            public static void UpdateLocalDB()
            {
                try
                {
                    Response response = AppResources.CurrentResources.PClient.GetResponse(new GetUpdates());
                    if (response.Code == MalterExitCodes.Success)
                    {
                        foreach (SystemEventData data in response.Value.FromJson<SystemEventData[]>())
                            DataUpdate.UpdateEventInDB(data);
                    }                    
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
            }            

            /// <summary>
            /// Signs up a user.
            /// </summary>
            /// <param name="data">The user to sign.</param>
            /// <returns>The server's response.</returns>
            public static Response SignUpUser(UserData data)
            {
                try
                {
                    return AppResources.CurrentResources.PClient.GetResponse(new SignUpUser() { Data = data });
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return new Response(null, MalterExitCodes.GeneralFailure);
                }
            }

            /// <summary>
            /// Signs up a device.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static Response SignUpDevice(DeviceData data)
            {
                try
                {
                    UserData currentUser = (UserData) GetCurrentUser(null);
                    if (currentUser == null)
                        return new Response(null, MalterExitCodes.NoUserSigned);
                    // Copy the data to another object.
                    data = new DeviceData(data);
                    // Send request.
                    Response response = AppResources.CurrentResources.PClient.GetResponse(new SignUpDevice() { Data = data });
                    if (response.Code == MalterExitCodes.Success)
                    {
                        // Get the device's full data.
                        DeviceData newDevice = response.Value.FromJson<DeviceData>();
                        // Add device to the database.
                        AppResources.CurrentResources.db.NewDevice(newDevice);
                        // Set the current device as the device that was signed.
                        AppResources.CurrentResources.db.SetCurrentDeviceId(newDevice.Id);
                    }
                    return response;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return new Response(null, MalterExitCodes.GeneralFailure);
                }
            }

            /// <summary>
            /// Authenticates (sign-in) a user.
            /// </summary>
            /// <param name="data">The user's credentials</param>
            /// <returns>Server's response.</returns>
            public static Response AuthenticateUser(UserData data)
            {
                try
                {
                    Response response = AppResources.CurrentResources.PClient.GetResponse(
                        new AuthenticateUser() { UserName = data.UserName, Password = data.Password });
                    if (response.Code == MalterExitCodes.Success)
                    {
                        // Get full user data.
                        UserData newUser = response.Value.FromJson<UserData>();
                        AppResources.CurrentResources.db.PutUserData(newUser);
                        // Set as main user for the app.
                        AppResources.CurrentResources.db.SetCurrentUserId(newUser.Id);
                    }
                    return response;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return new Response(null, MalterExitCodes.BadInput);
                }
            }

            /// <summary>
            /// Authenticates (sign-in) a device.
            /// </summary>
            /// <param name="data">The device's credentials.</param>
            /// <returns>Server's response.</returns>
            public static Response AuthenticateDevice(DeviceData data)
            {
                try
                {
                    return AppResources.CurrentResources.PClient.GetResponse(
                        new AuthenticateDevice() { Id = data.Id, Info = data.Info });
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return new Response(null, MalterExitCodes.GeneralFailure);
                }
            }

            /// <summary>
            /// Gets the data of the current logged in user.
            /// </summary>
            /// <param name="data">Reserved, not used.</param>
            /// <returns>Server's response.</returns>
            public static Response GetCurrentUserData(UserData data)
            {
                try
                {
                    Response response =  AppResources.CurrentResources.PClient.GetResponse(
                        new GetUserData() {Id=((UserData)GetCurrentUser()).Id });
                    AppResources.CurrentResources.db.UpdateUserData(response.Value.FromJson<UserData>());
                    return response;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return new Response(null, MalterExitCodes.GeneralFailure);
                }
            }

            /// <summary>
            /// Sends a process survey results to the server.
            /// </summary>
            /// <param name="survey"></param>
            /// <returns>Server's response.</returns>
            public static Response HandleProcessSurvey(ProcessSurvey survey)
            {
                if (survey.UserProcessAwareness)
                {
                    TestPassed testEvent = new TestPassed(mid: survey.SubjectProcess.Mid, testId: (int)Tests.UserAwareness);
                    testEvent.Signal(AppResources.CurrentResources.db);
                    MDebug.WriteLine(testEvent.ToString());
                }
                AppResources.CurrentResources.db.SetProcessDidSurvey(survey.SubjectProcess.Mid);
                return new Response(null, MalterExitCodes.Success);
            }

            /// <summary>
            /// Checks if the server is available.
            /// </summary>
            /// <returns>Server's response.</returns>
            public static Response Ping()
            {
                try
                {
                    Response response = AppResources.CurrentResources.PClient.GetResponse(new Ping());
                    return response;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return null;
                }
                
            }

            /// <summary>
            /// Removes a process from the local machine.
            /// </summary>
            /// <param name="data">Process' data.</param>
            public static void RemoveProcess(ProcessData data)
            {
                if (data.Pid != ProcessData.NOPID)
                {
                    GeneralOS.Process.RemoveProcess(data.Pid);
                    return;
                }
                    
                GeneralOS.Process.RemoveProcess(data.Path);
            }

            /// <summary>
            /// Gets the current logged in user data.
            /// </summary>
            /// <param name="reserved">Reserved, not used.</param>
            /// <returns>The current user's data.</returns>
            public static object GetCurrentUser(UserData reserved=null)
            {
                return AppResources.CurrentResources.AppUser;
            }

            /// <summary>
            /// Gets the current device data.
            /// </summary>
            /// <param name="reserved"></param>
            /// <returns>The current user's data.</returns>
            public static object GetCurrentDevice(DeviceData reserved = null)
            {
                return AppResources.CurrentResources.AppDevice;
            }
            
            /// <summary>
            /// Gets security information for user from server.
            /// </summary>
            /// <returns></returns>
            public static string GetSecurityData()
            {
                Response response = AppResources.CurrentResources.PClient.GetResponse(new GetSecurityData());
                if (response.Code == MalterExitCodes.Success)
                    return response.Value.ToString();
                return null;
            }
            
            /// <summary>
            /// Main entry point for initializing GUI client.
            /// </summary>
            public static void ClientGUIMain()
            {
                // Build static objects and configure other data.
                ConfigureData.CreateGUIObjects();
                new MalterClient().StartGUI();
            }

            /// <summary>
            /// Main entry point for initializing Server client.
            /// </summary>
            public static void ClientServiceMain()
            {
                // Build static objects and configure other data.
                ConfigureData.CreateServiceObjects(); 
                new MalterClient().StartService();
            }
            
            /// <summary>
            /// Start a GUI client.
            /// </summary>
            public void StartGUI()
            {
                try
                {
                    AppResources.CurrentResources.form.Start();                                        
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
            }

            /// <summary>
            /// Start a service client.
            /// </summary>
            public void StartService()
            {
                try
                {
                    while (true)
                    {
                        MDebug.WriteLine("");
                        MDebug.WriteLine("----- Iteration Start -----");
                        UpdateMonitor();
                        Console.WriteLine("Running");
                        Thread.Sleep(3000);
                    }
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
            }            
        }
         
        /// <summary>
        /// Malter main server.
        /// </summary>
        public class MalterServer : IMalterServer
        {
            /// <summary>
            /// Holds whether the server needs to be closed.
            /// </summary>
            public static bool IsDone = false;
            
            /// <summary>
            /// Creates a new MalterServer instance.
            /// </summary>
            public MalterServer()
            {                 
            }

            /// <summary>
            /// Handles a given request.
            /// </summary>
            /// <param name="request">Request to be handled</param>
            /// <returns>Response for request.</returns>
            public Response GetResponse(Request request)
            {
                try
                {
                    if (request == null)
                        return new Response(request, MalterExitCodes.NotFound);
                    return request.Execute(AppResources.CurrentResources.db);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return null;
                }
                
            }

            /// <summary>
            /// Starts the server.
            /// </summary>
            public void Start()
            {
                AppResources.CurrentResources.PServer = new ProxyServer(GetResponse,
                    GetClientEncryptor, ConfigureData.GetConfiguredServerDetails());
                new Thread(AppResources.CurrentResources.PServer.Start).Start();
                ServerConsole();
            }

            /// <summary>
            /// Main entry point for server.
            /// </summary>
            public static void ServerMain()
            {
                ConfigureData.CreateServerObjects();
                new MalterServer().Start();
            }

            /// <summary>
            /// Starts the server's console.
            /// </summary>
            public static void ServerConsole()
            {
                Console.WriteLine("Server Started. Type a command:");
                while (true)
                {
                    Console.Write("-->");
                    string fullCommand = Console.ReadLine(); // Get user input
                    string[] tokens = fullCommand.Split(' ');
                    switch (tokens[0])
                    {
                        case "AddForbiddenFile":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            NewMisc(MiscTypes.RawForbiddenFiles, tokens[1]);
                            Console.WriteLine("Update Noted");                       
                            break;

                        case "AddForbiddenDomain":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            NewMisc(MiscTypes.RawForbiddenDomains, tokens[1]);
                            Console.WriteLine("Update Noted");
                            break;

                        case "AddForbiddenKey":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            NewMisc(MiscTypes.RawForbiddenRegKeys, tokens[1]);
                            Console.WriteLine("Update Noted");
                            break;

                        case "RemoveForbiddenFile":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            RemoveMisc(MiscTypes.RawForbiddenFiles, tokens[1]);
                            Console.WriteLine("Update Noted");
                            break;

                        case "RemoveForbiddenDomain":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            RemoveMisc(MiscTypes.RawForbiddenDomains, tokens[1]);
                            Console.WriteLine("Update Noted");
                            break;

                        case "RemoveForbiddenKey":
                            if (tokens.Length < 2)
                            {
                                Help();
                                break;
                            }
                            RemoveMisc(MiscTypes.RawForbiddenRegKeys, tokens[1]);
                            Console.WriteLine("Update Noted");
                            break;

                        case "exit":
                            IsDone = true;
                            AppResources.CurrentResources.PServer.Close();
                            return;
                        case "NewProduct":
                            GenerateNewProduct();
                            break;
                        default:
                            Help();
                            break;
                    }
                }
            }

            /// <summary>
            /// Add a misc to the miscs list.
            /// </summary>
            /// <param name="type">The misc's type.</param>
            /// <param name="data">The misc's data.</param>
            public static void NewMisc(MiscTypes type, string data)
            {
                AddMisc update = new AddMisc(new MiscData((int)type, data));
                update.Execute();
                AppResources.CurrentResources.db.NewServerUpdateEvent(update.GetDataStruct());

            }

            /// <summary>
            /// Removes a misc item from the miscs list.
            /// </summary>
            /// <param name="type">The misc's type.</param>
            /// <param name="data">The misc's data.</param>
            public static void RemoveMisc(MiscTypes type, string data)
            {
                RemoveMisc update = new RemoveMisc(new MiscData((int)type, data));
                update.Execute();
                AppResources.CurrentResources.db.NewServerUpdateEvent(update.GetDataStruct());
            }

            /// <summary>
            /// Create an UpdateEvent dedicated to update the miscs list of a client.
            /// </summary>
            /// <param name="deviceId"></param>
            public static void ConfigureMiscsForDevice(int deviceId)
            {
                MiscData[] miscs = AppResources.CurrentResources.db.GetAllMiscs();
                int[] ids = new int[] { deviceId };
                foreach (MiscData misc in miscs)
                    AppResources.CurrentResources.db.NewServerUpdateEvent(new AddMisc(misc).GetDataStruct(), ids);
            }

            /// <summary>
            /// Prints help for server console.
            /// </summary>
            public static void Help()
            {
                Console.WriteLine("No such command");
                Console.WriteLine("Available Commands:");
                Console.WriteLine(" - AddForbiddenFile FileName");
                Console.WriteLine(" - RemoveForbiddenFile FileName");
                Console.WriteLine(" - AddForbiddenDomain DomainName");
                Console.WriteLine(" - RemoveForbiddenDomain DomainName");
                Console.WriteLine(" - AddForbiddenKey Key");
                Console.WriteLine(" - RemoveForbiddenKey Key");
                Console.WriteLine(" - exit - close the server");
                Console.WriteLine(" - NewProduct - generate new product");

            }                        
                       
            /// <summary>
            /// Create a new product and save its details in a file.
            /// </summary>
            public static void GenerateNewProduct()
            {
                byte[][] newKeys = MalterCrypto.GenerateEncryptionKeys(); // generate keys
                // Translate them to JSON
                string newKeysJson = ProductData.MalterKeysToJson(newKeys[0], newKeys[1]);
                // Save them in a file of a new product.
                string productFileName = AppResources.CurrentResources.db.NewProduct(new ProductData() { Keys = newKeysJson });
                // Inform the user.
                Console.WriteLine("Product Created: " + productFileName);
            }

            /// <summary>
            /// Get a client's encryption keys from the 
            /// database by a message it has sent.
            /// </summary>
            /// <param name="msg">A message the client has sent.</param>
            /// <returns></returns>
            public static byte[][] GetClientKeys(string msg)
            {
                // parse the client's product ID.
                string productIdString = msg.Split(new string[] { StateObject.DATA }, StringSplitOptions.None)[0];
                // Convert the ID to integer.
                int productId = int.Parse(productIdString);
                // Get the keys 
                byte[][] clientKeys = AppResources.CurrentResources.db.GetProductByteKeys(productId);
                return clientKeys;
            }
           
            /// <summary>
            /// Gets a client encryptor by the message the client has sent to the server.
            /// </summary>
            /// <param name="msg"></param>
            /// <returns>IEncryptor interface for decrypting and ecrypting client messages.</returns>
            public static IEncryptor GetClientEncryptor(string msg)
            {
                byte[][] byteKeys = GetClientKeys(msg);
                return byteKeys != null ? new AESEncryptor(byteKeys) : null ;
            }                        
        }
        
        public abstract class Request
        {
            /// <summary>
            /// Converts a request in a string format to a Reqeust object.
            /// </summary>
            /// <param name="requestJson"></param>
            /// <returns></returns>
            public delegate Request RequestSorter(string requestJson);
            /// <summary>
            /// Dictionary of RequestSorters for coverting all incoming string 
            /// requests for Requst objects.
            /// </summary>
            private static Dictionary<string, RequestSorter> RequestSorters =
                new Dictionary<string, RequestSorter>
            {   // Dictionary for creating the requests objects from JSON text.
                {SignUpDevice.RServiceName, SignUpDevice.FromJson },
                {GetDeviceData.RServiceName, GetDeviceData.FromJson },
                {AuthenticateDevice.RServiceName, AuthenticateDevice.FromJson },

                {SignUpUser.RServiceName, SignUpUser.FromJson },
                {GetUserData.RServiceName, GetUserData.FromJson },
                {AuthenticateUser.RServiceName, AuthenticateUser.FromJson },

                {Ping.RServiceName, Ping.FromJson },
                {GetUpdates.RServiceName, GetUpdates.FromJson },
                {GetProcessData.RServiceName, GetProcessData.FromJson },
                {UpdatePassedTest.RServiceName, UpdatePassedTest.FromJson },

                {GetSecurityData.RServiceName, GetSecurityData.FromJson }
            };
            /// <summary>
            /// The service's name.
            /// </summary>
            public abstract string ServiceName { get; }
            /// <summary>
            /// Details of the device that sent the request.
            /// </summary>
            public DeviceData ClientDevice;
            
            //
            public static string SERVICE_NAME = "ServiceName";
           
            public Request(DeviceData clientDevice=null)
            {
                if (AppTypeClass.IsClient)
                    this.ClientDevice = AppResources.CurrentResources.AppDevice;
                else
                    this.ClientDevice = clientDevice;
            }

            /// <summary>
            /// Executes the request.
            /// </summary>
            /// <param name="db">Database for execution.</param>
            /// <returns></returns>
            public abstract Response Execute(DataInterface db);

            public override string ToString()
            {
                return this.ToJson().ToNeatJson();
            }
            
            /// <summary>
            /// Convert the request to JSON format.
            /// </summary>
            /// <returns></returns>
            public string ToJson()
            {
                return JsonConvert.SerializeObject(this);
            }

            /// <summary>
            /// Gets a Request object by its string form.
            /// </summary>
            /// <param name="requestJson"></param>
            /// <returns></returns>
            public static Request FromJson(string requestJson)
            {
                if (requestJson == null)
                    return null;
                if (requestJson == "")
                    return null;
                // Get service name
                string service = (string) 
                    JObject.Parse(requestJson).Property(Request.SERVICE_NAME).Value;

                // Get request object by service name 
                RequestSorter sorter;
                if (Request.RequestSorters.TryGetValue(service, out sorter))
                    return sorter(requestJson);

                // If no such request is registered, return null.
                return null;

            }

            public static void AddSorter(string name, RequestSorter sorter)
            {
                Request.RequestSorters[name] = sorter;
            }
        }
        #region Requests
        /// <summary>
        /// Check if server is available.
        /// </summary>
        public class Ping : Request
        {
            public static string RServiceName = "Ping";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }

            public override Response Execute(DataInterface db)
            {
                return new Response(this, MalterExitCodes.Success);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<Ping>(requestJson);
            }
        }
        /// <summary>
        /// Gets a process' data by its hash.
        /// </summary>
        public class GetProcessData : Request
        {
            public static string RServiceName = "GetProcessData";
            public override string ServiceName
            {
                get
                {
                    return RServiceName ;
                }
            }
            /// <summary>
            /// The Process' data.
            /// </summary>
            public ProcessData Data;            

            public GetProcessData(ProcessData data)
            {
                this.Data = data;
            }

            public GetProcessData()
            {
            }

            public override Response Execute(DataInterface db)
            {
                // Authenticate the device
                Response authDevice = new AuthenticateDevice(this.ClientDevice.Id,
                    this.ClientDevice.Info).Execute(db);
                if (authDevice.Code != MalterExitCodes.Success)
                {
                    return new Response(this, MalterExitCodes.NoDeviceSigned);
                }
                // Search DB for process
                int mid = db.GetProcessMid(this.Data);
                if (mid == -1)
                {
                    // If it wasn't found, create a new one.
                    GeneralOS.Process.NewProcess(this.Data);
                    mid = db.GetProcessMid(this.Data);
                }
                // Get process details by the mid    
                ProcessData process = db.GetProcessByMid(mid);

                // Update the device's processes list 
                Device.ProcessIdentifiedInDevice(this.ClientDevice.Id, mid, db);

                return new Response(this, MalterExitCodes.Success, process);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<GetProcessData>(requestJson);
            }           
        }

        /// <summary>
        /// Sign-up a device.
        /// </summary>
        public class SignUpDevice : Request
        {
            public static string RServiceName = "SignUpDevice";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            /// <summary>
            /// The new device's data.
            /// </summary>
            public DeviceData Data;
            
            public override Response Execute(DataInterface db)
            {
                if (db.GetUniqueDevice(this.Data) != null)
                {
                    return new Response(this, MalterExitCodes.AccountAlreadyExists);
                }
                if (this.Data.DeviceName == "")
                    return new Response(this, MalterExitCodes.BadInput);
                // Create new device object
                DeviceData newDevice = new DeviceData();
                newDevice.DeviceName = this.Data.DeviceName;
                newDevice.OwnerId = this.Data.OwnerId;
                newDevice.Id = db.GetNewDeviceId();
                newDevice.Info = MalterCrypto.GetRandomString(MalterCrypto.DeviceCookieLength);

                // Store device in DB
                db.NewDevice(newDevice);

                // Update the new device's miscss
                MalterServer.ConfigureMiscsForDevice(newDevice.Id);

                // Return response - everything went well.
                return new Response(this, MalterExitCodes.Success, newDevice);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<SignUpDevice>(requestJson);
            }
        }

        /// <summary>
        /// Authenticate (sign-in) a device.
        /// </summary> 
        public class AuthenticateDevice : Request
        {
            public static string RServiceName = "AuthenticateDevice";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            /// <summary>
            /// Device's id.
            /// </summary>
            public int Id;
            /// <summary>
            /// Device's cookie.
            /// </summary>
            public string Info;

            public AuthenticateDevice(int id, string info)
            {
                this.Id = id;
                this.Info = info;
            }

            public AuthenticateDevice()
            {
            }

            public override Response Execute(DataInterface db)
            {
                string deviceInfo = db.GetDeviceInfo(this.Id);
                if (deviceInfo == "") // Info can't be empty
                    return new Response(this, MalterExitCodes.NoSuchAccount);

                // The infos must be equal.
                if (deviceInfo == this.Info)
                    return new Response(this, MalterExitCodes.Success);
                else
                    return new Response(this, MalterExitCodes.GeneralFailure);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<AuthenticateDevice>(requestJson);
            }
        }

        /// <summary>
        /// Sign up a user.
        /// </summary>
        public class SignUpUser : Request
        {
            public static string RServiceName = "SignUpUser";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            /// <summary>
            /// New user's data.
            /// </summary>
            public UserData Data;

            public override Response Execute(DataInterface db)
            {
                if (db.GetUniqueUser(this.Data) != null) // User can't exist
                    return new Response(this, MalterExitCodes.AccountAlreadyExists);
                // Some details must be typed.
                if (this.Data.UserName == "" || this.Data.Password == "" || !User.IsValidEmail(Data.Email))
                    return new Response(this, MalterExitCodes.BadInput);                

                UserData newUser = new UserData(this.Data);
                newUser.Id = db.GetNewUserId();
                db.NewUser(newUser);
                return new Response(this, MalterExitCodes.Success, newUser);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<SignUpUser>(requestJson);
            }
        }

        /// <summary>
        /// Authenticate (sign-in) a user.
        /// </summary>
        public class AuthenticateUser : Request
        {
            public static string RServiceName = "AuthenticateUser";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            public string UserName;
            public string Password;

            public override Response Execute(DataInterface db)
            {
                UserData data = db.GetUniqueUser(new UserData { UserName = this.UserName });
                if (data == null) // User must exist .
                    return new Response(this, MalterExitCodes.NoSuchAccount);

                // Password must be correct.
                if (data.Password == this.Password)
                    return new Response(this, MalterExitCodes.Success, data);
                else
                    return new Response(this, MalterExitCodes.GeneralFailure);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<AuthenticateUser>(requestJson);
            }
        }

        /// <summary>
        /// Gett
        /// </summary>
        public class GetDeviceData : Request
        {
            public static string RServiceName = "GetDeviceData";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            public int Id;

            public override Response Execute(DataInterface db)
            {
                // Authenticate the client device.
                Response authDevice = new AuthenticateDevice(this.ClientDevice.Id, this.ClientDevice.Info).Execute(db);
                if (authDevice.Code != MalterExitCodes.Success)
                {
                    return new Response(this, MalterExitCodes.NoDeviceSigned);
                }
                // Get the data.
                DeviceData data = db.GetDeviceById(this.Id);
                if (data == null)
                    return new Response(this, MalterExitCodes.NoSuchAccount);
                return new Response(this, MalterExitCodes.Success, data);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<GetDeviceData>(requestJson);
            }
        }

        public class GetUserData : Request
        {
            public static string RServiceName = "GetUserData";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            public int Id;

            public override Response Execute(DataInterface db)
            {
                // Authenticate device 
                Response authDevice = new AuthenticateDevice(this.ClientDevice.Id, this.ClientDevice.Info).Execute(db);
                if (authDevice.Code != MalterExitCodes.Success)
                {
                    return new Response(this, MalterExitCodes.NoDeviceSigned);
                }

                UserData data = db.GetUserById(this.Id);
                if (data == null)
                    return new Response(this, MalterExitCodes.NoSuchAccount);
                return new Response(this, MalterExitCodes.Success, data);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<GetUserData>(requestJson);
            }
        }

        /// <summary>
        /// Update the server that a process has passed a test.
        /// </summary>
        public class UpdatePassedTest : Request
        {
            public static string RServiceName = "UpdatePassedTest";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            /// <summary>
            /// Event data.
            /// </summary>
            public SystemEventData Data;

            public override Response Execute(DataInterface db)
            {
                // Autheticate the device
                Response authDevice = new AuthenticateDevice(this.ClientDevice.Id,
                    this.ClientDevice.Info).Execute(db);
                if (authDevice.Code != MalterExitCodes.Success)
                {
                    return new Response(this, MalterExitCodes.NoDeviceSigned);
                }
                // Get the event's object 
                TestPassed eventData = TestPassed.GetEventFromData(Data);
                // Increase the test counter for the process
                db.IncreaseTestCounterForProcess(eventData.Mid, eventData.TestId);

                // If the process should be checked, initiate a profile check
                if (GeneralOS.Process.UpdateTesting(eventData.Mid, db))
                {
                    // calculate the profile and check if it has changed
                    if (MalwareDetector.DetectProfile(eventData.Mid))                           
                    {
                        // Get new profile
                        int newProfile = AppResources.CurrentResources.db.
                            GetProcessProfileByMid(eventData.Mid);

                        // Update Profile in DB
                        AppResources.CurrentResources.db.NewServerUpdateEvent(
                            new UpdateProfile(mid:eventData.Mid,
                             profileId:newProfile).GetDataStruct(),
                            AppResources.CurrentResources.db.GetAllDevicesThatHaveProcess(eventData.Mid));
                    }
                }

                return new Response(this, MalterExitCodes.Success);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<UpdatePassedTest>(requestJson);
            }
        }

        /// <summary>
        /// Update the server that a new process was found, and get its data.
        /// </summary>
        public class UpdateNewProcessRequest : Request
        {
            public static string RServiceName = "UpdateNewProcess";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }
            public ProcessData Data;
            public override Response Execute(DataInterface db)
            {
                // Get process data 
                Response processDataResponse = new GetProcessData(Data).Execute(db);
                if (processDataResponse.Code == MalterExitCodes.Success)
                {
                    ProcessData newData = processDataResponse.Value.FromJson<ProcessData>();
                    AppResources.CurrentResources.db.NewServerUpdateEvent(
                    new UpdateNewProcess(newData).GetDataStruct(),
                        new int[] { this.ClientDevice.Id });
                }

                return new Response(this, MalterExitCodes.Success); 
            }
        }

        /// <summary>
        /// Get new updates 
        /// </summary>
        public class GetUpdates : Request
        {
            public static string RServiceName = "GetUpdates";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }

            public override Response Execute(DataInterface db)
            {
                SystemEventData[] updates = AppResources.CurrentResources.db.GetDeviceUpdateEvents(this.ClientDevice.Id);
                MDebug.WriteLine("Sending Updates " + updates.ToInfoString());
                AppResources.CurrentResources.db.ClearDeviceUpdateEventsIds(this.ClientDevice.Id);
                return new Response(this, MalterExitCodes.Success, updates);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<GetUpdates>(requestJson);
            }
        }

        /// <summary>
        /// Get security information for user.
        /// </summary>
        public class GetSecurityData : Request
        {
            public static string RServiceName = "GetSecurityData";
            public override string ServiceName
            {
                get
                {
                    return RServiceName;
                }
            }

            public override Response Execute(DataInterface db)
            {
                string data = db.GetMalterSecurityData();
                if (data == null)
                    return new Response(this, MalterExitCodes.GeneralFailure);
                return new Response(this, MalterExitCodes.Success, data);
            }

            public static new Request FromJson(string requestJson)
            {
                return JsonConvert.DeserializeObject<GetSecurityData>(requestJson);
            }
        }
        #endregion Requests

        public class Response
        {
            /// <summary>
            /// The response's exit code.
            /// </summary>
            public MalterExitCodes Code;
            /// <summary>
            /// The response's result.
            /// </summary>
            public object Value;
            /// <summary>
            /// The Original request.
            /// </summary>
            public Request OriginalRequest;
            private const string ORIGINAL_REQUEST = "OriginalRequest";
            private const string VALUE = "Value";
            private const string CODE = "Code";
            /// <summary>
            /// Create a new Response instance.
            /// </summary>
            /// <param name="request"></param>
            /// <param name="code"></param>
            /// <param name="value"></param>
            public Response(Request request, MalterExitCodes code=MalterExitCodes.Success, object value=null)
            {
                this.Code = code;
                this.Value = value;
                this.OriginalRequest = request;
            }

            public override string ToString()
            {
                return this.ToJson().ToNeatJson();
            }

            /// <summary>
            /// Convert the response to JSON format.
            /// </summary>
            /// <returns></returns>
            public string ToJson()
            {
                return JsonConvert.SerializeObject(this);
            }

            /// <summary>
            /// Gets a response object by a response JSON.
            /// </summary>
            /// <param name="responseJson"></param>
            /// <returns></returns>
            public static Response FromJson(string responseJson)
            {
                JObject responseJObject = JObject.Parse(responseJson);                
                string originalRequestJson = (string)responseJObject.Property(ORIGINAL_REQUEST).Value.ToString();
                Request request = Request.FromJson(originalRequestJson);
                object value = (object)responseJObject.Property(VALUE).Value;
                MalterExitCodes code = (MalterExitCodes)(int)responseJObject.Property(CODE).Value;
                Response response = new Response(request, code, value);
                return response;
            }
        }

        /// <summary>
        /// Exit code that represent the status of a response.
        /// </summary>
        public enum MalterExitCodes
        {
            /// <summary>
            /// The action was successful.
            /// </summary>
            Success = 0,
            /// <summary>
            /// The action failed, with no specific detail.
            /// </summary>
            GeneralFailure = 1,
            /// <summary>
            /// The account action failed, because the account doesn't exist.
            /// </summary>
            NoSuchAccount = 2,
            /// <summary>
            /// Creating the account failed. because the account already exists.
            /// </summary>
            AccountAlreadyExists = 3,
            /// <summary>
            /// Action failed because of invalid input.
            /// </summary>
            BadInput = 4,
            /// <summary>
            /// Can't handle this request.
            /// </summary>
            NotFound = 5,
            /// <summary>
            /// Action failed because there is no user signed.
            /// </summary>
            NoUserSigned = 6,
            /// <summary>
            /// Create a new product and save its details in a file.
            /// </summary>
            NoDeviceSigned = 7,
            /// <summary>
            /// Action failed because the server is not available.
            /// </summary>
            ServerNotAvailable = 8,
        }
        
        /// <summary>
        /// Extension methods for MalterExitCodes.
        /// </summary>
        public static class ExitCodesExtensions
        {
            public static string GetMSG(this MalterExitCodes code)
            {
                switch (code)
                {
                    case MalterExitCodes.Success:
                        return "Action sucessful,";
                    case MalterExitCodes.GeneralFailure:
                        return "Action failed.";
                    case MalterExitCodes.NoSuchAccount:
                        return "Account doesn't Exist";
                    case MalterExitCodes.AccountAlreadyExists:
                        return "Account already Exists.";
                    case MalterExitCodes.BadInput:
                        return "Bad Input";
                    case MalterExitCodes.NoDeviceSigned:
                        return "No Device Signed.";
                    case MalterExitCodes.NoUserSigned:
                        return "No User Signed.";
                    case MalterExitCodes.ServerNotAvailable:
                        return "Server Not Available.";
                    default:
                        return "Invalid code";
                }
            }
        }        
    }
}


