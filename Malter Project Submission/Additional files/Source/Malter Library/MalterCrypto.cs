﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MalterLib
{
    public static class MalterCrypto
    {
        /// <summary>
        /// Determines the cookie length for malter devices.
        /// </summary>
        public static int DeviceCookieLength = 6;

        /// <summary>
        /// Returns a string of random characters, by length.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetRandomString(int length)
        {
            const string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider())
            {
                while (length-- > 0)
                {
                    res.Append(valid[GetInt(rnd, valid.Length)]);
                }
            }
            return res.ToString();
        }

        /// <summary>
        /// Returns a random number between 0 and the number.
        /// </summary>
        /// <param name="rnd"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int GetInt(RNGCryptoServiceProvider rnd, int max)
        {
            byte[] r = new byte[4];
            int value;
            do
            {
                rnd.GetBytes(r);
                value = BitConverter.ToInt32(r, 0) & Int32.MaxValue;
            } while (value >= max * (Int32.MaxValue / max));
            return value % max;
        }

        /// <summary>
        /// Does SHA-256 hash. 
        /// based on: https://stackoverflow.com/questions/16999361/obtain-sha-256-string-of-a-string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetSHA256Hash(string value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        /// <summary>
        /// Gets a random byte array by length.
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static byte[] RandomByteArray(int length)
        {
            List<byte> bytes = new List<byte>();
            using (RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider())
                for (int i = 0; i < length; i++)
                    bytes.Add((byte)GetInt(rnd, 127));
            return bytes.ToArray();
        }               
        /// <summary>
        /// Generates new random encryption keys for AES encryption
        /// </summary>
        /// <returns></returns>
        public static byte[][] GenerateEncryptionKeys()
        {
            return new byte[][] {AESEncryptor.GenerateEncryptionVector(), AESEncryptor.GenerateEncryptionKey() };
        }        
    }

    /// <summary>
    /// Class for Murmur3 hash.
    /// based on : https://github.com/sebas77/Murmur3.net/blob/master/Murmur3hash.cs
    /// </summary>
    public class Murmur3
    {
        static public uint MurmurHash3_x86_32(byte[] data, uint length, uint seed)
        {
            uint nblocks = length >> 2;

            uint h1 = seed;

            const uint c1 = 0xcc9e2d51;
            const uint c2 = 0x1b873593;

            //----------
            // body

            int i = 0;

            for (uint j = nblocks; j > 0; --j)
            {
                uint k1l = BitConverter.ToUInt32(data, i);

                k1l *= c1;
                k1l = rotl32(k1l, 15);
                k1l *= c2;

                h1 ^= k1l;
                h1 = rotl32(h1, 13);
                h1 = h1 * 5 + 0xe6546b64;

                i += 4;
            }

            //----------
            // tail

            nblocks <<= 2;

            uint k1 = 0;

            uint tailLength = length & 3;

            if (tailLength == 3)
                k1 ^= (uint)data[2 + nblocks] << 16;
            if (tailLength >= 2)
                k1 ^= (uint)data[1 + nblocks] << 8;
            if (tailLength >= 1)
            {
                k1 ^= data[nblocks];
                k1 *= c1; k1 = rotl32(k1, 15); k1 *= c2; h1 ^= k1;
            }

            //----------
            // finalization

            h1 ^= length;

            h1 = fmix32(h1);

            return h1;
        }

        static uint fmix32(uint h)
        {
            h ^= h >> 16;
            h *= 0x85ebca6b;
            h ^= h >> 13;
            h *= 0xc2b2ae35;
            h ^= h >> 16;

            return h;
        }

        static uint rotl32(uint x, byte r)
        {
            return (x << r) | (x >> (32 - r));
        }

        static public bool VerificationTest()
        {
            byte[] key = new byte[256];
            byte[] hashes = new byte[1024];

            for (uint i = 0; i < 256; i++)
            {
                key[i] = (byte)i;

                uint result = MurmurHash3_x86_32(key, i, 256 - i);

                Buffer.BlockCopy(BitConverter.GetBytes(result), 0, hashes, (int)i * 4, 4);
            }

            // Then hash the result array

            uint finalr = MurmurHash3_x86_32(hashes, 1024, 0);

            uint verification = 0xB0F57EE3;

            //----------

            if (verification != finalr)
            {
                return false;
            }
            else
            {
                System.Console.WriteLine("works");

                return true;
            }
        }
    }

        /// <summary>
        /// Interface for encrypting and decryption strings.
        /// </summary>
        public interface IEncryptor
    {
        string EncryptToString(string TextValue);
        string DecryptString(string EncryptedValue);
    }

    /// <summary>
    /// Class for AES encryption.
    /// based on : https://stackoverflow.com/questions/165808/simple-insecure-two-way-obfuscation-for-c-sharp
    /// </summary>
    public class AESEncryptor : IEncryptor
    {        
        private ICryptoTransform EncryptorTransform, DecryptorTransform;
        private System.Text.UTF8Encoding UTFEncoder;

        /// <summary>
        /// Creates Encryptor instance.
        /// </summary>
        /// <param name="EncryptionKeys"></param>
        public AESEncryptor(byte[][] EncryptionKeys)
        {
            //This is our encryption method
            RijndaelManaged rm = new RijndaelManaged();
            
            //Create an encryptor and a decryptor using our encryption method, key, and vector.
            EncryptorTransform = rm.CreateEncryptor(EncryptionKeys[1], EncryptionKeys[0]);
            DecryptorTransform = rm.CreateDecryptor(EncryptionKeys[1], EncryptionKeys[0]);

            //Used to translate bytes to text and vice versa
            UTFEncoder = new System.Text.UTF8Encoding();
        }

        //// -------------- Two Utility Methods (not used but may be useful) -----------        
        /// <summary>
        ///  Generates an encryption key.       
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateEncryptionKey()
        {
            //Generate a Key.
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateKey();
            return rm.Key;
        }

        /// <summary>
        /// Generates a unique encryption vector       
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateEncryptionVector()
        {
            //Generate a Vector
            RijndaelManaged rm = new RijndaelManaged();
            rm.GenerateIV();
            return rm.IV;
        }


        /// ----------- The commonly used methods ------------------------------    
        /// Encrypt some text and return a string suitable for passing in a URL.
        public string EncryptToString(string TextValue)
        {
            return ByteArrToString(Encrypt(TextValue));
        }

        /// Encrypt some text and return an encrypted byte array.
        public byte[] Encrypt(string TextValue)
        {
            //Translates our text value into a byte array.
            Byte[] bytes = UTFEncoder.GetBytes(TextValue);

            //Used to stream the data in and out of the CryptoStream.
            MemoryStream memoryStream = new MemoryStream();

            /*
             * We will have to write the unencrypted bytes to the stream,
             * then read the encrypted result back from the stream.
             */
            #region Write the decrypted value to the encryption stream
            CryptoStream cs = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cs.Write(bytes, 0, bytes.Length);
            cs.FlushFinalBlock();
            #endregion

            #region Read encrypted value back out of the stream
            memoryStream.Position = 0;
            byte[] encrypted = new byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);
            #endregion

            //Clean up.
            cs.Close();
            memoryStream.Close();

            return encrypted;
        }

        /// The other side: Decryption methods
        public string DecryptString(string EncryptedString)
        {
            return Decrypt(StrToByteArray(EncryptedString));
        }

        /// Decryption when working with byte arrays.    
        public string Decrypt(byte[] EncryptedValue)
        {
            #region Write the encrypted value to the decryption stream
            MemoryStream encryptedStream = new MemoryStream();
            CryptoStream decryptStream = new CryptoStream(encryptedStream, DecryptorTransform, CryptoStreamMode.Write);
            decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
            decryptStream.FlushFinalBlock();
            #endregion

            #region Read the decrypted value from the stream.
            encryptedStream.Position = 0;
            Byte[] decryptedBytes = new Byte[encryptedStream.Length];
            encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
            encryptedStream.Close();
            #endregion
            return UTFEncoder.GetString(decryptedBytes);
        }

        /// Convert a string to a byte array.  NOTE: Normally we'd create a Byte Array from a string using an ASCII encoding (like so).
        //      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        //      return encoding.GetBytes(str);
        // However, this results in character values that cannot be passed in a URL.  So, instead, I just
        // lay out all of the byte values in a long string of numbers (three per - must pad numbers less than 100).
        public byte[] StrToByteArray(string str)
        {
            if (str.Length == 0)
                throw new Exception("Invalid string value in StrToByteArray");

            byte val;
            byte[] byteArr = new byte[str.Length / 3];
            int i = 0;
            int j = 0;
            do
            {
                val = byte.Parse(str.Substring(i, 3));
                byteArr[j++] = val;
                i += 3;
            }
            while (i < str.Length);
            return byteArr;
        }

        // Same comment as above.  Normally the conversion would use an ASCII encoding in the other direction:
        //      System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        //      return enc.GetString(byteArr);    
        public string ByteArrToString(byte[] byteArr)
        {
            byte val;
            string tempStr = "";
            for (int i = 0; i <= byteArr.GetUpperBound(0); i++)
            {
                val = byteArr[i];
                if (val < (byte)10)
                    tempStr += "00" + val.ToString();
                else if (val < (byte)100)
                    tempStr += "0" + val.ToString();
                else
                    tempStr += val.ToString();
            }
            return tempStr;
        }

        public byte[] StrToByteArray2(string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }

        public string ByteArrToString2(byte[] byteArr)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetString(byteArr);  
        }
    }


}
