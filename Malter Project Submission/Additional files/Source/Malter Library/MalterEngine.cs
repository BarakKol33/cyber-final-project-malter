﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MalterLib
{
    namespace MalterEngine
    {
        using MainMalter;
        using MalterObjects;
        using MalterData;
        using GeneralOS.OSAPI;
        using System.Diagnostics;

        #region Functions
        /// <summary>
        /// Dictates the logic of some function object.
        /// </summary>
        /// <param name="contextToken">A token for the context of the execution</param>
        /// <param name="fparams">Parameters for the function</param>
        /// <returns></returns>
        public delegate object Logic(int contextToken, params object[] fparams);
        /// <summary>
        /// CallBack parameter for Matches function.
        /// This function is called with every item in the given list.
        /// </summary>
        /// <param name="contextToken"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public delegate object MatchesHelper(int contextToken, object item);

        /// <summary>
        /// Class for representing a general function structure.
        /// </summary>
        /// <typeparam name="T">Function's return type.</typeparam>
        public abstract class Function<T>
        {
            /// <summary>
            /// Function's result.
            /// </summary>
            public T Result { get { return PResult; } }
            protected T PResult { get; set; }
            /// <summary>
            /// Calculate the function.
            /// </summary>
            /// <param name="contextToken">Token for the execution's context 
            /// ("global" parameters.)</param>
            /// <returns></returns>
            public abstract object Execute(int contextToken);
        }

        /// <summary>
        /// Class for representing a function with certain rules.
        /// </summary>
        public abstract class LightFunction : Function<object>
        {
            /// <summary>
            /// Function parameters.
            /// </summary>
            protected object[] FParams;
            /// <summary>
            /// The function's logic.
            /// </summary>
            protected abstract Logic logicFunction { get; }
            /// <summary>
            /// Function's ID.
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// Creates a new LightFunction instance.
            /// </summary>
            /// <param name="fparams"></param>
            public LightFunction(params object[] fparams)
            {
                this.FParams = fparams;
            }

            public override object Execute(int contextToken)
            {
                this.PResult = this.logicFunction(contextToken, this.FParams);
                return this.PResult;
            }

            public static string GroupToString(LightFunction[] functions)
            {
                return MDebug.EnumrableToString((from func in functions select func.Id.ToString()));
            }
        }
        
        /// <summary>
        /// Comparing two items.
        /// Receives two general items, and returns true if they are equal and 
        /// false if they are not.
        /// </summary>
        public class Cmp : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(CmpLogic); } }

            public static object CmpLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                bool result = Object.Equals(fparams[0], fparams[1]);
                if (result)
                    MDebug.WriteLine(fparams.ToInfoString() + " " + result);
                return result;
            }

            public Cmp(params object[] fparams) : base(fparams)
            {
            }
        }
        /// <summary>
        /// Receives two integers, and returns a boolean indicating whether the first is bigger than 
        /// the second.
        /// </summary>
        public class BiggerThan : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(BiggerThanLogic); } }

            public static object BiggerThanLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                return (int)fparams[0] > (int)fparams[1];
            }

            public BiggerThan(params object[] fparams) : base(fparams)
            {
            }
        }

        /// <summary>
        /// Receives two strings and checks if the first conatains the second.
        /// </summary>
        public class Contains : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(ContainsLogic); } }

            public static object ContainsLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                if (fparams[0] == null || fparams[1] == null)
                    return false;
                Debug.Assert(fparams[0].GetType() == fparams[1].GetType() && fparams[0].GetType() == typeof(string),
                    "Parameters of Conains method must be strings.");
                bool result = ((string)(fparams[0])).Contains((string)fparams[1]);
                if (result || true)
                    MDebug.WriteLine(fparams.ToInfoString() + " " + result);
                return result;
            }

            public Contains(params object[] fparams) : base(fparams)
            {
            }
        }

        /// <summary>
        /// Receives two strings and returns if the first ends with the second.
        /// </summary>
        public class EndsWith : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(EndsWithLogic); } }

            public static object EndsWithLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                if (fparams[0] == null || fparams[1] == null)
                    return false;
                Debug.Assert(fparams[0].GetType() == fparams[1].GetType() && fparams[0].GetType() == typeof(string),
                    "Parameters of Conains method must be strings.");
                //MDebug.WriteLine("{0} {1} {2}", fparams[0], fparams[1], ((string)(fparams[0])).EndsWith((string)fparams[1]));
                return ((string)(fparams[0])).EndsWith((string)fparams[1]);
            }

            public EndsWith(params object[] fparams) : base(fparams)
            {
            }
        }

        /// <summary>
        /// Receives an array of strings and a MatchesHelper callback, and returns whether one 
        /// of the items returned true for the callback.
        /// </summary>
        public class Matches : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(MatchesLogic); } }

            private static object MatchesLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                string[] options = (string[])fparams[0];                
                MatchesHelper matchDelegate = (MatchesHelper)fparams[1];
                for (int i = 0; i < options.Length; i++)
                {
                    if ((bool)matchDelegate(contextToken, options[i])) 
                        return true;
                }
                return false;
            }

            public Matches(params object[] fparams) : base(fparams)
            {
            }
        }

        /// <summary>
        /// Returns True.
        /// </summary>
        public class TrueFunc : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic((context, fparams) => true); } }
        }
        /// <summary>
        /// Returns False.
        /// </summary>
        public class FalseFunc : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic((context, fparams) => false); } }
        }

        /// <summary>
        /// Receives boolean values, performs AND with them all, and returns the result.
        /// </summary>
        public class And : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(AndLogic); } }

            public static object AndLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                foreach (object value in fparams)
                {
                    if (!(bool)value)
                        return false;
                }
                return true;
            }

            public And(params object[] fparams) : base(fparams)
            {
            }

        }
        /// <summary>
        /// Receives boolean values, performs OR with them all, and returns the result.
        /// </summary>
        public class Or : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(OrLogic); } }

            public static object OrLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                foreach (object value in fparams)
                {
                    if ((bool)value)
                        return true;
                }
                return false;
            }

            public Or(params object[] fparams) : base(fparams)
            {
            }
        }

        /// <summary>
        /// Receives a property name of an object in the TestContext's list, and returns 
        /// the value of the property.
        /// </summary>
        public class GetProperty : LightFunction
        {
            protected override Logic logicFunction { get { return new Logic(GetPropertyLogic); } }

            public GetProperty(params object[] fparams) : base(fparams)
            {
            }

            public static object GetPropertyLogic(int contextToken, params object[] fparams)
            {
                fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                Debug.Assert(fparams.Length > 0, "No property given for GetProperty");
                string propertyName = (string)fparams[0];
                JProperty property = TestContext.GetContext(contextToken).GetCurrentCheckedCall().Property(propertyName);
                if (property == null)
                {
                    //MDebug.WriteLine("Warning: No Such propertry " + propertyName);
                    return null;
                }
                    
                JToken value = property.Value;
                //MDebug.WriteLine(TestContext.GetContext(contextToken).GetCurrentCheckedCall());
                if (value.Type == JTokenType.Integer)
                    return (int)value;
                else if (value.Type == JTokenType.String)
                {
                    string svalue = (string)value;
                    return svalue;
                }
                    
                return null;
            }
        }

        /// <summary>
        /// Receives a misc name and returns its contents from the TestContext.
        /// </summary>
        public class GetMisc : LightFunction
        {
            protected override Logic logicFunction
            {
                get
                {
                    return GetMiscLogic;
                }
            }

            public GetMisc(params object[] fparams) : base(fparams)
            {
            }

            public static object GetMiscLogic(int contextToken, params object[] fparams)
            {
                string miscType = (string)fparams[0];
                object miscData = TestContext.GetContext(contextToken).GetMisc(miscType);                
                return miscData;
            }
        }

        #endregion Functions

        /// <summary>
        /// Holds global values shared across functions, and are used in their
        /// calculations.
        /// </summary>
        public class TestContext
        {
            /// <summary>
            /// Array containing the current list of APICalls being checked.
            /// </summary>
            private JArray CheckedCalls;
            /// <summary>
            /// Index of current call in CheckedCalls.
            /// </summary>
            private int CurrentCallIndex;
            /// <summary>
            /// Token for the context.
            /// </summary>
            public int ContextToken { get { return this._ContextToken; } }
            private int _ContextToken { get; set; }
            /// <summary>
            /// States whether there is need for more calculations.
            /// </summary>
            private bool IsDone;
            /// <summary>
            /// Holds the miscs data for the execution.
            /// </summary>
            private Dictionary<string, object> MiscsDataDict;
            /// <summary>
            /// Holds all contexts.
            /// </summary>
            private static List<TestContext> contexts = new List<TestContext>();

            /// <summary>
            /// Creates a new TestContext instance
            /// </summary>
            /// <param name="checkedCalls">Calls to be checked</param>
            /// <param name="miscsDataDict">Miscs data for call.</param>
            public TestContext(JArray checkedCalls, Dictionary<string, object> miscsDataDict=null)
            {
                this.CheckedCalls = checkedCalls;
                this._ContextToken = contexts.Count;
                this.CurrentCallIndex = 0;
                this.IsDone = false;
                this.CalcIsDone();                
                this.MiscsDataDict = miscsDataDict;

                TestContext.contexts.Add(this);

            }

            /// <summary>
            /// Gets a context by its token.
            /// </summary>
            /// <param name="contextToken"></param>
            /// <returns></returns>
            public static TestContext GetContext(int contextToken)
            {
                return TestContext.contexts[contextToken];
            }

            /// <summary>
            /// Returns a fresh copy of a given context
            /// (fresh meaning it starts in the first call of the array).
            /// </summary>
            /// <param name="contextToken"></param>
            /// <returns></returns>
            public static TestContext GetContextCopy(int contextToken)
            {
                TestContext original = TestContext.GetContext(contextToken);
                return new TestContext(original.CheckedCalls, original.MiscsDataDict);
            }

            /// <summary>
            /// Evaluates the given objects.
            /// </summary>
            /// <param name="fparams"></param>
            /// <returns></returns>
            public object[] EvalParams(object[] fparams)
            {
                return (from param in fparams select this.Eval(param)).ToArray();
            }

            /// <summary>
            /// Evaluates the given object. 
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public object Eval(object obj)
            {
                if (obj is LightFunction)
                    return ((LightFunction)obj).Execute(this.ContextToken);
                return obj;
            }

            /// <summary>
            /// Gets the current call that is checked.
            /// </summary>
            /// <returns></returns>
            public JObject GetCurrentCheckedCall()
            {
                if (this.IsDone)
                    return null;
                //MDebug.WriteLine(((JObject)this.CheckedCalls[this.CurrentCallIndex]).ToString() + "\n"+ this.CurrentCallIndex.ToString() + " "+this.ContextToken.ToString());
                return (JObject)this.CheckedCalls[this.CurrentCallIndex];
            }

            /// <summary>
            /// Gets a misc data by its name.
            /// </summary>
            /// <param name="miscType"></param>
            /// <returns></returns>
            public object GetMisc(string miscType)
            {
                object miscData;
                if (MiscsDataDict.TryGetValue(miscType, out miscData))
                    return miscData;
                return null;
            }

            /// <summary>
            /// Calculates if the check is over.
            /// </summary>
            private void CalcIsDone()
            {
                this.IsDone = this.CurrentCallIndex >= this.CheckedCalls.Count;
                //MDebug.WriteLine(string.Format("{0} {1} {2}", this.IsDone, this.CurrentCallIndex, this.CheckedCalls.Count));
            }

            /// <summary>
            /// Receives whether the check is over.
            /// </summary>
            /// <returns></returns>
            public bool GetIsDone()
            {
                return this.IsDone;
            }

            /// <summary>
            /// Signals the context to move to the next call.
            /// </summary>
            public void NextCall()
            {
                //MDebug.WriteLine(this.CurrentCallIndex.ToString() + " " + this.ContextToken);
                this.CurrentCallIndex += 1;
                this.CalcIsDone();
            }            
        }

        /// <summary>
        /// Class for gathering data on a process, used for generating a TestContext.
        /// </summary>
        public class ProcessDataProvider
        {
            /// <summary>
            /// Database access.
            /// </summary>
            private DataInterface db;
            private APICall[] Calls = null;
            private int Pid;
            public const string RAW_FORBIDDEN_FILES = "RawForbiddenFiles";
            public const string RAW_FORBIDDEN_REG_KEYS = "RawForbiddenRegKeys";
            public const string RAW_FORBIDDEN_DOMAINS = "RawForbiddenDomains";
            public static readonly string[] SupportedMiscs = new string[] 
            { RAW_FORBIDDEN_REG_KEYS, RAW_FORBIDDEN_FILES, RAW_FORBIDDEN_DOMAINS};
            public static Dictionary<string, MiscTypes> miscDict = new Dictionary<string, MiscTypes>
            {
                {RAW_FORBIDDEN_FILES, MiscTypes.RawForbiddenFiles },
                {RAW_FORBIDDEN_DOMAINS, MiscTypes.RawForbiddenDomains },
                {RAW_FORBIDDEN_REG_KEYS, MiscTypes.RawForbiddenRegKeys }
            };

            /// <summary>
            /// Creates a new ProcessDataProvider instance by database access.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="pid">Subject process' PID.</param>
            public ProcessDataProvider(DataInterface db, int pid)
            {
                this.Pid = pid;
                this.db = db;
            }

            /// <summary>
            /// Creates a new ProcessDataProvider instance by the calls themselves.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="pid">Subject process' PID.</param>
            public ProcessDataProvider(APICall[] calls, int pid)
            {
                this.Calls = calls;
                this.Pid = pid;
            }

            /// <summary>
            /// Gets the recent calls a process has made.
            /// </summary>
            /// <returns></returns>
            private APICall[] GetRecentCalls()
            {
                return APICall.GetRecentCalls(this.db, this.Pid, GeneralOS.Process.CHECK_COUNTER, clear:true);
            }

            /// <summary>
            /// Gets the tests need to be run on the process.
            /// </summary>
            /// <returns></returns>
            public LightFunction[] GetTests()
            {
                return Profile.GetTestsForProfile(db, (Profiles)db.GetProcessProfileByPid(this.Pid));
            }

            /// <summary>
            /// Gets a new context for testing the process.
            /// </summary>
            /// <returns></returns>
            public TestContext GetContext()
            {
                APICall[] callsForContext;
                if (this.Calls != null)
                    callsForContext = this.Calls;
                else
                    callsForContext = this.GetRecentCalls();
                TestContext newContext = new TestContext(JArray.FromObject(callsForContext), GetMiscs());
                MDebug.WriteLine(string.Format("Process {0} Calls: {1}", this.Pid, JArray.FromObject(callsForContext)));
                return newContext;
            }           

            /// <summary>
            /// Gets the profiles the process can turn to.
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public static Profiles[] GetOptionalProfiles(int mid)
            {
                int profile = AppResources.CurrentResources.db.GetProcessProfileByMid(mid);
                int[] optionalProfilesIds =  Profile.GetOptionalProfileForProfile(AppResources.CurrentResources.db, (Profiles)profile);
                //MDebug.WriteLine(optionalProfilesIds);
                Profiles[] optionalProfiles = (from optionalProfile in optionalProfilesIds select (Profiles)optionalProfile).ToArray();
                return optionalProfiles;
            }

            /// <summary>
            /// Gets the details of the tests a process has passed. 
            /// </summary>
            /// <param name="mid"></param>
            /// <returns></returns>
            public static Dictionary<int, int> GetProcessTestDetails(int mid)
            {
                int[] testsIds = AppResources.CurrentResources.db.GetTestsIds();
                Dictionary<int, int> testsDetails = new Dictionary<int, int>();
                foreach (int testId in testsIds)
                    testsDetails[testId] = AppResources.CurrentResources.db.GetProcessTestCounter(mid, testId);
                return testsDetails;
            }            

            /// <summary>
            /// Gets all miscs data, to be stored in context.
            /// </summary>
            /// <returns></returns>
            public static Dictionary<string, object> GetMiscs()
            {
                Dictionary<string, object> miscsDataDict = new Dictionary<string, object>();
                foreach (string miscType in SupportedMiscs)
                    miscsDataDict[miscType] = GetMisc(miscType);
                return miscsDataDict;
            } 

            /// <summary>
            /// Gets a misc data by its name.
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static object GetMisc(string type)
            {
                return AppResources.CurrentResources.db.GetAllMiscType((int)miscDict[type]);
        }


        }

        class MalwareDetector
        {
            private DataInterface db;
            /// <summary>
            /// Subject process PID.
            /// </summary>
            private int Pid;
            /// <summary>
            /// Data provider.
            /// </summary>
            private ProcessDataProvider PDProvider;
            /// <summary>
            /// Checks if the process is fit for the profile.
            /// </summary>
            /// <param name="pointsDetails">Profile points the process has.</param>
            /// <returns>Whether the process is fit for the profile.</returns>
            private delegate bool ProfileTest(Dictionary<Profiles, int> pointsDetails);
            /// <summary>
            /// Calculates profile points for a process of a specific profile.
            /// </summary>
            /// <param name="testsDetails">Test details</param>
            /// <returns></returns>
            private delegate int ProfilePoints(Dictionary<Tests, int> testsDetails);
            private static Dictionary<Profiles, ProfileTest> ProfileTestsSorters = new Dictionary<Profiles, ProfileTest>
            {
                {Profiles.Malware, MalwareTest },
                {Profiles.Innocent, InnocentTest },
                {Profiles.Virus, VirusTest }
            };
            private static Dictionary<Profiles, ProfilePoints> ProfilesPointsSorters = new Dictionary<Profiles, ProfilePoints>
            {
                {Profiles.Malware, MalwarePoints },
                {Profiles.Innocent, InnocentPoints },
                {Profiles.Virus, VirusPoints }
            };

            /// <summary>
            /// Creates a new MalwareDetector instance.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="pid"></param>
            public MalwareDetector(DataInterface db, int pid)
            {
                this.db = db;
                this.Pid = pid;
                this.PDProvider = new ProcessDataProvider(db, pid);
            }

            /// <summary>
            /// Creates a new MalwareDetector instance.
            /// </summary>
            /// <param name="calls"></param>
            /// <param name="pid"></param>
            public MalwareDetector(APICall[] calls, int pid)
            {
                this.Pid = pid;
                this.PDProvider = new ProcessDataProvider(calls, pid);
            }

            /// <summary>
            /// Initiates detection.
            /// </summary>
            public void Detect()
            {
                // Get all tests
                LightFunction[] tests = this.PDProvider.GetTests();
                // Execute tests and get results.
                Dictionary<int, bool> results = this.ExecuteTests(tests);
                
                // Get messages of passed tests.
                string[] passedTests = (from testId in results.Keys
                                        where results[testId]
                                        select ((Tests)testId).ToString()).ToArray();
                string passedMessage = string.Format("Process {0} passed Tests: {1}",
                    AppResources.CurrentResources.db.GetProcessNameByPid(Pid),
                    JsonConvert.SerializeObject(passedTests));
                // Debug 
                MDebug.WriteLine("Tested: " + results.Keys.ToInfoString());
                MDebug.WriteLine(passedMessage);
                // Signal event for Event View
                new SystemMessage(passedMessage).Signal(AppResources.CurrentResources.db);
                // Report other data.
                this.ReportTestsResults(results);
            }

            /// <summary>
            /// Executes all tests for the process.
            /// </summary>
            /// <param name="tests"></param>
            /// <returns></returns>
            public Dictionary<int, bool> ExecuteTests(LightFunction[] tests)
            {
                Dictionary<int, bool> testsResults = new Dictionary<int, bool>();
                TestContext context = this.PDProvider.GetContext();
                foreach (LightFunction test in tests)
                {
                    testsResults[test.Id] = MalwareDetector.ExecuteTest(test,context);
                }
                return testsResults;
            }

            /// <summary>
            /// Reports the tests results in the database.
            /// </summary>
            /// <param name="testsResults"></param>
            public void ReportTestsResults(Dictionary<int, bool> testsResults)
            {
                foreach (int testId in testsResults.Keys )
                {
                    if (testsResults[testId])
                    {
                        new TestPassed(this.db.GetProcessMid(this.Pid), testId).Signal(this.db);
                    }
                }
            }

            /// <summary>
            /// Executes a test and returns its result.
            /// </summary>
            /// <param name="test"></param>
            /// <param name="context"></param>
            /// <returns></returns>
            public static bool ExecuteTest(Function<object> test, TestContext context)
            {
                return (bool)test.Execute(context.ContextToken);
            }

            /// <summary>
            /// Convert tests Details dictionary keys from int to Tests.
            /// </summary>
            /// <param name="testsDetails"></param>
            /// <returns></returns>
            public static Dictionary<Tests, int> ConvertTestsDetailsDict(Dictionary<int, int> testsDetails)
            {
                Dictionary<Tests, int> newDetails = new Dictionary<Tests, int>();
                foreach (int testId in testsDetails.Keys)
                    newDetails[(Tests)testId] = testsDetails[testId];
                return newDetails;
            }

            /// <summary>
            /// Main action for class, provides the detection service.
            /// </summary>
            /// <param name="pid">Process' PID to be tested.</param>
            public static void DetectTests(int pid)
            {
                new MalwareDetector(AppResources.CurrentResources.db, pid).Detect();
            }

            /// <summary>
            /// Main action for class, provides the detection service.
            /// </summary>
            /// <param name="pid">Process' PID to be tested</param>
            /// <param name="calls">Process' calls.</param>
            public static void DetectTests(int pid, APICall[] calls)
            {
                new MalwareDetector(calls, pid).Detect();
            }

            /// <summary>
            /// Detects if a process' profile has changed, and stores the new profile in
            /// the database if so.
            /// </summary>
            /// <param name="mid">Process' MID to be tested</param>
            /// <returns>Whether the profile has changed.</returns>
            public static bool DetectProfile(int mid)   
            {
                bool didChange = false;

                // Calculate Tests Details
                Dictionary<Profiles, bool> profilesTestsResults = new Dictionary<Profiles, bool>();
                Profiles[] optionalProfiles = ProcessDataProvider.GetOptionalProfiles(mid);
                Dictionary<Tests, int> testsDetails = ConvertTestsDetailsDict(ProcessDataProvider.GetProcessTestDetails(mid));

                // Calculate Points
                Dictionary<Profiles, int> pointsDetails = optionalProfiles
                    .ToDictionary(profile => profile,profile => ProfilesPointsSorters[profile](testsDetails));
                MDebug.WriteLine("Points: "+ pointsDetails.ToInfoString());

                // Calculate Profile
                foreach (Profiles optionalProfile in optionalProfiles)
                {
                    bool result =  ProfileTestsSorters[optionalProfile](pointsDetails);
                    if (result)
                    {
                        // Debug
                        MDebug.WriteLine(string.Format("Process {0} passed profile {1}", mid, optionalProfile));                        
                        int currentProfile = AppResources.CurrentResources.db.GetProcessProfileByMid(mid);
                        if (currentProfile != (int)optionalProfile)
                            didChange = true;                        
                       
                        AppResources.CurrentResources.db.SetProcessProfile(mid, (int)optionalProfile);
                        break;
                    }
                    else
                        MDebug.WriteLine(string.Format("Process {0} didnt pass profile {1}", mid, optionalProfile));
                    profilesTestsResults[optionalProfile] = result;
                }

                return didChange;
            }

            /// <summary>
            /// ProfileTest for Malware profile.
            /// </summary>
            /// <param name="pointsDetails"></param>
            /// <returns></returns>
            public static bool MalwareTest(Dictionary<Profiles, int> pointsDetails)
            {
                return pointsDetails[Profiles.Malware] + pointsDetails[Profiles.Virus] > 10 &&
                    pointsDetails[Profiles.Innocent] < 4 ;
            }

            /// <summary>
            /// ProfileTest for Virus profile.
            /// </summary>
            /// <param name="pointsDetails"></param>
            /// <returns></returns>
            public static bool VirusTest(Dictionary<Profiles, int> pointsDetails)
            {
                return pointsDetails[Profiles.Virus] >= 6 && pointsDetails[Profiles.Innocent] < 4;
            }

            /// <summary>
            /// ProfileTest for Innocent profile.
            /// </summary>
            /// <param name="pointsDetails"></param>
            /// <returns></returns>
            public static bool InnocentTest(Dictionary<Profiles, int> pointsDetails)
            {
                return pointsDetails[Profiles.Malware] + pointsDetails[Profiles.Virus] < 3 && pointsDetails[Profiles.Innocent] > 6;
            }
            
            /// <summary>
            /// ProfilePoints for Malware profile.
            /// </summary>
            /// <param name="testsDetails"></param>
            /// <returns></returns>
            public static int MalwarePoints(Dictionary<Tests, int> testsDetails)
            {
                int points = 0;

                if (testsDetails[Tests.HostsFile] >= 1)
                    points += 2;
                if (testsDetails[Tests.ForbiddenRegKeys] >= 2)
                    points += 2;
                if (testsDetails[Tests.ForbiddenDomains] >= 2)
                    points += 4;
                if (testsDetails[Tests.ForbiddenFiles] >= 2)
                    points += 2;
                if (testsDetails[Tests.ProcessSetMalware] >= 4)
                    points += 6;
                if (testsDetails[Tests.UserAwareness] >= 3)
                    points += 6;
                if (testsDetails[Tests.ProcessKilled] >= 3)
                    points += 3;
                if (testsDetails[Tests.DeleteFilesTest] >= 3)
                    points += 3;

                return points;
            }

            /// <summary>
            /// ProfilePoints for virus profile.
            /// </summary>
            /// <param name="testsDetails"></param>
            /// <returns></returns>
            public static int VirusPoints(Dictionary<Tests, int> testsDetails)
            {
                int points = 0;
                                
                if (testsDetails[Tests.BootWriteTest] >= 3)
                    points += 3;
                if (testsDetails[Tests.ExeDistTest] >= 2)
                    points += 3;
                if (testsDetails[Tests.ExeAmountTest] >= 4)
                    points += 3;
                return points;
            }

            /// <summary>
            /// ProfilePoints for Innocent profile.
            /// </summary>
            /// <param name="testsDetails"></param>
            /// <returns></returns>
            public static int InnocentPoints(Dictionary<Tests, int> testsDetails)
            {
                int points = 0;

                if (testsDetails[Tests.ProcessSetInnocent] >= 4)
                    points += 5;
                if (testsDetails[Tests.UserAwareness] >= 4)
                    points += 2;
                return points;
            }

            /// <summary>
            /// Adds a test to the counters in the database.
            /// </summary>
            /// <param name="type"></param>
            public static void NewTest(Tests type)
            {
                AppResources.CurrentResources.db.AddTestToCounters((int)type);
            }
        }

        /// <summary>
        /// Enum for representing profiles.
        /// </summary>
        public enum Profiles
        {
            /// <summary>
            /// Innocent profile.
            /// </summary>
            Innocent = 0,
            /// <summary>
            /// Suspect profile
            /// </summary>
            Suspect = 1,
            /// <summary>
            /// Malware profile
            /// </summary>
            Malware = 2,
            /// <summary>
            /// Virus profile
            /// </summary>
            Virus = 3
        }

        /// <summary>
        /// Extension methods for Profiles and Tests enums.
        /// </summary>
        public static class ProfilesExtensions
        {
            /// <summary>
            /// Gets human message for a given profile.
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            public static string GetMsg(this Profiles p)
            {
                switch (p)
                {
                    case Profiles.Innocent:
                        return "Innocent";
                    case Profiles.Suspect:
                        return "Suspect";
                    case Profiles.Malware:
                        return "Malware";
                    case Profiles.Virus:
                        return "Virus";
                    default:
                        return "General Profile";
                }
            }
            /// <summary>
            /// Gets human message for a given test.
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            public static string GetMsg(this Tests t)
            {
                switch (t)
                {
                    case Tests.HostsFile:
                        return "Hosts File Test";
                    case Tests.UserAwareness:
                        return "User Awareness Test";
                    default:
                        return "General Test";
                }
            }
        }

        
        /// <summary>
        /// Representing ProcessSurvey results.
        /// </summary>
        public struct ProcessSurvey
        {
            public ProcessData SubjectProcess;
            public bool UserProcessAwareness;
            /// <summary>
            /// Creates a new ProcessSurvey instance.
            /// </summary>
            /// <param name="subjectProcess"></param>
            /// <param name="userAwareness"></param>
            public ProcessSurvey(ProcessData subjectProcess, bool userAwareness)
            {
                this.SubjectProcess = subjectProcess;
                this.UserProcessAwareness = userAwareness;
            }            

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this).ToNeatJson() ;
            }
        }

        /// <summary>
        /// Class for representing a profile 
        /// </summary>
        public class Profile
        {
            public Profiles Id;
            private int[] MonitorsIds;
            private int[] TestsIds;
            public LightFunction[] PTests;
            public int[] OptionalProfilesIds;
            public int[] InheritedProfiles;

            public Profile(Profiles id = 0, int[] monitorsIds = null,int[] testsIds = null,
                int[] optionalProfilesIds=null, int[] inheritedProfiles=null)
            {
                this.Id = id;
                this.MonitorsIds = monitorsIds;
                this.TestsIds = testsIds;
                this.OptionalProfilesIds = optionalProfilesIds;
                this.InheritedProfiles = inheritedProfiles;
            }

            /// <summary>
            /// Gets data object for profile.
            /// </summary>
            /// <returns></returns>
            public ProfileData GetDataStruct()
            {
                return new ProfileData()
                {
                    Id = (int)this.Id,
                    MonitorsIds = JsonConvert.SerializeObject(this.MonitorsIds),
                    TestsIds = JsonConvert.SerializeObject(this.TestsIds),
                    OptionalProfilesIds = JsonConvert.SerializeObject(this.OptionalProfilesIds),
                    InheritedProfiles = JsonConvert.SerializeObject(this.InheritedProfiles)
                };
            }

            /// <summary>
            /// Gets a profile object for data.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static Profile GetObjectFromData(ProfileData data)
            {
                //MDebug.WriteLine(data.MonitorsIds + data.TestsIds);
                return new Profile()
                {
                    Id = (Profiles)data.Id,
                    MonitorsIds = JsonConvert.DeserializeObject<int[]>(data.MonitorsIds),
                    TestsIds = JsonConvert.DeserializeObject<int[]>(data.TestsIds),
                    OptionalProfilesIds = JsonConvert.DeserializeObject<int[]>(data.OptionalProfilesIds),
                    InheritedProfiles = JsonConvert.DeserializeObject<int[]>(data.InheritedProfiles)
                };
            }

            /// <summary>
            /// Gets monitors ids for profile.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="p">Profile</param>
            /// <returns></returns>
            public static int[] GetMonitorsForProfile(DataInterface db, Profiles p)
            {
                return Profile.GetObjectFromData(db.GetProfileMonitorsByProfileId((int)p)).MonitorsIds;
            }

            /// <summary>
            /// Gets tests ids for profile.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="p"></param>
            /// <returns></returns>
            public static int[] GetTestsIdsForProfile(DataInterface db, Profiles p)
            {
                return Profile.GetObjectFromData(db.GetProfileTestsByProfileId((int)p)).TestsIds;
            }

            /// <summary>
            /// Gets optional profiles for profile.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="p"></param>
            /// <returns></returns>
            public static int[] GetOptionalProfileForProfile(DataInterface db, Profiles p)
            {
                return Profile.GetObjectFromData(db.GetOptionalProfiles((int)p)).OptionalProfilesIds;
            }


            public static int[] GetInheritedProfiles(DataInterface db, Profiles p)
            {
                return Profile.GetObjectFromData(db.GetInheritedProfiles((int)p)).InheritedProfiles;
            }

            /// <summary>
            /// Gets all tests for a profile.
            /// </summary>
            /// <param name="db"></param>
            /// <param name="p"></param>
            /// <returns></returns>
            public static LightFunction[] GetTestsForProfile(DataInterface db, Profiles p)
            {
                int[] testsIds = Profile.GetTestsIdsForProfile(db, p);
                LightFunction[] tests = (from testId in testsIds select ConfigureData.GetTestById(testId)).ToArray();
                return tests;
            }
        }

        /// <summary>
        /// Enum for representing tests.
        /// </summary>
        public enum Tests
        {
            UserAwareness = 1,
            HostsFile = 2,          // CALL TEST
            TrueTest = 3,
            ProcessKilled = 4,
            ProcessSetInnocent = 5,
            ProcessSetMalware = 6,  
            ForbiddenFiles = 7,         // CALL TEST
            ForbiddenRegKeys = 8,       // CALL TEST
            ForbiddenDomains = 9,         // CALL TEST
            DeleteFilesTest = 10,       // CALL TEST
            ExeDistTest = 11,           // CALL TEST
            ExeAmountTest = 13,         // CALL TEST
            BootWriteTest = 12,         // CALL TEST
            RunFilesTest = 14,          // CALL TEST
        }

        namespace MalwareBehavioralDetection
        {
            /// <summary>
            /// Receives a function representing the test's rules.
            /// </summary>
            public class CallTest : LightFunction
            {
                protected override Logic logicFunction { get { return new Logic(CallTestLogic); } }

                public static object CallTestLogic(int contextToken, params object[] fparams)
                {
                    Debug.Assert(fparams.Length >= 1, "CallTest receives atleast 1 parameters.");
                    Debug.Assert(fparams[0] is LightFunction, "CallTest receives a function");
                    return ((LightFunction)fparams[0]).Execute(contextToken);
                }

                public CallTest(params object[] fparams) : base(fparams)
                {
                }
            }

            /// <summary>
            /// Receives a CallTest and a number, and returns whether there are at least
            /// *number* calls that pass the CallTest.
            /// </summary>
            public class OccuranceTest : LightFunction
            {
                protected override Logic logicFunction { get { return OccuranceTestLogic; } }

                public static object OccuranceTestLogic(int contextToken, params object[] fparams)
                {
                    CallTest ctest = (CallTest)fparams[0];
                    int requiredAmount = (int)fparams[1];
                    int actualAmount = 0;
                    TestContext context = TestContext.GetContextCopy(contextToken);
                    int calls=0; // used for debugging

                    while (!context.GetIsDone())
                    {
                        // If passed increase counter.
                        if ((bool)ctest.Execute(context.ContextToken))
                        {
                            actualAmount++;
                        }
                            
                        // Go on
                        context.NextCall();                        
                        calls++;

                        // if there are enough then exit.
                        if (actualAmount >= requiredAmount)
                            return true;
                    }
                    return false;

                }

                public OccuranceTest(params object[] fparams) : base(fparams)
                {
                }
            }

            /// <summary>
            /// Checks if a sequence of tests can be found in a sequence of calls.
            /// </summary>
            public class SequenecTest : LightFunction
            {
                protected override Logic logicFunction { get { return new Logic(SequenceTestLogic); } }

                public static object SequenceTestLogic(int contextToken, params object[] fparams)
                {
                    CallTest[] ctests = (from ctest in fparams select (CallTest)ctest).ToArray();
                    TestContext context = TestContext.GetContextCopy(contextToken);
                    for (int i = 0; i < ctests.Length; i++)
                    {
                        if (context.GetIsDone())
                            return false;

                        bool match;
                        do
                        {
                            match = (bool)ctests[i].Execute(context.ContextToken);
                            context.NextCall();
                        } while (!match);
                    }
                    return true;
                }
            }

            /// <summary>
            /// Counts the instances of every exe file in the sequence of calls in the context,
            /// and retuns a dictionary containing the results.
            /// i.e : 
            /// [CreateFile("a.exe"), OpenFile("a.exe"), DeleteFile("b.exe")] 
            /// ==> {{"a.exe", 2}, {"b.txt", 1}}
            /// </summary>
            public class ExeFilesDist : LightFunction
            {
                protected override Logic logicFunction { get { return FilesDistLogic; } }

                public static object FilesDistLogic(int contextToken, object[] fparams)
                {
                    Dictionary<string, int> filesDistDict = new Dictionary<string, int>();
                    TestContext context = TestContext.GetContextCopy(contextToken);

                    while (!context.GetIsDone())
                    {
                        string callName = (string) new GetProperty(APICall.NAME).Execute(context.ContextToken);
                        if (callName == CreateFileW.CallDllFunction.FunctionName ||
                            callName == OpenFile.CallDllFunction.FunctionName ||
                            callName == ShellExecuteA.CallDllFunction.FunctionName)
                        {
                            string fileName = (string)new GetProperty(APICall.FILE_NAME).Execute(context.ContextToken);
                            //MDebug.WriteLine(fileName + " " + fileName.Contains(".exe").ToString());
                            if (fileName.Contains(".exe"))
                            {                                
                                if (filesDistDict.ContainsKey(fileName))
                                    filesDistDict[fileName]++;
                                else
                                    filesDistDict[fileName] = 1;

                            }
                        }                        
                        context.NextCall();
                    }
                    MDebug.WriteLine(filesDistDict.ToInfoString());
                    return filesDistDict;

                }

                public ExeFilesDist(params object[] fparams) : base(fparams)
                {
                }
            }

            /// <summary>
            /// Receives a dictionary of files instances counters and returns the 
            /// biggest counter's value.
            /// i.e: {{"a.exe", 2}, {"b.txt", 1}} ==> 2
            /// </summary>
            public class MaxFileDist : LightFunction
            {
                protected override Logic logicFunction
                {
                    get
                    {
                        return MaxFileDistLogic;
                    }
                }

                public static object MaxFileDistLogic(int contextToken, object[] fparams)
                {
                    fparams = TestContext.GetContext(contextToken).EvalParams(fparams);
                    Dictionary<string, int> filesDistDict = (Dictionary<string, int>)fparams[0];
                    string maxFileName = "";
                    int maxFileAmount = 0;
                    foreach (string fileName in filesDistDict.Keys)
                    {
                        if (filesDistDict[fileName] > maxFileAmount)
                        {
                            maxFileAmount = filesDistDict[fileName];
                            maxFileName = fileName;
                        }
                    }
                    return maxFileAmount;
                }

                public MaxFileDist(params object[] fparams) : base(fparams)
                {
                }
            }            
        }
    }
}
