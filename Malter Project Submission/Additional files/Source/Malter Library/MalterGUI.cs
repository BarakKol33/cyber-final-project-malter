﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalterLib
{
    namespace MalterGUI
    {
        using GeneralOS;
        using MainMalter;
        using MalterObjects;
        using System.Threading;
        using static ListView;

        /// <summary>
        /// Class for main display of Malter.
        /// </summary>
        public class MalterForm : Form
        {
            private Panel WelcomePanel;
            private TabControl MainTabControl;
            private TabPage ProcessViewTab;
            private TabPage SecurityInfoTab;
            private TabPage DUViewTab;
            private TabPage EventViewTab;
            private ListView EventsList;
            private ListView ProcessList;            
            private Button WelcomeButton;
            private Button SignUpUser;
            private Button UserSignInButton;            
            private Button SignUpDevice;
            private Button ArmoryHelpButton;
            private Button RemoveProcessButton;
            private Button ProcessSurveyButton;
            private Button KillProcessButton;
            private Button SetMalwareButton;
            private Button SetSuspectButton;
            private Button SetInnocentButton;
            private Button RefreshDataButton;
            private Button ExitButton;
            private Label WelcomeMessage;
            private Label LoggedUserData;
            private Label LoggedDeviceData;
            private Label ErrorMsgLabel;
            private Label SecurityInfoData;        
            private Label SecurityInfoHeader;                        
            private Label UserPrivateNameData;
            private Label DeviceNameHeader;
            private Label UserNameHeader;
            private Label UserPrivateNameHeader;
            private Label DeviceNameData;
            private Label CurrentDeviceHeader;
            private Label UserEmailHeader;
            private Label UserEmailData;
            private Label UserNameData;
            private Label CurrentUserHeader;
                                    
            
            private ProcessData[] DisplayedProcesses;
            public bool IsDone = false;
            public MalterFormDetails FormDetails;
            /// <summary>
            /// Regular font for form.
            /// </summary>
            public static Font RegularFont = new Font("Ariel", 11, FontStyle.Regular);
            /// <summary>
            /// Font for buttons in form.
            /// </summary>
            public static Font ButtonsFont = new Font("Ariel", 11, FontStyle.Regular);
            /// <summary>
            /// Font for medium-sized headlines in form.
            /// </summary>
            public static Font MediumHeadlineFont = new Font("Ariel", 18, FontStyle.Bold);
            /// <summary>
            /// Font for main headlines in form.
            /// </summary>
            public static Font PrimeHeadlineFont = new Font("Ariel", 25, FontStyle.Bold);

            /// <summary>
            /// Main color in form.
            /// </summary>
            public static Color MalterColor1 = Color.DarkGreen;            
            /// <summary>
            /// Secondary color in form.
            /// </summary>
            public static Color MalterColor2 = Color.White;
            /// <summary>
            /// Form icon.
            /// </summary>
            public static Icon MalterIcon = 
                Icon.ExtractAssociatedIcon(MalterData.MalterDBDetails.DBFiles.ICON);
            private static string PROFILE_CHANGE_MSG =
                "Process with MID {0} Was Identified as {1}." +
                "If you want to know more details or take action against it, you are welcome to go to the " +
                "ProcessView in the Malter app. There you can also improve our knowledge about this process";

            private static string ARMORY_HELP_MESSAGE =
                "This is the Armory. Here you can take different actions against processes.\n" +
                "'Kill Process' - This kills the running process.\n"+ 
                "'Set ProcessInnocent' - This tells the system to ignore the process from now on.\n"+ 
                "'Set Process Suspect' - This tells the system to monitor the process for malicious behavior.\n"+
                "'Set Process Malware' - This tells the system to prevent the process from doing any changes to your computer.\n"+
                "'Do Process Survey' - This lets you fill details about a process that help us analyze it.\n"+
                "'Remove Process' - Totally deletes the process from your computer";

            [STAThread]
            public static void StartForm(MalterForm form)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(form);
            }

            public MalterForm(MalterFormDetails formDetails)
            {
                this.FormDetails = formDetails;
                InitializeComponent();
                this.ModifyForm();
            }

            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }                       
            
            /// <summary>
            /// Updates the data displayed.
            /// </summary>
            public void UpdateGUI()
            {                
                ClearGUI();
                FillGUI();
            }

            /// <summary>
            /// Updates the data displayed, on a new thread.
            /// </summary>
            private void SmartUpdateGUI()
            {
                new Thread(UpdateGUI).Start();
            }

            /// <summary>
            /// Displays a message stating a process' profile has changed.
            /// </summary>
            /// <param name="pid">The process' PID.</param>
            /// <param name="profile">The process' new profile.</param>
            public static void DisplayProfileChangeMessage(int mid, string profile)
            {
                System.Windows.Forms.MessageBox.Show(string.Format(PROFILE_CHANGE_MSG, mid, profile), "Malter", System.Windows.Forms.MessageBoxButtons.OK);
            }

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.WelcomePanel = new System.Windows.Forms.Panel();
            this.ErrorMsgLabel = new System.Windows.Forms.Label();
            this.SignUpDevice = new System.Windows.Forms.Button();
            this.LoggedUserData = new System.Windows.Forms.Label();
            this.LoggedDeviceData = new System.Windows.Forms.Label();
            this.SignUpUser = new System.Windows.Forms.Button();
            this.UserSignInButton = new System.Windows.Forms.Button();
            this.WelcomeMessage = new System.Windows.Forms.Label();
            this.WelcomeButton = new System.Windows.Forms.Button();
            this.SecurityInfoTab = new System.Windows.Forms.TabPage();
            this.SecurityInfoData = new System.Windows.Forms.Label();
            this.SecurityInfoHeader = new System.Windows.Forms.Label();
            this.DUViewTab = new System.Windows.Forms.TabPage();
            this.RefreshDataButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.UserPrivateNameData = new System.Windows.Forms.Label();
            this.DeviceNameHeader = new System.Windows.Forms.Label();
            this.UserNameHeader = new System.Windows.Forms.Label();
            this.UserPrivateNameHeader = new System.Windows.Forms.Label();
            this.DeviceNameData = new System.Windows.Forms.Label();
            this.CurrentDeviceHeader = new System.Windows.Forms.Label();
            this.UserEmailHeader = new System.Windows.Forms.Label();
            this.UserEmailData = new System.Windows.Forms.Label();
            this.UserNameData = new System.Windows.Forms.Label();
            this.CurrentUserHeader = new System.Windows.Forms.Label();
            this.EventViewTab = new System.Windows.Forms.TabPage();
            this.EventsList = new System.Windows.Forms.ListView();
            this.ProcessViewTab = new System.Windows.Forms.TabPage();
            this.ArmoryHelpButton = new System.Windows.Forms.Button();
            this.RemoveProcessButton = new System.Windows.Forms.Button();
            this.ProcessSurveyButton = new System.Windows.Forms.Button();
            this.KillProcessButton = new System.Windows.Forms.Button();
            this.SetMalwareButton = new System.Windows.Forms.Button();
            this.SetSuspectButton = new System.Windows.Forms.Button();
            this.SetInnocentButton = new System.Windows.Forms.Button();
            this.ProcessList = new System.Windows.Forms.ListView();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.WelcomePanel.SuspendLayout();
            this.SecurityInfoTab.SuspendLayout();
            this.DUViewTab.SuspendLayout();
            this.EventViewTab.SuspendLayout();
            this.ProcessViewTab.SuspendLayout();
            this.MainTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // WelcomePanel
            // 
            this.WelcomePanel.Controls.Add(this.ErrorMsgLabel);
            this.WelcomePanel.Controls.Add(this.SignUpDevice);
            this.WelcomePanel.Controls.Add(this.LoggedUserData);
            this.WelcomePanel.Controls.Add(this.LoggedDeviceData);
            this.WelcomePanel.Controls.Add(this.SignUpUser);
            this.WelcomePanel.Controls.Add(this.UserSignInButton);
            this.WelcomePanel.Controls.Add(this.WelcomeMessage);
            this.WelcomePanel.Controls.Add(this.WelcomeButton);
            this.WelcomePanel.Location = new System.Drawing.Point(0, 0);
            this.WelcomePanel.Margin = new System.Windows.Forms.Padding(2);
            this.WelcomePanel.Name = "WelcomePanel";
            this.WelcomePanel.Size = new System.Drawing.Size(1043, 661);
            this.WelcomePanel.TabIndex = 0;
            // 
            // ErrorMsgLabel
            // 
            this.ErrorMsgLabel.AutoSize = true;
            this.ErrorMsgLabel.Location = new System.Drawing.Point(630, 326);
            this.ErrorMsgLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ErrorMsgLabel.Name = "ErrorMsgLabel";
            this.ErrorMsgLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorMsgLabel.TabIndex = 7;
            // 
            // SignUpDevice
            // 
            this.SignUpDevice.Location = new System.Drawing.Point(395, 365);
            this.SignUpDevice.Margin = new System.Windows.Forms.Padding(2);
            this.SignUpDevice.Name = "SignUpDevice";
            this.SignUpDevice.Size = new System.Drawing.Size(203, 35);
            this.SignUpDevice.TabIndex = 6;
            this.SignUpDevice.Text = "Sign Up Device";
            this.SignUpDevice.UseVisualStyleBackColor = true;
            this.SignUpDevice.Click += new System.EventHandler(this.SignUpDevice_Click);
            // 
            // LoggedUserData
            // 
            this.LoggedUserData.AutoSize = true;
            this.LoggedUserData.Location = new System.Drawing.Point(189, 326);
            this.LoggedUserData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LoggedUserData.Name = "LoggedUserData";
            this.LoggedUserData.Size = new System.Drawing.Size(53, 13);
            this.LoggedUserData.TabIndex = 5;
            this.LoggedUserData.Text = "<Device>";
            // 
            // LoggedDeviceData
            // 
            this.LoggedDeviceData.AutoSize = true;
            this.LoggedDeviceData.Location = new System.Drawing.Point(364, 326);
            this.LoggedDeviceData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LoggedDeviceData.Name = "LoggedDeviceData";
            this.LoggedDeviceData.Size = new System.Drawing.Size(41, 13);
            this.LoggedDeviceData.TabIndex = 4;
            this.LoggedDeviceData.Text = "<User>";
            this.LoggedDeviceData.Click += new System.EventHandler(this.LoggedDeviceData_Click);
            // 
            // SignUpUser
            // 
            this.SignUpUser.Location = new System.Drawing.Point(611, 365);
            this.SignUpUser.Margin = new System.Windows.Forms.Padding(2);
            this.SignUpUser.Name = "SignUpUser";
            this.SignUpUser.Size = new System.Drawing.Size(203, 35);
            this.SignUpUser.TabIndex = 3;
            this.SignUpUser.Text = "Sign Up User";
            this.SignUpUser.UseVisualStyleBackColor = true;
            this.SignUpUser.Click += new System.EventHandler(this.SignUpUser_Click);
            // 
            // UserSignInButton
            // 
            this.UserSignInButton.Location = new System.Drawing.Point(168, 365);
            this.UserSignInButton.Margin = new System.Windows.Forms.Padding(2);
            this.UserSignInButton.Name = "UserSignInButton";
            this.UserSignInButton.Size = new System.Drawing.Size(203, 35);
            this.UserSignInButton.TabIndex = 2;
            this.UserSignInButton.Text = "User Sign In";
            this.UserSignInButton.UseVisualStyleBackColor = true;
            this.UserSignInButton.Click += new System.EventHandler(this.SignIn_Click);
            // 
            // WelcomeMessage
            // 
            this.WelcomeMessage.AutoSize = true;
            this.WelcomeMessage.Location = new System.Drawing.Point(306, 178);
            this.WelcomeMessage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.WelcomeMessage.Name = "WelcomeMessage";
            this.WelcomeMessage.Size = new System.Drawing.Size(99, 13);
            this.WelcomeMessage.TabIndex = 1;
            this.WelcomeMessage.Text = "Welcome to Malter!";
            // 
            // WelcomeButton
            // 
            this.WelcomeButton.Location = new System.Drawing.Point(169, 256);
            this.WelcomeButton.Margin = new System.Windows.Forms.Padding(2);
            this.WelcomeButton.Name = "WelcomeButton";
            this.WelcomeButton.Size = new System.Drawing.Size(645, 53);
            this.WelcomeButton.TabIndex = 0;
            this.WelcomeButton.Text = "Start Malter";
            this.WelcomeButton.UseVisualStyleBackColor = true;
            this.WelcomeButton.Click += new System.EventHandler(this.Welcome_Click);
            // 
            // SecurityInfoTab
            // 
            this.SecurityInfoTab.Controls.Add(this.SecurityInfoData);
            this.SecurityInfoTab.Controls.Add(this.SecurityInfoHeader);
            this.SecurityInfoTab.Location = new System.Drawing.Point(4, 22);
            this.SecurityInfoTab.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.SecurityInfoTab.Name = "SecurityInfoTab";
            this.SecurityInfoTab.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.SecurityInfoTab.Size = new System.Drawing.Size(3242, 836);
            this.SecurityInfoTab.TabIndex = 5;
            this.SecurityInfoTab.Text = "Security Information";
            this.SecurityInfoTab.UseVisualStyleBackColor = true;
            // 
            // SecurityInfoData
            // 
            this.SecurityInfoData.AutoSize = true;
            this.SecurityInfoData.Location = new System.Drawing.Point(89, 98);
            this.SecurityInfoData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.SecurityInfoData.Name = "SecurityInfoData";
            this.SecurityInfoData.Size = new System.Drawing.Size(98, 13);
            this.SecurityInfoData.TabIndex = 1;
            this.SecurityInfoData.Text = "<SecurityInfoData>";
            // 
            // SecurityInfoHeader
            // 
            this.SecurityInfoHeader.AutoSize = true;
            this.SecurityInfoHeader.Location = new System.Drawing.Point(89, 32);
            this.SecurityInfoHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.SecurityInfoHeader.Name = "SecurityInfoHeader";
            this.SecurityInfoHeader.Size = new System.Drawing.Size(100, 13);
            this.SecurityInfoHeader.TabIndex = 0;
            this.SecurityInfoHeader.Text = "Security Information";
            // 
            // DUViewTab
            // 
            this.DUViewTab.Controls.Add(this.RefreshDataButton);
            this.DUViewTab.Controls.Add(this.ExitButton);
            this.DUViewTab.Controls.Add(this.UserPrivateNameData);
            this.DUViewTab.Controls.Add(this.DeviceNameHeader);
            this.DUViewTab.Controls.Add(this.UserNameHeader);
            this.DUViewTab.Controls.Add(this.UserPrivateNameHeader);
            this.DUViewTab.Controls.Add(this.DeviceNameData);
            this.DUViewTab.Controls.Add(this.CurrentDeviceHeader);
            this.DUViewTab.Controls.Add(this.UserEmailHeader);
            this.DUViewTab.Controls.Add(this.UserEmailData);
            this.DUViewTab.Controls.Add(this.UserNameData);
            this.DUViewTab.Controls.Add(this.CurrentUserHeader);
            this.DUViewTab.Location = new System.Drawing.Point(4, 22);
            this.DUViewTab.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.DUViewTab.Name = "DUViewTab";
            this.DUViewTab.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.DUViewTab.Size = new System.Drawing.Size(3242, 836);
            this.DUViewTab.TabIndex = 4;
            this.DUViewTab.Text = "Device and User View";
            this.DUViewTab.UseVisualStyleBackColor = true;
            // 
            // RefreshDataButton
            // 
            this.RefreshDataButton.Location = new System.Drawing.Point(281, 294);
            this.RefreshDataButton.Name = "RefreshDataButton";
            this.RefreshDataButton.Size = new System.Drawing.Size(174, 34);
            this.RefreshDataButton.TabIndex = 13;
            this.RefreshDataButton.Text = "Refresh Displayed Data";
            this.RefreshDataButton.UseVisualStyleBackColor = true;
            this.RefreshDataButton.Click += new System.EventHandler(this.RefreshDataButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(73, 292);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(174, 36);
            this.ExitButton.TabIndex = 12;
            this.ExitButton.Text = "exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // UserPrivateNameData
            // 
            this.UserPrivateNameData.AutoSize = true;
            this.UserPrivateNameData.Location = new System.Drawing.Point(283, 204);
            this.UserPrivateNameData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserPrivateNameData.Name = "UserPrivateNameData";
            this.UserPrivateNameData.Size = new System.Drawing.Size(125, 13);
            this.UserPrivateNameData.TabIndex = 11;
            this.UserPrivateNameData.Text = "<UserPrivateNameData>";
            // 
            // DeviceNameHeader
            // 
            this.DeviceNameHeader.AutoSize = true;
            this.DeviceNameHeader.Location = new System.Drawing.Point(559, 94);
            this.DeviceNameHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.DeviceNameHeader.Name = "DeviceNameHeader";
            this.DeviceNameHeader.Size = new System.Drawing.Size(75, 13);
            this.DeviceNameHeader.TabIndex = 9;
            this.DeviceNameHeader.Text = "Device Name:";
            // 
            // UserNameHeader
            // 
            this.UserNameHeader.AutoSize = true;
            this.UserNameHeader.Location = new System.Drawing.Point(66, 94);
            this.UserNameHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserNameHeader.Name = "UserNameHeader";
            this.UserNameHeader.Size = new System.Drawing.Size(60, 13);
            this.UserNameHeader.TabIndex = 8;
            this.UserNameHeader.Text = "UserName:";
            // 
            // UserPrivateNameHeader
            // 
            this.UserPrivateNameHeader.AutoSize = true;
            this.UserPrivateNameHeader.Location = new System.Drawing.Point(66, 204);
            this.UserPrivateNameHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserPrivateNameHeader.Name = "UserPrivateNameHeader";
            this.UserPrivateNameHeader.Size = new System.Drawing.Size(74, 13);
            this.UserPrivateNameHeader.TabIndex = 6;
            this.UserPrivateNameHeader.Text = "Private Name:";
            // 
            // DeviceNameData
            // 
            this.DeviceNameData.AutoSize = true;
            this.DeviceNameData.Location = new System.Drawing.Point(805, 94);
            this.DeviceNameData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.DeviceNameData.Name = "DeviceNameData";
            this.DeviceNameData.Size = new System.Drawing.Size(104, 13);
            this.DeviceNameData.TabIndex = 5;
            this.DeviceNameData.Text = "<DeviceNameData>";
            // 
            // CurrentDeviceHeader
            // 
            this.CurrentDeviceHeader.AutoSize = true;
            this.CurrentDeviceHeader.Location = new System.Drawing.Point(559, 34);
            this.CurrentDeviceHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CurrentDeviceHeader.Name = "CurrentDeviceHeader";
            this.CurrentDeviceHeader.Size = new System.Drawing.Size(78, 13);
            this.CurrentDeviceHeader.TabIndex = 4;
            this.CurrentDeviceHeader.Text = "Current Device";
            // 
            // UserEmailHeader
            // 
            this.UserEmailHeader.AutoSize = true;
            this.UserEmailHeader.Location = new System.Drawing.Point(66, 148);
            this.UserEmailHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserEmailHeader.Name = "UserEmailHeader";
            this.UserEmailHeader.Size = new System.Drawing.Size(60, 13);
            this.UserEmailHeader.TabIndex = 3;
            this.UserEmailHeader.Text = "User Email:";
            // 
            // UserEmailData
            // 
            this.UserEmailData.AutoSize = true;
            this.UserEmailData.Location = new System.Drawing.Point(283, 148);
            this.UserEmailData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserEmailData.Name = "UserEmailData";
            this.UserEmailData.Size = new System.Drawing.Size(89, 13);
            this.UserEmailData.TabIndex = 2;
            this.UserEmailData.Text = "<UserEmailData>";
            // 
            // UserNameData
            // 
            this.UserNameData.AutoSize = true;
            this.UserNameData.Location = new System.Drawing.Point(278, 94);
            this.UserNameData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.UserNameData.Name = "UserNameData";
            this.UserNameData.Size = new System.Drawing.Size(92, 13);
            this.UserNameData.TabIndex = 1;
            this.UserNameData.Text = "<UserNameData>";
            // 
            // CurrentUserHeader
            // 
            this.CurrentUserHeader.AutoSize = true;
            this.CurrentUserHeader.Location = new System.Drawing.Point(66, 34);
            this.CurrentUserHeader.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CurrentUserHeader.Name = "CurrentUserHeader";
            this.CurrentUserHeader.Size = new System.Drawing.Size(66, 13);
            this.CurrentUserHeader.TabIndex = 0;
            this.CurrentUserHeader.Text = "Current User";
            // 
            // Event View
            // 
            this.EventViewTab.Controls.Add(this.EventsList);
            this.EventViewTab.Location = new System.Drawing.Point(4, 22);
            this.EventViewTab.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.EventViewTab.Name = "tabPage1";
            this.EventViewTab.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.EventViewTab.Size = new System.Drawing.Size(3242, 836);
            this.EventViewTab.TabIndex = 0;
            this.EventViewTab.Text = "Event View";
            this.EventViewTab.UseVisualStyleBackColor = true;
            // 
            // EventsList
            // 
            this.EventsList.Location = new System.Drawing.Point(5, 4);
            this.EventsList.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.EventsList.Name = "EventsList";
            this.EventsList.Size = new System.Drawing.Size(3218, 806);
            this.EventsList.TabIndex = 0;
            this.EventsList.UseCompatibleStateImageBehavior = false;
            // 
            // ProcessViewTab
            // 
            this.ProcessViewTab.Controls.Add(this.ArmoryHelpButton);
            this.ProcessViewTab.Controls.Add(this.RemoveProcessButton);
            this.ProcessViewTab.Controls.Add(this.ProcessSurveyButton);
            this.ProcessViewTab.Controls.Add(this.KillProcessButton);
            this.ProcessViewTab.Controls.Add(this.SetMalwareButton);
            this.ProcessViewTab.Controls.Add(this.SetSuspectButton);
            this.ProcessViewTab.Controls.Add(this.SetInnocentButton);
            this.ProcessViewTab.Controls.Add(this.ProcessList);
            this.ProcessViewTab.Location = new System.Drawing.Point(4, 22);
            this.ProcessViewTab.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ProcessViewTab.Name = "ProcessViewTab";
            this.ProcessViewTab.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ProcessViewTab.Size = new System.Drawing.Size(3242, 836);
            this.ProcessViewTab.TabIndex = 2;
            this.ProcessViewTab.Text = "Process View";
            this.ProcessViewTab.UseVisualStyleBackColor = true;
            this.ProcessViewTab.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // ArmoryHelpButton
            // 
            this.ArmoryHelpButton.Location = new System.Drawing.Point(844, 451);
            this.ArmoryHelpButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ArmoryHelpButton.Name = "ArmoryHelpButton";
            this.ArmoryHelpButton.Size = new System.Drawing.Size(55, 41);
            this.ArmoryHelpButton.TabIndex = 8;
            this.ArmoryHelpButton.Text = "?";
            this.ArmoryHelpButton.UseVisualStyleBackColor = true;
            this.ArmoryHelpButton.Click += new System.EventHandler(this.ArmoryHelpButton_Click);
            // 
            // RemoveProcessButton
            // 
            this.RemoveProcessButton.Location = new System.Drawing.Point(594, 477);
            this.RemoveProcessButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.RemoveProcessButton.Name = "RemoveProcessButton";
            this.RemoveProcessButton.Size = new System.Drawing.Size(240, 41);
            this.RemoveProcessButton.TabIndex = 7;
            this.RemoveProcessButton.Text = "Remove Process";
            this.RemoveProcessButton.UseVisualStyleBackColor = true;
            this.RemoveProcessButton.Click += new System.EventHandler(this.RemoveProcessButton_Click);
            // 
            // ProcessSurveyButton
            // 
            this.ProcessSurveyButton.Location = new System.Drawing.Point(5, 477);
            this.ProcessSurveyButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ProcessSurveyButton.Name = "ProcessSurveyButton";
            this.ProcessSurveyButton.Size = new System.Drawing.Size(274, 41);
            this.ProcessSurveyButton.TabIndex = 6;
            this.ProcessSurveyButton.Text = "Do Process Survey";
            this.ProcessSurveyButton.UseVisualStyleBackColor = true;
            this.ProcessSurveyButton.Click += new System.EventHandler(this.ProcessSurveyButton_Click);
            // 
            // KillProcessButton
            // 
            this.KillProcessButton.Location = new System.Drawing.Point(299, 477);
            this.KillProcessButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.KillProcessButton.Name = "KillProcessButton";
            this.KillProcessButton.Size = new System.Drawing.Size(276, 41);
            this.KillProcessButton.TabIndex = 5;
            this.KillProcessButton.Text = "Kill process";
            this.KillProcessButton.UseVisualStyleBackColor = true;
            this.KillProcessButton.Click += new System.EventHandler(this.KillProcessButton_Click);
            // 
            // SetMalwareButton
            // 
            this.SetMalwareButton.Location = new System.Drawing.Point(594, 428);
            this.SetMalwareButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.SetMalwareButton.Name = "SetMalwareButton";
            this.SetMalwareButton.Size = new System.Drawing.Size(240, 41);
            this.SetMalwareButton.TabIndex = 4;
            this.SetMalwareButton.Text = "Set Process as Malware";
            this.SetMalwareButton.UseVisualStyleBackColor = true;
            this.SetMalwareButton.Click += new System.EventHandler(this.SetMalwareButton_Click);
            // 
            // SetSuspectButton
            // 
            this.SetSuspectButton.Location = new System.Drawing.Point(299, 428);
            this.SetSuspectButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.SetSuspectButton.Name = "SetSuspectButton";
            this.SetSuspectButton.Size = new System.Drawing.Size(276, 41);
            this.SetSuspectButton.TabIndex = 3;
            this.SetSuspectButton.Text = "Set Process as Suspect";
            this.SetSuspectButton.UseVisualStyleBackColor = true;
            this.SetSuspectButton.Click += new System.EventHandler(this.SetSuspectButton_Click);
            // 
            // SetInnocentButton
            // 
            this.SetInnocentButton.Location = new System.Drawing.Point(5, 428);
            this.SetInnocentButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.SetInnocentButton.Name = "SetInnocentButton";
            this.SetInnocentButton.Size = new System.Drawing.Size(274, 41);
            this.SetInnocentButton.TabIndex = 2;
            this.SetInnocentButton.Text = "Set Process as Innocent";
            this.SetInnocentButton.UseVisualStyleBackColor = true;
            this.SetInnocentButton.Click += new System.EventHandler(this.SetInnocentButton_Click);
            // 
            // ProcessList
            // 
            this.ProcessList.Location = new System.Drawing.Point(0, 0);
            this.ProcessList.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ProcessList.Name = "ProcessList";
            this.ProcessList.Size = new System.Drawing.Size(1024, 415);
            this.ProcessList.TabIndex = 0;
            this.ProcessList.UseCompatibleStateImageBehavior = false;
            this.ProcessList.View = System.Windows.Forms.View.Details;
            this.ProcessList.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged_1);
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.ProcessViewTab);
            this.MainTabControl.Controls.Add(this.EventViewTab);
            this.MainTabControl.Controls.Add(this.DUViewTab);
            this.MainTabControl.Controls.Add(this.SecurityInfoTab);
            this.MainTabControl.Location = new System.Drawing.Point(3, -2);
            this.MainTabControl.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(3250, 862);
            this.MainTabControl.TabIndex = 0;
            // 
            // MalterForm
            // 
            this.ClientSize = new System.Drawing.Size(1049, 568);
            this.Controls.Add(this.WelcomePanel);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "MalterForm";
            this.Text = "MalterForm";
            this.Load += new System.EventHandler(this.MalterForm_Load);
            this.WelcomePanel.ResumeLayout(false);
            this.WelcomePanel.PerformLayout();
            this.SecurityInfoTab.ResumeLayout(false);
            this.SecurityInfoTab.PerformLayout();
            this.DUViewTab.ResumeLayout(false);
            this.DUViewTab.PerformLayout();
            this.EventViewTab.ResumeLayout(false);
            this.ProcessViewTab.ResumeLayout(false);
            this.MainTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

            }

            /// <summary>
            /// Modifies elements in form.
            /// </summary>
            private void ModifyForm()
            {
                this.ModifyColors();
                this.ModifyProcessView();                
                this.ModifyEventView();                
                this.ModifyOpening();
                this.DisableAllActions();
                this.ModifyFonts();
                this.Icon = MalterIcon;
            }

            /// <summary>
            /// Modifies colors in the form.
            /// </summary>
            private void ModifyColors()
            {
                this.WelcomePanel.ForeColor = MalterColor2;
                this.ProcessViewTab.ForeColor = MalterColor2;
                this.EventViewTab.ForeColor = MalterColor2;
                this.SecurityInfoTab.ForeColor = MalterColor2;
                this.DUViewTab.ForeColor = MalterColor2;
                this.ProcessList.ForeColor = MalterColor2;
                this.EventsList.ForeColor = MalterColor2;


                this.WelcomePanel.BackColor = MalterColor1;
                this.ProcessViewTab.BackColor = MalterColor1;
                this.EventViewTab.BackColor = MalterColor1;
                this.SecurityInfoTab.BackColor = MalterColor1;
                this.DUViewTab.BackColor = MalterColor1;
                this.ProcessList.BackColor = MalterColor1;
                this.EventsList.BackColor = MalterColor1;
                                
                // Modify Buttons
                this.WelcomeButton.ForeColor = MalterColor1;
                this.KillProcessButton.ForeColor = MalterColor1;
                this.SetMalwareButton.ForeColor = MalterColor1;
                this.SetSuspectButton.ForeColor = MalterColor1;
                this.SetInnocentButton.ForeColor = MalterColor1;
                this.SignUpUser.ForeColor = MalterColor1;
                this.UserSignInButton.ForeColor = MalterColor1;
                this.SignUpDevice.ForeColor = MalterColor1;
                this.ProcessSurveyButton.ForeColor = MalterColor1;
                this.ExitButton.ForeColor = MalterColor1;
                this.RemoveProcessButton.ForeColor = MalterColor1;
                this.RefreshDataButton.ForeColor = MalterColor1;
                this.ArmoryHelpButton.ForeColor = MalterColor1;              
            }

            /// <summary>
            /// Modifies fonts in the form.
            /// </summary>
            private void ModifyFonts()
            {
                // Modify labels                
                this.EventsList.Font = RegularFont;                
                this.ProcessList.Font = RegularFont;
                this.UserPrivateNameHeader.Font = RegularFont;
                this.DeviceNameData.Font = RegularFont;
                this.CurrentDeviceHeader.Font = MediumHeadlineFont;
                this.UserEmailHeader.Font = RegularFont;
                this.UserEmailData.Font = RegularFont;
                this.UserNameData.Font = RegularFont;
                this.CurrentUserHeader.Font = MediumHeadlineFont;
                this.UserPrivateNameData.Font = RegularFont;
                this.DeviceNameHeader.Font = RegularFont;
                this.UserNameHeader.Font = RegularFont;
                this.LoggedUserData.Font = RegularFont;
                this.LoggedDeviceData.Font = RegularFont;
                this.ErrorMsgLabel.Font = RegularFont;
                this.SecurityInfoHeader.Font = MediumHeadlineFont;
                this.SecurityInfoData.Font = RegularFont;

                // Modify Buttons
                this.WelcomeButton.Font = RegularFont;
                this.KillProcessButton.Font = RegularFont;
                this.SetMalwareButton.Font = RegularFont;
                this.SetSuspectButton.Font = RegularFont;
                this.SetInnocentButton.Font = RegularFont;
                this.SignUpUser.Font = RegularFont;
                this.UserSignInButton.Font = RegularFont;
                this.SignUpDevice.Font = RegularFont;
                this.ProcessSurveyButton.Font = RegularFont;
                this.ExitButton.Font = RegularFont;
                this.RemoveProcessButton.Font = RegularFont;
            }

            /// <summary>
            /// Modifies opening screen.
            /// </summary>
            private void ModifyOpening()
            {
                this.WelcomeMessage.Font = PrimeHeadlineFont;
                //this.FormBorderStyle = FormBorderStyle.FixedDialog;
                // Set the MaximizeBox to false to remove the maximize box.
                //this.MaximizeBox = true;                
                // Set the start position of the form to the center of the screen.
                //this.StartPosition = FormStartPosition.CenterScreen;                

                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal; //  Maximize window
            }

            /// <summary>
            /// Fills the Process View.
            /// </summary>
            private void ModifyProcessView()
            {
                this.ProcessList.Columns.Add("Mid", 70, HorizontalAlignment.Left);
                this.ProcessList.Columns.Add("Pid", 70, HorizontalAlignment.Left);
                this.ProcessList.Columns.Add("Name", 200, HorizontalAlignment.Left);
                this.ProcessList.Columns.Add("Path", 400, HorizontalAlignment.Left);
                this.ProcessList.Columns.Add("Profile", 100, HorizontalAlignment.Left);
            }
            
            /// <summary>
            /// Fills the Event View.
            /// </summary>
            private void ModifyEventView()
            {
                this.EventsList.View = View.Details;
                this.EventsList.Columns.Add("Id", 70, HorizontalAlignment.Left);
                this.EventsList.Columns.Add("Time", 130, HorizontalAlignment.Left);
                this.EventsList.Columns.Add("Details", 500, HorizontalAlignment.Left);
            }            
            
            /// <summary>
            /// Gets selected process' index.
            /// </summary>
            /// <returns></returns>
            private int GetSelectedIndex()
            {
                for (int i=0;i<this.ProcessList.Items.Count; i++)
                {
                    if (this.ProcessList.Items[i].Selected)
                        return i;
                }
                return -1;
            }

            /// <summary>
            /// Adds a row to a table.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="row"></param>
            private void AddRow(ListViewItemCollection table, string[] row)
            {
                table.Add(new ListViewItem(row));
            }

            /// <summary>
            /// Adds rows to a table.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="rows"></param>
            private void AddRows(ListViewItemCollection table, string[][] rows)
            {
                foreach (string[] row in rows) this.AddRow(table, row);
            }

            /// <summary>
            /// Adds rows to a table.
            /// </summary>
            /// <param name="table"></param>
            /// <param name="rows"></param>
            private void AddRows2(ListViewItemCollection table, string[][] rows)
            {
                table.AddRange((from row in rows select new ListViewItem(row)).ToArray());
            }

            /// <summary>
            /// Adds rows to a table using BeginUpdate and EndUpdate.
            /// </summary>
            /// <param name="lsv"></param>
            /// <param name="rows"></param>
            private void SmartAddRows(ListView lsv,string[][] rows)
            {
                lsv.BeginUpdate();
                this.AddRows2(lsv.Items, rows);
                lsv.EndUpdate();
            }

            /// <summary>
            /// Clears all rows in the ListView.
            /// </summary>
            /// <param name="lsv"></param>
            private void ClearRows(ListView lsv)
            {
                lsv.Items.Clear();
            }

            /// <summary>
            /// Clears the ProcessView.
            /// </summary>
            private void ClearProcesses()
            {
                this.ClearRows(this.ProcessList);
            }

            /// <summary>
            /// Clears the EventsView
            /// </summary>
            private void ClearEvents()
            {
                this.ClearRows(this.EventsList);
            }
            
            /// <summary>
            /// Clears the security info.
            /// </summary>
            private void ClearSecurityInfo()
            {
                this.SecurityInfoData.Text = "";
            }
           
            /// <summary>
            /// Fills the form's data.
            /// </summary>
            private void FillGUI()
            {
                DisplayProcesses();                
                DisplayEvents();
                DisplayCurrentDevice();
                DisplayCurrentUser();
                DisplaySecurityInfo();
                DisableAllActions();
            }

            /// <summary>
            /// Clears the form's data.
            /// </summary>
            private void ClearGUI()
            {
                ClearProcesses();
                ClearEvents();                
                ClearSecurityInfo();
            }

            private void DisplayProcesses()
            {
                ProcessData[] processesData = (ProcessData[]) this.FormDetails.GetProcesses(null);
                Array.Sort(processesData, ((data1, data2) => { return data1.Pid - data2.Pid; }));
                this.DisplayedProcesses = processesData;
                string[][] processes = Process.PrepareProcessesForDisplay(processesData);
                this.SmartAddRows(this.ProcessList, processes);
            }

            /// <summary>
            /// Gets the data of the current selected process.
            /// </summary>
            /// <returns></returns>
            private ProcessData GetSelectedProcess()
            {
                return this.DisplayedProcesses[this.ProcessList.SelectedIndices[0]];
            }

            private void DisplayEvents()
            {
                SystemEventData[] eventsData = (SystemEventData[])this.FormDetails.GetNotifications();
                string[][] events = SystemEvent.PrepareEventsForDisplay(eventsData);
                this.SmartAddRows(this.EventsList, events);
            }
            
            /// <summary>
            /// Displays the current user on form.
            /// </summary>
            private void DisplayCurrentUser()
            {
                UserData userData = (UserData) this.FormDetails.GetCurrentUser(null);
                if (userData == null)
                    return;
                this.UserNameData.Text = userData.UserName;
                this.UserPrivateNameData.Text = userData.PrivateName; 
                this.UserEmailData.Text = userData.Email;
            }

            /// <summary>
            /// Displays the current device on form.
            /// </summary>
            private void DisplayCurrentDevice()
            {
                DeviceData deviceData = (DeviceData) this.FormDetails.GetCurrentDevice(null);
                if (deviceData == null)
                    return;
                this.DeviceNameData.Text = deviceData.DeviceName;                
            }
            
            /// <summary>
            /// Displays security information for user.
            /// </summary>
            private void DisplaySecurityInfo()
            {
                this.SecurityInfoData.Text = (string) this.FormDetails.GetSecurityInfo(); 
            }
            
            /// <summary>
            /// Load the opening screen in the form.
            /// </summary>
            private void LoadOpeningScreen()
            {
                this.Controls.Add(this.WelcomePanel);
            }
            
            /// <summary>
            /// Cleans the opening screen if displayed.
            /// </summary>
            private void CleanOpeningScreen()
            {
                if (this.Controls.Contains(this.WelcomePanel))
                    this.Controls.Remove(this.WelcomePanel);
            }

            /// <summary>
            /// Load the main screen in the form.
            /// </summary>
            private void LoadMainScreen()
            {
                this.Controls.Add(this.MainTabControl);
                this.FillGUI();
            }

            /// <summary>
            /// Cleans the main screen if displayed.
            /// </summary>
            private void CleanMainScreen()
            {
                if (this.Controls.Contains(this.MainTabControl))
                    this.Controls.Remove(this.MainTabControl);
            }

            /// <summary>
            /// Switch to opening screen.
            /// </summary>
            private void SwitchToOpeningScreen()
            {
                this.CleanMainScreen();
                this.LoadOpeningScreen();
            }

            /// <summary>
            /// Switch to main screen.
            /// </summary>
            private void SwitchToMainScreen()
            {
                this.CleanOpeningScreen();
                this.LoadMainScreen();
            }

            /// <summary>
            /// Disables all actions buttons on a process.
            /// </summary>
            private void DisableAllActions()
            {
                this.KillProcessButton.Enabled = false;
                this.SetMalwareButton.Enabled = false;
                this.SetInnocentButton.Enabled = false;
                this.SetSuspectButton.Enabled = false;
                this.ProcessSurveyButton.Enabled = false;
                this.RemoveProcessButton.Enabled = false;
            }

            /// <summary>
            /// Update logged device message in opening screen.
            /// </summary>
            private void UpdateLoggedDeviceData()
            {
                DeviceData data = (DeviceData)this.FormDetails.GetCurrentDevice(null);
                if (data != null)
                {
                    this.LoggedDeviceData.Text = "Device is signed in.";
                }
                else
                    this.LoggedDeviceData.Text = "No device signed in.";
            }

            /// <summary>
            /// Update logged user message in opening screen.
            /// </summary>
            private void UpdateLoggedUserData()
            {
                UserData data = (UserData)this.FormDetails.GetCurrentUser(null);
                if (data != null)
                {
                    this.LoggedUserData.Text = "User is signed in.";
                }
                else
                    this.LoggedUserData.Text = "No user signed in.";
            }

            #region Elements event handlers

            private void OnFormClosing(Object sender, FormClosingEventArgs e)
            {
                this.IsDone = true;
            }            

            private void MalterForm_Load(object sender, EventArgs e)
            {                
                this.UpdateLoggedUserData();
                this.UpdateLoggedDeviceData();
            }

            private void listView1_SelectedIndexChanged(object sender, EventArgs e)
            {

            }
            
            private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
            {                 
                if (this.ProcessList.SelectedItems.Count == 1)
                {
                    ProcessData selectedProcess = this.GetSelectedProcess();
                    this.SetProcessesActions(selectedProcess);
                }
                else
                {
                    this.DisableAllActions();
                }
            }
            
            private void tabPage2_Click(object sender, EventArgs e)
            {
                        
            }

            private void listView2_SelectedIndexChanged(object sender, EventArgs e)
            {

            }

            private void Welcome_Click(object sender, EventArgs e)
            {
                DeviceData currentDevice = (DeviceData)this.FormDetails.GetCurrentDevice(null);
                UserData currentUser = (UserData)this.FormDetails.GetCurrentUser(null);

                // A user must be logged in.
                if (currentUser == null)
                {
                    this.DisplayError(MalterExitCodes.NoUserSigned);
                    return;
                }
                // A device must be logged in.
                if (currentDevice == null)
                {
                    this.DisplayError(MalterExitCodes.NoDeviceSigned);
                    return;
                }

                // Start Malter.                
                this.FormDetails.MalterStartHandler();
                this.SwitchToMainScreen();
            }                        

            private void SignIn_Click(object sender, EventArgs e)
            {
                MalterSignInUserForm signInform = new MalterSignInUserForm(this.FormDetails.AuthenticateUser);
                signInform.Start();
                signInform.IsDone.WaitOne();
                this.UpdateLoggedUserData();
            }

            private void SignUpDevice_Click(object sender, EventArgs e)
            {
                MalterSignUpDeviceForm signInform = new MalterSignUpDeviceForm(this.FormDetails.SignUpDevice);
                signInform.Start();
                signInform.IsDone.WaitOne();
                this.UpdateLoggedDeviceData();                
            }

            private void SignUpUser_Click(object sender, EventArgs e)
            {
                MalterSignUpUserForm signUpForm = new MalterSignUpUserForm(this.FormDetails.SignUpUser);
                signUpForm.Start();
                signUpForm.IsDone.WaitOne();
            }

            private void DisplayError(MalterExitCodes code)
            {
                this.ErrorMsgLabel.Text = "Error: " + code.GetMSG();
            }            

            private void SetInnocentButton_Click(object sender, EventArgs e)
            {
                this.FormDetails.SetInnocentHandler(GetSelectedProcess());
                SmartUpdateGUI();
            }

            private void SetSuspectButton_Click(object sender, EventArgs e)
            {
                this.FormDetails.SetSuspectHandler(GetSelectedProcess());
                SmartUpdateGUI();
            }

            private void SetMalwareButton_Click(object sender, EventArgs e)
            {
                this.FormDetails.SetMalwareHandler(GetSelectedProcess());
                SmartUpdateGUI();
            }

            private void KillProcessButton_Click(object sender, EventArgs e)
            {
                this.FormDetails.KillProcessHandler(GetSelectedProcess());
                this.FormDetails.RefreshMonitors();
                SmartUpdateGUI();
            }

            private void SetProcessesActions(ProcessData selectedProcess)
            {
                this.KillProcessButton.Enabled = selectedProcess.CanProcessKill();
                this.SetMalwareButton.Enabled = selectedProcess.CanSetMalware();
                this.SetInnocentButton.Enabled = selectedProcess.CanSetInnocent();
                this.SetSuspectButton.Enabled = selectedProcess.CanSetSuspect();
                this.ProcessSurveyButton.Enabled = selectedProcess.CanSurvey();
                this.RemoveProcessButton.Enabled = true;
            }                                               

            private void LoggedDeviceData_Click(object sender, EventArgs e)
            {

            }
                      
            private void ProcessSurveyButton_Click(object sender, EventArgs e)
            {
                MalterProcessSurveyForm form = new MalterProcessSurveyForm(this.FormDetails.ProcessSurveyHandler, 
                    this.GetSelectedProcess());
                form.Start();
            }
            
            private void ExitButton_Click(object sender, EventArgs e)
            {
                this.Close();
            }
            
            private void RefreshDataButton_Click(object sender, EventArgs e)
            {
                SmartUpdateGUI();
            }
            
            private void RefreshMonitors_Click(object sender, EventArgs e)
            {

                this.FormDetails.RefreshMonitors();
            }
            
            private void RemoveProcessButton_Click(object sender, EventArgs e)
            {
                this.FormDetails.RemoveProcessHandler(this.GetSelectedProcess());
            }           

            private void ArmoryHelpButton_Click(object sender, EventArgs e)
            {
                System.Windows.Forms.MessageBox.Show(ARMORY_HELP_MESSAGE, "Malter", System.Windows.Forms.MessageBoxButtons.OK);
            }

            #endregion Elements event handlers
        }

        /// <summary>
        /// Class for gathering Event handlers for MalterForm.
        /// </summary>
        public class MalterFormDetails
        {
            public static ProcessAction ProcessDefaultAction = ((ProcessData p) => (null));
            public static UserAction UserDefaultAction = ((UserData p) => (null));
            public static DeviceAction DeviceDefaultAction = ((DeviceData p) => (null));
            public static GUIHandler DefaultGUIHandler = ((p) => null);
            public delegate object GUIHandler(params object[] hparams);
            public ProcessAction SetInnocentHandler { get; set; } = ProcessDefaultAction;
            public ProcessAction SetSuspectHandler { get; set; } = ProcessDefaultAction;
            public ProcessAction SetMalwareHandler { get; set; } = ProcessDefaultAction;
            public ProcessAction KillProcessHandler { get; set; } = ProcessDefaultAction;
            public ProcessAction RemoveProcessHandler { get; set; } = ProcessDefaultAction;
            public UserAction SignUpUser { get; set; } = UserDefaultAction;
            public UserAction AuthenticateUser { get; set; } = UserDefaultAction;
            public DeviceAction SignUpDevice { get; set; } = DeviceDefaultAction;
            public DeviceAction AuthenticateDevice { get; set; } = DeviceDefaultAction;
            
            public UserAction GetCurrentUser { get; set; } = UserDefaultAction;
            public DeviceAction GetCurrentDevice { get; set; } = DeviceDefaultAction;
            public GUIHandler ProcessSurveyHandler { get; set; } = DefaultGUIHandler;
            public GUIHandler MalterStartHandler { get; set; } = DefaultGUIHandler;
            public GUIHandler RefreshMonitors { get; set; } = DefaultGUIHandler;
            public GUIHandler GetSecurityInfo { get; set; } = DefaultGUIHandler;
            public GUIHandler GetNotifications { get; set; } = DefaultGUIHandler;
            public GUIHandler GetProcesses { get; set; } = DefaultGUIHandler;
            public GUIHandler GetCalls { get; set; } = DefaultGUIHandler;
        }
    }
}