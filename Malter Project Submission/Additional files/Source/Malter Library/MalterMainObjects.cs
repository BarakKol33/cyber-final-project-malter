﻿using MalterLib.MalterData;
using MalterLib.MalterEngine;
using MalterLib.MalterGUI;
using MalterLib.MalterNetwork;
using MalterLib.MalterObjects;
using MalterLib.ProcessMonitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MalterLib
{
    namespace MainMalter
    {
        /// <summary>
        /// A general action that is performed on a system user, such as sign-in or sign-up.
        /// </summary>
        /// <param name="data">The user's data</param>
        /// <returns>The action's result</returns>
        public delegate object UserAction(UserData data);

        /// <summary>
        /// A general action that is performed on a system device, such as sign-in or sign-up.
        /// </summary>
        /// <param name="data">The devices's data.</param>
        /// <returns>The action's result.</returns>
        public delegate object DeviceAction(DeviceData data);

        /// <summary>
        /// Class for holding resources that are shared across classes, such as database 
        /// or network access.
        /// </summary>
        public class AppResources
        {
            /// <summary>
            /// Builds a new resources object.
            /// </summary>
            /// <param name="isClient">Wheter the resources are for a client app.</param>
            public AppResources(bool isClient=true)
            {
                AppTypeClass.SetAppType(isClient);
            }

            /// <summary>
            /// Holds the resources of the current running processes.
            /// </summary>
            public static AppResources CurrentResources;
            
            /// <summary>
            /// Database access
            /// </summary>
            public DataInterface db { get; set;}

            /// <summary>
            /// Access to the GUI.
            /// </summary>
            public MalterForm form { get; set; } = null;

            /// <summary>
            /// Access to the process' monitor.
            /// </summary>
            public ProcessMonitor pm { get; set; }
            
            /// <summary>
            /// Access to the device that is currently logged in (null if there is no such device).
            /// </summary>
            public DeviceData AppDevice { get { return  db.GetCurrentDevice(); }}

            /// <summary>
            /// Access to the user that is currently logged in (null if there is no such user).
            /// </summary>
            public UserData AppUser { get { return db.GetCurrentUser(); }}

            /// <summary>
            /// Access to network communication, from client side.
            /// </summary>
            public ProxyClient PClient { get; set; }

            /// <summary>
            /// Access to network communication, from server side.
            /// </summary>
            public ProxyServer PServer { get; set; }            
        }

        /// <summary>
        /// Class for representing a user in the system.
        /// </summary>
        public class User
        {
            /// <summary>
            /// User's Id.
            /// </summary>
            private int Id;

            /// <summary>
            /// User's name.
            /// </summary>
            private string Name;
            /// <summary>
            /// User's username.
            /// </summary>
            private string UserName;
            /// <summary>
            /// User's password.
            /// </summary>
            private string Password;
            /// <summary>
            /// User's email address.
            /// </summary>
            private string EmailAddress;
            /// <summary>
            /// Builds a new user instance.
            /// </summary>
            /// <param name="userName"></param>
            /// <param name="id"></param>
            /// <param name="name"></param>
            /// <param name="password"></param>
            /// <param name="emailAddress"></param>
            public User(string userName, int id, string name = "", string password = "", string emailAddress = "")
            {
                this.Id = id;
                this.Name = name;
                this.UserName = userName;
                this.Password = password;
                this.EmailAddress = emailAddress;
            }            

            /// <summary>
            /// Returns wheter an email address is valid or not.
            /// </summary>
            /// <param name="email"></param>
            /// <returns></returns>
            public static bool IsValidEmail(string email)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(email);
                    return addr.Address == email;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Class for representing a Device in the system.
        /// </summary>
        public class Device
        {
            /// <summary>
            /// Device's ID.
            /// </summary>
            public int Id;
            /// <summary>
            /// Devices owner's ID.
            /// </summary>
            public int OwnerId;
            /// <summary>
            /// Device's name, as called by the user.
            /// </summary>
            public string DeviceName;
            /// <summary>
            /// Device's cookie used for authentication.
            /// </summary>
            public string Info;
            /// <summary>
            /// Array of the processes' MID that were identified in this device.
            /// </summary>
            public int[] Processes;

            /// <summary>
            /// Creates a new device instance.
            /// </summary>
            /// <param name="id"></param>
            /// <param name="deviceName"></param>
            /// <param name="ownerId"></param>
            /// <param name="info"></param>
            /// <param name="processes"></param>
            public Device(int id, string deviceName, int ownerId, string info, int[] processes)
            {
                this.Id = id;
                this.DeviceName = deviceName;
                this.OwnerId = ownerId;
                this.Processes = new int[processes.Length];
                if (processes != null)
                    Array.Copy(processes, this.Processes, processes.Length);
                this.Info = info;
            }
                        
            /// <summary>
            /// Gets a DeviceData object containing this device's data.
            /// </summary>
            /// <returns></returns>
            public DeviceData GetDataStruct()
            {
                return new DeviceData
                {
                    Id = this.Id,
                    DeviceName = this.DeviceName,
                    OwnerId = this.OwnerId,
                    Info = this.Info,
                    Processes = JArray.FromObject(this.Processes).ToString()
                };
            }

            /// <summary>
            /// Gets a Device object based on a DeviceData object.
            /// </summary>
            /// <param name="deviceData"></param>
            /// <returns></returns>
            public static Device GetObjectFromData(DeviceData deviceData)
            {
                if (deviceData == null)
                    return null;
                return new Device(id: deviceData.Id, deviceName: deviceData.DeviceName,ownerId: deviceData.OwnerId,
                    info: deviceData.Info, processes:JsonConvert.DeserializeObject<int[]>(deviceData.Processes));
            }

            /// <summary>
            /// Returns wheter the process was identified in the device based on their IDs 
            /// and a database.
            /// </summary>
            /// <param name="deviceId"></param>
            /// <param name="mid"></param>
            /// <param name="db"></param>
            public static void ProcessIdentifiedInDevice(int deviceId, int mid, DataInterface db)
            {
                if (!db.DoesDeviceHasProcess(deviceId, mid))
                    db.AddProcessToDevice(deviceId, mid);
            }

            /// <summary>
            /// Gets a string containing the device's data.
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return GetDataStruct().ToString();
            }
        }

        /// <summary>
        /// Class for representing a general type of event.
        /// </summary>
        public abstract class SystemEvent
        {
            /// <summary>
            /// Event's ID.
            /// </summary>
            public int Id;
            /// <summary>
            /// The time the event occured in.
            /// </summary>
            public string Time;
            /// <summary>
            /// The event's type.
            /// </summary>
            public abstract Events Type { get; }
            /// <summary>
            /// A collection of EventSorters, used for converting a SystemEventData object to a SystemEvent object.
            /// </summary>
            public static Dictionary<Events, EventSorter> EventSorters = new Dictionary<Events, EventSorter>
            {
                { Events.NewProcess, NewProcess.GetEventFromData},
                { Events.TestPassed, TestPassed.GetEventFromData},
                { Events.UpdateProfile, UpdateProfile.GetEventFromData},
                { Events.ProcessActionTaken, ProcessActionTaken.GetEventFromData},
                { Events.NewProcessUpdate, UpdateNewProcess.GetEventFromData},
                { Events.SystemMessage, SystemMessage.GetEventFromData }
            };
            
            /// <summary>
            /// Delegate that converts a SystemEventData object to a SystemEvent object.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public delegate SystemEvent EventSorter(SystemEventData data);
            /// <summary>
            /// Delegate that converts a SystemEventData object to a DataUpdate object.
            /// </summary>
            /// <param name="data"></param>
            public delegate void EventUpdateSorter(SystemEventData data);

            /// <summary>
            /// Creates a new SystemEvent instance.
            /// </summary>
            /// <param name="id"></param>
            /// <param name="time"></param>
            public SystemEvent(int id = 0, string time = "")
            {
                this.Id = id;
                if (time == "")
                    this.Time = WindowsModule.WindowsInterface.GetTimeNowString();
                else
                    this.Time = time;

            }
            
            /// <summary>
            /// Gets a JSON string containing details of an event from a specific type.
            /// </summary>
            /// <returns></returns>
            public abstract string GetEventDetails();

            /// <summary>
            /// Gets a SystemEventData object conatining the current event's data.
            /// </summary>
            /// <returns></returns>
            public SystemEventData GetDataStruct()
            {
                return new SystemEventData
                {
                    Id = this.Id,
                    Time = this.Time,
                    Type = (int)this.Type,
                    SEvent = GetEventDetails()
                };
            }

            /// <summary>
            /// Converts a SystemEventData object to a SystemEvent object.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static SystemEvent GetEventFromData(SystemEventData data)
            {
                EventSorter sorter;
                if (EventSorters.TryGetValue((Events)data.Type, out sorter))
                    return sorter(data);
                return null;
            }

            /// <summary>
            /// Signal that the event happend in a database.
            /// </summary>
            /// <param name="db">The database to be changed.</param>
            public void Signal(DataInterface db)
            {
                db.NewEvent(this.GetDataStruct());
            }

            /// <summary>
            /// Gets a string containing the event's data.
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return this.GetDataStruct().ToString();
            }

            /// <summary>
            /// Converts a group of events to one string.
            /// </summary>
            /// <param name="events"></param>
            /// <returns></returns>
            public static string GroupToString(SystemEventData[] events)
            {
                return new JArray((from sEvent in events select sEvent.ToJson()).ToArray()).ToString();
            }

            /// <summary>
            /// Adds an EventSorter to the static EventSorters collection, used for converting SystemEventData
            /// To SystemEvent objects.
            /// </summary>
            /// <param name="events"></param>
            /// <param name="sorter"></param>
            public static void AddEventSorter(Events events, EventSorter sorter)
            {
                SystemEvent.EventSorters[events] = sorter;
            }

            /// <summary>
            /// Gets the Event's data in a human message format.
            /// </summary>
            /// <returns></returns>
            public abstract string GetMsg();

            /// <summary>
            /// Preparing a group of events to be displayed in an Event View.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string[][] PrepareEventsForDisplay(SystemEventData[] data)
            {
                return (from edata in data select SystemEvent.PrepareEventForDisplay(edata)).ToArray();
            }

            /// <summary>
            /// Preparing an event to be displayed in an Event View.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string[] PrepareEventForDisplay(SystemEventData data)
            {
                return new string[] { data.Id.ToString(), data.Time.ToString(), SystemEvent.GetEventFromData(data).GetMsg() };
            }
        }

        #region Events
        /// <summary>
        /// A new process was found.
        /// </summary>
        public class NewProcess : SystemEvent
        {
            public override Events Type { get { return Events.NewProcess; } }
            public int Mid;

            public NewProcess(int mid, int id = 0, string time = "") : base(id: id, time: time)
            {
                this.Mid = mid;
            }

            public override string GetEventDetails()
            {
                return string.Format("{{'Mid':{0}}}", this.Mid);
            }

            public static new NewProcess GetEventFromData(SystemEventData data)
            {
                int Mid = (int)JObject.Parse(data.SEvent).Property("Mid").Value;
                return new NewProcess(mid: Mid, id: data.Id, time: data.Time);
            }

            public override string GetMsg()
            {
                return string.Format("Process {0} Discovered", this.Mid);
            }
        }
        /// <summary>
        /// A process has passed a test.
        /// </summary>
        public class TestPassed : SystemEvent
        {
            public override Events Type { get { return Events.TestPassed; } }
            /// <summary>
            /// Passed test's ID.
            /// </summary>
            public int TestId;
            /// <summary>
            /// Process' MID.
            /// </summary>
            public int Mid;

            public TestPassed(int mid, int testId, int id = 0, string time = "") : base(id, time)
            {
                this.TestId = testId;
                this.Mid = mid;
            }

            public override string GetEventDetails()
            {
                return string.Format("{{'Mid':{0}, 'TestId':{1}}}", this.Mid, this.TestId);
            }

            public static new TestPassed GetEventFromData(SystemEventData data)
            {
                JObject sevent = JObject.Parse(data.SEvent);
                int testId = (int)sevent.Property("TestId").Value;
                int mid = (int)sevent.Property("Mid").Value;
                return new TestPassed(mid, testId, data.Id, data.Time);
            }

            public override string GetMsg()
            {
                return string.Format("Process {0} Passed Test {1}",
                    AppResources.CurrentResources.db.GetProcessName(this.Mid),
                    ((Tests)this.TestId).GetMsg());
            }
        }

        /// <summary>
        /// An action was taken against a process.
        /// </summary>
        public class ProcessActionTaken : SystemEvent
        {
            public override Events Type
            {
                get
                {
                    return Events.ProcessActionTaken;
                }
            }
            /// <summary>
            /// Process' MID.
            /// </summary>
            public int Mid;
            /// <summary>
            /// Action type.
            /// </summary>
            public ProcessActions Action;

            public ProcessActionTaken(int mid, ProcessActions action, int id = 0, string time = "")
                : base(id, time)
            {
                this.Mid = mid;
                this.Action = action;
            }

            public override string GetEventDetails()
            {
                return string.Format("{{'Action':{0}, 'Mid':{1}}}", (int)this.Action, this.Mid);
            }

            public static new ProcessActionTaken GetEventFromData(SystemEventData data)
            {
                JObject sevent = JObject.Parse(data.SEvent);
                int mid = (int)sevent.Property("Mid").Value;
                ProcessActions action = (ProcessActions)(int)sevent.Property("Action").Value;
                return new ProcessActionTaken(mid, action, data.Id, data.Time);
            }

            public override string GetMsg()
            {
                return string.Format("{0} was taken against process {1}", this.Action,
                    AppResources.CurrentResources.db.GetProcessName(this.Mid));
            }
        }

        /// <summary>
        /// Signal a general message.
        /// </summary>
        public class SystemMessage : SystemEvent
        {
            public override Events Type
            {
                get
                {
                    return Events.SystemMessage;
                }
            }
            public string Message;

            public SystemMessage(string message, int id = 0, string time = "") 
                : base(id, time)
            {
                this.Message = message;
            }

            public override string GetEventDetails()
            {
                return string.Format("{{'Message':'{0}'}}", this.Message);
            }

            public override string GetMsg()
            {
                return this.Message;
            }

            public static new SystemMessage GetEventFromData(SystemEventData data)
            {
                JProperty messageProp = JObject.Parse(data.SEvent).Property("Message");
                if (messageProp == null)
                    return null;
                return new SystemMessage((string)messageProp.Value, data.Id, data.Time);
            }
        }

        /// <summary>
        /// Class for representing a general data update from the server to the client.
        /// </summary>
        public abstract class DataUpdate: SystemEvent
        {
            
            public delegate DataUpdate DataUpdateSorter(SystemEventData data);
            public static Dictionary<Events, DataUpdateSorter> DataUpdateSorters = new Dictionary<Events, DataUpdateSorter>
            {
                { Events.UpdateProfile,  UpdateProfile.GetEventFromData  },
                { Events.AddMisc, AddMisc.GetEventFromData },
                { Events.RemoveMisc, RemoveMisc.GetEventFromData }
            };

            public static void UpdateEventInDB(SystemEventData data)
            {
                DataUpdate update = DataUpdateSorters[(Events)data.Type](data);
                MDebug.WriteLine(update.GetMsg());
                update.Execute();
            }

            public DataUpdate(int id = 0, string time = "") : base (id, time)
            {   
            }

            public abstract void Execute();
        }
        /// <summary>
        /// A process' profile has changed.
        /// </summary>
        public class UpdateProfile : DataUpdate
        {
            public override Events Type { get { return Events.UpdateProfile; } }
            public int ProfileId;
            public int Mid;

            public UpdateProfile(int profileId, int mid, int id = 0, string time = "") : base(id, time)
            {
                this.ProfileId = profileId;
                this.Mid = mid;
            }

            public override string GetEventDetails()
            {
                return string.Format("{{'ProfileId':{0}, 'Mid':{1}}}", this.ProfileId, this.Mid);
            }

            public static new UpdateProfile GetEventFromData(SystemEventData data)
            {
                JObject sevent = JObject.Parse(data.SEvent);
                int mid = (int) sevent.Property("Mid").Value;
                int profileId = (int)sevent.Property("ProfileId").Value;
                return new UpdateProfile(profileId:profileId, mid:mid, id:data.Id, time:data.Time);
            }

            public override void Execute()
            {
                // Signal event.
                this.Signal(AppResources.CurrentResources.db);
                // Display message for user.
                MalterGUI.MalterForm.DisplayProfileChangeMessage(this.Mid, ((Profiles)this.ProfileId).GetMsg());
                // Set the process' profile in local database.
                AppResources.CurrentResources.db.SetProcessProfile(mid: this.Mid, profile: this.ProfileId);
            }

            public override string GetMsg()
            {
                return string.Format("Server Update: Updated Profile of Process {0} to Profile {1}", this.Mid, ((Profiles)this.ProfileId).GetMsg());
            }
        } 

        

        /// <summary>
        /// A new misc needs to be added.
        /// </summary>
        public class AddMisc : DataUpdate
        {
            public override Events Type { get { return Events.AddMisc; } }
            public MiscData NewMisc;

            public override string GetEventDetails()
            {
                return this.NewMisc.ToJson();
            }

            public AddMisc(MiscData newMisc, int id = 0, string time = "") : base(id, time)
            {
                this.NewMisc = newMisc;                
            }

            public static new AddMisc GetEventFromData(SystemEventData data)
            {
                return new AddMisc(data.SEvent.FromJson<MiscData>(), time:data.Time);
            }

            public override void Execute()
            {
                AppResources.CurrentResources.db.AddMisc(this.NewMisc);
            }

            public override string GetMsg()
            {
                return string.Format("New element for {0} list: {1}", ((MiscTypes)this.NewMisc.Type).GetMsg(), this.NewMisc.Data);
            }
        }       

        /// <summary>
        /// A misc needs to be removed.
        /// </summary>
        public class RemoveMisc : DataUpdate
        {
            public override Events Type { get { return Events.RemoveMisc; } }
            public MiscData MiscToRemove;

            public override string GetEventDetails()
            {
                return this.MiscToRemove.ToJson();
            }

            public RemoveMisc(MiscData toRemove, int id = 0, string time = "") : base(id, time)
            {
                this.MiscToRemove = toRemove;                
            }

            public static new RemoveMisc GetEventFromData(SystemEventData data)
            {
                return new RemoveMisc(data.SEvent.FromJson<MiscData>(), time: data.Time);
            }

            public override void Execute()
            {
                AppResources.CurrentResources.db.RemoveMisc(this.MiscToRemove);
            }

            public override string GetMsg()
            {
                return string.Format("Element Removed for {0} list: {1}", ((MiscTypes)this.MiscToRemove.Type).GetMsg(), this.MiscToRemove.Data);
            }
        }
        /// <summary>
        /// Update the server that a new process was found.
        /// </summary>
        public class UpdateNewProcess : DataUpdate
        {
            public override Events Type { get { return Events.NewProcessUpdate; } }
            public ProcessData TheNewProcess;

            public override string GetEventDetails()
            {
                return TheNewProcess.ToJson();
            }

            public UpdateNewProcess(ProcessData theNewProcess, int id=0, string time="") : base(id, time)
            {
                this.TheNewProcess = theNewProcess;
            }

            public static new UpdateNewProcess GetEventFromData(SystemEventData data)
            {
                return new UpdateNewProcess(data.SEvent.FromJson<ProcessData>(), data.Id, data.Time);
            }

            public override void Execute()
            {
                AppResources.CurrentResources.db.StoreProcess(TheNewProcess);
            }

            public override string GetMsg()
            {
                return string.Format("New Process Found: {0}", TheNewProcess.Name );
            }
        }       
        #endregion Events

    }
}
