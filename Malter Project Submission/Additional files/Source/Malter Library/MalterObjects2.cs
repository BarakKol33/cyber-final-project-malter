﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace MalterLib
{
    using MalterData.MalterDBDetails;
    using MainMalter;
    using ProcessMonitoring;
    using MalterNetwork;
    using MalterObjects;
    using System.Security.Cryptography;
    using MalterData;
    using MalterGUI;
    using MalterEngine;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    namespace MalterObjects
    {
        /// <summary>
        /// Class for gathering constants and general useful services.
        /// </summary>
        public static class GeneralData
        {
            public static int IntGen = 13;
            public static string StringGen = "";
            public static string JArrayGen = "[]";
            public static string JObjectGen = "{}";
            public static bool BoolGen = false;

            /// <summary>
            /// Get The value of an int column by one of the rows of query,
            /// the query's selections, and its name.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <param name="columnName"></param>
            /// <returns></returns>
            public static int GetIntColData(DataRow row, string[] selectionsArray, string columnName)
            {
                return selectionsArray == null || selectionsArray.Contains(columnName) ? (int)row[columnName] : IntGen;
            }

            /// <summary>
            /// Get The value of a string column by one of the rows of query,
            /// the query's selections, and its name.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <param name="columnName"></param>
            /// <returns></returns>
            public static string GetStringColData(DataRow row, string[] selectionsArray, string columnName)
            {
                return selectionsArray == null || selectionsArray.Contains(columnName) ? (string)row[columnName] : StringGen;
            }
        }
        
        /// <summary>
        /// Class for gathering basic data for a user.
        /// </summary>
        public class UserData : IData
        {
            public int Id               = GeneralData.IntGen;
            public string PrivateName   = GeneralData.StringGen;
            public string UserName      = GeneralData.StringGen;
            public string Email         = GeneralData.StringGen;
            public string Password      = GeneralData.StringGen;

            public UserData(UserData data)
            {
                this.Id = data.Id;
                this.PrivateName = data.PrivateName;
                this.UserName = data.UserName;
                this.Email = data.Email;
                this.Password = data.Password;
            }

            public UserData()
            {
            }

            public override string ToString()
            {
                return JsonConvert.SerializeObject(this);
                //return string.Format("UserD Id: {0} PrivateName: {1} Username: {2}", this.Id, this.PrivateName, this.UserName);
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    { UserTable.Id, this.Id.ToString()},
                    { UserTable.PrivateName, this.PrivateName.ToSQLParm()},
                    { UserTable.Username, this.UserName.ToSQLParm()},
                    { UserTable.Password, this.Password.ToSQLParm()},
                    { UserTable.Email, this.Email.ToSQLParm() }
                };
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static UserData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new UserData
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, UserTable.Id),
                    PrivateName = GeneralData.GetStringColData(row, selectionsArray, UserTable.PrivateName),
                    UserName = GeneralData.GetStringColData(row, selectionsArray, UserTable.Username),
                    Email = GeneralData.GetStringColData(row, selectionsArray, UserTable.Email),
                    Password = GeneralData.GetStringColData(row, selectionsArray, UserTable.Password)
                };
            }
        }

        /// <summary>
        /// Class for gathering basic data for a device.
        /// </summary>
        public class DeviceData : IData
        {
            public int Id               = GeneralData.IntGen;
            public string DeviceName    = GeneralData.StringGen;
            public int OwnerId          = GeneralData.IntGen;
            public string Info          = GeneralData.StringGen;
            [JsonIgnore]
            public string Processes     = GeneralData.JArrayGen;
            [JsonIgnore]
            public string UpdateEvents  = GeneralData.JArrayGen;
            //public string Keys          = GeneralData.JArrayGen;
            public DeviceData(DeviceData data)
            {
                this.Id = data.Id;
                this.DeviceName = data.DeviceName;
                this.OwnerId = data.OwnerId;
                this.Info = data.Info;
                this.Processes = data.Processes;
                //this.Keys = data.Keys;
            }

            public DeviceData()
            {
            }

            public override string ToString()
            {
                return string.Format("DeviceD Id: {0} DeviceName: {1} OwnerId: {2}", this.Id, this.DeviceName, this.OwnerId);
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    {DeviceTable.Id, this.Id.ToSQLParm() },
                    {DeviceTable.DeviceName, this.DeviceName.ToSQLParm()},
                    {DeviceTable.OwnerId,  this.OwnerId.ToSQLParm() },
                    {DeviceTable.Info, this.Info.ToSQLParm() },
                    {DeviceTable.Processes, this.Processes.ToSQLParm()},
                    {DeviceTable.UpdateEvents, this.UpdateEvents.ToSQLParm() },
                    //{DeviceTable.Keys, this.Keys.ToSQLParm() }
                };
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static DeviceData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new DeviceData()
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, UserTable.Id),
                    DeviceName = GeneralData.GetStringColData(row, selectionsArray, DeviceTable.DeviceName),
                    OwnerId = GeneralData.GetIntColData(row, selectionsArray, DeviceTable.OwnerId),
                    Info = GeneralData.GetStringColData(row, selectionsArray, DeviceTable.Info),
                    Processes = GeneralData.GetStringColData(row, selectionsArray, DeviceTable.Processes),
                    UpdateEvents = GeneralData.GetStringColData(row, selectionsArray, DeviceTable.UpdateEvents)
                    //Keys = GeneralData.GetStringColData(row, selectionsArray, DeviceTable.Keys)
                };
            }
        }
       
        /// <summary>
        /// Class for gathering basic data for a system event.
        /// </summary>
        public class SystemEventData : IData
        {
            public int Id       = GeneralData.IntGen;
            public string Time  = GeneralData.StringGen;
            public int Type     = GeneralData.IntGen;
            public string SEvent= GeneralData.JObjectGen;

            public override string ToString()
            {
                return string.Format("Event Id:{0} Type:{1} Time:{2} SEvent:{3}", this.Id, this.Type, this.Time, this.SEvent);
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    { SystemEventTable.Id, this.Id.ToSQLParm()},
                    { SystemEventTable.Type, this.Type.ToSQLParm()},
                    { SystemEventTable.Time, this.Time.ToSQLParm() },
                    { SystemEventTable.SEvent, this.SEvent.ToSQLParm()}
                };
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static SystemEventData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new SystemEventData
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, SystemEventTable.Id),
                    Type = GeneralData.GetIntColData(row, selectionsArray, SystemEventTable.Type),
                    Time = GeneralData.GetStringColData(row, selectionsArray, SystemEventTable.Time),
                    SEvent = GeneralData.GetStringColData(row, selectionsArray, SystemEventTable.SEvent),
                };
            }

            /// <summary>
            /// Checks if an event type should be sent to the server.
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static bool IsUpdateEvent(int type)
            {                 
                Events etype = (Events)type;
                return etype == Events.TestPassed ||
                    etype == Events.NewProcessUpdate;
            }

            /// <summary>
            /// Checks if an event type is a notification.
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static bool IsNotification(int type)
            {
                Events etype = (Events)type;
                return etype == Events.ProcessActionTaken ||
                    etype == Events.NewProcessUpdate ||
                    etype == Events.SystemMessage ||
                    etype == Events.UpdateProfile;
            }
        }

        /// <summary>
        /// Class for gathering basic data for an API call.
        /// </summary>
        public class APICallData : IData
        {
            public int Id       = GeneralData.IntGen;
            public int Pid      = GeneralData.IntGen;
            public int Mid      = GeneralData.IntGen;
            public string Name  = GeneralData.StringGen;
            public string Time  = GeneralData.StringGen;
            public string SCall = GeneralData.JObjectGen;
            public string ProcessName = GeneralData.StringGen;
            public override string ToString()
            {
                return string.Format("APICall Id:{0} MID:{4} PID:{5} Name:{1} Time:{2} SCall:{3}",
                    this.Id, this.Name, this.Time, this.SCall, this.Mid, this.Pid);
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    {APICallTable.Id, this.Id.ToString()  },
                    {APICallTable.Name,  this.Name.ToSQLParm()},
                    {APICallTable.Mid, this.Mid.ToSQLParm() },
                    {APICallTable.Pid, this.Pid.ToSQLParm()},
                    {APICallTable.Time, this.Time.ToSQLParm()},
                    {APICallTable.SCall, this.SCall.ToSQLParm()}
                };
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static APICallData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new APICallData
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, APICallTable.Id),
                    Name = GeneralData.GetStringColData(row, selectionsArray, APICallTable.Name),
                    Pid = GeneralData.GetIntColData(row, selectionsArray, APICallTable.Pid),
                    Mid = GeneralData.GetIntColData(row, selectionsArray, APICallTable.Mid),
                    Time = GeneralData.GetStringColData(row, selectionsArray, APICallTable.Time),
                    SCall = GeneralData.GetStringColData(row, selectionsArray, APICallTable.SCall),
                };
            }            

            public string[] ToDataArray()
            {
                return new string[] { this.Id.ToString(), this.Name, this.Mid.ToString(), this.Pid.ToString(), this.Time, this.SCall };
            }
        }

        /// <summary>
        /// Class for gathering basic data for a process.
        /// </summary>
        public class ProcessData : IData
        {
            public int Mid          = GeneralData.IntGen;
            public int Pid          = GeneralData.IntGen;
            public string Name      = GeneralData.StringGen;
            public string Path      = GeneralData.StringGen;
            public string Hash      = GeneralData.StringGen;
            public int Profile      = GeneralData.IntGen;
            public int CheckCounter = GeneralData.IntGen;
            public int DidSurvey    = GeneralData.IntGen;
            public static int NOPID = 0;

            public override string ToString()
            {
                return string.Format("ProcessD Mid:{0} Pid:{1} Name:{2} Path:{3} Hash:{4} Profile:{5} Counter:{6} Survey:{7}",
                    this.Mid, this.Pid, this.Name, this.Path, this.Hash, this.Profile, this.CheckCounter, this.DidSurvey);
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    {ProcessTable.Mid, this.Mid.ToSQLParm() },
                    { ProcessTable.Pid, this.Pid.ToSQLParm()},
                    { ProcessTable.Name, this.Name.ToSQLParm() },
                    { ProcessTable.Path, this.Path.ToSQLParm() },
                    { ProcessTable.Hash, this.Hash.ToSQLParm() },
                    { ProcessTable.Profile, this.Profile.ToSQLParm() },
                    { ProcessTable.CheckCounter, this.CheckCounter.ToSQLParm() },
                    { ProcessTable.DidSurvey, this.DidSurvey.ToSQLParm() }
                };
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>           
            public static ProcessData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new ProcessData
                {
                    Mid = GeneralData.GetIntColData(row, selectionsArray, ProcessTable.Mid),
                    Pid = GeneralData.GetIntColData(row, selectionsArray, ProcessTable.Pid),
                    Name = GeneralData.GetStringColData(row, selectionsArray, ProcessTable.Name),
                    Path = GeneralData.GetStringColData(row, selectionsArray, ProcessTable.Path),
                    Hash = GeneralData.GetStringColData(row, selectionsArray, ProcessTable.Hash),
                    Profile = GeneralData.GetIntColData(row, selectionsArray, ProcessTable.Profile),
                    CheckCounter = GeneralData.GetIntColData(row, selectionsArray, ProcessTable.CheckCounter),
                    DidSurvey = GeneralData.GetIntColData(row, selectionsArray, ProcessTable.DidSurvey)
                };
            }

            public bool CanProcessKill()
            {
                return this.Pid > 0;
            }

            public bool CanSetInnocent()
            {
                return this.Profile != (int)Profiles.Innocent;
            }

            public bool CanSetSuspect()
            {
                return this.Profile != (int)Profiles.Suspect;
            }

            public bool CanSetMalware()
            {
                return this.Profile != (int)Profiles.Malware;
            }

            public bool CanSurvey()
            {
                return this.DidSurvey == 0;
            }

            /// <summary>
            /// Gets tests counters for a query.
            /// </summary>
            /// <param name="row">A row in the query's result.</param>
            /// <param name="selectionsArray">The selected tests.</param>
            /// <returns></returns>
            public static int[] GetTestsCounterRowData(DataRow row, string[] selectionsArray)
            {
                return (from test in selectionsArray select (int)row[test]).ToArray();
            }

        }

        /// <summary>
        /// Class for gathering basic data for a monitor call.
        /// </summary>
        public class MonitorCallData : IData
        {
            public int Id                   = GeneralData.IntGen;
            public string CallDllFunction   = GeneralData.JObjectGen;
            public string BlockFiltersIds   = GeneralData.JArrayGen;
            public string StoreFiltersIds   = GeneralData.JArrayGen;

            public override string ToString()
            {
                return string.Format("MonitorCall Id:{0} CallDllFunction:{1} StoreFilters:{2} BlockFilters:{3} ",
                    this.Id, this.CallDllFunction, this.StoreFiltersIds, this.BlockFiltersIds);
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static MonitorCallData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new MonitorCallData
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, MonitorCallTable.Id),
                    CallDllFunction = GeneralData.GetStringColData(row, selectionsArray, MonitorCallTable.CallDllFunction),
                    StoreFiltersIds = GeneralData.GetStringColData(row, selectionsArray, MonitorCallTable.StoreFiltersIds),
                    BlockFiltersIds = GeneralData.GetStringColData(row, selectionsArray, MonitorCallTable.BlockFiltersIds),
                };
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    { MonitorCallTable.Id, this.Id.ToSQLParm() },
                    { MonitorCallTable.CallDllFunction, this.CallDllFunction.ToSQLParm()},
                    { MonitorCallTable.StoreFiltersIds, this.StoreFiltersIds.ToSQLParm()},
                    {MonitorCallTable.BlockFiltersIds, this.BlockFiltersIds.ToSQLParm() },
                };
                
            }
        }

        /// <summary>
        /// Class for gathering basic data for a profile.
        /// </summary>
        public class ProfileData : IData
        {
            public int Id               = GeneralData.IntGen;
            public string MonitorsIds   = GeneralData.JArrayGen;
            public string TestsIds      = GeneralData.JArrayGen;
            public string OptionalProfilesIds = GeneralData.JArrayGen;
            public string InheritedProfiles = GeneralData.JArrayGen;

            public override string ToString()
            {
                return string.Format("Profile Id:{0} MonitorIds:{1} TestIds:{2}", this.Id, this.MonitorsIds, this.TestsIds);
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static ProfileData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new ProfileData()
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, ProfileTable.Id),
                    MonitorsIds = GeneralData.GetStringColData(row, selectionsArray, ProfileTable.MonitorsIds),
                    TestsIds = GeneralData.GetStringColData(row, selectionsArray, ProfileTable.TestsIds),
                    OptionalProfilesIds = GeneralData.GetStringColData(row, selectionsArray, ProfileTable.OptionalProfilesIds), 
                    InheritedProfiles = GeneralData.GetStringColData(row, selectionsArray, ProfileTable.InheritedProfiles)
                };
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>()
                {
                    { ProfileTable.Id, this.Id.ToSQLParm()},
                    { ProfileTable.MonitorsIds, this.MonitorsIds.ToSQLParm()},
                    { ProfileTable.TestsIds, this.TestsIds.ToSQLParm() },
                    { ProfileTable.OptionalProfilesIds, this.OptionalProfilesIds.ToSQLParm() },
                    { ProfileTable.InheritedProfiles, this.InheritedProfiles.ToSQLParm() }
                };
            }
        }        

        /// <summary>
        /// Class for gathering basic data for a misc.
        /// </summary>
        public class MiscData : IData
        {
            public int Type = GeneralData.IntGen;
            public string Data = GeneralData.StringGen;

            public MiscData()
            {
            }

            public MiscData(int type, string data)
            {
                this.Type = type;
                this.Data = data;
            }

            public override string ToString()
            {
                return this.ToJson();
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static MiscData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new MiscData
                {
                    Type = GeneralData.GetIntColData(row, selectionsArray, MiscTable.Type),
                    Data = GeneralData.GetStringColData(row, selectionsArray, MiscTable.Data)
                };
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>
                {
                    { MiscTable.Data, this.Data.ToSQLParm() },
                    {MiscTable.Type, this.Type.ToSQLParm() }
                };
            }
        }

        /// <summary>
        /// Class for gathering basic data for a product.
        /// </summary>
        public class ProductData : IData
        {
            public int Id = GeneralData.IntGen;
            public string Keys = GeneralData.StringGen;

            public ProductData()
            {
            }

            public ProductData(int id, string keys)
            {
                this.Id = id;
                this.Keys = keys;
            }

            /// <summary>
            /// Convert a row to the data object.
            /// </summary>
            /// <param name="row"></param>
            /// <param name="selectionsArray"></param>
            /// <returns></returns>
            public static ProductData GetRowData(DataRow row, string[] selectionsArray)
            {
                return new ProductData()
                {
                    Id = GeneralData.GetIntColData(row, selectionsArray, ProductTable.Id),
                    Keys = GeneralData.GetStringColData(row, selectionsArray, ProductTable.Keys)
                };
            }

            /// <summary>
            /// Get the object's data in a dictionary format.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, string> GetDict()
            {
                return new Dictionary<string, string>
                {
                    {ProductTable.Id, this.Id.ToSQLParm() },
                    {ProductTable.Keys, this.Keys.ToSQLParm() }
                };
            }
            
            public static string MalterKeysToJson(byte[] vector, byte[] key)
            {
                return string.Format("{{'Vector': '{0}', 'Key':'{1}'}}",
                    JsonConvert.SerializeObject(vector),
                   JsonConvert.SerializeObject(key));
            }

            public static byte[][] MalterKeysFromJson(string keysString)
            {
                JObject keysObj = JObject.Parse(keysString);
                byte[] vector = JsonConvert.DeserializeObject<byte[]>((string)keysObj.Property("Vector").Value);
                byte[] key = JsonConvert.DeserializeObject<byte[]>((string)keysObj.Property("Key").Value);
                return new byte[][] { vector, key };
            }
        }

        /// <summary>
        /// Class for identifying app type.
        /// </summary>
        public static class AppTypeClass
        {
            private enum AppType { Server = 1, Client = 2 };
            private static AppType Type { get; set; }
            public  static bool IsServer { get { return Type == AppType.Server; } }
            public static bool IsClient { get { return Type == AppType.Client; } }
            public static void SetAppType(bool isClient)
            {
                Type = isClient ? AppType.Client : AppType.Server;                    
            }
        }
        
        /// <summary>
        /// Enum for listing misc types.
        /// </summary>
        public enum MiscTypes
        {
            RawForbiddenFiles = 1,
            RawForbiddenRegKeys = 2,
            RawForbiddenDomains = 3
        }

        /// <summary>
        /// Extention methods for MiscTypes.
        /// </summary>
        public static class MiscExtensions
        {
            public static string GetMsg(this MiscTypes type)
            {
                switch (type)
                {
                    case MiscTypes.RawForbiddenFiles:
                        return "Raw Forbidden Files";
                    case MiscTypes.RawForbiddenDomains:
                        return "Raw Forbidden Domains";
                    case MiscTypes.RawForbiddenRegKeys:
                        return "Raw Forbidden Registry Keys";
                    default:
                        return "General Misc";
                }
            }
        }
        /// <summary>
        /// Enum for listing events types.
        /// </summary>
        public enum Events
        {
            GeneralEvent = 0,
            NewProcess = 1,
            TestPassed = 2,
            UpdateProfile = 3,
            AddMisc = 4,
            RemoveMisc = 5,
            NewProcessUpdate = 6,
            ProcessActionTaken = 7,
            SystemMessage = 8
        };

        /// <summary>
        /// Enum for listing actions that can be done on a process.
        /// </summary>
        public enum ProcessActions
        {
            KillProcess = 0,
            SetInnocent = 1,
            SetSuspect = 2,
            SetMalware = 3
        }

        /// <summary>
        /// Interface for data objects.
        /// </summary>
        internal interface IData { }        
    }
}

