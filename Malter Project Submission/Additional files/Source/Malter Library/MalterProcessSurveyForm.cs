﻿using MalterLib.MainMalter;
using MalterLib.MalterEngine;
using MalterLib.MalterObjects;
using System;
using System.Threading;
using System.Windows.Forms;
using static MalterLib.MalterGUI.MalterFormDetails;

namespace MalterLib
{
    namespace MalterGUI
    {
        /// <summary>
        /// Class for User to fill a process survey.
        /// </summary>
        class MalterProcessSurveyForm : Form
        {
            private Panel ProcessSurveyPanel;
            private RadioButton UserProcessAwarenessYes;
            private RadioButton UserProcessAwarenessNo;            
            private Button SubmitButton;
            
            private Label UserProcessAwarenessQuestion;
            private Label ProcessSurveyHeader;
            private Label SubjectProcessData;
            private Label ErrorMsgLabel;

            private GUIHandler ProcessSurveyHandler;
            public ManualResetEvent IsDone;
            private ProcessData SubjectProcess;

            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            public MalterProcessSurveyForm(GUIHandler processSurveyHandler, ProcessData processData)
            {
                this.ProcessSurveyHandler = processSurveyHandler;
                this.IsDone = new ManualResetEvent(false);
                this.SubjectProcess = processData;

                InitializeComponent();
                this.Icon = MalterForm.MalterIcon;
                this.ModfiyFonts();
                this.DisplaySubjectProcess();
                this.ModifyColors();    
            }

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.ProcessSurveyPanel = new System.Windows.Forms.Panel();
            this.ErrorMsgLabel = new System.Windows.Forms.Label();
            this.SubjectProcessData = new System.Windows.Forms.Label();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.ProcessSurveyHeader = new System.Windows.Forms.Label();
            this.UserProcessAwarenessYes = new System.Windows.Forms.RadioButton();
            this.UserProcessAwarenessNo = new System.Windows.Forms.RadioButton();
            this.UserProcessAwarenessQuestion = new System.Windows.Forms.Label();
            this.SubjectProcessHeader = new System.Windows.Forms.Label();
            this.ProcessSurveyPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProcessSurveyPanel
            // 
            this.ProcessSurveyPanel.Controls.Add(this.SubjectProcessHeader);
            this.ProcessSurveyPanel.Controls.Add(this.ErrorMsgLabel);
            this.ProcessSurveyPanel.Controls.Add(this.SubjectProcessData);
            this.ProcessSurveyPanel.Controls.Add(this.SubmitButton);
            this.ProcessSurveyPanel.Controls.Add(this.ProcessSurveyHeader);
            this.ProcessSurveyPanel.Controls.Add(this.UserProcessAwarenessYes);
            this.ProcessSurveyPanel.Controls.Add(this.UserProcessAwarenessNo);
            this.ProcessSurveyPanel.Controls.Add(this.UserProcessAwarenessQuestion);
            this.ProcessSurveyPanel.Location = new System.Drawing.Point(0, 0);
            this.ProcessSurveyPanel.Name = "ProcessSurveyPanel";
            this.ProcessSurveyPanel.Size = new System.Drawing.Size(487, 379);
            this.ProcessSurveyPanel.TabIndex = 0;
            // 
            // ErrorMsgLabel
            // 
            this.ErrorMsgLabel.AutoSize = true;
            this.ErrorMsgLabel.Location = new System.Drawing.Point(54, 173);
            this.ErrorMsgLabel.Name = "ErrorMsgLabel";
            this.ErrorMsgLabel.Size = new System.Drawing.Size(0, 20);
            this.ErrorMsgLabel.TabIndex = 6;
            // 
            // SubjectProcessData
            // 
            this.SubjectProcessData.AutoSize = true;
            this.SubjectProcessData.Location = new System.Drawing.Point(211, 126);
            this.SubjectProcessData.Name = "SubjectProcessData";
            this.SubjectProcessData.Size = new System.Drawing.Size(0, 20);
            this.SubjectProcessData.TabIndex = 5;
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(58, 322);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(91, 31);
            this.SubmitButton.TabIndex = 4;
            this.SubmitButton.Text = "Done";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // ProcessSurveyHeader
            // 
            this.ProcessSurveyHeader.AutoSize = true;
            this.ProcessSurveyHeader.Location = new System.Drawing.Point(54, 18);
            this.ProcessSurveyHeader.Name = "ProcessSurveyHeader";
            this.ProcessSurveyHeader.Size = new System.Drawing.Size(118, 20);
            this.ProcessSurveyHeader.TabIndex = 3;
            this.ProcessSurveyHeader.Text = "Process Survey";
            // 
            // UserProcessAwarenessYes
            // 
            this.UserProcessAwarenessYes.AutoSize = true;
            this.UserProcessAwarenessYes.Location = new System.Drawing.Point(58, 248);
            this.UserProcessAwarenessYes.Name = "UserProcessAwarenessYes";
            this.UserProcessAwarenessYes.Size = new System.Drawing.Size(282, 24);
            this.UserProcessAwarenessYes.TabIndex = 2;
            this.UserProcessAwarenessYes.TabStop = true;
            this.UserProcessAwarenessYes.Text = "I have no idea what this program is.";
            this.UserProcessAwarenessYes.UseVisualStyleBackColor = true;
            this.UserProcessAwarenessYes.CheckedChanged += new System.EventHandler(this.UserProcessAwarenessYes_CheckedChanged);
            // 
            // UserProcessAwarenessNo
            // 
            this.UserProcessAwarenessNo.AutoSize = true;
            this.UserProcessAwarenessNo.Location = new System.Drawing.Point(58, 206);
            this.UserProcessAwarenessNo.Name = "UserProcessAwarenessNo";
            this.UserProcessAwarenessNo.Size = new System.Drawing.Size(176, 24);
            this.UserProcessAwarenessNo.TabIndex = 1;
            this.UserProcessAwarenessNo.TabStop = true;
            this.UserProcessAwarenessNo.Text = "I know this program.";
            this.UserProcessAwarenessNo.UseVisualStyleBackColor = true;
            // 
            // UserProcessAwarenessQuestion
            // 
            this.UserProcessAwarenessQuestion.AutoSize = true;
            this.UserProcessAwarenessQuestion.Location = new System.Drawing.Point(54, 173);
            this.UserProcessAwarenessQuestion.Name = "UserProcessAwarenessQuestion";
            this.UserProcessAwarenessQuestion.Size = new System.Drawing.Size(201, 20);
            this.UserProcessAwarenessQuestion.TabIndex = 0;
            this.UserProcessAwarenessQuestion.Text = "Do you know this program?";
            // 
            // SubjectProcessHeader
            // 
            this.SubjectProcessHeader.AutoSize = true;
            this.SubjectProcessHeader.Location = new System.Drawing.Point(54, 82);
            this.SubjectProcessHeader.Name = "SubjectProcessHeader";
            this.SubjectProcessHeader.Size = new System.Drawing.Size(132, 20);
            this.SubjectProcessHeader.TabIndex = 7;
            this.SubjectProcessHeader.Text = "Subject Process: ";
            // 
            // MalterProcessSurveyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 379);
            this.Controls.Add(this.ProcessSurveyPanel);
            this.Name = "MalterProcessSurveyForm";
            this.Text = "Process Survey";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnFormClosing);
            this.ProcessSurveyPanel.ResumeLayout(false);
            this.ProcessSurveyPanel.PerformLayout();
            this.ResumeLayout(false);

            }            

            private void OnFormClosing(Object sender, FormClosingEventArgs e)
            {
                this.IsDone.Set();
            }

            public void DisplayError(MalterExitCodes code)
            {
                this.ErrorMsgLabel.Text = "Error: " + code.GetMSG();
            }

            public void DisplaySubjectProcess()
            {
                this.SubjectProcessData.Text = this.SubjectProcess.Name;
            }       

            private void SubmitButton_Click(object sender, System.EventArgs e)
            {
                Response response = (Response)this.ProcessSurveyHandler(
                    new ProcessSurvey(this.SubjectProcess, UserProcessAwarenessYes.Checked));
                if (response.Code != MalterExitCodes.Success)
                {
                    this.DisplayError(response.Code);
                    return;
                }

                this.Close();

            }
           
            private void UserProcessAwarenessYes_CheckedChanged(object sender, EventArgs e)
            {

            }

            private void ModfiyFonts()
            {
                this.UserProcessAwarenessYes.Font = MalterForm.ButtonsFont;
                this.UserProcessAwarenessNo.Font = MalterForm.ButtonsFont;
                this.SubmitButton.Font = MalterForm.ButtonsFont;

                this.UserProcessAwarenessQuestion.Font = MalterForm.RegularFont;
                this.ProcessSurveyHeader.Font = MalterForm.MediumHeadlineFont;
                this.SubjectProcessData.Font = MalterForm.RegularFont;
                this.ErrorMsgLabel.Font = MalterForm.RegularFont;
                this.SubjectProcessHeader.Font = MalterForm.RegularFont;
            }

            private void ModifyColors()
            {
                this.ProcessSurveyPanel.ForeColor = MalterForm.MalterColor2;
                this.ProcessSurveyPanel.BackColor = MalterForm.MalterColor1;
                this.UserProcessAwarenessYes.ForeColor = MalterForm.MalterColor2;
                this.UserProcessAwarenessNo.ForeColor = MalterForm.MalterColor2;
                this.SubmitButton.ForeColor = MalterForm.MalterColor1;
            }

            private Label SubjectProcessHeader;
        }
    }
}