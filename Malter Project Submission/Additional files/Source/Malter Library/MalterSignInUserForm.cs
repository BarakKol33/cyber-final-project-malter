﻿using MalterLib.MainMalter;
using MalterLib.MalterObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalterLib
{
    namespace MalterGUI
    {
        /// <summary>
        /// Class for signing in a user.
        /// </summary>
        public class MalterSignInUserForm : Form
        {
            private Panel SignUserPanel;
            private TextBox SignUserPasswordText;
            private TextBox SignUserNameText;
            private Label SignUserPasswordHeader;
            private Label SignUserNameHeader;
            private Label SignUserHeader;
            private Label ErrorMsgLabel;
            private Button SignButton;
            private UserAction SignInHandler;

            public ManualResetEvent IsDone;
            

            public MalterSignInUserForm(UserAction signInHandler)
            {
                this.SignInHandler = signInHandler;
                this.IsDone = new ManualResetEvent(false);
                InitializeComponent();
                this.ModifyFonts();
                this.ModifyColors();
                this.Icon = MalterForm.MalterIcon;
            }

            public void DisplayError(MalterExitCodes code)
            {
                this.ErrorMsgLabel.Text = "Error: "+code.GetMSG();
            }

            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                this.SignUserPanel = new System.Windows.Forms.Panel();
                this.ErrorMsgLabel = new System.Windows.Forms.Label();
                this.SignButton = new System.Windows.Forms.Button();
                this.SignUserPasswordText = new System.Windows.Forms.TextBox();
                this.SignUserNameText = new System.Windows.Forms.TextBox();
                this.SignUserPasswordHeader = new System.Windows.Forms.Label();
                this.SignUserNameHeader = new System.Windows.Forms.Label();
                this.SignUserHeader = new System.Windows.Forms.Label();
                this.SignUserPanel.SuspendLayout();
                this.SuspendLayout();
                // 
                // SignUserPanel
                // 
                this.SignUserPanel.Controls.Add(this.ErrorMsgLabel);
                this.SignUserPanel.Controls.Add(this.SignButton);
                this.SignUserPanel.Controls.Add(this.SignUserPasswordText);
                this.SignUserPanel.Controls.Add(this.SignUserNameText);
                this.SignUserPanel.Controls.Add(this.SignUserPasswordHeader);
                this.SignUserPanel.Controls.Add(this.SignUserNameHeader);
                this.SignUserPanel.Controls.Add(this.SignUserHeader);
                this.SignUserPanel.Location = new System.Drawing.Point(0, 0);
                this.SignUserPanel.Name = "SignUserPanel";
                this.SignUserPanel.Size = new System.Drawing.Size(423, 386);
                this.SignUserPanel.TabIndex = 0;
                // 
                // ErrorMsgLabel
                // 
                this.ErrorMsgLabel.AutoSize = true;                
                this.ErrorMsgLabel.Location = new System.Drawing.Point(170, 183);
                this.ErrorMsgLabel.Name = "ErrorMsgLabel";
                this.ErrorMsgLabel.Size = new System.Drawing.Size(0, 20);
                this.ErrorMsgLabel.TabIndex = 9;
                // 
                // SignButton
                // 
                this.SignButton.Location = new System.Drawing.Point(171, 244);
                this.SignButton.Name = "SignButton";
                this.SignButton.Size = new System.Drawing.Size(110, 32);
                this.SignButton.TabIndex = 8;
                this.SignButton.Text = "Sign In!";
                this.SignButton.UseVisualStyleBackColor = true;
                this.SignButton.Click += new System.EventHandler(this.SignButton_Click);
                // 
                // SignUserPasswordText
                // 
                this.SignUserPasswordText.Location = new System.Drawing.Point(156, 138);
                this.SignUserPasswordText.Name = "SignUserPasswordText";
                this.SignUserPasswordText.Size = new System.Drawing.Size(240, 26);
                this.SignUserPasswordText.TabIndex = 6;
                // 
                // SignUserNameText
                // 
                this.SignUserNameText.Location = new System.Drawing.Point(156, 77);
                this.SignUserNameText.Name = "SignUserNameText";
                this.SignUserNameText.Size = new System.Drawing.Size(240, 26);
                this.SignUserNameText.TabIndex = 5;
                // 
                // SignUserPasswordHeader
                // 
                this.SignUserPasswordHeader.AutoSize = true;
                this.SignUserPasswordHeader.Location = new System.Drawing.Point(27, 144);
                this.SignUserPasswordHeader.Name = "SignUserPasswordHeader";
                this.SignUserPasswordHeader.Size = new System.Drawing.Size(82, 20);
                this.SignUserPasswordHeader.TabIndex = 4;
                this.SignUserPasswordHeader.Text = "Password:";
                // 
                // SignUserNameHeader
                // 
                this.SignUserNameHeader.AutoSize = true;
                this.SignUserNameHeader.Location = new System.Drawing.Point(27, 77);
                this.SignUserNameHeader.Name = "SignUserNameHeader";
                this.SignUserNameHeader.Size = new System.Drawing.Size(89, 20);
                this.SignUserNameHeader.TabIndex = 1;
                this.SignUserNameHeader.Text = "UserName:";
                // 
                // SignUserHeader
                // 
                this.SignUserHeader.AutoSize = true;
                this.SignUserHeader.Location = new System.Drawing.Point(167, 24);
                this.SignUserHeader.Name = "SignUserHeader";
                this.SignUserHeader.Size = new System.Drawing.Size(97, 20);
                this.SignUserHeader.TabIndex = 0;
                this.SignUserHeader.Text = "Sign In User";
                // 
                // MalterSignInUserForm
                // 
                this.ClientSize = new System.Drawing.Size(422, 300);
                this.Controls.Add(this.SignUserPanel);
                this.Name = "MalterSignInUserForm";
                this.Load += new System.EventHandler(this.MalterSignInUserForm_Load);
                this.SignUserPanel.ResumeLayout(false);
                this.SignUserPanel.PerformLayout();
                this.ResumeLayout(false);
                this.FormClosing += new FormClosingEventHandler(this.OnFormClosing);
            }
           
            private void SignButton_Click(object sender, EventArgs e)
            {
                string userName = this.SignUserNameText.Text;
                string password = this.SignUserPasswordText.Text;
                Response response = (Response) this.SignInHandler(new UserData() { UserName = userName, Password = password });
                if (response.Code != MalterExitCodes.Success)
                {
                    this.DisplayError(response.Code);
                }
                else
                {
                    this.IsDone.Set();
                    this.Close();
                }
            }
            
            private void OnFormClosing(Object sender, FormClosingEventArgs e)
            {
                this.IsDone.Set();
            }

            private void MalterSignInUserForm_Load(object sender, EventArgs e)
            {

            }

            public void ModifyFonts()
            {
                this.SignUserPasswordText.Font = MalterForm.RegularFont;
                this.SignUserNameText.Font = MalterForm.RegularFont;
                this.SignUserPasswordHeader.Font = MalterForm.RegularFont;
                this.SignUserNameHeader.Font = MalterForm.RegularFont;
                this.SignUserHeader.Font = MalterForm.MediumHeadlineFont;
                this.ErrorMsgLabel.Font = MalterForm.RegularFont;
                this.SignButton.Font = MalterForm.ButtonsFont;
            }

            public void ModifyColors()
            {
                this.SignUserPanel.BackColor = MalterForm.MalterColor1;
                this.SignUserPanel.ForeColor = MalterForm.MalterColor2;
                this.SignButton.ForeColor = MalterForm.MalterColor1;
            }
        }
    }
}
