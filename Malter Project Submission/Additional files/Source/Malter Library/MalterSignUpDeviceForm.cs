﻿using MalterLib.MainMalter;
using MalterLib.MalterObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalterLib
{
    namespace MalterGUI
    {
        /// <summary>
        /// Class for signing up a device.
        /// </summary>
        public class MalterSignUpDeviceForm : Form
        {
            private Panel SignDevicePanel;
            private TextBox SignDeviceNameText;
            private Label SignDeviceNameHeader;
            private Label SignDeviceHeader;
            private Label ErrorMsgLabel;
            private Button SignButton;

            private DeviceAction SignUpHandler;
            public ManualResetEvent IsDone;
            

            public MalterSignUpDeviceForm(DeviceAction signUphandler)
            {
                this.IsDone = new ManualResetEvent(false);
                this.SignUpHandler = signUphandler;
                InitializeComponent();
                this.ModifyFonts();
                this.ModifyColors();
                this.Icon = MalterForm.MalterIcon;
            }

            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                
                this.SignDevicePanel = new System.Windows.Forms.Panel();
                this.SignButton = new System.Windows.Forms.Button();
                this.SignDeviceNameText = new System.Windows.Forms.TextBox();
                this.SignDeviceNameHeader = new System.Windows.Forms.Label();
                this.SignDeviceHeader = new System.Windows.Forms.Label();
                this.ErrorMsgLabel = new System.Windows.Forms.Label();
                this.SignDevicePanel.SuspendLayout();
                this.SuspendLayout();
                // 
                // SignDevicePanel
                // 
                this.SignDevicePanel.Controls.Add(this.ErrorMsgLabel);
                this.SignDevicePanel.Controls.Add(this.SignButton);
                this.SignDevicePanel.Controls.Add(this.SignDeviceNameText);
                this.SignDevicePanel.Controls.Add(this.SignDeviceNameHeader);
                this.SignDevicePanel.Controls.Add(this.SignDeviceHeader);
                this.SignDevicePanel.Location = new System.Drawing.Point(0, 0);
                this.SignDevicePanel.Name = "SignDevicePanel";
                this.SignDevicePanel.Size = new System.Drawing.Size(423, 248);
                this.SignDevicePanel.TabIndex = 0;
                // 
                // SignButton
                // 
                this.SignButton.Location = new System.Drawing.Point(156, 183);
                this.SignButton.Name = "SignButton";
                this.SignButton.Size = new System.Drawing.Size(110, 32);
                this.SignButton.TabIndex = 8;
                this.SignButton.Text = "Sign Up!";
                this.SignButton.UseVisualStyleBackColor = true;
                this.SignButton.Click += new System.EventHandler(this.SignButton_Click);
                // 
                // SignDeviceNameText
                // 
                this.SignDeviceNameText.Location = new System.Drawing.Point(156, 77);
                this.SignDeviceNameText.Name = "SignDeviceNameText";
                this.SignDeviceNameText.Size = new System.Drawing.Size(240, 26);
                this.SignDeviceNameText.TabIndex = 5;
                // 
                // SignDeviceNameHeader
                // 
                this.SignDeviceNameHeader.AutoSize = true;
                this.SignDeviceNameHeader.Location = new System.Drawing.Point(27, 77);
                this.SignDeviceNameHeader.Name = "SignDeviceNameHeader";
                this.SignDeviceNameHeader.Size = new System.Drawing.Size(107, 20);
                this.SignDeviceNameHeader.TabIndex = 1;
                this.SignDeviceNameHeader.Text = "Device Name:";
                // 
                // SignDeviceHeader
                // 
                this.SignDeviceHeader.AutoSize = true;
                this.SignDeviceHeader.Location = new System.Drawing.Point(167, 24);
                this.SignDeviceHeader.Name = "SignDeviceHeader";
                this.SignDeviceHeader.Size = new System.Drawing.Size(118, 20);
                this.SignDeviceHeader.TabIndex = 0;
                this.SignDeviceHeader.Text = "Sign Up Device";
                // 
                // label1
                // 
                this.ErrorMsgLabel.AutoSize = true;
                this.ErrorMsgLabel.Location = new System.Drawing.Point(158, 130);
                this.ErrorMsgLabel.Name = "label1";
                this.ErrorMsgLabel.Size = new System.Drawing.Size(107, 20);
                this.ErrorMsgLabel.TabIndex = 9;
                this.ErrorMsgLabel.Text = "";
                // 
                // MalterSignUpDeviceForm
                // 
                this.ClientSize = new System.Drawing.Size(422, 244);
                this.Controls.Add(this.SignDevicePanel);
                this.Name = "MalterSignUpDeviceForm";
                this.Load += new System.EventHandler(this.MalterSignUpUserForm_Load);
                this.SignDevicePanel.ResumeLayout(false);
                this.SignDevicePanel.PerformLayout();
                this.ResumeLayout(false);
                this.FormClosing += new FormClosingEventHandler(this.OnFormClosing);                
            }
           
            private void label1_Click(object sender, EventArgs e)
            {

            }

            private void textBox3_TextChanged(object sender, EventArgs e)
            {

            }

            private void SignButton_Click(object sender, EventArgs e)
            {
                string deviceName = this.SignDeviceNameText.Text;
                Response response = (Response) SignUpHandler(new DeviceData() { DeviceName = deviceName });
                if (response.Code != MalterExitCodes.Success)
                {
                    this.DisplayError(response.Code);
                }

                else
                {
                    this.Close();
                    this.IsDone.Set();
                }
                    
            }

            public void DisplayError(MalterExitCodes code)
            {
                this.ErrorMsgLabel.Text = "Error: " + code.GetMSG();
            }

            private void MalterSignUpUserForm_Load(object sender, EventArgs e)
            {

            }

            private void OnFormClosing(Object sender, FormClosingEventArgs e)
            {
                this.IsDone.Set();
            }
            
            private void ModifyFonts()
            {
                this.SignDeviceNameText.Font    = MalterForm.RegularFont;
                this.SignDeviceNameHeader.Font  = MalterForm.RegularFont;
                this.SignDeviceHeader.Font      = MalterForm.MediumHeadlineFont;
                this.ErrorMsgLabel.Font         = MalterForm.RegularFont;
                this.SignButton.Font            = MalterForm.ButtonsFont;
            }            

            private void ModifyColors()
            {
                this.SignDevicePanel.ForeColor = MalterForm.MalterColor2;
                this.SignDevicePanel.BackColor = MalterForm.MalterColor1;
                this.SignButton.ForeColor = MalterForm.MalterColor1;
                
            }
        }
    }
}
