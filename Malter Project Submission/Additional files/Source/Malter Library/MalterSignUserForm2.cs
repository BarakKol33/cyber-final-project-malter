﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MalterLib
{
    namespace MalterGUI
    {
        using MainMalter;
        using MalterObjects;
        using System.Drawing;
        using System.Threading;
        using System.Windows.Forms;
        /// <summary>
        /// Class for signing up a user.
        /// </summary>
        public class MalterSignUpUserForm : Form
        {
            private Panel SignUserPanel;
            private TextBox SignUserPrivateNameText;
            private TextBox SignUserPasswordText;
            private TextBox SignUserNameText;
            private TextBox SignUserEmailText;
            private Label SignUserPasswordHeader;
            private Label SignUserPrivateNameHeader;
            private Label SignUserNameHeader;
            private Label SignUpUserHeader;
            private Label SignEmailHeader;
            private Label ErrorMsgLabel;
            private Button SkipButton;
            private Button SignButton;            
            
            private UserAction SignUpHandler;
            public ManualResetEvent IsDone;
            

            public MalterSignUpUserForm(UserAction signUpHandler)
            {
                this.SignUpHandler = signUpHandler;
                this.IsDone = new ManualResetEvent(false);
                InitializeComponent();
                this.ModifyFonts();
                this.ModifyColors();
                this.Icon = MalterForm.MalterIcon;
            }

            /// <summary>
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary>
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            /// <summary>
            /// Required method for Designer support - do not modify
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
                this.SignUserPanel = new System.Windows.Forms.Panel();
                this.SkipButton = new System.Windows.Forms.Button();
                this.ErrorMsgLabel = new System.Windows.Forms.Label();
                this.SignEmailHeader = new System.Windows.Forms.Label();
                this.SignButton = new System.Windows.Forms.Button();
                this.SignUserEmailText = new System.Windows.Forms.TextBox();
                this.SignUserPrivateNameText = new System.Windows.Forms.TextBox();
                this.SignUserPasswordText = new System.Windows.Forms.TextBox();
                this.SignUserNameText = new System.Windows.Forms.TextBox();
                this.SignUserPasswordHeader = new System.Windows.Forms.Label();
                this.SignUserPrivateNameHeader = new System.Windows.Forms.Label();
                this.SignUserNameHeader = new System.Windows.Forms.Label();
                this.SignUpUserHeader = new System.Windows.Forms.Label();
                this.SignUserPanel.SuspendLayout();
                this.SuspendLayout();
                // 
                // SignUserPanel
                // 
                this.SignUserPanel.Controls.Add(this.SkipButton);
                this.SignUserPanel.Controls.Add(this.ErrorMsgLabel);
                this.SignUserPanel.Controls.Add(this.SignEmailHeader);
                this.SignUserPanel.Controls.Add(this.SignButton);
                this.SignUserPanel.Controls.Add(this.SignUserEmailText);
                this.SignUserPanel.Controls.Add(this.SignUserPrivateNameText);
                this.SignUserPanel.Controls.Add(this.SignUserPasswordText);
                this.SignUserPanel.Controls.Add(this.SignUserNameText);
                this.SignUserPanel.Controls.Add(this.SignUserPasswordHeader);
                this.SignUserPanel.Controls.Add(this.SignUserPrivateNameHeader);
                this.SignUserPanel.Controls.Add(this.SignUserNameHeader);
                this.SignUserPanel.Controls.Add(this.SignUpUserHeader);
                this.SignUserPanel.Location = new System.Drawing.Point(0, 0);
                this.SignUserPanel.Name = "SignUserPanel";
                this.SignUserPanel.Size = new System.Drawing.Size(423, 423);
                this.SignUserPanel.TabIndex = 0;
                // 
                // SkipButton
                // 
                this.SkipButton.Location = new System.Drawing.Point(243, 365);
                this.SkipButton.Name = "SkipButton";
                this.SkipButton.Size = new System.Drawing.Size(142, 32);
                this.SkipButton.TabIndex = 12;
                this.SkipButton.Text = "Skip To Sign In";
                this.SkipButton.UseVisualStyleBackColor = true;
                this.SkipButton.Click += new System.EventHandler(this.SkipButton_Click);
                // 
                // ErrorMsgLabel
                // 
                this.ErrorMsgLabel.AutoSize = true;
                this.ErrorMsgLabel.Location = new System.Drawing.Point(176, 311);
                this.ErrorMsgLabel.Name = "ErrorMsgLabel";
                this.ErrorMsgLabel.Size = new System.Drawing.Size(0, 20);
                this.ErrorMsgLabel.TabIndex = 11;
                // 
                // SignEmailHeader
                // 
                this.SignEmailHeader.AutoSize = true;
                this.SignEmailHeader.Location = new System.Drawing.Point(27, 257);
                this.SignEmailHeader.Name = "SignEmailHeader";
                this.SignEmailHeader.Size = new System.Drawing.Size(52, 20);
                this.SignEmailHeader.TabIndex = 8;
                this.SignEmailHeader.Text = "E-mail:";
                this.SignEmailHeader.Click += new System.EventHandler(this.label1_Click);
                // 
                // SignButton
                // 
                this.SignButton.Location = new System.Drawing.Point(66, 365);
                this.SignButton.Name = "SignButton";
                this.SignButton.Size = new System.Drawing.Size(110, 32);
                this.SignButton.TabIndex = 10;
                this.SignButton.Text = "Sign Up!";
                this.SignButton.UseVisualStyleBackColor = true;
                this.SignButton.Click += new System.EventHandler(this.SignButton_Click);
                // 
                // SignUserEmailText
                // 
                this.SignUserEmailText.Location = new System.Drawing.Point(156, 254);
                this.SignUserEmailText.Name = "SignUserEmailText";
                this.SignUserEmailText.Size = new System.Drawing.Size(240, 26);
                this.SignUserEmailText.TabIndex = 9;
                // 
                // SignUserPrivateNameText
                // 
                this.SignUserPrivateNameText.Location = new System.Drawing.Point(156, 198);
                this.SignUserPrivateNameText.Name = "SignUserPrivateNameText";
                this.SignUserPrivateNameText.Size = new System.Drawing.Size(240, 26);
                this.SignUserPrivateNameText.TabIndex = 7;
                this.SignUserPrivateNameText.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
                // 
                // SignUserPasswordText
                // 
                this.SignUserPasswordText.Location = new System.Drawing.Point(156, 138);
                this.SignUserPasswordText.Name = "SignUserPasswordText";
                this.SignUserPasswordText.Size = new System.Drawing.Size(240, 26);
                this.SignUserPasswordText.TabIndex = 6;
                // 
                // SignUserNameText
                // 
                this.SignUserNameText.Location = new System.Drawing.Point(156, 77);
                this.SignUserNameText.Name = "SignUserNameText";
                this.SignUserNameText.Size = new System.Drawing.Size(240, 26);
                this.SignUserNameText.TabIndex = 5;
                // 
                // SignUserPasswordHeader
                // 
                this.SignUserPasswordHeader.AutoSize = true;
                this.SignUserPasswordHeader.Location = new System.Drawing.Point(27, 141);
                this.SignUserPasswordHeader.Name = "SignUserPasswordHeader";
                this.SignUserPasswordHeader.Size = new System.Drawing.Size(82, 20);
                this.SignUserPasswordHeader.TabIndex = 4;
                this.SignUserPasswordHeader.Text = "Password:";
                // 
                // SignUserPrivateNameHeader
                // 
                this.SignUserPrivateNameHeader.AutoSize = true;
                this.SignUserPrivateNameHeader.Location = new System.Drawing.Point(27, 204);
                this.SignUserPrivateNameHeader.Name = "SignUserPrivateNameHeader";
                this.SignUserPrivateNameHeader.Size = new System.Drawing.Size(107, 20);
                this.SignUserPrivateNameHeader.TabIndex = 3;
                this.SignUserPrivateNameHeader.Text = "Private Name:";
                // 
                // SignUserNameHeader
                // 
                this.SignUserNameHeader.AutoSize = true;
                this.SignUserNameHeader.Location = new System.Drawing.Point(27, 77);
                this.SignUserNameHeader.Name = "SignUserNameHeader";
                this.SignUserNameHeader.Size = new System.Drawing.Size(89, 20);
                this.SignUserNameHeader.TabIndex = 1;
                this.SignUserNameHeader.Text = "UserName:";
                // 
                // SignUpUserHeader
                // 
                this.SignUpUserHeader.AutoSize = true;
                this.SignUpUserHeader.Location = new System.Drawing.Point(167, 24);
                this.SignUpUserHeader.Name = "SignUpUserHeader";
                this.SignUpUserHeader.Size = new System.Drawing.Size(104, 20);
                this.SignUpUserHeader.TabIndex = 0;
                this.SignUpUserHeader.Text = "Sign Up User";
                // 
                // MalterSignUpUserForm
                // 
                this.ClientSize = new System.Drawing.Size(422, 420);
                this.Controls.Add(this.SignUserPanel);
                this.Name = "MalterSignUpUserForm";
                this.Load += new System.EventHandler(this.MalterSignUpUserForm_Load);
                this.SignUserPanel.ResumeLayout(false);
                this.SignUserPanel.PerformLayout();
                this.ResumeLayout(false);
                this.FormClosing += new FormClosingEventHandler(this.OnFormClosing);                
            }
            
            private void label1_Click(object sender, EventArgs e)
            {

            }

            private void textBox3_TextChanged(object sender, EventArgs e)
            {

            }

            private void SignButton_Click(object sender, EventArgs e)
            {
                string userName = this.SignUserNameText.Text;
                string email = this.SignUserEmailText.Text;
                string password = this.SignUserPasswordText.Text;
                string privateName = this.SignUserPrivateNameText.Text;
                Response response = (Response)this.SignUpHandler(new UserData() { UserName = userName, Email = email, Password = password,  PrivateName=privateName});
                if (response.Code != MalterExitCodes.Success)
                {
                    this.DisplayError(response.Code);
                }
                else
                {
                    this.IsDone.Set();
                    this.Close();
                }
            }

            private void MalterSignUpUserForm_Load(object sender, EventArgs e)
            {

            }

            public void DisplayError(MalterExitCodes code)
            {
                this.ErrorMsgLabel.Text = "Error: " + code.GetMSG();
            }

            private void OnFormClosing(Object sender, FormClosingEventArgs e)
            {
                this.IsDone.Set();
            }
           
            private void SkipButton_Click(object sender, EventArgs e)
            {
                this.Close();
                this.IsDone.Set();
            }

            private void ModifyFonts()
            {
                this.SignUserPrivateNameText.Font   = MalterForm.RegularFont;
                this.SignUserPasswordText.Font      = MalterForm.RegularFont; 
                this.SignUserNameText.Font          = MalterForm.RegularFont;
                this.SignUserEmailText.Font         = MalterForm.RegularFont;
                this.SignUserPasswordHeader.Font    = MalterForm.RegularFont;
                this.SignUserPrivateNameHeader.Font = MalterForm.RegularFont;
                this.SignUserNameHeader.Font        = MalterForm.RegularFont;
                this.SignUpUserHeader.Font          = MalterForm.MediumHeadlineFont;
                this.SignEmailHeader.Font           = MalterForm.RegularFont;
                this.ErrorMsgLabel.Font             = MalterForm.RegularFont;
                this.SkipButton.Font                = MalterForm.ButtonsFont;
                this.SignButton.Font                = MalterForm.ButtonsFont;
            }

            private void ModifyColors()
            {
                this.SignButton.ForeColor = MalterForm.MalterColor1;
                this.SignUserPanel.ForeColor = MalterForm.MalterColor2;
                this.SignUserPanel.BackColor = MalterForm.MalterColor1;
                this.SkipButton.ForeColor = MalterForm.MalterColor1;                   
            }
        }
    }
}
