﻿using MalterLib.MalterData;
using MalterLib.GeneralOS.APIHooking;
using MalterLib.GeneralOS.OSAPI;
using MalterLib.MainMalter;
using MalterLib.MalterEngine;
using MalterLib.MalterEngine.MalwareBehavioralDetection;
using MalterLib.MalterNetwork;
using MalterLib.MalterObjects;
using MalterLib.ProcessMonitoring;
using MalterLib.MalterGUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MalterLib
{
    using MalterData.MalterDBDetails;
    using GeneralOS;
    using Microsoft.Win32;
    using System.IO; 

    /// <summary>
    /// Class for testing and checking different features and functions in the Malter project.
    /// </summary>
    public class MalterTests
    {
        #region Tests Methods
        public static void TestsMain()
        {
            //StartServerProcess();   System.Threading.Thread.Sleep(500);
            //StartServiceProcess();  
            ConfigureData.CreateTestObjects();
            MalterClient.Ping();
            Console.ReadKey();
                    
        }        
            
        public static void TestMain2()
        {           
            MalterServer.ServerMain();
        }

        public static void TestMain3()
        {
            MalterClient.ClientServiceMain();
        }

        public static void StartGUIProcess()
        {
            string path1 = @"C:\Users\Meir\Documents\Malter\repo\ConsoleApplication1\MalterFormApplication\bin\Debug\MalterFormApplication.exe";
            System.Diagnostics.Process.Start(path1);
        }

        public static void StartServiceProcess()
        {
            System.Diagnostics.Process.Start(@"C:\Users\Meir\Documents\Malter\repo\ConsoleApplication1\ConsoleApplication3\bin\Debug\ConsoleApplication3.exe");
        }

        public static void StartServerProcess()
        {
            string path1 = @"C:\Users\Meir\Documents\Malter\repo\ConsoleApplication1\ConsoleApplication2\bin\Debug\ConsoleApplication2.exe";
            System.Diagnostics.Process.Start(path1);
        }

        public static string EscapeJson(string value)
        {
            return string.Format("\u000b'{0}'", value);
        }

        public static void InjectProcessNoDBTest()
        {
            ProcessHookingManager manager = new ProcessHookingManager();

            //string cmd = "cmd.exe";
            string notepad = @"c:\windows\notepad.exe";
            //string notepadplus = @"C:\Program Files (x86)\Notepad++\notepad++.exe";
            //string pycharm = @"C:\Program Files (x86)\JetBrains\PyCharm Community Edition 2016.1.4\bin\pycharm.exe";

            Int32 targetPID = MalterLib.WindowsModule.WindowsInterface.RunAndGetPID(notepad);

            manager.Inject(targetPID);

            Console.WriteLine("Press ESCAPE key to quit...");
            while (Console.ReadKey(true).KeyChar != 27) ;

            System.Diagnostics.Process.GetProcessById(targetPID).Kill();

        }

        public static void InjectProcessWithDBTest()
        {
            ProcessHookingManager manager = new ProcessHookingManager();

            //string cmd = "cmd.exe";
            string notepad = @"c:\windows\notepad.exe";
            //string notepadplus = @"C:\Program Files (x86)\Notepad++\notepad++.exe";
            //string pycharm = @"C:\Program Files (x86)\JetBrains\PyCharm Community Edition 2016.1.4\bin\pycharm.exe";

            System.Diagnostics.Process proc = MalterLib.WindowsModule.WindowsInterface.RunAndGetWProcess(notepad);
            new DataInterface().NewProcess(new MalterLib.GeneralOS.Process(proc).GetDataStruct());
            manager.Inject(proc.Id);

            Console.WriteLine("Press ESCAPE key to quit...");
            while (Console.ReadKey(true).KeyChar != 27) ;

            System.Diagnostics.Process.GetProcessById(proc.Id).Kill();

        }

        public static void NewUserTest()
        {
            DataInterface db = new DataInterface();
            db.NewUser(new UserData() { Id = 1, PrivateName = "barak", UserName = "barakkol" });
        }

        public static void NewDeviceTest()
        {
            DataInterface db = new DataInterface();
            db.NewDevice(new DeviceData() { Id = 1, DeviceName = "General", OwnerId = 1 });
        }

        public static void CallJsonTests()
        {
            CreateFileW call = new CreateFileW("c:\\a.txt", 1,2,3);
            APICallData callData = call.GetDataStruct();
            APICall call2 = APICall.GetCallFromData(callData);
            Console.WriteLine(call);
            Console.WriteLine(callData);
            Console.WriteLine(call2);

            Console.WriteLine(new RegSetValueEx("a", "b", "c", "d", 1,2,3,"4").GetDataStruct());
        }

        public static void NewCallTest()
        {
            DataInterface db = new DataInterface();
            db.NewCall(new CreateFileW("a.txt").GetDataStruct());
        }

        public static void NewProcessTest()
        {
            DataInterface db = new DataInterface();
            //db.NewProcess(new ProcessData { Mid = 1089, Pid = 0, Name = "d", Path = @"C:\bla.exe", Hash = "f" });
            Process.NewProcess(new ProcessData { Name = "wow.exe", Path = @"c:\wow.exe" });
        }

        public static void ProcessStartedTest()
        {
            DataInterface db = new DataInterface();
            //db.NewProcess(new ProcessData { Mid = 2, Pid = 0, Name = "d", Path = @"C:\", Hash = "f" });

            db.ProcessStarted(mid: 1385, pid: 6024);
            //db.ProcessTerminated(mid: 2);
        }

        public static void GetAllProcessesTest()
        {
            DataInterface db = new DataInterface();
            ProcessData[] pd = db.GetAllProcesses();
            //Console.WriteLine(new MProcess(pd[0]));
            //MDebug.WriteLine(pd);
        }

        public static void GetAllCallsTest()
        {
            DataInterface db = new DataInterface();
            APICallData[] pd = db.GetAllCalls();
            Console.WriteLine(APICall.GetCallFromData(pd[0]));
            //Tests.PrintEnumrable(pd);
        }

        public static void CurrentDeviceAndUserTests()
        {
            DataInterface db = new DataInterface();
            db.SetCurrentDeviceId(1);
            db.SetCurrentUserId(1);

            Console.WriteLine(string.Format("Device:{0} \nUser:{1}", db.GetCurrentDeviceId(), db.GetCurrentUserId()));
        }

        public static void CurrentUserTest()
        {
            DataInterface db = new DataInterface();
            //db.NewUser(new UserData() { Id = 1, PrivateName = "public", UserName = "public" });

            Console.WriteLine(db.GetCurrentUser());
        }

        public static void CurrentDeviceTest()
        {
            DataInterface db = new DataInterface();
            //db.NewDevice(new DeviceData() { Id = 1, DeviceName = "General", OwnerId = 1 });

            Console.WriteLine(Device.GetObjectFromData(db.GetCurrentDevice()));
        }

        public static void SQLCommandsTests()
        {
            Dictionary<string, string> wdict = new Dictionary<string, string>() { { "col1", SQLiteDB.StringParm("val1") }, { "col2", SQLiteDB.StringParm("val2") } };
            //Dictionary<string, string> idict = new Dictionary<string, string>() { { "col1", "val1" }, { "col2", SQLiteDB.StringParm("val2") } };
            string table = "table";
            string where = SQLiteDB.GetWhereClause(wdict);
            //string where2 = SQLiteDB.GetWhereClause(wdict, format: "{0} OR {0}");
            string select = SQLiteDB.GetSelectStatement(table, new string[] { "id", "name" }, where);
            string insert = SQLiteDB.GetInsertStatement(table, wdict);
            string update = SQLiteDB.GetUpdateStatement(table, wdict, where);

            Console.WriteLine(select);
            Console.WriteLine(insert);
            Console.WriteLine(update);
        }

        public static void ProcessExistsTest()
        {

        }

        public static void GetProcessNameTest()
        {
            Console.WriteLine(new DataInterface().GetProcessName(6));
        }

        public static void RunningProcessesTest()
        {
            
        }

        public static void DeleteProcessesTest()
        {
            new DataInterface().DeleteProcessTable();
        }

        public static void NewAndSelectEventTest()
        {
            //  Checking translations:
            //SystemEvent e = new SystemEvent();
            //Console.WriteLine(e);
            //SystemEventData data = e.GetDataStruct();
            //Console.WriteLine(data);
            //Console.WriteLine(SystemEvent.GetEventByData(data));

            //  Checking NewProcess translations:
            //NewProcess p = new NewProcess(4, id:2);
            //Console.WriteLine(p);
            //SystemEventData data = p.GetDataStruct();
            //Console.WriteLine(data);
            //Console.WriteLine(SystemEvent.GetEventByData(data));

            DataInterface db = new DataInterface();
            //db.NewEvent(new SystemEventData { Id = 1, Time = "2", Type = 3, SEvent="" });
            Console.WriteLine(db.GetAllEvents());
        }

        public static void DllFuncJsonTest()
        {
            MalterLib.GeneralOS.DllFunction df = new MalterLib.GeneralOS.DllFunction() { DllName = "d", FunctionName = "f" };
            string dfjson = df.GetJson();
            MalterLib.GeneralOS.DllFunction df2 = MalterLib.GeneralOS.DllFunction.GetDataFromJson(dfjson);
            Console.WriteLine();
        }

        public static void NewMonitorCallTest()
        {
            DataInterface db = new DataInterface();
            MonitorCall mc = new MonitorCall(1, CreateFileW.CallDllFunction, new int[] { 1, 2 }, new int[] { 1, 4, });
            db.NewMonitorCall(mc.GetDataStruct());
            Console.WriteLine(db.GetMonitorCallById(1));

        }

        public static void GetMonitorByIdTest()
        {
            Console.WriteLine(new DataInterface().GetMonitorCallById(1));
        }

        public static void NewProfileTest()
        {
            new DataInterface().NewProfile(new Profile(Profiles.Suspect, new int[] { 0 }, new int[] { }).GetDataStruct());
        }

        public static void GetMonitorsTest()
        {
            MDebug.WriteLine(Profile.GetMonitorsForProfile(new DataInterface(), (Profiles)1));
        }

        public static void GetProcessProfileByMidTest()
        {
            Console.WriteLine(new DataInterface().GetProcessProfileByPid(840));
        }        

        public static void GetMonitorCallsByIdTest()
        {
            Console.WriteLine(MonitorCall.GetMonitorCallsById(new DataInterface(), new int[] { 1 }));
        }

        public static void ConfigureDataTest()
        {
            ConfigureData.ConfigureAllData();
        }

        public static void GetMonitorCallsByPidTest()
        {
            MDebug.WriteLine(new HookingServer().GetMonitorCalls(3240));
        }

        public static void TestMalterJsonData()
        {
            Console.WriteLine(new DataInterface().GetMalterJsonData(MalterLib.MalterData.MalterDBDetails.JsonDBKeys.DEVICEDATA));
        }

        public static void GetNewProcessIdTest()
        {
            new DataInterface().GetNewProcessId();
        }

        public static void GetNewDeviceIdTest()
        {
            new DataInterface().GetNewDeviceId();
        }

        public static void DeleteAllConfiguredDataTest()
        {
            new DataInterface().DeleteAllConfiguredData();
        }

        public static void ReConfigureAllDataTest()
        {
            ConfigureData.ReConfigureAllData();
        }

        public static void SetDBPasswordTest()
        {
            new DataInterface().SetDBPassword("123");
        }

        public static void FunctionsTest()
        {
            try
            {
                int tc = new ProcessDataProvider(calls:new APICall[] { }, pid:0).GetContext().ContextToken;
                Debug.Assert((bool)(new Contains("ab", "b").Execute(0)) == true);
                //Debug.Assert((bool)(new Contains(1, "b").Execute(0)) == false); // Assertion error
                Debug.Assert((bool)(new Contains("a", "b").Execute(0)) == false);
                Debug.Assert((bool)(new Cmp(1, 1).Execute(0)) == true);
                Debug.Assert((bool)(new Cmp(1, "1").Execute(0)) == false);
                Debug.Assert((bool)(new Matches(new string[] { "1", "9", "1", "3", "5", "2" }, new MatchesHelper((token, num) => (bool)new Cmp("1", num).Execute(token))).Execute(0)) == true);
                Debug.Assert((bool)(new Matches(new string[] { "2","3","4" }, new MatchesHelper((token, num) => (bool)new Cmp("1", num).Execute(token))).Execute(0)) == false);
                Debug.Assert((bool)(new TrueFunc().Execute(0)) == true);
                Debug.Assert((bool)(new FalseFunc().Execute(0)) == false);

                Debug.Assert((bool)(new Matches(new GetMisc(ProcessDataProvider.RAW_FORBIDDEN_FILES), new MatchesHelper((token, num) => (bool)new Cmp(@"C:\b.txt", num).Execute(token))).Execute(0)) == false);
                Debug.Assert((bool)(new And(new TrueFunc(), new TrueFunc()).Execute(0)) == true);
                Debug.Assert((bool)(new And(new TrueFunc(), new FalseFunc()).Execute(0)) == false);
                Debug.Assert((bool)(new Or(new TrueFunc(), new TrueFunc()).Execute(0)) == true);
                Debug.Assert((bool)(new Or(new TrueFunc(), new FalseFunc()).Execute(0)) == true);
                Debug.Assert((bool)(new Or(new FalseFunc(), new FalseFunc()).Execute(0)) == false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void TestsTest()
        {
            //CallTest test = new CallTest(new And(new TrueFunc(), new EndsWith(new GetProperty(APICall.FILE_NAME), ".txt")));
            CallTest test2 = new CallTest(new Cmp(new GetProperty("Name"), "CreateFileW")); // True
            APICall call = new CreateFileW("fa.txt");
            bool result = MalwareDetector.ExecuteTest(test2, new TestContext (JArray.FromObject(new APICall[] { call })));
            Console.WriteLine(result);  
        }

        public static void GetProcessMidTest()
        {
            Console.WriteLine(new DataInterface().GetProcessMid(7472));
        }

        public static void GetRecentCallsTest()
        {
            MDebug.WriteLine(AppResources.CurrentResources.db.GetRecentProcessAPICalls(15896, 30));
        }

        public static void MalwareDetectorTest()
        {
            new MalwareDetector(new DataInterface(), 10844).Detect();
        }
        
        public static void SignUpTest()
        {
            MalterServer s = new MalterServer();
            //Response result = s.SignUpDevice(new DeviceData { DeviceName = "Waa" });           
            //Response result = s.SignUpUser(new UserData { UserName = "", Password="afa" });
            //Console.WriteLine(result);
        }

        public static void AuthenticationTest()
        {
            MalterServer s = new MalterServer();
            //Response result = s.AuthenticateDevice(0, "XXW730");    
            //Response result = s.AuthenticateUser("Waa", "");
            //Console.WriteLine(result);
        }

        public static void GetByIdTest()
        {
            Console.WriteLine(new DataInterface().GetDeviceById(0));
            Console.WriteLine(new DataInterface().GetUserById(0));
        }

        public static void GetDataTest()
        {
            MalterServer s = new MalterServer();
            //Response result = s.GetDeviceData();    
            //Response result = s.GetUserData(0);  
            //Console.WriteLine(result);
        }

        public static void RequestSortersTest()
        {
            string json = (new GetDeviceData() { Id = 1 }).ToJson();
            GetDeviceData r = (GetDeviceData)Request.FromJson(json);
            Console.WriteLine(r.Id);
        }        

        public static void ClientTests()
        {
            ProxyClient pc = new ProxyClient(ConfigureData.GetConfiguredServerDetails());
            Response rSignUpDevice = pc.GetResponse(new SignUpDevice() { Data = new DeviceData() { OwnerId = 0, DeviceName = "awesome" } });
            Response rAuthenticateDevice = pc.GetResponse(new AuthenticateDevice() { Id = 1, Info = "" });
            Response rDeviceData = pc.GetResponse(new GetDeviceData() { Id = 0 });

            Response rSignUpUser = pc.GetResponse(new SignUpUser() { Data = new UserData() { UserName = "barak33", PrivateName = "barak", Password="123" } });
            Response rAuthenticateUser = pc.GetResponse(new AuthenticateUser() { UserName = "barak33", Password = "123" });
            Response rUserData = pc.GetResponse(new GetUserData() { Id = 1 });
            Response rProcessMid = pc.GetResponse(new GetProcessData() { Data = new ProcessData() { Path = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" } });

            Console.WriteLine(rSignUpDevice);
            Console.WriteLine();
            Console.WriteLine(rAuthenticateDevice);
            Console.WriteLine();
            Console.WriteLine(rDeviceData);
            Console.WriteLine();
            Console.WriteLine(rSignUpUser);
            Console.WriteLine();
            Console.WriteLine(rAuthenticateUser);
            Console.WriteLine();
            Console.WriteLine(rUserData);
            Console.WriteLine();
            Console.WriteLine(rProcessMid);
        }

        public static void ServerTests()
        {
            new MalterLib.MainMalter.MalterServer().Start();
        }

        public static void KillProcessTest()
        {
            GeneralOS.Process.KillProcess(8508);
        }

        public static void CheckCounterTest()
        {
            new DataInterface().IncreaseProcessCounter(2);
            Console.WriteLine(new DataInterface().GetProcessCheckCounter(2));
        }

        public static void Sha256Test()
        {
            Console.WriteLine(MalterCrypto.GetSHA256Hash("133"));
        }

        public static void Murmur3Test()
        {
            Console.WriteLine(Murmur3.MurmurHash3_x86_32(new byte[] { 2, 31, 19 }, 3, 3));
        }

        public static void ProcessNewCallTest()
        {
            GeneralOS.Process.NewCall(new CreateFileW("a.txt", mid:2).GetDataStruct(), new DataInterface());
        }

        public static void AddProcessToDeviceTest()
        {
            //new DataInterface().AddProcessToDevice(0, 2);
            bool t = new DataInterface().DoesDeviceHasProcess(0, 2);
            Console.WriteLine(t);
        }

        public static void SetProcessProfileTest()
        {
            new DataInterface().SetProcessProfile(2, 13);
        }

        public static void ProcessGetDataTest()
        {
            Console.WriteLine(new GeneralOS.Process().GetDataStruct());
        }

        public static void FilterProcessesTest()
        {
            Process p1 = new Process(pid: 1, path: "a");
            Process p2 = new Process(pid: 2, path: "b");
            Process p3 = new Process(pid: 3, path: "c");
            Process p4 = new Process(pid: 4, path: "d");

            ProcessMonitor.FilterOldProcesses(new Process[] { p1, p2 }, new Process[] {  });
        }

        public static void SignUpDeviceFormTest()
        {
            MalterSignUpDeviceForm form = new MalterSignUpDeviceForm((data) => { Console.WriteLine(data.ToString()); return new Response(null); });
            form.Start();
            form.IsDone.WaitOne();
        }

        public static void SignUpUserFormTest()
        {
            MalterSignUpUserForm form = new MalterSignUpUserForm((data) => { Console.WriteLine(data.ToString()); return new Response(null); });
            form.Start();
            form.IsDone.WaitOne();
            Console.WriteLine("wow");
        }

        public static void SignInUserFormTest()
        {
            MalterSignInUserForm form = new MalterSignInUserForm((data) => { Console.WriteLine(data.ToString()); return new Response(null, MalterExitCodes.BadInput); });
            form.Start();
            form.IsDone.WaitOne();
        }

        public static void ManualServerTest()
        {
            /*
            SocketServerDetails d = new SocketServerDetails() { IsManual = true };
            ProxyClient pc = new ProxyClient(d);

            Response rSignUpDevice = MalterClient.SignUpDevice(new DeviceData() { OwnerId = 0, DeviceName = "awesome" });
            Response rAuthenticateDevice = MalterClient.AuthenticateDevice(new DeviceData { Id = 1, Info = "" });

            Response rSignUpUser = MalterClient.SignUpUser(new UserData() { UserName = "barak33", PrivateName = "barak", Password = "123" });
            Response rAuthenticateUser = MalterClient.AuthenticateUser(new UserData{ UserName = "barak33", Password = "123" });
            Response rUserData = MalterClient.GetCurrentUserData(new UserData() { Id = 1 });

            Console.WriteLine(rSignUpDevice);
            Console.WriteLine();
            Console.WriteLine(rAuthenticateDevice);
            Console.WriteLine();
            Console.WriteLine(rSignUpUser);
            Console.WriteLine();
            Console.WriteLine(rAuthenticateUser);
            Console.WriteLine();
            Console.WriteLine(rUserData);
            */
        }

        public static void UpdateUserTest()
        {
            AppResources.CurrentResources.db.UpdateUserData(new UserData() { Id=1, UserName="barak31"});
        }

        public static void SQlAddColumnTest()
        {
            string column = SQLiteDB.GetAddColumnStatement("TestCounters", "Test2", SQLiteDB.INT_TYPE, 0.ToSQLParm());
            Console.WriteLine(column);
        }

        public static void GetProcessTestCounterTest()
        {
            //AppResources.CurrentResources.db.AddTestToCounters(2); Tested the add column method for the TestsCounters table.
            int counter = AppResources.CurrentResources.db.GetProcessTestCounter(1,1);
            Console.WriteLine(counter);
        }

        public static void IncreaseTestCounterTest()
        {
            AppResources.CurrentResources.db.IncreaseTestCounterForProcess(1, 1);
        }

        public static void UpdatePassedTestTest()
        {
            TestPassed p = new TestPassed(1, 1);
            Response r = AppResources.CurrentResources.PClient.GetResponse(
                new UpdatePassedTest() { Data= p.GetDataStruct() });
            Console.WriteLine(r);
        }

        public static void UpdateTestsDBTest()
        {
            SystemEventData test = new TestPassed(1,1).GetDataStruct();
            //AppResources.CurrentResources.db.InsertUpdateTests(new SystemEventData[] { test});
            //AppResources.CurrentResources.db.ClearUpdateTests();
            Console.WriteLine(MDebug.EnumrableToString(AppResources.CurrentResources.db.GetUpdateEvents()));
        }

        public static void GetTestsTest()
        {
            Console.WriteLine(MDebug.EnumrableToString(AppResources.CurrentResources.db.GetTestsIds()));
        }

        public static void GetTestsDetailsTest()
        {
            MDebug.WriteLine(ProcessDataProvider.GetProcessTestDetails(1391));
        }

        public static void ProfileTestsTest()
        {
            MalwareDetector.DetectProfile(1391);
        }

        public static void ProcessSurveyFormTest()
        {
            MalterProcessSurveyForm form = new MalterProcessSurveyForm(
                hparams => MalterClient.HandleProcessSurvey((ProcessSurvey)hparams[0]),
            new ProcessData() { Name = "wow.exe" , Mid=1391});
            form.Start();
            form.IsDone.WaitOne();
        }

        public static void SetProcessSurveyTest()
        {
            AppResources.CurrentResources.db.SetProcessDidSurvey(1391);
        }

        public static void UpdateServerTest()
        {
            new TestPassed(mid: 23, testId: 1).Signal(AppResources.CurrentResources.db);
            new TestPassed(mid: 23, testId: 1).Signal(AppResources.CurrentResources.db);
            new TestPassed(mid: 23, testId: 1).Signal(AppResources.CurrentResources.db);

            MalterClient.UpdateServer();
        }

        public static void MiscTest()
        {
            //AppResources.CurrentResources.db.AddMisc(new MiscData { Type = (int)MiscTypes.RawForbiddenFiles, Data = @"C:\a.txt" });
            //AppResources.CurrentResources.db.AddMisc(new MiscData { Type = (int)MiscTypes.RawForbiddenRegKeys, Data = @"s\t" });
            //AppResources.CurrentResources.db.AddMisc(new MiscData { Type = (int)MiscTypes.RawForbiddenFiles, Data = @"C:\b.txt" });
            //Console.WriteLine(AppResources.CurrentResources.db.GetAllMiscType((int)MiscTypes.RawForbiddenFiles).ToInfoString());
            //AppResources.CurrentResources.db.RemoveMisc(new MiscData { Type = (int)MiscTypes.RawForbiddenFiles, Data = @"C:\a.txt" });
            Console.WriteLine(AppResources.CurrentResources.db.GetMisc("d.txt").ToInfoString());

        }

        public static void RandomByteTest()
        {
            Console.WriteLine(MalterCrypto.RandomByteArray(20).ToInfoString());
        }

        public static void FileHashTest()
        {

            string slog = @"C:\Users\Meir\Documents\Malter\source\slog.txt";
            string ca1 = @"C:\Users\Meir\Documents\Malter\source\ConsoleApplication1.exe";
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine(GeneralOS.File.GetFileHash(slog));
            }

            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine(GeneralOS.File.GetFileHash(ca1));
            }
        }

        public static void MalwareDetectorMoreTests()
        {
            //ConfigureData.ConfigureMiscsInDB();
            //Console.WriteLine(JArray.FromObject(APICall.GetRecentCalls(AppResources.CurrentResources.db, 9456, 10)));
            new MalwareDetector(AppResources.CurrentResources.db, 14864).Detect();
        }

        public static void SecurityDataTest()
        {
            Console.WriteLine(AppResources.CurrentResources.db.GetMalterSecurityData());
        }

        public static void FilesDistTest()
        {
            //Console.WriteLine(JArray.FromObject(APICall.GetRecentCalls(AppResources.CurrentResources.db, 14864, 40)));
            //return;
            int tc = new ProcessDataProvider(AppResources.CurrentResources.db, pid: 14864).GetContext().ContextToken;
            Dictionary<string, int> d = (Dictionary<string, int>) new ExeFilesDist().Execute(tc);
            Console.WriteLine(d.ToInfoString());
            Console.WriteLine(new MaxFileDist(d).Execute(tc));
        }

        public static void ProductIdTests()
        {
            AppResources.CurrentResources.db.SetProductId(2);
            int currentId = AppResources.CurrentResources.db.GetThisProductId();
            Console.WriteLine(currentId);
        }

        public static void MalterDataTests()
        {
            //ConfigureData.ConfigureMalterDataInReg();
            AppResources.CurrentResources.db.SetCurrentDeviceId(1);
        }

        public static void ProfileFinalTests()
        {
            /*
            Dictionary<Tests, int> tdict1 = GetTestsDict();
            tdict1[Tests.ProcessSetInnocent] = 4;
            tdict1[Tests.UserAwareness] = 2;
            int a = MalwareDetector.InnocentPoints(tdict1);
            */

            Dictionary<Tests, int> tdict1 = GetTestsDict();
            tdict1[Tests.ProcessSetMalware] = 8;
            tdict1[Tests.ProcessKilled] = 4;
            int a = MalwareDetector.MalwarePoints(tdict1);

            Console.WriteLine(a);
        }

        public static Dictionary<Tests, int> GetTestsDict()
        {
            IEnumerable<Tests> values = Enum.GetValues(typeof(Tests)).Cast<Tests>();
            Dictionary<Tests, int> dict = new Dictionary<Tests, int>();
            foreach (Tests test in values)
                dict[test] = 0;
            return dict;
                        
        }

        public static void RequestJsonTest()
        {
                        string r = @"{
              'Data': null,
              'ClientDevice': {
                            'Id': 13,
                'DeviceName': 'My-PC 2',
                'OwnerId': 0,
                'Info': 'YOCK1V'
              },
              'ServiceName': 'GetProcessData'
            }";
            Console.WriteLine(Request.FromJson(r).ClientDevice.Id);
        }
        #endregion Tests Methods
    }
}
