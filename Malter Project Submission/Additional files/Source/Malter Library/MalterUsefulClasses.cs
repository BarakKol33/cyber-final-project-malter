﻿using MalterLib.MalterData;
using MalterLib.GeneralOS.OSAPI;
using MalterLib.MalterEngine;
using MalterLib.MalterEngine.MalwareBehavioralDetection;
using MalterLib.MalterGUI;
using MalterLib.MalterNetwork;
using MalterLib.MalterObjects;
using MalterLib.ProcessMonitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalterLib
{
    namespace MainMalter
    {
        using GeneralOS.OSAPI;
        /// <summary>
        /// Class for Configuring data in DB, or creating static objects for later use such 
        /// as network or database connection.
        /// </summary>
        public class ConfigureData
        {
            /// <summary>
            /// Call filters static database.
            /// </summary>
            private static IFilter[] FiltersDB = new IFilter[1];
            /// <summary>
            /// Process Tests static database.
            /// </summary>
            private static Dictionary<int, LightFunction> TestsDB = new Dictionary<int, LightFunction>();

            /// <summary>
            /// Creates the filters static database.
            /// </summary>
            public static void CreateFilters()
            {
                ConfigureData.FiltersDB[0] = new CallFilter(new TrueFunc());
            }

            /// <summary>
            /// Creates the tests static database.
            /// </summary>
            public static void CreateTests()
            {
                LightFunction trueTest = new OccuranceTest(new CallTest(new TrueFunc()), 1);
                trueTest.Id = (int)Tests.TrueTest;

                LightFunction hostsFileTest =
                        new OccuranceTest
                        (
                            new CallTest
                            (
                                new And
                                (
                                    new Cmp(new GetProperty(APICall.NAME),
                                        CreateFileW.CallDllFunction.FunctionName),
                                    new Contains(new GetProperty(APICall.FILE_NAME),
                                        @"C:\Windows\System32\drivers\etc\hosts")
                                )
                            ), 1
                    );
                hostsFileTest.Id = (int)Tests.HostsFile;

                LightFunction fileTest =
                    new OccuranceTest(
                        new CallTest(
                            new And(
                                new Or(
                                    new Cmp(new GetProperty(APICall.NAME), CreateFileW.CallDllFunction.FunctionName),
                                    new Cmp(new GetProperty(APICall.NAME), OpenFile.CallDllFunction.FunctionName)
                                    ),                             
                                new Matches(
                                    new GetMisc(ProcessDataProvider.RAW_FORBIDDEN_FILES),
                                    new MatchesHelper((token, item) =>
                                    new Cmp(item, new GetProperty(APICall.FILE_NAME)).Execute(token))
                                    )
                                )                            
                            ), 1
                        );

                fileTest.Id = (int)Tests.ForbiddenFiles;

                LightFunction regTest =
                    new OccuranceTest(
                        new CallTest(
                            new And(
                                new Cmp(new GetProperty(APICall.NAME), RegOpenKeyW.CallDllFunction.FunctionName),
                                new Matches(
                                    new GetMisc(ProcessDataProvider.RAW_FORBIDDEN_REG_KEYS),
                                    new MatchesHelper((token, item) =>
                                    new Cmp(item, new GetProperty(APICall.KEY)).Execute(token))
                                    )
                                )                            
                            ), 1
                        );

                regTest.Id = (int)Tests.ForbiddenRegKeys;

                LightFunction hostTest =
                    new OccuranceTest(
                        new CallTest(
                            new And(
                                new Cmp(new GetProperty(APICall.NAME), DNSQuery_W.CallDllFunction.FunctionName),
                                new Matches(
                                    new GetMisc(ProcessDataProvider.RAW_FORBIDDEN_DOMAINS),
                                    new MatchesHelper((token, item) =>
                                    new Cmp(item, new GetProperty(APICall.DOMAIN_NAME)).Execute(token))
                                    )
                                )                            
                            ), 1
                        );

                hostTest.Id = (int)Tests.ForbiddenDomains;

                LightFunction deleteFilesTest =
                    new OccuranceTest(
                        new CallTest(new Cmp(new GetProperty(APICall.NAME), DeleteFileA.CallDllFunction.FunctionName)),
                        10
                        );

                deleteFilesTest.Id = (int)Tests.DeleteFilesTest;

                LightFunction runFilesTest =
                    new OccuranceTest(
                        new CallTest(
                            new Cmp(new GetProperty(APICall.NAME), ShellExecuteA.CallDllFunction.FunctionName),
                            new EndsWith(new GetProperty(APICall.FILE_NAME), ".exe")
                            ),
                        5
                        );

                runFilesTest.Id = (int)Tests.RunFilesTest;


                LightFunction exeAmountTest =
                        new OccuranceTest(
                            new CallTest(new EndsWith(new GetProperty(APICall.FILE_NAME), ".exe")), 5
                            );
                exeAmountTest.Id = (int)Tests.ExeAmountTest;

                LightFunction exeDistTest = new BiggerThan(
                    new MaxFileDist(new ExeFilesDist()), 8
                    );
                exeDistTest.Id = (int)Tests.ExeDistTest;

                LightFunction bootWriteTest = new OccuranceTest(
                    new CallTest(new Contains(new GetProperty(APICall.FILE_NAME), @"\\.\")), 1
                    );
                bootWriteTest.Id = (int)Tests.BootWriteTest;

                TestsDB[trueTest.Id] = trueTest;

                TestsDB[hostTest.Id] = hostTest;
                TestsDB[regTest.Id] = regTest;
                TestsDB[fileTest.Id] = fileTest;

                TestsDB[runFilesTest.Id] = runFilesTest;
                TestsDB[exeAmountTest.Id] = exeAmountTest;
                TestsDB[exeDistTest.Id] = exeDistTest;

                TestsDB[bootWriteTest.Id] = bootWriteTest;
                TestsDB[deleteFilesTest.Id] = deleteFilesTest;                 
                TestsDB[hostsFileTest.Id] = hostsFileTest;
                                    
            }                      

            /// <summary>
            /// Gets a call filter by its id.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public static IFilter GetFilterById(int id)
            {
                //MainMalter.MDebug.WriteLine("Taking Filter: " + id.ToString());
                //return new CallFilter(new TrueFunc());
                return FiltersDB[id];
            }

            /// <summary>
            /// Gets a test by its id.
            /// </summary>
            /// <param name="id"></param>
            /// <returns></returns>
            public static LightFunction GetTestById(int id)
            {
                return ConfigureData.TestsDB[id];
            }

            /// <summary>
            /// Configures the Profiles in the database.
            /// </summary>
            /// <param name="db"></param>
            public static void ConfigureProfilesInDB(DataInterface db)
            {
                ProfileData suspect = new ProfileData
                {
                    Id = (int)Profiles.Suspect,
                    OptionalProfilesIds = JArray.FromObject(new int[] { (int)Profiles.Innocent,
                        (int)Profiles.Malware , (int)Profiles.Virus }).ToString().RemoveBR(),
                    MonitorsIds = "[0,2,3,4,5,6,7]",
                    TestsIds = JArray.FromObject(new int[] {
                        (int)Tests.BootWriteTest,
                        (int)Tests.ExeAmountTest,
                        (int)Tests.ExeDistTest,
                        (int)Tests.HostsFile,
                        (int)Tests.DeleteFilesTest,
                        (int)Tests.ForbiddenFiles,
                        (int)Tests.ForbiddenDomains,
                        (int)Tests.ForbiddenRegKeys,  
                        (int)Tests.RunFilesTest                      
                        }).ToString().RemoveBR(),
                };
                db.NewProfile(suspect);
                db.NewProfile(new ProfileData { Id = (int)Profiles.Innocent });                
                db.NewProfile(new ProfileData { Id = (int)Profiles.Malware });
                db.NewProfile(new ProfileData { Id = (int)Profiles.Virus ,
                    InheritedProfiles = string.Format("[{0}]", (int)Profiles.Malware)});
            }

            /// <summary>
            /// Configures the MonitorCalls in the database.
            /// </summary>
            /// <param name="db"></param>
            public static void ConfigureMonitorsInDB(DataInterface db)
            {
                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 0,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = CreateFileW.CallDllFunction.GetJson()
                });
                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 1,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = RegSetValueEx.CallDllFunction.GetJson()
                });
                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 2,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = OpenFile.CallDllFunction.GetJson()
                });
                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 3,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = WriteFile.CallDllFunction.GetJson()
                });

                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 4,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = DNSQuery_W.CallDllFunction.GetJson()
                });

                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 5,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = RegOpenKeyW.CallDllFunction.GetJson()
                });

                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 6,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = DeleteFileA.CallDllFunction.GetJson()
                });

                db.NewMonitorCall(new MonitorCallData
                {
                    Id = 7,
                    StoreFiltersIds = "[0]",
                    BlockFiltersIds = "[]",
                    CallDllFunction = ShellExecuteA.CallDllFunction.GetJson()
                });

            }

            /// <summary>
            /// Configures the TestsCounters in the database.
            /// </summary>
            public static void ConfigureTestsCountersInDB()
            {
                MalwareDetector.NewTest(Tests.UserAwareness);
                MalwareDetector.NewTest(Tests.HostsFile);
                MalwareDetector.NewTest(Tests.TrueTest);
                MalwareDetector.NewTest(Tests.ProcessKilled);
                MalwareDetector.NewTest(Tests.ProcessSetInnocent);
                MalwareDetector.NewTest(Tests.ProcessSetMalware);
                MalwareDetector.NewTest(Tests.ForbiddenFiles);
                MalwareDetector.NewTest(Tests.ForbiddenRegKeys);               
                MalwareDetector.NewTest(Tests.ForbiddenDomains);
                MalwareDetector.NewTest(Tests.DeleteFilesTest);
                MalwareDetector.NewTest(Tests.ExeDistTest);
                MalwareDetector.NewTest(Tests.BootWriteTest);
                MalwareDetector.NewTest(Tests.ExeAmountTest);
                MalwareDetector.NewTest(Tests.RunFilesTest);
            }

            /// <summary>
            /// Configures Miscs in the database.
            /// </summary>
            public static void ConfigureMiscsInDB()
            {
                string[] keys = new string[] {
                    @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders",
                    @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders",
                    @"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunServices",
                    @"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce",
                    @"HKEY_LOCAL_MACHINE\Software\Microsoft\Active Setup\Installed Components\KeyName"
                };
                string[] files = new string[] { @"AppData\Roaming\Microsoft\Templates\normal.dot" };
                string[] domains = new string[] { "www.mwsl.org.cn/", "openphish.com", "yhiltd.co.uk", "guyouellette.org" };


                foreach (string file in files)
                    AppResources.CurrentResources.db.AddMisc(new MiscData((int)MiscTypes.RawForbiddenFiles, file));

                foreach (string key in keys)
                    AppResources.CurrentResources.db.AddMisc(new MiscData((int)MiscTypes.RawForbiddenRegKeys, key));

                foreach (string domain in domains)
                    AppResources.CurrentResources.db.AddMisc(new MiscData((int)MiscTypes.RawForbiddenDomains, domain));
            }
                
            /// <summary>
            /// Configures all data in the database.
            /// </summary>
            public static void ConfigureAllData()
            {
                ConfigureData.ConfigureMonitorsInDB(AppResources.CurrentResources.db);
                ConfigureData.ConfigureProfilesInDB(AppResources.CurrentResources.db);
            }

            /// <summary>
            /// Configures all server basic data in the database.
            /// </summary>
            public static void ConfigureAllServerData()
            {
                ConfigureAllData();
                ConfigureMiscsInDB();
            }

            /// <summary>
            /// Delete all configured client data in the database.
            /// </summary>
            public static void DeleteAllConfiguredData()
            {
                AppResources.CurrentResources.db.DeleteAllConfiguredData();
            }            

            /// <summary>
            /// Reconfigures client data in the database.
            /// </summary>
            public static void ReConfigureAllData()
            {
                ConfigureData.DeleteAllConfiguredData();
                ConfigureData.ConfigureAllData();
            }


            /// <summary>
            /// Configures the MalterData in registry by the MalterData file.
            /// </summary>
            public static void ConfigureMalterDataInReg()
            {
                string dataFileName = MalterData.MalterDBDetails.DBFiles.MALTER_DATA_FILE;
                if (IsMalterDataFilePresent())
                {
                    string data = System.IO.File.ReadAllText(dataFileName);
                    AppResources.CurrentResources.db.SetMalterData(data);
                    System.IO.File.Delete(dataFileName);
                }                
            }

            /// <summary>
            /// Configures All registry data (if it is possible) using files present in
            /// Malter directory.
            /// </summary>
            public static void ConfigureDataFilesInReg()
            {
                ConfigureMalterDataInReg();
                ConfigureProductInReg(); 
            }

            /// <summary>
            /// Checks if the MalterData file is present.
            /// </summary>
            /// <returns></returns>
            public static bool IsMalterDataFilePresent()
            {
                return File.Exists(MalterData.MalterDBDetails.DBFiles.MALTER_DATA_FILE);
            }

            /// <summary>
            /// Checks if the Product file is present.
            /// </summary>
            /// <returns></returns>
            public static bool IsProductFilePresent()
            {
                return File.Exists(MalterData.MalterDBDetails.DBFiles.PRODUCT_FILE);
            }

            /// <summary>
            /// Configures Product data in the registry by the Product file.
            /// </summary>
            public static void ConfigureProductInReg()
            {
                if (IsProductFilePresent())
                {
                    string productFileName = MalterData.MalterDBDetails.DBFiles.PRODUCT_FILE;
                    ProductData product = AppResources.CurrentResources.db.GetProductDataFromFile(productFileName);
                    byte[][] keys = ProductData.MalterKeysFromJson(product.Keys);
                    int productId = product.Id;
                    AppResources.CurrentResources.db.SetProductId(product.Id);
                    AppResources.CurrentResources.db.SetThisProductByteKeys(keys);
                    System.IO.File.Delete(productFileName);
                }
            }

            /// <summary>
            /// Creates static objects for a testing app.
            /// </summary>
            public static void CreateTestObjects()
            {
                MDebug.CurrentDebug = MDebug.DebugType.Console;
                AppResources.CurrentResources = new AppResources(true);
                AppResources.CurrentResources.db = new DataInterface(IDataDetails.DefaultGUIClientDB,
                    () => (MalterCrypto.GetRandomString(8)));
                AppResources.CurrentResources.db.Connect();
                AppResources.CurrentResources.PClient = new ProxyClient(GetConfiguredServerDetails());
                CreateFilters();
                CreateTests();
                
            }

            /// <summary>
            /// Creates static objects for the malter client service.
            /// </summary>
            public static void CreateServiceObjects()
            {                
                MDebug.CurrentDebug = MDebug.DebugType.LogFile; // The service output is a log file
                AppResources.CurrentResources = new AppResources(true); 

                AppResources.CurrentResources.db = new DataInterface(IDataDetails.DefaultServiceClientDB,
                    () => (MalterCrypto.GetRandomString(8))); // Create Data interface
                ConfigureDataFilesInReg();
                AppResources.CurrentResources.db.Connect();
                AppResources.CurrentResources.db.ResetAllProcessesPids(); // Resetting PIDs from previous runs
                AppResources.CurrentResources.pm = new ProcessMonitor();    // Create the process monitor

                // Create The proxy client  
                AppResources.CurrentResources.PClient = 
                    new ProxyClient(GetConfiguredServerDetails());
                ProxyClient.Debug = false; // set debug options
                SQLiteDB.Debug = false;

                // Create some static objects
                CreateFilters();
                CreateTests();

                // Delete Former API calls 
                AppResources.CurrentResources.db.DeleteAPICalls();                                                              
            }

            /// <summary>
            /// Creates static objects for the malter client GUI.
            /// </summary>
            public static void CreateGUIObjects()
            {
                MDebug.CurrentDebug = MDebug.DebugType.MsgBox;
                AppResources.CurrentResources = new AppResources(true);
                AppResources.CurrentResources.form = new MalterForm(GetFormDetails());
                AppResources.CurrentResources.db = new DataInterface(IDataDetails.DefaultGUIClientDB);
                AppResources.CurrentResources.db.Connect();
                AppResources.CurrentResources.PClient = new ProxyClient(GetConfiguredServerDetails());
                ProxyClient.Debug = true;
            }

            /// <summary>
            /// Creates static objects for an injected app.
            /// </summary>
            public static void CreateHookedClientObjects()
            {                               
            }

            /// <summary>
            /// Creates static objects for a malter server.
            /// </summary>
            public static void CreateServerObjects()
            {
                SQLiteDB.Debug = true;
                MDebug.CurrentDebug = MDebug.DebugType.LogFile;
                AppResources.CurrentResources = new AppResources(false);                
                AppResources.CurrentResources.db = new DataInterface(IDataDetails.DefaultServerDB);
                AppResources.CurrentResources.db.Connect();
            }          

            /// <summary>
            /// Gets MalterFormDetails for a MalterForm.
            /// </summary>
            /// <returns></returns>
            public static MalterFormDetails GetFormDetails()
            {
                MalterFormDetails mfd = new MalterFormDetails();
                mfd.KillProcessHandler = GeneralOS.Process.KillProcess;
                mfd.SetInnocentHandler = GeneralOS.Process.SetProcessProfileInnocent;
                mfd.SetSuspectHandler = GeneralOS.Process.SetProcessProfileSuspect;
                mfd.SetMalwareHandler = GeneralOS.Process.SetProcessProfileMalware;
                mfd.SignUpUser = MalterClient.SignUpUser;
                mfd.SignUpDevice = MalterClient.SignUpDevice;
                mfd.AuthenticateDevice = MalterClient.AuthenticateDevice;
                mfd.AuthenticateUser = MalterClient.AuthenticateUser;                
                mfd.ProcessSurveyHandler = hparams => MalterClient.HandleProcessSurvey((ProcessSurvey)hparams[0]);              
                mfd.RefreshMonitors = hparams => { new Thread(MalterClient.UpdateMonitor).Start(); return null; };
                mfd.RemoveProcessHandler = data => { MalterClient.RemoveProcess(data); return null; };

                mfd.GetCurrentUser = MalterClient.GetCurrentUser;
                mfd.GetCurrentDevice = MalterClient.GetCurrentDevice;
                mfd.GetSecurityInfo = data => MalterClient.GetSecurityData();
                mfd.GetNotifications = data => AppResources.CurrentResources.db.GetRecentNotifications();
                mfd.GetProcesses = data => AppResources.CurrentResources.db.GetAllProcesses();                
                return mfd;
            }
           
            /// <summary>
            /// Gets the server that was configured for the app.
            /// </summary>
            /// <returns></returns>
            public static SocketServerDetails GetConfiguredServerDetails()
            {
                return AppResources.CurrentResources.db.GetThisServerDetails()
                    .FromJson<SocketServerDetails>();
        }
        }       

        /// <summary>
        /// Class used for displaying debug messages or performing certain actions on strings. 
        /// </summary>
        public class MDebug
        {
            public enum DebugType { Form, Console, LogFile, MsgBox, None};
            /// <summary>
            /// The debug output type of the application.
            /// </summary>
            public static DebugType CurrentDebug = DebugType.None;
            public static string LogFile = @"C:\Users\Meir\Documents\Malter\source\logc2.txt";
            public static bool IsForm { get { return CurrentDebug == DebugType.Form; } }

            /// <summary>
            /// Converts an enumerable to an understandable string.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="e"></param>
            /// <returns></returns>
            public static string EnumrableToString<T>(IEnumerable<T> e)
            {
                return "E: [" + string.Join(",", e) + "]";
            }

            /// <summary>
            /// Splits a string into chunks.
            /// </summary>
            /// <param name="str">Source string.</param>
            /// <param name="chunkSize">Constant chunk size.</param>
            /// <returns></returns>
            public static string[] Split(string str, int chunkSize)
            {
                List<string> pieces = new List<string>();
                int i = 0;
                while (i <= str.Length)
                {
                    if (str.Length - i >= chunkSize)
                        pieces.Add(str.Substring(i, chunkSize));
                    else
                        pieces.Add(str.Substring(i, str.Length - i));
                    i += chunkSize;
                }
                return pieces.ToArray();

            }

            /// <summary>
            /// Splits a string into chunks and joins them by the seperator.
            /// </summary>
            /// <param name="str">Source string.</param>
            /// <param name="chunckSize">Constant chunk size.</param>
            /// <param name="sep">Seperator</param>
            /// <returns></returns>
            public static string SplitBySeparator(string str, int chunckSize, string sep = "\n")
            {
                return string.Join(sep, MDebug.Split(str, chunckSize));
            }

            /// <summary>
            /// Write a message to the output of the application.
            /// </summary>
            /// <param name="msg"></param>
            public static void WriteLine(string msg)
            {                
                switch (CurrentDebug)
                {
                    case DebugType.Console:
                        System.Console.WriteLine(msg);
                        return;                    
                    case DebugType.LogFile:
                        AppResources.CurrentResources.db.LogMsg(msg);
                        return;
                    case DebugType.MsgBox:
                        //System.Windows.Forms.MessageBox.Show(msg, "Malter");
                        return;
                }                    
            }

            /// <summary>
            /// Writes an enumerable to the output of the application.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="e"></param>
            public static void WriteLine<T>(IEnumerable<T> e)
            {
                MDebug.WriteLine(MDebug.EnumrableToString(e));
            }

            /// <summary>
            /// Convert a string to its literal form.
            /// </summary>
            /// <param name="input"></param>
            /// <returns></returns>
            public static string ToLiteral(string input)
            {
                int i = 0;
                const int CHUNCK = 70;
                string literalChunk, nextChunk;
                bool stringEnded = false;
                StringBuilder sb = new StringBuilder();
                while (!stringEnded)
                {
                    if (input.Length - i >= CHUNCK)
                    {
                        nextChunk = input.Substring(i, CHUNCK);
                        i += CHUNCK;
                    }                        
                    else
                    {
                        nextChunk = input.Substring(i, input.Length - i);                        
                        stringEnded = true;
                    }
                    literalChunk = ChunkToLiteral(nextChunk);
                    sb.Append(literalChunk.Substring(1, literalChunk.Length - 2));                    
                }
                return "\""+sb.ToString()+"\"";
            }

            /// <summary>
            /// Converts a chunk of characters to a literal string.
            /// </summary>
            /// <param name="input"></param>
            /// <returns></returns>
            public static string ChunkToLiteral(string input)
            {
                using (var writer = new StringWriter())
                {
                    using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                    {
                        provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                        return writer.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Class for extending existing classes.
        /// </summary>
        public static class ObjectExtensions
        {
            /// <summary>
            /// Convert a string to an SQL parameter.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string ToSQLParm(this String st)
            {
                st = st.Replace("'", "''");
                return "'" + st + "'";
            }

            /// <summary>
            /// Convert an int to an SQL parameter.
            /// </summary>
            /// <param name="num"></param>
            /// <returns></returns>
            public static string ToSQLParm(this Int32 num)
            {
                return num.ToString();
            }

            /// <summary>
            /// Convert a string to a JSON parameter.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string ToJSONParm(this String st)
            {
                return string.Format("'{0}'", MDebug.ToLiteral(st));
            }

            /// <summary>
            /// Convert a JSON to an organized JSON, with tabs.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string ToNeatJson(this String st)
            {
                try
                {
                    return JObject.Parse(st).ToString();
                }
                catch
                {
                    return st;
                }
                
            }

            /// <summary>
            /// Convert object to JSON.
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public static string ToJson(this Object obj)
            {
                return JsonConvert.SerializeObject(obj);
            }

            /// <summary>
            /// Convert JSON string to an object.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="obj"></param>
            /// <returns></returns>
            public static T FromJson<T>(this object obj)
            {
                return JsonConvert.DeserializeObject<T>(obj.ToString());
            }

            /// <summary>
            /// Convert a string to its literal form.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string ToLiteral(this String st)
            {
                return  MDebug.ToLiteral(st);
            }

            /// <summary>
            /// Remove Line-breaks from a string.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static string RemoveBR(this String st)
            {
                string newSt =  st.Replace("\n", "");
                newSt = newSt.Replace("\r", "");
                return newSt;
            }

            /// <summary>
            /// Start a form.
            /// </summary>
            /// <param name="form"></param>
            public static void Start(this Form form)
            {
                Application.EnableVisualStyles();
                new Thread(() => { Application.Run(form); }).Start();
            }
            
            /// <summary>
            /// Convert an enumerable to a string what points out its items.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="e"></param>
            /// <returns></returns>
            public static string ToInfoString<T>(this IEnumerable<T> e)
            {
                return MDebug.EnumrableToString(e);
            }

            /// <summary>
            /// Converts byte array to string.
            /// </summary>
            /// <param name="bytes"></param>
            /// <returns></returns>
            public static string ConvertToString(this byte[] bytes)
            {
                return System.Text.Encoding.UTF8.GetString(bytes);
            }

            /// <summary>
            /// Converts a string to a byte array.
            /// </summary>
            /// <param name="st"></param>
            /// <returns></returns>
            public static byte[] ConvertToBytes(this String st)
            {
                return Encoding.ASCII.GetBytes(st);
            }            
        }
    }
}
    