using MalterLib.MainMalter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MalterLib
{
    namespace MalterNetwork
    {
        /// <summary>
        /// Class for passing client requests from communication to their logic, in server side.
        /// </summary>
        public class ProxyServer
        {
            /// <summary>
            /// Requst Handler for server. 
            /// </summary>
            public RequestHandler Handler;
            /// <summary>
            /// Means of communication for server.
            /// </summary>
            public SecureSocketServer SServer;
            /// <summary>
            /// Handles an incoming request.
            /// </summary>
            /// <param name="request"></param>
            /// <returns></returns>
            public delegate Response RequestHandler(Request request);
            /// <summary>
            /// Make the server log his acions.
            /// </summary>
            public static bool Debug = true;
            /// <summary>
            /// Create a new ProxyServer instance.
            /// </summary>
            /// <param name="handler"></param>
            /// <param name="encryptorProvider">Delegate that makes encryption possible.</param>
            /// <param name="sDetails">Connection details</param>
            public ProxyServer(RequestHandler handler, GetEncryptor encryptorProvider,
                SocketServerDetails sDetails)
            {
                this.SServer = new SecureSocketServer(encryptorProvider, sDetails,
                    StringResponseHandler);
                this.Handler = handler;
            }

            /// <summary>
            /// Start the server.
            /// </summary>
            public void Start()
            {
                if (Debug) MDebug.WriteLine("Start Proxy Server");
                this.SServer.Start();
                if (Debug) MDebug.WriteLine("End Proxy Server ");
            }
            
            /// <summary>
            /// Handles incoming messages from the client.
            /// </summary>
            /// <param name="requestJson"></param>
            /// <returns></returns>
            public string StringResponseHandler(string requestJson)
            {
                if (requestJson == null || requestJson == "")
                    return "";;
                if (Debug)
                    MDebug.WriteLine(string.Format("Got Request: {0}", requestJson.ToNeatJson()));
                Request request = Request.FromJson(requestJson); // object -> string 
                Response response = this.Handler(request);  // Handle request. 
                string responseJson = response.ToJson();    // string -> object 
                if (Debug) MDebug.WriteLine(string.Format("Sending Response {0}", response));
                return responseJson;
            }

            /// <summary>
            /// Close the server.
            /// </summary>
            public void Close()
            {
                this.SServer.IsDone = true;
                this.SServer.acceptDone.Set();
            }
        }

        /// <summary>
        /// Class for passing client requests from client functions to communication with the server.
        /// </summary>
        public class ProxyClient
        {
            /// <summary>
            /// Makes the client log its actions.
            /// </summary>
            public static bool Debug = false;

            /// <summary>
            /// Server's connection details.
            /// </summary>
            public SocketServerDetails SDetails;

            /// <summary>
            /// Creates a new ProxyClient instance.
            /// </summary>
            /// <param name="sdetails"></param>
            public ProxyClient(SocketServerDetails sdetails)
            {
                this.SDetails = sdetails;
            }

            public Response GetResponse(Request request)
            {
                if (this.SDetails.IsManual)
                {
                    return new MalterServer().GetResponse(request);
                }
                else
                {
                    if (Debug)  
                    {
                        // Log some messages
                        string message = "Start Proxy Client";
                        if (!this.SDetails.IsManual)
                            message += string.Format(" On {0} {1}", this.SDetails.SIPAddress, this.SDetails.Port);
                        MDebug.WriteLine(message);
                    }
                        
                    string requestString = request.ToJson();    // object -> string
                    if (Debug) MDebug.WriteLine(string.Format("Sending Request: {0}", requestString.ToNeatJson()));
                    // Send request.
                    string responseString = SecureSocketClient.GetResponse(new AESEncryptor(AppResources.CurrentResources.db.GetThisProductByteKeys()),
                        requestString,this.SDetails, productId:AppResources.CurrentResources.db.GetThisProductId());                       
                   
                    Response response;
                    if (responseString != null) // Server was reached
                    {
                        response = Response.FromJson(responseString);   // string -> object
                        
                    }
                    else // Server couldn't be reached
                    {
                        response = new Response(request, MalterExitCodes.ServerNotAvailable);
                        responseString = response.ToString();
                    }

                    if (Debug)
                    {
                        MDebug.WriteLine("Got Response: " + responseString.ToNeatJson());
                        MDebug.WriteLine("End Proxy Client");
                    }
                    return response;
                }
            }
        }

        /// <summary>
        /// Class for saving session related data.
        /// </summary>
        public class StateObject
        {            
            /// <summary>
            /// Message end reached.
            /// </summary>
            public const string END = "<EOF>";  
            /// <summary>
            /// Client to server product ID end reached 
            /// i.e "1<DATA>..."
            /// </summary>
            public const string DATA = "<DATA>";
            /// <summary>
            /// Client socket.   
            /// </summary>
            public Socket workSocket = null;
                        
            /// <summary>
            /// Size of receive buffer. 
            /// </summary>
            public const int BufferSize = 256;
            
            /// <summary>
            /// Receive buffer.   
            /// </summary>
            public byte[] buffer = new byte[BufferSize];
            
            /// <summary>
            /// Received data string.   
            /// </summary>
            public List<byte> currrentByteData = new List<byte>();
            /// <summary>
            /// Received data bits list.
            /// </summary>
            public List<string> DataList = new List<string>();
            /// <summary>
            /// Client encryptor.
            /// </summary>
            public IEncryptor Encryptor;
            /// <summary>
            /// Is done sending the message.
            /// </summary>
            public ManualResetEvent sendDone = new ManualResetEvent(false);
            /// <summary>
            /// Is done receiving the message.
            /// </summary>
            public ManualResetEvent receiveDone = new ManualResetEvent(false);

            /// <summary>
            /// Get the last data that was received.
            /// </summary>
            /// <returns></returns>
            public string GetLastData()
            {
                if (this.DataList.Count > 0)
                    return this.DataList[this.DataList.Count - 1];
                else
                    return null;
            }

            /// <summary>
            /// Add a bit to the bits list.
            /// </summary>
            /// <param name="byteData"></param>
            public void AddDataPart(byte[] byteData)
            {
                currrentByteData.AddRange(byteData);
            }

            /// <summary>
            /// Check if received the whole message.
            /// </summary>
            /// <returns></returns>
            public bool IsFinishedReading()
            {
                return ByteToString(this.currrentByteData.ToArray()).Contains(END);
            }

            public byte[] GetCurrentData()
            {
                return currrentByteData.ToArray();
            }

            public void EndReading()
            {
                this.DataList.Add(ByteToString(currrentByteData.ToArray()));
            }

            /// <summary>
            /// Convert string to byte array.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static byte[] StringToByte(string data)
            {
                return Encoding.ASCII.GetBytes(data);
            }

            /// <summary>
            /// Convert byte array to string.
            /// </summary>
            /// <param name="byteData"></param>
            /// <returns></returns>
            public static string ByteToString(byte[] byteData)
            {
                return System.Text.Encoding.UTF8.GetString(byteData);
            }

            /// <summary>
            /// Prepare message for sending.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static byte[] PrepareForSending(string data)
            {
                return StringToByte(data + END);
            }

            /// <summary>
            /// Prepare a message for receiving.
            /// </summary>
            /// <param name="byteData"></param>
            /// <returns></returns>
            public static string PrepareForReceiveing(byte[] byteData)
            {
                string stringData = ByteToString(byteData);
                string Strippeddata = stringData.Substring(0, stringData.IndexOf(END));
                return Strippeddata;
            }
        }

        /// <summary>
        /// Holds server connection details.
        /// </summary>
        public struct SocketServerDetails
        {
            /// <summary>
            /// Is the server manual (no socket connection.)
            /// </summary>
            [JsonIgnore]
            public bool IsManual;
            /// <summary>
            /// Server's IP address.
            /// </summary>
            public string SIPAddress;
            /// <summary>
            /// Server's port.
            /// </summary>
            public int Port;            
        }

        /// <summary>
        /// Class for server-side socket communication.
        /// based on : https://msdn.microsoft.com/en-us/library/fx6588te(v=vs.110).aspx
        /// </summary>
        public class SocketServer
        {
            /// <summary>
            /// Server's port
            /// </summary>
            public int Port;
            /// <summary>
            /// Server's IP adderess.
            /// </summary>
            public IPAddress SIPAddress;
            /// <summary>
            /// Server's socket.
            /// </summary>
            public Socket SSocket;
            public IPEndPoint LocalEndPoint;
            public ManualResetEvent acceptDone = new ManualResetEvent(false);
            /// <summary>
            /// Handles incoming requests
            /// </summary>
            public RequestHandler RHandler;
            public delegate string RequestHandler(string requestString);
            public bool IsDone = false;
            private bool Failed = false;
            public SocketServer(SocketServerDetails details, RequestHandler handler)
            {
                this.SIPAddress = IPAddress.Parse(details.SIPAddress);
                this.Port = details.Port;
                this.RHandler = handler;
                this.LocalEndPoint = new IPEndPoint(this.SIPAddress, this.Port);
                // Create a TCP/IP socket.  
                this.SSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
            }

            /// <summary>
            /// Start the server.
            /// </summary>
            public void Start()
            {
                // Bind the socket to the local endpoint and listen for incoming connections.  
                try
                {
                    //Console.WriteLine("Starting Accept");
                    SSocket.Bind(this.LocalEndPoint);
                    this.SSocket.Listen(100);

                    while (true)
                    {
                        if (IsDone) return;
                        // Set the event to nonsignaled state.  
                        acceptDone.Reset();

                        // Start an asynchronous socket to listen for connections.  
                        MDebug.WriteLine(string.Format("Socket Server Waiting for a connection On {0} {1}...", this.SIPAddress, this.Port));
                        this.SSocket.BeginAccept(new AsyncCallback(AcceptCallback), this.SSocket);

                        // Wait until a connection is made before continuing.  
                        acceptDone.WaitOne();

                        if (IsDone) return;
                    }
                    //Console.WriteLine("Ending Accept");

                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
            }

            /// <summary>
            /// Accept connection callback.
            /// </summary>
            /// <param name="ar"></param>
            public void AcceptCallback(IAsyncResult ar)
            {
                MDebug.WriteLine("Socket Server Accepted Connection");
                // Signal the main thread to continue.  
                this.acceptDone.Set();

                // Get the socket that handles the client request.  
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = handler;
                this.HandleClient(state);
                //Console.WriteLine("End Accepting");

            }

            /// <summary>
            /// Receive a message from client.
            /// </summary>
            /// <param name="clientState"></param>
            /// <returns></returns>
            public virtual string Receive(StateObject clientState)
            {
                //Console.WriteLine("Start Receive");
                clientState.workSocket.BeginReceive(clientState.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), clientState);
                // Wait for receive to finish.
                clientState.receiveDone.WaitOne();
                
                if (Failed) // If received failed
                    return null;

                string content = clientState.GetLastData();
                content = StateObject.PrepareForReceiveing(clientState.GetCurrentData());
                //Console.WriteLine("End Receive: " + content);                
                return content;
            }

            /// <summary>
            /// Read callback,
            /// </summary>
            /// <param name="ar"></param>
            public void ReadCallback(IAsyncResult ar)
            {
                // Retrieve the state object and the handler socket  
                // from the asynchronous state object.  
                //Console.WriteLine("Receiving...");
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket.  
                int bytesRead;
                try
                {
                    bytesRead = handler.EndReceive(ar);
                }
                catch
                {
                    bytesRead = 1;
                    Failed = true;
                }

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.  
                    //state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                    if (!Failed)
                        state.AddDataPart(state.buffer);
                    // Check if message ended.   
                    if (state.IsFinishedReading() || Failed )
                    {
                        // All the data has been read from the client                        
                        //Console.WriteLine("End Receiving");
                        //Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",content.Length, content);                        
                        state.EndReading();
                        state.receiveDone.Set();

                    }
                    else
                    {
                        // Not all data received. Get more.  
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), state);
                    }
                }
            }

            /// <summary>
            /// Send a message to a client.
            /// </summary>
            /// <param name="clientState">Client's details</param>
            /// <param name="data"></param>
            public virtual void Send(StateObject clientState, String data)
            {
                // Convert the string data to byte data.  
                //Console.WriteLine("Started Send");
                byte[] byteData = StateObject.PrepareForSending(data);
                Socket clientSocket = clientState.workSocket;
                // Begin sending the data to the remote device.  
                clientSocket.BeginSend(byteData, 0, byteData.Length, 0,
                    new AsyncCallback(SendCallback), clientState);
                clientState.sendDone.WaitOne();
                //Console.WriteLine("End Send");
            }

            /// <summary>
            /// Send callback.
            /// </summary>
            /// <param name="ar"></param>
            private void SendCallback(IAsyncResult ar)
            {
                try
                {
                    //Console.WriteLine("Sending...");
                    // Retrieve the socket from the state object.  
                    StateObject clientState = (StateObject)ar.AsyncState;
                    Socket clientSocket = clientState.workSocket;
                    // Complete sending the data to the remote device.  
                    int bytesSent = clientSocket.EndSend(ar);
                    //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                    clientState.sendDone.Set();
                    //Console.WriteLine("End Sending");
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                }
            }
            /// <summary>
            /// Close the server socket.
            /// </summary>
            /// <param name="clientSocket"></param>
            private void DestroyClientSocket(Socket clientSocket)
            {
                //Console.WriteLine("Start Destroy");
                clientSocket.Shutdown(SocketShutdown.Both);
                clientSocket.Close();
                //Console.WriteLine("End Destroy");
            }

            /// <summary>
            /// Handle a new client.
            /// </summary>
            /// <param name="clientState"></param>
            public void HandleClient(StateObject clientState)
            {
                // Receive a message from the client
                string requestString = this.Receive(clientState);
                // if it is not empty
                if (requestString != null)
                {
                    // Handle the message
                    string responseString = this.RHandler(requestString);
                    if (responseString != null)
                        // if there is data to send back, do it.
                        this.Send(clientState, responseString);
                }
                // Close client.
                this.DestroyClientSocket(clientState.workSocket);
            }            
        }

        /// <summary>
        /// Class for client-side socket communication.
        /// based on : https://msdn.microsoft.com/en-us/library/bew39x2a(v=vs.110).aspx
        /// </summary>
        public class SocketClient
        {            
            /// <summary>
            /// Client Socket.
            /// </summary>
            private Socket CSocket;
            private bool ConnectionSucceded = false;
            private SocketServerDetails SDetails;
            // ManualResetEvent instances signal completion.  
            private ManualResetEvent connectDone = new ManualResetEvent(false);
            private ManualResetEvent sendDone = new ManualResetEvent(false);
            private ManualResetEvent receiveDone = new ManualResetEvent(false);
            
            // The response from the remote device.  
            private static String response = String.Empty;

            public SocketClient(SocketServerDetails details)
            {                
                this.SDetails = details;
                // Create a TCP/IP socket.  
                this.CSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
            }

            public bool Connect()
            {
                // Establish the remote endpoint for the socket.                    
                //Console.WriteLine("Start Connect");                
                IPAddress ipAddressObj = IPAddress.Parse(this.SDetails.SIPAddress);
                IPEndPoint remoteEP = new IPEndPoint(ipAddressObj, this.SDetails.Port);
                // Connect to the remote endpoint.  
                CSocket.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), this.CSocket);
                connectDone.WaitOne(2000); // Wait only a short amount of time for connection to succeed.
                return ConnectionSucceded;
                //Console.WriteLine("End Connect");
            }

            /// <summary>
            /// Send a message to the server.
            /// </summary>
            /// <param name="data">Message to send.</param>
            public virtual void Send(string data)
            {
                try
                {                     
                    //Console.WriteLine("Start Send");
                    byte[] byteData = StateObject.PrepareForSending(data);

                    this.CSocket.BeginSend(byteData, 0, byteData.Length, 0,
                        new AsyncCallback(SendCallback), this.CSocket);

                    this.sendDone.WaitOne();
                    //Console.WriteLine("End Send");

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            }
            /// <summary>
            /// Receive a message from the server.
            /// </summary>
            /// <returns></returns>
            public virtual string Receive()
            {
                try
                {
                    //Console.WriteLine("Start Receive");
                    // Create the state object.  
                    StateObject state = new StateObject();
                    state.workSocket = this.CSocket;

                    // Begin receiving the data from the remote device.  
                    this.CSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                    receiveDone.WaitOne();
                    //Console.WriteLine("End Receive");
                    return response;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return null;
                }
            }
            /// <summary>
            /// Connect callback
            /// </summary>
            /// <param name="ar"></param>
            private void ConnectCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.  
                    Socket client = (Socket)ar.AsyncState;

                    try
                    {
                        // Complete the connection.  
                        client.EndConnect(ar);
                    }
                    catch 
                    {
                        ConnectionSucceded = false;
                        connectDone.Set();
                        return;
                    }

                    MDebug.WriteLine(string.Format("Socket Client connected to {0}", client.RemoteEndPoint.ToString()));

                    // Signal that the connection has been made.  
                    ConnectionSucceded = true;
                    connectDone.Set();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            /// <summary>
            /// Receive callback
            /// </summary>
            /// <param name="ar"></param>
            private void ReceiveCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the state object and the client socket   
                    // from the asynchronous state object.  
                    StateObject state = (StateObject)ar.AsyncState;
                    Socket client = state.workSocket;

                    // Read data from the remote device.  
                    int bytesRead = client.EndReceive(ar);
                    //Console.WriteLine("Receiving...");
                    if (bytesRead > 0)
                    {
                        // There might be more data, so store the data received so far.  
                        //state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                        state.AddDataPart(state.buffer);
                        // Get the rest of the data.  
                        client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReceiveCallback), state);
                    }
                    else
                    {
                        // All the data has arrived; put it in response.  
                        if (true) // CHG
                        {
                            response = StateObject.PrepareForReceiveing(state.currrentByteData.ToArray());
                            //Console.WriteLine("Received from server: " + response);
                            //Console.WriteLine("End Receiving");
                        }
                        // Signal that all bytes have been received.  
                        receiveDone.Set();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            /// <summary>
            /// Send callback.
            /// </summary>
            /// <param name="ar"></param>
            private void SendCallback(IAsyncResult ar)
            {
                try
                {
                    // Retrieve the socket from the state object.  
                    Socket client = (Socket)ar.AsyncState;

                    // Complete `ending the data to the remote device.  
                    int bytesSent = client.EndSend(ar);
                    //Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                    // Signal that all bytes have been sent.  
                    sendDone.Set();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            /// <summary>
            /// Close client.
            /// </summary>
            public void Destroy()
            {
                // Release the socket.  
                this.CSocket.Shutdown(SocketShutdown.Both);
                this.CSocket.Close();
            }

            /// <summary>
            /// Get a response from the server by a request.
            /// </summary>
            /// <param name="requestString">Request to send.</param>
            /// <param name="details">Server's details</param>
            /// <returns></returns>
            public static string GetResponse(string requestString, SocketServerDetails details)
            {
                // Create client
                SocketClient sc = new SocketClient(details);
                // Try to connect
                if (sc.Connect())
                {
                    // Send the request
                    sc.Send(requestString);
                    // Receive a response
                    string data = sc.Receive();
                    // Close client
                    sc.Destroy();
                    // Return response
                    return data;
                }
                return null;
                    
            }            
        }

        /// <summary>
        /// Gets an Encryptor for a client by the message he sent.
        /// </summary>
        /// <param name="msg">The message the client sent.</param>
        /// <returns>Encryptor for client.</returns>
        public delegate IEncryptor GetEncryptor(string msg);

        /// <summary>
        /// Class for secure server-side socket communication
        /// </summary>
        public class SecureSocketServer : SocketServer
        {
            private GetEncryptor EncryptorProvider;
            /// <summary>
            /// Creates a new SecureSocketServer instance.
            /// </summary>
            /// <param name="encryptorProvider">Encryptor provider for clients.</param>
            /// <param name="details">Server details.</param>
            /// <param name="handler">Request handler.</param>
            public SecureSocketServer(GetEncryptor encryptorProvider,
                SocketServerDetails details, RequestHandler handler) : base(details, handler)
            {
                this.EncryptorProvider = encryptorProvider;
            }

            public override void Send(StateObject clientState, String data)
            {
                base.Send(clientState, clientState.Encryptor.EncryptToString(data));
            }

            public override string Receive(StateObject clientState)
            {
                // Receive data
                string data = base.Receive(clientState);
                // Check if empty #new
                if (data == null || data.Length == 0)
                    return null;
                // Get the encryptor for the client.
                clientState.Encryptor = this.EncryptorProvider(data);
                // If no encryptor is available - pass.
                if (clientState.Encryptor == null)
                    return null;
                // Get the actual message.
                data = data.Split(new string[] { StateObject.DATA }, StringSplitOptions.None)[1];
                // Decrypt message.                
                data = clientState.Encryptor.DecryptString(data);
                // return decrypted message.
                return data;
            }
        }
        /// <summary>
        /// Class for secure client-side socket communication.
        /// </summary>
        public class SecureSocketClient : SocketClient
        {
            private IEncryptor Encryptor;
            private int ProductId;

            public SecureSocketClient(IEncryptor encryptor, SocketServerDetails details,
                int productId) : base(details)
            {
                this.ProductId = productId;
                this.Encryptor = encryptor;
            }

            // Send And Receive are not used in this project.
            public override void Send(String data)
            {
                data = this.Encryptor.EncryptToString(data);
                data = this.ProductId + StateObject.DATA + data;
                base.Send(this.Encryptor.EncryptToString(data));
            }

            public override string Receive()
            {
                return this.Encryptor.DecryptString(base.Receive());
            }
            /// <summary>
            /// Send a request to a server and receive it's response.
            /// </summary>
            /// <param name="encryptor"></param>
            /// <param name="requestString"></param>
            /// <param name="details"></param>
            /// <param name="productId"></param>
            /// <returns></returns>
            public static string GetResponse(IEncryptor encryptor, string requestString, SocketServerDetails details, int productId)
            {
                string encryptedRequest = encryptor.EncryptToString(requestString);
                string data = productId + StateObject.DATA + encryptedRequest;
                string encryptedResponse = SocketClient.GetResponse(data, details);
                if (encryptedResponse == null || encryptedResponse.Length == 0) // nothing was received #new
                    return null;
                string decryptedResponse = encryptor.DecryptString(encryptedResponse);
                return decryptedResponse;
            }
        }
    }
}