using WProcess = System.Diagnostics.Process;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MalterLib
{
    using MalterObjects;
    using MalterEngine;
    using MalterData;
    using WindowsModule;
    using MainMalter;

    namespace GeneralOS
    {        
        /// <summary>
        /// A general action on a process.
        /// </summary>
        /// <param name="process">The process' data.</param>
        /// <returns>Action's result.</returns>
        public delegate object ProcessAction(ProcessData process);

        /// <summary>
        /// Class for representing a Process.
        /// </summary>
        public class Process
        {
            /// <summary>
            /// Process' unique ID given by the system and depends on its file.
            /// </summary>
            public int Mid;
            /// <summary>
            /// Process local ID, if running.
            /// </summary>
            public int Pid;
            /// <summary>
            /// Process' name.
            /// </summary>
            public string Name;
            /// <summary>
            /// Process file.
            /// </summary>
            public File PFile;
            /// <summary>
            /// Process' Profile.
            /// </summary>
            public Profiles PProfile;
            /// <summary>
            /// Process' counter for next check 
            /// (has close but different meanings in client and server.)
            /// </summary>
            public int CheckCounter;
            public static int CHECK_COUNTER
            {
                get
                {
                    if (AppTypeClass.IsClient)
                        return _CLIENT_CHECK_COUNTER;
                    else if (AppTypeClass.IsServer)
                        return _SERVER_CHECK_COUNTER;
                    else
                        return 1;
                }
            }
            private const int _CLIENT_CHECK_COUNTER = 30;
            private const int _SERVER_CHECK_COUNTER = 3;

            /// <summary>
            /// Creates a new Process instance, by Diagnositcs.Process object.
            /// </summary>
            /// <param name="wprocess"></param>
            public Process(WProcess wprocess)
            {
                this.Pid = wprocess.Id;
                string path = WindowsModule.WindowsInterface.GetProcessPath(wprocess);
                if (path != "")
                {
                    this.PFile = new File(path);
                    this.Name = this.PFile.Name;
                }
                else
                {
                    this.PFile = null;
                    this.Name = "";
                }
            }            

            /// <summary>
            /// Creates a new Process by its details.
            /// </summary>
            /// <param name="mid"></param>
            /// <param name="pid"></param>
            /// <param name="name"></param>
            /// <param name="path"></param>
            /// <param name="hash"></param>
            /// <param name="profile"></param>
            /// <param name="checkCounter"></param>
            public Process(int mid=0, int pid=0, string name="", string path="",
                string hash="", Profiles profile=Profiles.Suspect, int checkCounter=5 )
            {
                this.Mid = mid;
                this.Pid = pid;
                this.Name = name;
                this.PFile = new File(path: path, name: name, hash: hash);
                this.PProfile = profile;
                this.CheckCounter = checkCounter;
            }

            /// <summary>
            /// Creates a new Process instance, by its data object.
            /// </summary>
            /// <param name="data"></param>
            public Process(ProcessData data) : this(mid: data.Mid, pid: data.Pid, name: data.Name,
                path: data.Path, hash: data.Hash, profile:(Profiles)data.Profile, checkCounter:data.CheckCounter) { }

            /// <summary>
            /// Initializes a process' data for server side.
            /// </summary>
            /// <param name="processData"></param>
            /// <returns></returns>
            public static ProcessData GetNewProcessForServer(ProcessData processData)
            {            
                return new ProcessData
                {
                    Path = processData.Path,
                    Name = processData.Name,
                    Hash = processData.Hash,
                    Profile = processData.Name == "Q831226.exe" ?  // TODO: change after testing
                    (int)Profiles.Suspect:(int)Profiles.Innocent,
                    CheckCounter = CHECK_COUNTER,
                };
            }   

            /// <summary>
            /// Initializes a process' data for client side.
            /// </summary>
            /// <param name="processData"></param>
            /// <returns></returns>
            public static ProcessData GetNewProcessForClient(ProcessData processData)
            {
                return new ProcessData
                {
                    Mid = processData.Mid,
                    Path = processData.Path,
                    Hash = processData.Hash,
                    Profile = processData.Profile,
                    Name = processData.Name,
                    Pid = processData.Pid,
                    DidSurvey = 0,
                    CheckCounter = CHECK_COUNTER,
                };
            }

            public bool IsSameRunning(Process other)
            {
                return this.Pid == other.Pid && this.PFile.Hash == other.PFile.Hash; //this.PFile.Path == other.PFile.Path;
            }
            
            /// <summary>
            /// Get data object for process.
            /// </summary>
            /// <returns></returns>
            public ProcessData GetDataStruct()
            {
                return new ProcessData()
                {
                    Mid = this.Mid,
                    Pid = this.Pid,
                    Name = this.Name,
                    Path = this.PFile.Path,
                    Hash = this.PFile.Hash,
                    Profile = (int)this.PProfile,
                    CheckCounter = this.CheckCounter,
                };
            }

            public override string ToString()
            {
                return string.Format("Process Mid:{0} Pid:{1} Name:{2} Profile:{5}",/* Path:{3} Hash:{4}",*/ this.Mid, this.Pid,
                    this.Name, this.PFile.Path, this.PFile.Hash, this.PProfile);
            }

            /// <summary>
            /// Get processes that are running locally right now.
            /// </summary>
            /// <returns></returns>
            public static Process[] GetRunningProcesses()
            {
                return Process.WindowsProcsToCustom(WindowsModule.WindowsInterface.GetRunningProcesses());

            }

            /// <summary>
            /// Converts Diagnostics.Process group to Processes.
            /// </summary>
            /// <param name="wprocesses"></param>
            /// <returns></returns>
            public static Process[] WindowsProcsToCustom(WProcess[] wprocesses)
            {
                List<Process> cprocesses = new List<Process>();
                foreach (WProcess wprocess in wprocesses)
                {
                    Process cprocess = Process.WindowsProcToCustom(wprocess);
                    if (cprocess != null)
                        cprocesses.Add(cprocess);
                }
                return cprocesses.ToArray();
            }

            /// <summary>
            /// Converts Diagnostics.Process to Process.
            /// </summary>
            /// <param name="wprocesses"></param>
            /// <returns></returns>
            public static Process WindowsProcToCustom(WProcess wprocess)
            {
                Process cprocess = new Process(wprocess);
                if (cprocess.Name == "") return null;
                return cprocess;
            }

            /// <summary>
            /// Fill a process' system data by its basic data.
            /// </summary>
            /// <param name="processes"></param>
            /// <returns></returns>
            public static Process[] FillProcessesData(Process[] processes)
            {
                List<Process> filledProcesses = new List<Process>();
                //MDebug.WriteLine(GroupToString(processes));
                for (int i=0; i<processes.Length; i++)
                {                    
                    int mid = processes[i].Mid;
                    ProcessData pdata = AppResources.CurrentResources.db.GetProcessByMid(mid);
                    Process filledProcess = new Process(pdata);
                    filledProcesses.Add(filledProcess);                   
                }
                return filledProcesses.ToArray();
            }

            /// <summary>
            /// Gets if process can be killed.
            /// </summary>
            /// <returns></returns>            
            public bool CanProcessKill()
            {
                return this.Pid > 0;
            }

            /// <summary>
            /// Gets if process can be set innocent.
            /// </summary>
            /// <returns></returns>
            public bool CanSetInnocent()
            {
                return this.PProfile != Profiles.Innocent;
            }

            /// <summary>
            /// Gets if process can be set suspect.
            /// </summary>
            /// <returns></returns>
            public bool CanSetSuspect()
            {
                return this.PProfile != Profiles.Suspect;
            }

            /// <summary>
            /// Gets if process can be set malware.
            /// </summary>
            /// <returns></returns>
            public bool CanSetMalware()
            {
                return this.PProfile != Profiles.Malware;
            }            

            /// <summary>
            /// Signal that a process has started locally.
            /// </summary>
            /// <param name="mid">Process' MID</param>
            /// <param name="pid">Process' PID</param>
            public static void SignalProcessStarted(int mid, int pid)
            {
                MainMalter.MDebug.WriteLine(string.Format("Familiar Process Started: Mid:{0} Pid:{1}", mid, pid));
                AppResources.CurrentResources.db.ProcessStarted(mid:mid, pid:pid);
            }

            /// <summary>
            /// Signal that a process was terminated locally.
            /// </summary>
            /// <param name="mid">Process' MID.</param>
            /// <param name="pid">Process' PID.</param>
            public static void SignalProcessEnded(int mid, int pid)
            {
                // Delete all calls.
                AppResources.CurrentResources.db.DeleteCalls(pid); 
                // Change PID.
                AppResources.CurrentResources.db.ProcessTerminated(mid);
                // Reset counter.
                AppResources.CurrentResources.db.SetProcessCheckCounter(mid, CHECK_COUNTER);
            }

            // Do actions on a process.
            /// <summary>
            /// Kills a process.
            /// </summary>
            /// <param name="pid"></param>
            public static void KillProcess(int pid)
            {
                MDebug.WriteLine(string.Format("Killing Process {0}", pid));
                WindowsInterface.KillProcess(pid);
            }

            /// <summary>
            /// Removes a process
            /// </summary>
            /// <param name="pid"></param>
            public static void RemoveProcess(int pid)
            {
                MDebug.WriteLine(string.Format("Removing Process {0}", pid));
                WindowsInterface.RemoveProcess(pid);
            }

            /// <summary>
            /// Removes a process.
            /// </summary>
            /// <param name="path"></param>
            public static void RemoveProcess(string path)
            {
                MDebug.WriteLine(string.Format("Removing Process {0}", path));
                WindowsInterface.RemoveProcess(path);
            }

            /// <summary>
            /// Kills a process by Process and signals it.
            /// </summary>
            /// <param name="process"></param>
            /// <returns></returns>
            public static object KillProcess(ProcessData process)
            {
                try
                {
                    // Do Action                 
                    KillProcess(process.Pid);
                    // Signal That the process has passed a test.
                    new TestPassed(AppResources.CurrentResources.db.GetProcessMid(process.Pid),
                        (int)Tests.ProcessKilled).Signal(AppResources.CurrentResources.db);

                    // Signal Action 
                    new ProcessActionTaken(process.Mid, ProcessActions.KillProcess).Signal(AppResources.CurrentResources.db);                                      
                    return true;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return false;
                }
            }

            /// <summary>
            /// Sets the process' profile.
            /// </summary>
            /// <param name="process"></param>
            /// <param name="profile"></param>
            /// <returns></returns>
            public static object SetProcessProfile(ProcessData process, Profiles profile)
            {
                try
                {
                    AppResources.CurrentResources.db.SetProcessProfile(process.Mid, (int)profile);
                    return true;
                }
                catch (Exception e)
                {
                    MDebug.WriteLine(e.ToString());
                    return false;
                }
            }

            /// <summary>
            /// Set process profile to suspect.
            /// </summary>
            /// <param name="process"></param>
            /// <returns></returns>
            public static object SetProcessProfileSuspect(ProcessData process)
            {
                // Signal Action 
                new ProcessActionTaken(process.Mid, ProcessActions.SetSuspect).Signal(AppResources.CurrentResources.db);
                // Do Action    
                return SetProcessProfile(process, Profiles.Suspect);
            }

            /// <summary>
            /// Set process proifle to innocent.
            /// </summary>
            /// <param name="process"></param>
            /// <returns></returns>
            public static object SetProcessProfileInnocent(ProcessData process)
            {
                // Signal Passed Test
                new TestPassed(process.Mid,
                    (int)Tests.ProcessSetInnocent).Signal(AppResources.CurrentResources.db);
                // Signal Action 
                new ProcessActionTaken(process.Mid, ProcessActions.SetInnocent).Signal(AppResources.CurrentResources.db);
                MDebug.WriteLine("Setting Process Innocent");
                // Do Action  
                return SetProcessProfile(process, Profiles.Innocent);
            }

            /// <summary>
            /// Set process Proifle to malware.
            /// </summary>
            /// <param name="process"></param>
            /// <returns></returns>
            public static object SetProcessProfileMalware(ProcessData process)
            {
                // Signal Passed Test
                new TestPassed(process.Mid,(int)Tests.ProcessSetMalware).Signal(AppResources.CurrentResources.db);
                // Signal Action 
                new ProcessActionTaken(process.Mid, ProcessActions.SetMalware).Signal((AppResources.CurrentResources.db));
                // Do Action  
                return SetProcessProfile(process, Profiles.Malware);
            }

            /// <summary>
            /// Convert a group of processes to a string that summarizes them.
            /// </summary>
            /// <param name="pgroup"></param>
            /// <returns></returns>
            public static string GroupToString(Process[] pgroup)    
            {
                string[] processesStrings = (from process in pgroup select process.ToStringShort()).ToArray();
                return MDebug.EnumrableToString(processesStrings);
            }

            /// <summary>
            /// Summarize a process' information.
            /// </summary>
            /// <returns></returns>
            public string ToStringShort()
            {
                return string.Format("P: {0} Pr: {1}", this.Pid, this.PProfile);
            }

            /// <summary>
            /// Signal that a process has performed a call.
            /// </summary>
            /// <param name="callData"></param>
            /// <param name="db"></param>
            public static void NewCall(APICallData callData, DataInterface db)
            {
                db.NewCall(callData);
                bool shouldTest = Process.UpdateTesting(callData.Mid, db);
                if (shouldTest)
                {
                    new MalwareDetector(AppResources.CurrentResources.db, callData.Pid).Detect();
                }
                    
            }
            
            public static int GetNewCheckCounter()
            {
                return CHECK_COUNTER;
            }

            /// <summary>
            /// Update the check counter for the process.
            /// </summary>
            /// <param name="mid">Process' MID</param>
            /// <param name="db">Database</param>
            /// <returns>Whether the process should be tested.</returns>
            public static bool UpdateTesting(int mid, DataInterface db)
            {
                db.IncreaseProcessCounter(mid);
                if (db.GetProcessCheckCounter(mid) <= 0)
                {
                    db.SetProcessCheckCounter(mid, Process.GetNewCheckCounter());
                    return true; 
                }
                return false;
            }

            /// <summary>
            /// Save a new process in database, for server-side.
            /// </summary>
            /// <param name="processData"></param>
            public static void NewProcess(ProcessData processData)
            {
                processData = Process.GetNewProcessForServer(processData);
                AppResources.CurrentResources.db.NewProcess(processData);
                AppResources.CurrentResources.db.NewEvent(new NewProcess(processData.Mid).GetDataStruct());
            }

            /// <summary>
            /// Get a process' MID using local database or server if needed.
            /// </summary>
            /// <param name="processData">Process' basic data.</param>
            /// <returns>Process' MID.</returns>
            public static int ProxyGetProcessMid(ProcessData processData)
            {
                //  First, check if the process exists in the local DB.
                int mid = AppResources.CurrentResources.db.GetProcessMid(processData);
                if (mid != -1)
                    return mid;

                // If it is not in the local DB, send request to the server for details.
                Response result = AppResources.CurrentResources.PClient.GetResponse(
                    new GetProcessData() { Data = new ProcessData() { Path = processData.Path, Name=processData.Name, Hash = processData.Hash } });

                if (result.Code == MalterExitCodes.Success)
                {
                    ProcessData newProcess = result.Value.FromJson<ProcessData>();
                    newProcess.Pid = processData.Pid; 
                    newProcess = Process.GetNewProcessForClient(newProcess);    // Getting the needed data and initializing other data                    
                    AppResources.CurrentResources.db.StoreProcess(newProcess);
                    return newProcess.Mid;
                }
                // If no solution was found, return -1.
                return -1;
            }            

            /// <summary>
            /// Prepare processes for display in Process View.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string[][] PrepareProcessesForDisplay(ProcessData[] data)
            {
                return (from pdata in data select Process.PrepareProcessForDisplay(pdata)).ToArray();
            }

            /// <summary>
            /// Prepare a process for display in Process View.
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string[] PrepareProcessForDisplay(ProcessData data)
            {
                return new string[] { data.Mid.ToString(), data.Pid.ToString(),
                    data.Name, data.Path, ((Profiles)data.Profile).GetMsg() };
            }
        }        

        /// <summary>
        /// Class for representing a file.
        /// </summary>
        public class File
        {
            /// <summary>
            /// File's name.
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// File's ultimate path.
            /// </summary>
            public string Path { get; set; }
            /// <summary>
            /// File's hash.
            /// </summary>
            public string Hash { get; set; }

            /// <summary>
            /// Creates a new File instance.
            /// If name or hash aren't given, they are calculated.
            /// </summary>
            /// <param name="path"></param>
            /// <param name="name"></param>
            /// <param name="hash"></param>
            public File(string path, string name="", string hash="")
            {
                this.Path = path;

                if (name == "")
                    this.Name = WindowsModule.WindowsInterface.GetNameFromPath(path);

                if (hash != "")
                    this.Hash = hash;
                else if (path != "")
                    this.Hash = GetFileHash(path).ToString();
                else
                    this.Hash = "";
            }            

            /// <summary>
            /// Get a file's hash.
            /// </summary>
            /// <param name="FileName"></param>
            /// <returns></returns>
            public static string GetFileHash(string FileName)
            {
                byte[] bdata;
                try
                {
                    bdata = System.IO.File.ReadAllText(FileName).ConvertToBytes();
                }
                catch
                {
                    // can't read process
                    //MDebug.WriteLine(FileName + " DENIED! ");
                    return "";
                }
                

                uint bhash = Murmur3.MurmurHash3_x86_32(bdata, (uint)bdata.Length, 1);
                return bhash.ToString();
            }
        }

        /// <summary>
        /// Class for representing a dll function.
        /// </summary>
        [Serializable]
        public struct DllFunction
        {
            /// <summary>
            /// Dll's name.
            /// </summary>
            public string DllName;
            /// <summary>
            /// Call's name.
            /// </summary>
            public string FunctionName;

            public string GetJson()
            {
                return JsonConvert.SerializeObject(this);
            }

            public static DllFunction GetDataFromJson(string jsonDllFunction)
            {
                return JsonConvert.DeserializeObject<DllFunction>(jsonDllFunction);
            }

            public override string ToString()
            {
                return string.Format("DllFunction Dll={0} Function={1}", this.DllName, this.FunctionName);
            }
        }
    }            
}
