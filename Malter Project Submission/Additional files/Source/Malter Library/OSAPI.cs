﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MalterLib
{
    namespace GeneralOS
    {
        using MalterObjects;
        using MalterEngine;
        using MalterData;
        using MainMalter;
        using System.Runtime.InteropServices;

        namespace OSAPI
        {
            /// <summary>
            /// Class for representing a general APICall.
            /// </summary>
            [Serializable]
            public abstract class APICall
            {
                public const string Kernel32 = "kernel32.dll", User32 = "user32.dll",
                    Advapi32 = "advapi32.dll", Shell32 = "Shell32.dll";
                public const string FILE_NAME = "FileName";
                public const string DOMAIN_NAME = "DomainName",QUERY = "Query", RECORD_TYPE = "RecordType";
                public const string KEY = "Key";
                public const string NAME = "Name";
                public const string DNSAPI = "dnsapi";
                /// <summary>
                /// Call's ID.
                /// </summary>
                public int Id;
                /// <summary>
                /// Call's name.
                /// </summary>
                public abstract string Name { get; }
                /// <summary>
                /// Calling Process MID.
                /// </summary>
                public int Mid;
                /// <summary>
                /// Calling Process PID.
                /// </summary>
                public int Pid;
                /// <summary>
                /// Time of call.
                /// </summary>
                public string Time { get; set; }
                /// <summary>
                /// Dictionary for converting calls in string format to APICall object.
                /// </summary>
                public static Dictionary<string, CallSorter> CallsSorters = new Dictionary<string, CallSorter>
                {
                    {CreateFileW.CallDllFunction.FunctionName, CreateFileW.GetCallFromData },
                    {OpenFile.CallDllFunction.FunctionName, OpenFile.GetCallFromData },
                    {WriteFile.CallDllFunction.FunctionName, WriteFile.GetCallFromData },
                    {RegSetValueEx.CallDllFunction.FunctionName, RegSetValueEx.GetCallFromData },
                    {DNSQuery_W.CallDllFunction.FunctionName, DNSQuery_W.GetCallFromData },
                    {RegOpenKeyW.CallDllFunction.FunctionName, RegOpenKeyW.GetCallFromData },
                    {DeleteFileA.CallDllFunction.FunctionName, DeleteFileA.GetCallFromData },
                    {ShellExecuteA.CallDllFunction.FunctionName, ShellExecuteA.GetCallFromData }
                };
                   
                public delegate APICall CallSorter(APICallData data);

                public APICall(int id = 0, int mid=0, int pid=0, string time = "")
                {
                    this.Id = id;
                    this.Mid = mid;
                    this.Pid = pid;
                    
                    if (time == "")
                        this.Time = WindowsModule.WindowsInterface.GetTimeNowString(); 
                    else
                        this.Time = time;
                }

                /// <summary>
                /// Returns details for call in a JSON format as string object.
                /// </summary>
                /// <returns></returns>
                public abstract string GetParmJson();

                /// <summary>
                /// Get data object for instance.
                /// </summary>
                /// <returns></returns>
                public virtual APICallData GetDataStruct()
                {
                    return new APICallData
                    {
                        Id = this.Id,
                        Time = this.Time,
                        Name = this.Name,
                        SCall = this.GetParmJson(),
                        Mid = this.Mid,
                        Pid = this.Pid
                    };
                }

                /// <summary>
                /// Get call object from data object.
                /// </summary>
                /// <param name="data"></param>
                /// <returns></returns>
                public static APICall GetCallFromData(APICallData data)
                {
                    CallSorter sorter;
                    if (CallsSorters.TryGetValue(data.Name, out sorter))
                    {
                        return sorter(data);
                    }
                    return null;
                }

                /// <summary>
                /// Prepare calls for display.
                /// </summary>
                /// <param name="db"></param>
                /// <param name="callsData"></param>
                /// <returns></returns>
                public static APICallData[] PrepareCalls(DataInterface db, APICallData[] callsData)
                {
                    APICallData[] newCallsData = new APICallData[callsData.Length];
                    for (int i=0; i<callsData.Length; i++)
                    {
                        callsData[i].ProcessName = db.GetProcessName(callsData[i].Mid);
                        newCallsData[i] = callsData[i];
                    }
                    return newCallsData;
                }

                public override string ToString()
                {
                    return this.GetDataStruct().ToString();
                }

                public static void AddCallSorter(string callName, CallSorter sorter)
                {
                    APICall.CallsSorters[callName] = sorter;
                }

                /// <summary>
                /// Get recent calls of a process.
                /// </summary>
                /// <param name="db">Database</param>
                /// <param name="pid">Process' PID</param>
                /// <param name="amount">Max amount of calls.</param>
                /// <param name="clear">Wheter calls should be deleted after query.</param>
                /// <returns></returns>
                public static APICall[] GetRecentCalls(DataInterface db, int pid, int amount, bool clear = false)
                {
                    APICallData[] callsData = db.GetRecentProcessAPICalls(pid, amount);
                    if (clear)
                        db.DeleteCalls(pid);
                    return (from call in callsData select APICall.GetCallFromData(call)).ToArray();
                }

            }

            #region API Calls 
            [Serializable]
            public class CreateFileW : APICall
            {
                public string FileName { get; set; }
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public static DllFunction CallDllFunction = new DllFunction() { DllName = APICall.Kernel32, FunctionName = "CreateFileW" };

                public CreateFileW(string fileName, int id = 0, int mid=0, int pid=0, string time = "") 
                    : base(id, mid, pid, time)
                {
                    this.FileName = fileName;
                }                

                public string ToString2()
                {
                    return string.Format("CreateFileW: file:{0} time:{1}", this.FileName, this.Time);
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'{1}': {0}}}", this.FileName.ToLiteral(), FILE_NAME);
                }
                
                public new static CreateFileW GetCallFromData(APICallData data)
                {
                    string fileName = (string)JObject.Parse((data.SCall)).Property(FILE_NAME).Value;
                    return new CreateFileW(fileName, data.Id, data.Mid, data.Pid, data.Time);
                }
            }

            [Serializable]
            public class RegSetValueEx : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction() { DllName=APICall.Advapi32, FunctionName="RegSetValueExW" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string HKey;
                public string SubKey;
                public string Value;
                public string Data;

                public RegSetValueEx(string hkey, string subkey, string value, string data,
                    int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid, pid, time)
                {
                    this.HKey = hkey;
                    this.SubKey = subkey;
                    this.Value = value;
                    this.Data = data;
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'HKey':{0}, 'SubKey':{1}, 'Value':{2}, 'Data':{3}}}",
                        HKey, SubKey.ToLiteral(), Value.ToLiteral(), Data.ToLiteral());
                }
                
                public static new RegSetValueEx GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse((data.SCall));
                    string hkey = (string)scall.Property("HKey").Value;
                    string subkey = (string)scall.Property("SubKey").Value;
                    string value = (string)scall.Property("Value").Value;
                    string valueData = (string)scall.Property("Data").Value;

                    return new RegSetValueEx( hkey, subkey, value, valueData,
                        data.Id, data.Mid, data.Mid, data.Time);
                }
            }

            [Serializable]
            public class RegOpenKeyW : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction { DllName = APICall.Advapi32, FunctionName = "RegOpenKeyW" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string Key;
                private static Dictionary<uint, string> RootKeys = new Dictionary<uint, string>
                {
                    {0x80000002,"HKEY_LOCAL_MACHINE"  },
                    {0x80000001, "HKEY_CURRENT_USER" }
                };
                public RegOpenKeyW(uint hkey, string subkey, int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid, pid, time)
                {
                    this.Key = GetFullKey(hkey, subkey);
                }

                public RegOpenKeyW(string fullKey, int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid, pid, time)
                {
                    this.Key = fullKey;
                }

                public override string GetParmJson()
                {
                    //MDebug.WriteLine("this: "+string.Format(@"{{'Key':{0}}}", this.Key.ToLiteral()));
                    return string.Format(@"{{'{1}':{0}}}", this.Key.ToLiteral(), KEY);
                }

                public static string GetFullKey(uint hkey, string subkey)
                {
                    return RootKeys[hkey] + "\\" + subkey;
                }

                public static new RegOpenKeyW GetCallFromData(APICallData call)
                {
                    string key = (string)JObject.Parse(call.SCall).Property(KEY).Value;
                    return new RegOpenKeyW(key, id: call.Id, mid: call.Mid, pid: call.Pid, time: call.Time);
                }
            }            

            [Serializable]
            public class OpenFile : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction() { DllName = APICall.Kernel32, FunctionName = "OpenFile" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string FileName;

                public OpenFile(string filename, int id = 0, int mid = 0, int pid = 0, string time = "")
                    : base(id, mid, pid, time)
                {
                    this.FileName = filename;
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'{1}': {0}}}", this.FileName.ToLiteral(), FILE_NAME);
                }

                public static new OpenFile GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse((data.SCall));
                    string fileName = (string)JObject.Parse((data.SCall)).Property(FILE_NAME).Value;
                    return new OpenFile(fileName, data.Id, data.Mid, data.Pid, data.Time);

                }
            }

            [Serializable]
            public class WriteFile : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction() { DllName = APICall.Kernel32, FunctionName = "WriteFile" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string FileName;

                public WriteFile(string fileName, int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid, pid, time)
                {
                    this.FileName = fileName;
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'{1}': {0}}}", this.FileName.ToLiteral(), FILE_NAME);
                }

                public static new WriteFile GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse((data.SCall));
                    string fileName = (string)JObject.Parse((data.SCall)).Property(FILE_NAME).Value;
                    return new WriteFile(fileName, data.Id, data.Mid, data.Pid, data.Time);

                }
            }

            [Serializable]
            public class DeleteFileA : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction() { DllName = APICall.Kernel32, FunctionName = "DeleteFileA" };
                public override string Name
                {
                    get
                    {
                        return CallDllFunction.FunctionName;
                    }
                }
                public string FileName;

                public DeleteFileA(string fileName, int id = 0, int mid = 0, int pid = 0, string time = "") : base(mid: mid, pid: pid, time: time)
                {
                    this.FileName = fileName;
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'{1}': {0}}}", this.FileName.ToLiteral(), FILE_NAME);
                }

                public static new DeleteFileA GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse((data.SCall));
                    string fileName = (string)JObject.Parse((data.SCall)).Property(FILE_NAME).Value;
                    return new DeleteFileA(fileName, data.Id, data.Mid, data.Pid, data.Time);

                }
            }

            [Serializable]
            public class DNSQuery_W : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction() { DllName = APICall.DNSAPI, FunctionName = "DnsQuery_W" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string DomainName;
                public DNSQueryOptions QueryOption;
                public DNSRecordTypes RecordType;

                //public 
                public DNSQuery_W(string domainName, DNSQueryOptions queryOption, DNSRecordTypes recordType,
                    int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid,pid, time)
                {
                    this.DomainName = domainName;
                    this.QueryOption = queryOption;
                    this.RecordType = recordType;
                }

                public override string GetParmJson()
                {
                    return string.Format("{{ '{3}':'{0}', '{4}':{1}, '{5}':{2}}}", this.DomainName,
                        (int)this.QueryOption, (int)this.RecordType,
                        DOMAIN_NAME, QUERY, RECORD_TYPE );
                }

                public static new DNSQuery_W GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse(data.SCall);
                    string domainName = (string)scall.Property(DOMAIN_NAME).Value;
                    DNSQueryOptions query = (DNSQueryOptions)(int)scall.Property(QUERY).Value;
                    DNSRecordTypes recordType = (DNSRecordTypes)(int)scall.Property(RECORD_TYPE).Value;
                    return new DNSQuery_W(domainName, query, recordType, data.Id, data.Mid, data.Pid, data.Time);
                }
            }

            [Serializable]
            public class ShellExecuteA : APICall
            {
                public static DllFunction CallDllFunction = new DllFunction()
                { DllName = APICall.Shell32, FunctionName = "ShellExecuteA" };
                public override string Name { get { return CallDllFunction.FunctionName; } }
                public string FileName;

                public ShellExecuteA(string fileName, int id = 0, int mid = 0, int pid = 0, string time = "") : base(id, mid, pid, time)
                {
                    this.FileName = fileName;
                }

                public override string GetParmJson()
                {
                    return string.Format(@"{{'{1}': {0}}}", this.FileName.ToLiteral(), FILE_NAME);
                }

                public static new ShellExecuteA GetCallFromData(APICallData data)
                {
                    JObject scall = JObject.Parse((data.SCall));
                    string fileName = (string)JObject.Parse((data.SCall)).Property(FILE_NAME).Value;
                    return new ShellExecuteA(fileName, data.Id, data.Mid, data.Pid, data.Time);

                }
            }
            #endregion API Calls 

            #region WinAPI Objects
            /// <summary>
            /// See http://msdn.microsoft.com/en-us/library/windows/desktop/cc982162(v=vs.85).aspx
            /// </summary>
            [Flags]
            public enum DNSQueryOptions
            {
                DNS_QUERY_STANDARD = 0x0,
                DNS_QUERY_ACCEPT_TRUNCATED_RESPONSE = 0x1,
                DNS_QUERY_USE_TCP_ONLY = 0x2,
                DNS_QUERY_NO_RECURSION = 0x4,
                DNS_QUERY_BYPASS_CACHE = 0x8,
                DNS_QUERY_NO_WIRE_QUERY = 0x10,
                DNS_QUERY_NO_LOCAL_NAME = 0x20,
                DNS_QUERY_NO_HOSTS_FILE = 0x40,
                DNS_QUERY_NO_NETBT = 0x80,
                DNS_QUERY_WIRE_ONLY = 0x100,
                DNS_QUERY_RETURN_MESSAGE = 0x200,
                DNS_QUERY_MULTICAST_ONLY = 0x400,
                DNS_QUERY_NO_MULTICAST = 0x800,
                DNS_QUERY_TREAT_AS_FQDN = 0x1000,
                DNS_QUERY_ADDRCONFIG = 0x2000,
                DNS_QUERY_DUAL_ADDR = 0x4000,
                DNS_QUERY_MULTICAST_WAIT = 0x20000,
                DNS_QUERY_MULTICAST_VERIFY = 0x40000,
                DNS_QUERY_DONT_RESET_TTL_VALUES = 0x100000,
                DNS_QUERY_DISABLE_IDN_ENCODING = 0x200000,
                DNS_QUERY_APPEND_MULTILABEL = 0x800000,
                DNS_QUERY_RESERVED = unchecked((int)0xF0000000)
            }

            /// <summary>
            /// See http://msdn.microsoft.com/en-us/library/windows/desktop/cc982162(v=vs.85).aspx
            /// Also see http://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml
            /// </summary>
            public enum DNSRecordTypes
            {
                DNS_TYPE_A = 0x1,
                DNS_TYPE_NS = 0x2,
                DNS_TYPE_MD = 0x3,
                DNS_TYPE_MF = 0x4,
                DNS_TYPE_CNAME = 0x5,
                DNS_TYPE_SOA = 0x6,
                DNS_TYPE_MB = 0x7,
                DNS_TYPE_MG = 0x8,
                DNS_TYPE_MR = 0x9,
                DNS_TYPE_NULL = 0xA,
                DNS_TYPE_WKS = 0xB,
                DNS_TYPE_PTR = 0xC,
                DNS_TYPE_HINFO = 0xD,
                DNS_TYPE_MINFO = 0xE,
                DNS_TYPE_MX = 0xF,
                DNS_TYPE_TEXT = 0x10,       // This is how it's specified on MSDN
                DNS_TYPE_TXT = DNS_TYPE_TEXT,
                DNS_TYPE_RP = 0x11,
                DNS_TYPE_AFSDB = 0x12,
                DNS_TYPE_X25 = 0x13,
                DNS_TYPE_ISDN = 0x14,
                DNS_TYPE_RT = 0x15,
                DNS_TYPE_NSAP = 0x16,
                DNS_TYPE_NSAPPTR = 0x17,
                DNS_TYPE_SIG = 0x18,
                DNS_TYPE_KEY = 0x19,
                DNS_TYPE_PX = 0x1A,
                DNS_TYPE_GPOS = 0x1B,
                DNS_TYPE_AAAA = 0x1C,
                DNS_TYPE_LOC = 0x1D,
                DNS_TYPE_NXT = 0x1E,
                DNS_TYPE_EID = 0x1F,
                DNS_TYPE_NIMLOC = 0x20,
                DNS_TYPE_SRV = 0x21,
                DNS_TYPE_ATMA = 0x22,
                DNS_TYPE_NAPTR = 0x23,
                DNS_TYPE_KX = 0x24,
                DNS_TYPE_CERT = 0x25,
                DNS_TYPE_A6 = 0x26,
                DNS_TYPE_DNAME = 0x27,
                DNS_TYPE_SINK = 0x28,
                DNS_TYPE_OPT = 0x29,
                DNS_TYPE_DS = 0x2B,
                DNS_TYPE_RRSIG = 0x2E,
                DNS_TYPE_NSEC = 0x2F,
                DNS_TYPE_DNSKEY = 0x30,
                DNS_TYPE_DHCID = 0x31,
                DNS_TYPE_UINFO = 0x64,
                DNS_TYPE_UID = 0x65,
                DNS_TYPE_GID = 0x66,
                DNS_TYPE_UNSPEC = 0x67,
                DNS_TYPE_ADDRS = 0xF8,
                DNS_TYPE_TKEY = 0xF9,
                DNS_TYPE_TSIG = 0xFA,
                DNS_TYPE_IXFR = 0xFB,
                DNS_TYPE_AFXR = 0xFC,
                DNS_TYPE_MAILB = 0xFD,
                DNS_TYPE_MAILA = 0xFE,
                DNS_TYPE_ALL = 0xFF,
                DNS_TYPE_ANY = 0xFF,
                DNS_TYPE_WINS = 0xFF01,
                DNS_TYPE_WINSR = 0xFF02,
                DNS_TYPE_NBSTAT = DNS_TYPE_WINSR
            }

            /// <summary>
            /// See http://msdn.microsoft.com/en-us/library/windows/desktop/ms682056(v=vs.85).aspx
            /// </summary>
            public enum DNS_FREE_TYPE
            {
                DnsFreeFlat = 0,
                DnsFreeRecordList = 1,
                DnsFreeParsedMessageFields = 2
            }

            /// <summary>
            /// See http://msdn.microsoft.com/en-us/library/windows/desktop/ms682082(v=vs.85).aspx
            /// These field offsets could be different depending on endianness and bitness
            /// </summary>

            [System.Runtime.InteropServices.StructLayout(LayoutKind.Sequential)]
            public struct OFSTRUCT
            {
                public byte cBytes;
                public byte fFixedDisc;
                public UInt16 nErrCode;
                public UInt16 Reserved1;
                public UInt16 Reserved2;
                [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 128)]
                public string szPathName;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
            public struct FILE_ID_BOTH_DIR_INFO
            {
                public uint NextEntryOffset;
                public uint FileIndex;
                public LargeInteger CreationTime;
                public LargeInteger LastAccessTime;
                public LargeInteger LastWriteTime;
                public LargeInteger ChangeTime;
                public LargeInteger EndOfFile;
                public LargeInteger AllocationSize;
                public uint FileAttributes;
                public uint FileNameLength;
                public uint EaSize;
                public char ShortNameLength;
                [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 12)]
                public string ShortName;
                public LargeInteger FileId;
                [MarshalAsAttribute(UnmanagedType.ByValTStr, SizeConst = 1)]
                public string FileName;
            }

            [StructLayout(LayoutKind.Explicit)]
            public struct LargeInteger
            {
                [FieldOffset(0)]
                public int Low;
                [FieldOffset(4)]
                public int High;
                [FieldOffset(0)]
                public long QuadPart;

                // use only when QuadPart canot be passed
                public long ToInt64()
                {
                    return ((long)this.High << 32) | (uint)this.Low;
                }

                // just for demonstration
                public static LargeInteger FromInt64(long value)
                {
                    return new LargeInteger
                    {
                        Low = (int)(value),
                        High = (int)((value >> 32))
                    };
                }

            }
            /// <summary>
            /// Used for WinAPI functions that return info about certain files.
            /// </summary>
            public enum FILE_INFO_BY_HANDLE_CLASS
            {
                FileBasicInfo = 0,
                FileStandardInfo = 1,
                FileNameInfo = 2,
                FileRenameInfo = 3,
                FileDispositionInfo = 4,
                FileAllocationInfo = 5,
                FileEndOfFileInfo = 6,
                FileStreamInfo = 7,
                FileCompressionInfo = 8,
                FileAttributeTagInfo = 9,
                FileIdBothDirectoryInfo = 10,// 0x0A
                FileIdBothDirectoryRestartInfo = 11, // 0xB
                FileIoPriorityHintInfo = 12, // 0xC
                FileRemoteProtocolInfo = 13, // 0xD
                FileFullDirectoryInfo = 14, // 0xE
                FileFullDirectoryRestartInfo = 15, // 0xF
                FileStorageInfo = 16, // 0x10
                FileAlignmentInfo = 17, // 0x11
                FileIdInfo = 18, // 0x12
                FileIdExtdDirectoryInfo = 19, // 0x13
                FileIdExtdDirectoryRestartInfo = 20, // 0x14
                MaximumFileInfoByHandlesClass
            }
            
            #endregion WinAPI Objects

            /// <summary>
            /// Interface for representing a call filter, that receives a call and based 
            /// on some rules returns a bool value.
            /// </summary>
            public interface IFilter
            {
                /// <summary>
                /// Get filter ID.
                /// </summary>
                /// <returns></returns>
                int GetId();
                /// <summary>
                /// Check if call passes the filter.
                /// </summary>
                /// <param name="call"></param>
                /// <returns></returns>
                bool Check(APICall call);
                /// <summary>
                /// Get number of filters.
                /// </summary>
                int Count { get; }
            }

            class CallsCollectionFilter : IFilter
            {
                private IFilter[] Filters;
                public int Count { get { return Filters.Length; } }

                /// <summary>
                /// Creates a new CallsCollectionFilter instance.
                /// </summary>
                /// <param name="id">Filter ID</param>
                /// <param name="filters"> Filters collection</param>
                public CallsCollectionFilter(int id, IFilter[] filters)
                {
                    this.Filters = filters;
                }

                /// <summary>
                /// Get filter's ID.
                /// </summary>
                /// <returns></returns>
                public int GetId()
                {
                    return -1;
                }

                /// <summary>
                /// Check call in filter.
                /// </summary>
                /// <param name="call"></param>
                /// <returns></returns>
                public bool Check(APICall call)
                {
                    //MDebug.WriteLine("Filters: "+MDebug.EnumrableToString(this.Filters));
                    foreach (CallFilter filter in this.Filters )
                        if (filter.Check(call))
                            return true;
                    
                    return false;
                }

                public override string ToString()
                {
                    return string.Format("CCF: " + string.Join<IFilter>(",", this.Filters));
                }

                /// <summary>
                /// Returns a collection by filters IDs.
                /// </summary>
                /// <param name="filterIds"></param>
                /// <returns></returns>
                public static IFilter GetFilter(int[] filterIds)
                {
                    // Create array.
                    IFilter[] filters = new IFilter[filterIds.Length];
                    foreach (int id in filterIds)
                    {
                        // Get the filters
                        filters[id] = MainMalter.ConfigureData.GetFilterById(id);
                        //MDebug.WriteLine("is filter null: " + (filters[id] == null).ToString());
                    }

                    return new CallsCollectionFilter(0, filters);


                }
            }

            class CallFilter : IFilter
            {
                private APICall Call { get; }
                /// <summary>
                /// Function that sets the rules for the filter.
                /// </summary>
                private Function<object> FilterFunc { get; } = null;
                public int Count {get{ return 1; }}
                private int Id;

                public CallFilter(APICall call, int id=0)
                {
                    this.Id = id;
                    this.Call = call;
                }

                public CallFilter(Function<object> filterFunc)
                {
                    this.FilterFunc = filterFunc;
                }
                
                /// <summary>
                /// Get filter's ID.
                /// </summary>
                /// <returns></returns>
                public int GetId()
                {
                    return this.Id;
                }
                
                /// <summary>
                /// Check call in filter.
                /// </summary>
                /// <param name="otherCall"></param>
                /// <returns></returns>
                public bool Check(APICall otherCall)
                {
                    return MalwareDetector.ExecuteTest(this.FilterFunc, new TestContext(JArray.FromObject(new APICall[] { otherCall })));
                }

                public override string ToString()
                {
                    return string.Format("CF:{0}", this.Id);
                }
            }
        }
    }
}