﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MalterLib
{
    namespace ProcessMonitoring
    {
        using GeneralOS;
        using MainMalter;
        using MalterData;
        using MalterEngine;
        using MalterObjects;

        /// <summary>
        /// Class for monitoring local processes.
        /// </summary>
        public class ProcessMonitor
        {
            /// <summary>
            /// The running processes as gathered in the current iteration.
            /// </summary>
            public Process[] NewRunningProcesses;

            /// <summary>
            /// The running processes as gathered in the former iteration.
            /// </summary>
            private Process[] _RunningProcesses;

            /// <summary>
            /// The running processes as gathered in the former iteration.
            /// </summary>
            private Process[] RunningProcesses { get { return _RunningProcesses; } }

            /// <summary>
            /// HookingManager for the monitor, provides data gathering services.
            /// </summary>
            private GeneralOS.APIHooking.ProcessHookingManager HookingManager = null;

            /// <summary>
            /// Initiates a new ProcessMonitor object.
            /// </summary>
            public ProcessMonitor()
            {
                this.HookingManager = new GeneralOS.APIHooking.ProcessHookingManager();
            }

            /// <summary>
            /// Main procedure for class, updates the processes saved data.
            /// </summary>
            public void Monitor()
            {
                // Get the processes that have just started. processes that were closed and processes that had no change.
                Process[][] runningProcessesDist = this.GetRunningProcessesDist();
                
                // Get the processes by who is new to the system and who the system has already seen 
                Process[] addedFamiliarProcesses = SearchUnfamiliarProcesses(runningProcessesDist[0])[0];
                Process[] removedFamiliarProcesses = SearchUnfamiliarProcesses(runningProcessesDist[1])[0];

                // Update Running Processes data, such as PID.
                this.UpdateProcessRunningData(new Process[][] { addedFamiliarProcesses,  removedFamiliarProcesses});

                // Get all relevant data for the processes
                addedFamiliarProcesses = Process.FillProcessesData(addedFamiliarProcesses);
                
                // Get processes by profile
                ProcessProfileDist processesDist = ProcessProfileDist.GetProcessDistribution(addedFamiliarProcesses);                
                //MDebug.WriteLine(processesDist.ToString());

                foreach (Process process in processesDist.InnocentProcesses)
                    ; // Don't do anything
                foreach (Process process in processesDist.SuspectProcesses)
                {
                    this.HookingManager.Inject(process.Pid); // Start hooking
                }
                foreach (Process process in processesDist.MalwareProcesses)
                    Process.KillProcess(process.Pid);
            }

            /// <summary>
            /// Updates the processes data in the database.
            /// </summary>
            public void UpdateProcessRunningData(Process[][] runningDist)
            {                
                foreach (Process proc in runningDist[1]) // update processes that were closed.
                {
                    // It is important to run detection tests before signaling the process has ended, becasue the tests 
                    // depend on the Process' Pid.
                    MalwareDetector.DetectTests(proc.Pid);                    
                    Process.SignalProcessEnded(mid: proc.Mid, pid:proc.Pid);
                }

                foreach (Process proc in runningDist[0]) // update processes that started
                {
                    Process.SignalProcessStarted(mid: proc.Mid, pid: proc.Pid);
                }                
            }
            
            /// <summary>
            /// Searches any unfamiliar processes.
            /// </summary>
            /// <param name="processes">All processes.</param>
            /// <returns>Array of processes groups, where the first is the familiar processes 
            /// and the second is unfamiliar processes.</returns>
            public Process[][] SearchUnfamiliarProcesses(Process[] processes)
            {
                //MDebug.WriteLine("Searching Unfamiliar Processes");
                List<Process> familiar = new List<Process>();
                List<Process> unfamiliar = new List<Process>();
                foreach (var process in processes)
                {
                    ProcessData pData = process.GetDataStruct();
                    int mid = Process.ProxyGetProcessMid(pData);
                    if (mid != -1)
                    {
                        process.Mid = mid;
                        familiar.Add(process);                        
                    }
                    else
                    {
                        MDebug.WriteLine("New process " + pData.Name);
                        //new UpdateNewProcess(pData).Signal(AppResources.CurrentResources.db);
                    }
                }
                return new Process[][] { familiar.ToArray(), unfamiliar.ToArray() };
            }

            /// <summary>
            /// Gets the processes that were added, removed and stayed running, in that order.
            /// </summary>
            /// <returns></returns>
            public Process[][] GetRunningProcessesDist()
            {
                Process[] nowRunningProcesses = FilterDuplicatesProcesses(Process.GetRunningProcesses());
                Process[][] runnningProcessesDist=  FilterOldProcesses(this.RunningProcesses, nowRunningProcesses);
                this._RunningProcesses = nowRunningProcesses;
                return runnningProcessesDist;
            }

            /// <summary>
            /// Gets the processes that were added, removed and stayed running, in that order.
            /// </summary>
            /// <param name="oldRunningProcesses">Running processes in former iteration.</param>
            /// <param name="newRunningProcesses">Running processes in current iteration</param>
            /// <returns></returns>
            public static Process[][] FilterOldProcesses(Process[] oldRunningProcesses, Process[] newRunningProcesses)
            {
                List<Process> AddedProcesses = new List<Process>();
                List<Process> RemovedProcesses = new List<Process>();
                List<Process> SameProcesses = new List<Process>();
                 
                if (oldRunningProcesses == null || oldRunningProcesses.Length == 0)       // if there are no old processes, all of them are new.
                    AddedProcesses.AddRange(newRunningProcesses);
                else if (newRunningProcesses == null || newRunningProcesses.Length == 0)        // if there are no new processes, nothing is new.
                    RemovedProcesses.AddRange(oldRunningProcesses);
                else
                {
                    foreach (Process newProcess in newRunningProcesses)
                    {
                        bool foundProcess = false ;
                        foreach (Process oldProcess in oldRunningProcesses)
                        {
                            if (newProcess.IsSameRunning(oldProcess))
                            {
                                SameProcesses.Add(newProcess);
                                foundProcess = true;
                                break;
                            }
                        }

                        if (!foundProcess)
                            AddedProcesses.Add(newProcess);
                    }

                    foreach (Process oldProcess in oldRunningProcesses)
                    {
                        bool foundProcess = false;
                        foreach (Process newProcess in newRunningProcesses)
                        {
                            if (newProcess.IsSameRunning(oldProcess))
                            {                            
                                foundProcess = true;
                                break;
                            }
                        }
                        if (!foundProcess)
                            RemovedProcesses.Add(oldProcess);
                    }
                }
                //MDebug.WriteLine("Added: "+Process.GroupToString(AddedProcesses.ToArray()));
                Process[][] result = new Process[][] { AddedProcesses.ToArray(), RemovedProcesses.ToArray(), RemovedProcesses.ToArray() };
                return result;
            }
            
            /// <summary>
            /// Removes duplicate processes from an array of processes.
            /// </summary>
            /// <param name="processes"></param>
            /// <returns>The filtered processes</returns>
            public static Process[] FilterDuplicatesProcesses(Process[] processes)
            {
                //MDebug.WriteLine("filtering");
                List<Process> filtered = new List<Process>();
                foreach (Process process in processes)
                {
                    if (FindProcessDuplicate(process, filtered.ToArray()) == -1)
                        filtered.Add(process);
                    else
                        ;//MDebug.WriteLine("filtered");
                }
                return filtered.ToArray();
            }

            /// <summary>
            /// Gets the index of a process of the same program from an array of processes.
            /// If there is no such process, it returns -1.
            /// </summary>
            /// <param name="targetProcess"></param>
            /// <param name="processes"></param>
            /// <returns></returns>
            public static int FindProcessDuplicate(Process targetProcess, Process[] processes)
            {
                for (int i = 0; i < processes.Length; i++)
                {
                    if (processes[i].PFile.Hash == targetProcess.PFile.Hash)
                        return i;
                }
                return -1;
            }
        }

        /// <summary>
        /// Class for splitting a process group to multiple groups by their profile.
        /// </summary>
        public struct ProcessProfileDist
        {
            public Process[] InnocentProcesses;
            public Process[] SuspectProcesses;
            public Process[] MalwareProcesses;     
                                      
            /// <summary>
            /// Gets a distribution of processes by their profiles.
            /// </summary>
            /// <param name="processes">Group of processes.</param>
            /// <returns>ProrcessProfilesDist structure.</returns>
                          
            public static ProcessProfileDist GetProcessDistribution(Process[] processes)
            {
                Process[] innocentP = (from process in processes
                                       where process.PProfile == Profiles.Innocent
                                       select process ).ToArray();

                Process[] suspectP = (from process in processes
                                       where process.PProfile == Profiles.Suspect
                                       select process).ToArray();

                Process[] malwareP = (from process in processes
                                      where (process.PProfile == Profiles.Malware
                                      || process.PProfile == Profiles.Virus)
                                       select process).ToArray();
                return new ProcessProfileDist
                { InnocentProcesses = innocentP, SuspectProcesses = suspectP, MalwareProcesses = malwareP };
            }

            public override string ToString()
            {
                return string.Format("Dist: Innocent:{0}\nSuspect:{1}\nMalware:{2}",
                    Process.GroupToString(this.InnocentProcesses),
                    Process.GroupToString(this.SuspectProcesses),
                    Process.GroupToString(this.MalwareProcesses)); 
            }
        }
    }
}
