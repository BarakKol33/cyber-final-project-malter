﻿using Microsoft.Win32.SafeHandles;
using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using WProcess = System.Diagnostics.Process;

namespace MalterLib
{
    namespace WindowsModule
    {
        /// <summary>
        /// Class for gathering Some Windows OS related services.
        /// </summary>
        public class WindowsInterface
        {
            /// <summary>
            /// Gets file name from file absoulute path.
            /// </summary>
            /// <param name="path">The File's path.</param>
            /// <returns>The file's name.</returns>
            public static string GetNameFromPath(string path)
            {
                return Path.GetFileName(path);
            }            

            /// <summary>
            /// Gets the processes that are locally currenty running.
            /// </summary>
            /// <returns>The Processes</returns>
            public static WProcess[] GetRunningProcesses()
            {
                return System.Diagnostics.Process.GetProcesses();
            }

            /// <summary>
            /// Gets the current time in string format.
            /// </summary>
            /// <returns>Current time.</returns>
            public static string GetTimeNowString()
            {
                return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ffff");
            }

            /// <summary>
            /// Starts a process by its name.
            /// </summary>
            /// <param name="name">The process' name.</param>
            /// <returns>The process' PID.</returns>
            public static int RunAndGetPID(string name)
            {
                WProcess proc;
                Int32 pid = 0;
                
                proc = new WProcess();
                proc.StartInfo.FileName = name;
                if (proc.Start() != false)
                {
                    //proc.WaitForInputIdle();
                    try
                    {
                        pid = proc.Id;
                    }
                    catch (Exception)
                    {
                        pid = 0;
                    }
                }
                return pid;
            }
            /// <summary>
            /// Starts a process by its name.
            /// </summary>
            /// <param name="name">The process' name.</param>
            /// <returns>Processes' Diagnostics.Process object.</returns>
            public static WProcess RunAndGetWProcess(string name)
            {
                WProcess proc;
                Int32 pid = 0;

                proc = new WProcess();
                proc.StartInfo.FileName = name;
                if (proc.Start() != false)
                {
                    //proc.WaitForInputIdle();
                    try
                    {
                        pid = proc.Id;
                    }
                    catch (Exception)
                    {
                        pid = 0;
                    }
                }
                return WProcess.GetProcessById(pid);
            }

            /// <summary>
            /// Gets the MAC address of the computer's main network interface.
            /// </summary>
            /// <returns>Computer main network interface MAC address.</returns>
            public static string GetMacAddress()
            {
                var macAddr =
                (
                    from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.OperationalStatus == OperationalStatus.Up
                    select nic.GetPhysicalAddress().ToString()
                ).FirstOrDefault();
                return macAddr;
            }

            /// <summary>
            /// Gets a process' path by its Diagonstics.Process object.
            /// </summary>
            /// <param name="wprocess"></param>
            /// <returns>The process path</returns>
            public static string GetProcessPath(WProcess wprocess)
            {
                try
                {
                    return wprocess.MainModule.FileName;
                }
                catch 
                {
                    //Console.WriteLine(string.Format("PID: {0} ACCESS DENIED ", wprocess.Id));
                    return "";
                }                
            }

            /// <summary>
            /// Kills a process.
            /// </summary>
            /// <param name="pid">Process' PID.</param>
            /// <returns>Whether the action was successfull or not.</returns>
            public static bool KillProcess(int pid)
            {
                try
                {
                    WProcess.GetProcessById(pid).Kill();
                    return true;
                }
                catch (Exception e)
                {                    
                    return false;
                }
                
            }

            /// <summary>
            /// Kills (if needed) and Deletes a process from the system. 
            /// </summary>
            /// <param name="pid">The process' PID.</param>
            /// <returns>Whether the action was successfull or not.</returns>
            public static bool RemoveProcess(int pid)
            {
                string path = GetProcessPath(WProcess.GetProcessById(pid));
                KillProcess(pid);
                System.Threading.Thread.Sleep(300);
                Console.WriteLine(path);
                if (path == "")
                    return false;
                return RemoveProcess(path);
            }

            /// <summary>
            /// Deletes a program from the system.
            /// </summary>
            /// <param name="path">The program's path</param>
            /// <returns>Whether the action was successfull or not.</returns>
            public static bool RemoveProcess(string path)
            {
                try
                {
                    File.Delete(path);
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
            }

            /// <summary>
            /// Gets the current process' path.
            /// </summary>
            /// <returns>Current process' path.</returns>
            public static string GetMyPath()
            {
                return System.Reflection.Assembly.GetEntryAssembly().Location;
            }
        }        
    }
}
